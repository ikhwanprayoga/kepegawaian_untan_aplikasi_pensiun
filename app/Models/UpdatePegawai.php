<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpdatePegawai extends Model
{

        protected $table = 'update_pegawai';
        protected $primaryKey = 'id';
        protected $fillable = [
            'usulan_pensiun_id',
            'NAMA_LENGKAP',
            'NIP',
            'Tempat_Lahir',
            'Tgl_Lahir',
            'no_hp',
            'Agama',
            'JK',
            'No_Karpeg',
            'Status_Perkawinan',
            'JAB_FUNGSIONAL',
            'NIDN',
            'ALAMAT_EMAIL',
            'Gol',
            'Jurusan',
            'Program_Studi',
            'pangkat',
            'tmt',
            'gaji_pokok_terakhir',
            'masa_kerja_kp_terakhir',
            'masa_kerja_golongan',
            'masa_kerja_pns',
            'masa_kerja_pensiun',
            'cltn',
            'peninjauan_masa_kerja',
            'pendidikan_dasar',
            'lulus_tahun',
            'foto',
            'no_hp',
            'npwp',
            'alamat_jl',
            'alamat_rt',
            'alamat_rw',
            'alamat_kelurahan',
            'alamat_kecamatan',
            'alamat_kota',
            'alamat_provinsi',
            'masa_kerja_golongan_tahun',
            'masa_kerja_golongan_bulan',
            'masa_kerja_golongan_kalender',
            'masa_kerja_pensiun_tahun',
            'masa_kerja_pensiun_bulan',
            'mulai_masuk_pns',
            'belum_pns_dari',
            'belum_pns_sampai_dengan',
        ];
        public $timesstamps = false;

        public function usulanPensiun()
    {
        return $this->belongsTo('App\Models\UsulanPensiun');
    }
}
