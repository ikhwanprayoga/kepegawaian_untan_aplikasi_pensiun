<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatatanBerkas extends Model
{
    protected $table = 'catatan_berkas';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'catatan', 'status_kepeg_fakultas', 'berkas_usulan_pensiun_id'
    ];
    public $timesstamps = true;

    public function berkasUsulanPensiun()
    {
    	return $this->belongsTo('App\Models\BerkasUsulanPensiun');
    }
}
