<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateKeluarga extends Model
{
    protected $table = 'update_keluarga';
        protected $primaryKey = 'id';
        protected $fillable = [ 
           
           
            'NIP',
            'nik',
            'nama',
            'tanggal_lahir',
            'tanggal_kawin',
            'tanggal_cerai',
            'pasangan_keberapa',
           
        ];
        public $timesstamps = false;
       
}
