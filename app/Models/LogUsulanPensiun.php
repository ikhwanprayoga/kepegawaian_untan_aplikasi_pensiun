<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUsulanPensiun extends Model
{
    protected $table = 'log_usulan_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'usulan_pensiun_id', 'nama'
    ];
    public $timesstamps = true;

    public function usulanPensiun()
    {
        return $this->belongsTo('App\Models\UsulanPensiun');
    }
}
