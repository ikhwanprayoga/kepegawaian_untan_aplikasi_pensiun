<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoFakultas extends Model
{
    protected $table = 'info_fakultas';
    protected $primaryKey = 'id';
    protected $fillable = [ 'fakultas_id', 
        'nama_lengkap_fakultas', 
        'nama_dekan', 
        'nip_dekan', 
        'pangkat_dekan', 
        'golongan_dekan', 
        'alamat_fakultas', 
        'no_telepon_fakultas', 
        'email_fakultas', 
        'fax_fakultas', 
        'kode_pos_fakultas'
    ];
    public $timesstamps = true;

    public function fakultas()
    {
        return $this->belongsTo('App\Models\Fakultas');
    }

    public function dataPegawai()
    {
        return $this->belongsTo('App\Models\Dosen', 'nip_dekan', 'NIP');
    }
}
