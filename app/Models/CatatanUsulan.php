<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatatanUsulan extends Model
{
    protected $table = 'catatan_usulan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'usulan_pensiun_id', 'isi_catatan', 'status'
    ];
    public $timesstamps = true;

    public function usulanPensiun()
    {
    	return $this->belongsTo('App\Models\UsulanPensiun');
    }

}
