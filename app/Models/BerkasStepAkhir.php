<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BerkasStepAkhir extends Model
{
    protected $table = 'berkas_step_akhir';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'usulan_pensiun_id', 'file_sk', 'file_dpcp', 'file_sp4', 'file_surat_pengantar', 'no_resi'
    ];
    public $timesstamps = true;

    public function usulanPensiun()
    {
        return $this->belongsTo('App\Models\UsulanPensiun');
    }
}
