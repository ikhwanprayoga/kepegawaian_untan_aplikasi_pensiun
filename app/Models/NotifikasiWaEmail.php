<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotifikasiWaEmail extends Model
{
    protected $table = 'notifikasi_wa_email';
    protected $primaryKey = 'id';
    protected $fillable = [
        'pesan',
        'waktu_kirim',
        'email',
        'no_hp',
        'untuk_user_id',
        'dari_user_id',
        'usulan_pensiun_id',
        'nip',
    ];
    public $timesstamps = true;
}
