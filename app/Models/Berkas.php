<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Berkas extends Model
{
    protected $table = 'berkas';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'nama_berkas', 'direktori', 'uploader_berkas_user_id', 'uploader_berkas_pegawai_nip', 'catatan', 'status_kepeg_fakultas'
    ];
    public $timesstamps = true;
}
