<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotifikasiPemberitahuanPengajuanPensiun extends Model
{
    protected $table = 'notifikasi_pemberitahuan_pengajuan_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'nip', 
        'nama', 
        'pesan', 
        'tgl_lahir', 
        'jabatan', 
        'unit_kerja', 
        'tahun_pensiun', 
        'waktu_kirim_notifikasi', 
        'no_hp_pegawai', 
        'email_pegawai', 
        'no_hp_operator_fakultas', 
        'no_hp_operator_untan', 
        'status'
    ];
    public $timesstamps = true;

    public function dataDosen()
    {
        return $this->belongsTo('App\Models\Dosen', 'nip', 'NIP');
    }
}
