<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'dosen';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'NAMA_LENGKAP', 'NAMA_TANPA_GELAR', 'Jnj_Homebase', 'Program_Studi', 'Jurusan', 'Unit_Kerja', 'NIDN', 'TMT_CPNS1', 'TMT_PNS', 'NIP_Lama',
        'NIP', 'Karpeg', 'Tempat_Lahir', 'Tgl_Lahir', 'JK', 'Agama', 'Gol', 'TMT_Gol', 'Jabatan_Tambahan', 'Kode_Jabatan', 'JAB_FUNGSIONAL',
        'kode_jafung', 'Kum_Terakhir', 'Nomor_SK_Jabatan', 'tanggal_SK_Jabatan', 'Pejabat_SK', 'TMT_Jab', 'masa_kerja', 'SERTIFIKASI',
        'No_Peserta', 'No_Reg_Akademik', 'tahun_serdos', 'TGLKELUAR_SERDOS', 'Bidang_Ilmu', 'nira', 'no_nira', 'Bidang_Ilmu_Asesor',
        'PT_Penyelenggara_Sertifikasi', 'NO_KTP', 'ALAMAT', 'NO_TELPON', 'NO_HP', 'ALAMAT_EMAIL', 'CPNS', 'No_SK_CPNS', 'Tgl_CPNS',
        'TMT_CPNS2', 'Tahun_CPNS', 'Bulan__CPNS'
    ];
    public $timesstamps = false;
}
