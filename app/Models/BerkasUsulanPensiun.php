<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BerkasUsulanPensiun extends Model
{
    protected $table = 'berkas_usulan_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'usulan_pensiun_id', 'master_berkas_pensiun_id', 'berkas_id', 'status_verifikasi_kepeg_fakultas', 'status_verifikasi_kepeg_untan'
    ];
    public $timesstamps = true;

    public function berkas()
    {
    	return $this->belongsTo('App\Models\Berkas');
    }

    public function masterBerkasPensiun()
    {
        return $this->belongsTo('App\Models\MasterBerkasPensiun');
    }

    public function catatanBerkas()
    {
        return $this->hasMany('App\Models\CatatanBerkas');
    }

    public function usulanPensiun()
    {
    	return $this->belongsTo('App\Models\UsulanPensiun');
    }
}
