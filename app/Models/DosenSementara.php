<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DosenSementara extends Model
{
    protected $table = 'dosen_sementara';
    protected $primaryKey = 'id';
    protected $fillable = [ 'nip', 'nama', 'tgl_lahir', 'jabatan', 'unit_kerja', 'no_hp', 'email', 'status'];
    public $timesstamps = true;
}
