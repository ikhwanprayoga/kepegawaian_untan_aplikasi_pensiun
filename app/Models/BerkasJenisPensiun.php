<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BerkasJenisPensiun extends Model
{
    protected $table = 'berkas_jenis_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'jenis_pensiun_id', 'master_berkas_pensiun_id'
    ];
    public $timesstamps = true;

    public function masterBerkasPensiun()
    {
        return $this->belongsTo('App\Models\MasterBerkasPensiun');
    }
}
