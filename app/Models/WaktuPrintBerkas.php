<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaktuPrintBerkas extends Model
{
    protected $table = 'waktu_print_berkas';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'usulan_pensiun_id', 'waktu_dpcp', 'waktu_sp4', 'waktu_surat_pengantar', 'waktu_surat_pidana', 'waktu_surat_hukuman', 'no_surat_pengantar', 'no_surat_pidana', 'no_surat_hukuman'
    ];
    public $timesstamps = true;

    public function usulanPensiun()
    {
        return $this->belongsTo('App\Models\UsulanPensiun');
    }
}
