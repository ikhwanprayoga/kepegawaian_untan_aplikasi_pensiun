<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    protected $table = 'notifikasi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'usulan_pensiun_id',
        'dari_users_id',
        'untuk_users_id',
        'untuk_role',
        'fakultas_id',
        'nip_dosen',
        'pesan',
        'status',
        'link',
        'waktu_notifikasi_dikirim',
        'waktu_notifikasi_dibaca',
    ];
    public $timesstamps = true;
}
