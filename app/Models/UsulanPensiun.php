<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsulanPensiun extends Model
{
    protected $table = 'usulan_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kode_usulan', 'nip', 'fakultas_id', 'jenis_pensiun_id', 'status_usulan_kepeg_fakultas', 'status_usulan_kepeg_untan',
        'no_resi_pengiriman_berkas', 'tanggal_kirim_berkas', 'jasa_pengiriman'
    ];
    public $timesstamps = true;

    public function lihatCatatan()
    {
        return $this->hasMany('App\Models\CatatanUsulan');
    }

    public function dataPegawai()
    {
        return $this->belongsTo('App\Models\UpdatePegawai','nip','NIP');
    }

    public function berkasStepAkhir()
    {
        return $this->hasOne('App\Models\BerkasStepAkhir');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen', 'nip', 'NIP');
    }

    public function revisi()
    {
        return $this->hasMany('App\Models\Revisi');
    }

    public function logUsulanPensiun()
    {
        return $this->hasMany('App\Models\LogUsulanPensiun');
    }

    public function berkasUsulanPensiun()
    {
        return $this->hasMany('App\Models\BerkasUsulanPensiun');
    }

    public function fakultas()
    {
    	return $this->belongsTo('App\Models\Fakultas');
    }

    public function pegawai()
    {
    	return $this->belongsTo('App\Models\Pegawai', 'nip', 'NIP');
    }

    public function jenisPensiun()
    {
    	return $this->belongsTo('App\Models\JenisPensiun');
    }
}
