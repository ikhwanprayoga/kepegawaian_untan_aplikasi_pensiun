<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LihatProfil extends Model
{
    protected $table='users';
    protected $primarykey='id';
}
