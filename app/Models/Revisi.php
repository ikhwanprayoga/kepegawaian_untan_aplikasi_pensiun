<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revisi extends Model
{
    protected $table = 'revisi';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'usulan_pensiun_id',
        'catatan_revisi',
        'status',
    ];
    public $timesstamps = true;

    public function usulanPensiun()
    {
    	return $this->belongsTo('App\Models\UsulanPensiun');
    }

}
