<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsiCatatanUsulan extends Model
{
    protected $table = 'isi_catatan_usulan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'isi_catatan'
    ];
    public $timesstamps = true;

    public function catatanUsulan()
    {
    	return $this->hasOne('App\Models\CatatanUsulan');
    }

}
