<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisPensiun extends Model
{
    protected $table = 'jenis_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'nama', 'kode', 'keterangan'
    ];
    public $timesstamps = true;

    public function berkasJenisPensiun()
    {
        return $this->hasMany('App\Models\BerkasJenisPensiun');
    }
}
