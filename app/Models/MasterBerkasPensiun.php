<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterBerkasPensiun extends Model
{
    protected $table = 'master_berkas_pensiun';
    protected $primaryKey = 'id';
    protected $fillable = [ 
        'judul_berkas', 'pegawai_dapat_upload', 'keterangan'
    ];
    public $timesstamps = true;

    public function berkasUsulanPensiun()
    {
        return $this->hasMany('App\Models\BerkasUsulanPensiun');
    }

    public function berkasJenisPensiun()
    {
        return $this->hasMany('App\Models\BerkasJenisPensiun');
    }

    public function berkasJenis()
    {
        return $this->hasOne('App\Models\BerkasJenisPensiun');
    }
}
