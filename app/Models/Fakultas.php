<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $table = 'fakultas';
    protected $primaryKey = 'id';
    protected $fillable = [ 'nama_fakultas'];
    public $timesstamps = true;

    public function usulanPensiun()
    {
        return $this->hasMany('App\Models\UsulanPensiun');
    }

    public function infoFakultas()
    {
        return $this->hasOne('App\Models\InfoFakultas');
    }
}
