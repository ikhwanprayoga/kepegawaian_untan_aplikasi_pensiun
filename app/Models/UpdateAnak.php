<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UpdateAnak extends Model
{
    protected $table = 'update_anak';
    protected $primaryKey = 'id';
    protected $fillable = [ 
       
       
        'NIP',
        'nik',
        'nama',
        'tanggal_lahir',
        'nama_ayah',
        'nama_ibu',
        'keterangan',
       
    ];
    public $timesstamps = false;
}
