<?php

namespace App\Http\Controllers\pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Alert;

class LoginController extends Controller
{
    public function index()
    {
        if (!Session::has(['nama', 'nip'])) {
            return view('pegawai.login.index');
        } else {
            return redirect()->route('pegawai.beranda');
        }
    }

    public function login(Request $request)
    {
        // return $request->all();
        // $json = file_get_contents('http://203.24.50.30:7475/Datasnap/Rest/Tservermethods1/logindosen/X'.$request->nip.'/X'.$request->password);
        $json = file_get_contents('http://servicedosen.siakad.untan.ac.id/datasnap/rest/tservermethods1/logindosen/X'.$request->nip.'/X'.$request->password);
        if (!$json) {
            Alert::error('Maaf Terjadi Gangguan Disistem Kami', 'Gagal!');
            return redirect()->back();
        }
        $dt = json_decode($json, TRUE);

        if($dt['result'][0]['stat'] == 'aktif') {
            $request->session()->flush();
            $session = session()->put([
                'nip'   =>  $request->nip,
                'nama'  =>  $dt['result'][0]['nama']
            ]);

            Alert::success('Data anda benar. <br>Silahkan lengkapi data sesuai dengan form yang disediakan.', 'Berhasil!')->html()->persistent('Close');

            return redirect()->route('pegawai.beranda');
        } else {
            Alert::error('Data tidak valid. Silahkan coba lagi.', 'Gagal!');
            return redirect()->back();
        }

    }
}
