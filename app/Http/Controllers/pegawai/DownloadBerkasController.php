<?php

namespace App\Http\Controllers\pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DownloadBerkasController extends Controller
{
    public function index(Request $request)
    {
        if($request->session()->has('nama') && $request->session()->has('nip')) {
            
            return view('pegawai.download_berkas.index');
        } else {
            Alert::error('Tidak memiliki izin akses!!', 'Error!');
            return redirect()->route('pegawai.login.index');
        }
    }
}
