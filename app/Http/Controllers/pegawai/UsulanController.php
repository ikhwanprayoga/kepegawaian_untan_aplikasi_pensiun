<?php

namespace App\Http\Controllers\pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use DB;

use App\Models\Dosen;
use App\Models\UsulanPensiun;

class UsulanController extends Controller
{
    public function index(Request $request)
    {
        if($request->session()->has('nama') && $request->session()->has('nip')) {
            $nipDosen = session()->get('nip');
            $dosen = Dosen::where('NIP', $nipDosen)->orWhere('NIP_Lama', $nipDosen)->first();

            $usulan = UsulanPensiun::where('nip', $dosen->NIP)->with('logUsulanPensiun')->first();
            $dosen = DB::table('dosen')->where('NIP', $dosen->NIP)->first();

            if (empty($usulan)) {
                $usulan = null;
            }

            return view('pegawai.usulan.index', compact('usulan', 'dosen'));

        } else {
            Alert::error('Tidak memiliki izin akses!!', 'Error!');
            return redirect()->route('pegawai.login.index');
        }
    }
}
