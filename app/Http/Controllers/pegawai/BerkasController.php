<?php

namespace App\Http\Controllers\Pegawai;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Alert;

use App\Models\UsulanPensiun;
use App\Models\Berkas;
use App\Models\Dosen;
use App\Models\LogUsulanPensiun;
use App\Models\BerkasUsulanPensiun;
use App\Models\MasterBerkasPensiun;

class BerkasController extends Controller
{
    public function index()
    {
        if (!Session::has(['nama', 'nip'])) {
            return view('pegawai.login.index');
        } else {
            $pegawaiNip = session()->get('nip');
            $nipDosen = session()->get('nip');
            $dosen = Dosen::where('NIP', $nipDosen)->orWhere('NIP_Lama', $nipDosen)->first();
            $usulan = UsulanPensiun::where('nip', $dosen->NIP)->with('jenisPensiun')->first();
            // return $berkas = $usulan->jenisPensiun->berkasJenisPensiun-;
            return view('pegawai.berkas.index', compact('usulan'));
        }
    }

    public function tambah_berkas(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'master_berkas_pensiun_id' => 'required',
            'berkas' => 'required|mimes:jpeg,JPG,png,pdf|max:5048',
        ],[
            'master_berkas_pensiun_id.required' => 'master berkas pensiun id tidak boleh kosong!',
            'berkas.required' => 'Berkas tidak boleh kosong!',
            'berkas.mimes' => 'File berkas hanya boleh JPG, JPEG, PDF. Maskimal ukuran 5MB!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $nipDosen = session()->get('nip');
            $dosen = Dosen::where('NIP', $nipDosen)->orWhere('NIP_Lama', $nipDosen)->first();

            $pegawaiNip = $dosen->NIP;

            $usulan = UsulanPensiun::where('id', $request->usulan_pensiun_id)->first();
            $file_berkas = $request->berkas;
            $master_berkas_pensiun_id = $request->master_berkas_pensiun_id;

            $cek_berkas = BerkasUsulanPensiun::where([['usulan_pensiun_id', $usulan->id], ['master_berkas_pensiun_id', $master_berkas_pensiun_id]]);

            if ($cek_berkas->count() > 0) {
                //jika data sudah ada, hapus file lama
                $berkas = $cek_berkas->first()->berkas;
                $hapus_berkas = $this->delete_file($usulan->id, $berkas->nama_berkas);
                // $hapus_data = BerkasUsulanPensiun::find($cek_berkas->first()->id)->delete();
                $nama_berkas = $this->save_file($usulan->id, $master_berkas_pensiun_id, $file_berkas);

                $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
                $direktori = $fakultas.'/'.$usulan->nip;

                $simpan_berkas = Berkas::find($berkas->id)->update([
                    'nama_berkas'                   => $nama_berkas,
                    'direktori'                     => $direktori,
                    'uploader_berkas_pegawai_nip'   => $pegawaiNip,
                ]);

                $berkas_id = $berkas->id;
            } else {
                //jika data baru
                $nama_berkas = $this->save_file($usulan->id, $master_berkas_pensiun_id, $file_berkas);

                $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
                $direktori = $fakultas.'/'.$usulan->nip;

                //simpan data berkas
                $simpan_berkas = Berkas::create([
                    'nama_berkas'                   => $nama_berkas,
                    'direktori'                     => $direktori,
                    'uploader_berkas_pegawai_nip'   => $pegawaiNip,
                ]);

                $berkas_id = $simpan_berkas->id;
            }

            //simpan data berkas
            // $simpan_berkas = Berkas::create([
            //     'nama_berkas' => $nama_berkas,
            //     'uploader_berkas_pegawai_nip' => $pegawaiNip,
            // ]);

            // $berkas_id = $simpan_berkas->id;

            //simpan daftar berkas
            // $berkas_usulan_pensiun = BerkasUsulanPensiun::create([
            //     'usulan_pensiun_id' => $usulan->id,
            //     'master_berkas_pensiun_id' => $master_berkas_pensiun_id,
            //     'berkas_id' => $berkas_id,
            //     'status_verifikasi_kepeg_fakultas' => 0,
            //     'status_verifikasi_kepeg_untan' => 0,
            // ]);

            //simpan daftar berkas
            $berkas_usulan_pensiun = BerkasUsulanPensiun::updateOrCreate(
                [
                    'usulan_pensiun_id' => $usulan->id,
                    'master_berkas_pensiun_id' => $master_berkas_pensiun_id,
                ],[
                    'berkas_id' => $berkas_id,
                    'status_verifikasi_kepeg_fakultas' => 0,
                    'status_verifikasi_kepeg_untan' => 0,
                ]
            );

            // $nama_master_berkas = MasterBerkasPensiun::where('id', $master_berkas_pensiun_id)->first();
            $nama_berkas = $berkas_usulan_pensiun->masterBerkasPensiun->judul_berkas;
            $logUsulanPensiun = $this->logUsulan($usulan->id, 'Pegawai mengupload berkas '.$nama_berkas);

            Alert::success('Berkas berhasil di upload!');
            return redirect()->back();

        }
    }

    //private function
    private function save_file($usulan_id, $master_berkas_pensiun_id, $file)
    {
        $usulan         = UsulanPensiun::where('id', $usulan_id)->first();
        $master_berkas  = MasterBerkasPensiun::where('id', $master_berkas_pensiun_id)->first();
        
        $nama_berkas    = str_replace(' ', '_', $master_berkas->judul_berkas);
        $fakultas       = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);

        $name = $nama_berkas.'_'.$usulan->nip.'_'.md5(date('Ymdhis')).'.'.$file->getClientOriginalExtension();
        $filename = str_replace(str_split('\\/:*?"<>|'), '_', $name);
        Storage::putFileAs('public/pensiun/'.$fakultas.'/'.$usulan->nip, $file, $filename);

        return $filename;
    }

    private function delete_file($usulan_id, $file)
    {
        if (isset($file)) {
            $usulan = UsulanPensiun::where('id', $usulan_id)->first();
            $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
            Storage::delete('public/pensiun/'.$fakultas.'/'.$usulan->nip.'/'.$file);
        } else {
            return 'file does not exist';
        }

    }

    private function logUsulan($usulan_pensiun_id, $nama)
    {
        $logUsulanPensiun = LogUsulanPensiun::create([
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nama' => $nama,
        ]);
        return true;
    }
}
