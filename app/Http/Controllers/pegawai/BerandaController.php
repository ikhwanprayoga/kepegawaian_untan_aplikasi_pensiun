<?php

namespace App\Http\Controllers\pegawai;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;

use App\Models\UsulanPensiun;
use App\Models\Dosen;

class BerandaController extends Controller
{
    public function index(Request $request)
    {

        if($request->session()->has('nama') && $request->session()->has('nip')) {

            $nipDosen = session()->get('nip');
            $dosen = Dosen::where('NIP', $nipDosen)->orWhere('NIP_Lama', $nipDosen)->first();
            $usulan = UsulanPensiun::where('nip', $dosen->NIP)->first();

            if (empty($usulan)) {
                $usulan = null;
            }
            
            return view('pegawai.beranda.index', compact('usulan'));
        } else {
            Alert::error('Tidak memiliki izin akses!!', 'Error!');
            return redirect()->route('pegawai.login.index');
        }

        
    }
}
