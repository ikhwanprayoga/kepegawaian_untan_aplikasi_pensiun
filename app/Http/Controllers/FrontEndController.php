<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function beranda()
    {
        return view('frontend.beranda.index');
    }

    public function tentang()
    {
        return view('frontend.tentang.index');
    }

    public function panduan_aplikasi()
    {
        return view('frontend.panduan.aplikasi');
    }

    public function panduan_berkas()
    {
        return view('frontend.panduan.berkas');
    }

    public function layanan_sop()
    {
        return view('frontend.layanan.sop');
    }

    public function layanan_produk_hukum()
    {
        return view('frontend.layanan.produk_hukum');
    }

    public function kontak()
    {
        return view('frontend.kontak.index');
    }
}
