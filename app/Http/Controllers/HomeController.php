<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();

        if ($user->hasRole('superadmin')) {
            return redirect()->route('superadmin.beranda');
        } elseif ($user->hasRole('operator untan')) {
            return redirect()->route('kepegawaian-untan.beranda');
        } elseif ($user->hasRole('operator fakultas')) {
            return redirect()->route('kepegawaian-fakultas.beranda');
        } elseif ($user->hasRole('kepala kepegawaian untan')) {
            return redirect()->route('kepala-kepegawaian.usulan.index');
        } else {
            return 'ada tidak punya hak akses';
        }

    }
}
