<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Notifikasi;

class NotifikasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function baca($id)
    {
        $data = Notifikasi::where('id', $id)->first();

        $data->Update([
            'status' => 1,
            'waktu_notifikasi_dibaca' => \Carbon\Carbon::now()
        ]);

        return redirect($data->link);


    }


   public function bacasemua($fakultas_id)
   {

    $data = Notifikasi::where('fakultas_id', $fakultas_id)->where('status','=','0')->get();

    foreach ($data as $value) {
        $value->update([
            'status' => 1
        ]);
    }

    return  redirect()->back();
   }

   public function bacasemua_untan($fakultas_id)
   {
    $data = Notifikasi::where('fakultas_id', $fakultas_id)->where('untuk_role','operator untan')->where('status','=','0')->get();

    foreach ($data as $value) {
        $value->update([
            'status' => 1
        ]);
    }

    return  redirect()->back();
   }

}
