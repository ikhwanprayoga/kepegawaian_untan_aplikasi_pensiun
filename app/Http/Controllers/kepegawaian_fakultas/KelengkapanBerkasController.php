<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\UsulanPensiun;

class KelengkapanBerkasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($usulan_id)
    {
        $usulan = UsulanPensiun::where('id', $usulan_id)->with('jenisPensiun')->first();

        return view('kepegawaian_fakultas.usulan.kelengkapan_berkas.index', compact('usulan'));
    }

    public function srt_pernyataan_tidak_pernah_pidana($usulan_id)
    {
        $usulan = UsulanPensiun::where('id',$usulan_id)->with('dataPegawai')->first();
        return view('kepegawaian_fakultas.usulan.kelengkapan_berkas.srt_pernyataan_tidak_pernah_pidana', compact('usulan'));
    }

    public function daftar_riwayat_pekerjaan($usulan_id)
    {
        return view('kepegawaian_fakultas.usulan.kelengkapan_berkas.daftar_riwayat_pekerjaan', compact('usulan'));
    }

    public function srt_pernyataan_bebas_hukuman($usulan_id)
    {
        $usulan = UsulanPensiun::where('id',$usulan_id)->with('dataPegawai')->first();
        return view('kepegawaian_fakultas.usulan.kelengkapan_berkas.srt_pernyataan_bebas_hukuman', compact('usulan'));
    }

    public function susunan_keluarga($usulan_id)
    {
        return view('kepegawaian_fakultas.usulan.kelengkapan_berkas.susunan_keluarga', compact('usulan'));
    }

    public function srt_pernyataan_pembayaran_pensiun($usulan_id)
    {
        $usulan = UsulanPensiun::where('id',$usulan_id)->with('dataPegawai')->first();
        return view ('kepegawaian_fakultas.usulan.kelengkapan_berkas.srt_pernyataan_pembayaran_pensiun_pertama', compact('usulan'));
    }

    public function usul_pemberhentian($usulan_id)
    {
        $usulan = UsulanPensiun::where('id',$usulan_id)->with('dataPegawai')->first();
        return view ('kepegawaian_fakultas.usulan.kelengkapan_berkas.srt_usul_pemberhentian', compact('usulan'));
    }
}
