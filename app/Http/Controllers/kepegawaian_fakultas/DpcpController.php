<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

use App\Models\Dosen;
use App\Models\UsulanPensiun;
use App\Models\UpdatePegawai;
use App\Models\UpdateKeluarga;
use App\Models\UpdateAnak;

class DpcpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($nip)
    {
        $dataPegawai = UpdatePegawai::where('NIP', $nip)->first();

        if (isset($dataPegawai)) {
            $pegawai = $dataPegawai;
        } else {
            $pegawai = Dosen::where('NIP', $nip)->first();
        }
        $keluarga = UpdateKeluarga::where('NIP', $nip)->get();
        $dataAnak = UpdateAnak::where('NIP', $nip)->get();
        // $date = Carbon::createFromFormat('j F, Y', $data['due_date']);

        return view('kepegawaian_fakultas.usulan.dpcp', compact('pegawai','keluarga','dataAnak'));

    }

    public function cetak($nip)
    {
        $dataPegawai = UpdatePegawai::where('NIP', $nip)->first();

        if (isset($dataPegawai)) {
            $pegawai = $dataPegawai;
        } else {
            $pegawai = Dosen::where('NIP', $nip)->first();
        }

        $pdf = PDF::loadView('kepegawaian_fakultas.usulan.dpcp',['pegawai'=>$pegawai]);
        return $pdf->stream();
        return $pdf->download('dpcp_'.$nip.'.pdf');
    }
}
