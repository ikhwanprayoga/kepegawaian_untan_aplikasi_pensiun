<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Storage;
use Alert;

use App\Models\UsulanPensiun;
use App\Models\Notifikasi;
use App\Models\Berkas;
use App\Models\BerkasUsulanPensiun;
use App\Models\LogUsulanPensiun;
use App\Models\MasterBerkasPensiun;
use App\Models\Dosen;
use App\Models\UpdatePegawai;
use App\Models\UpdateKeluarga;
use App\Models\UpdateAnak;

class UpdatedataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($nip)
    {


        $usulan = UsulanPensiun::where('nip', $nip)->first();
        // $nip= $dosen->NIP;
        $pegawai = UpdatePegawai::get();
        $dosen = Dosen::get();
        $keluarga = UpdateKeluarga::get();
        $anak = UpdateAnak::get();


        if ($pegawai->where('NIP', $nip)->count() > 0) {
            // return 'l';
            $data = UpdatePegawai::where('NIP', $nip)->orderBy('nip', 'desc')->first();
        } else {
            $data = Dosen::where('NIP', $nip)->first();
            $bkd = file_get_contents('http://bkd.untan.ac.id/api.php/'.$data->NIP);
            $dataBkd = json_decode($bkd, true);
            $data['no_hp'] = $dataBkd['alamat_telp'];
            // return $data;
        }

        if ($keluarga->where('NIP', $nip)->count() > 0) {
            $datas = UpdateKeluarga::where('NIP', $nip)->first();
        } else {
            $datas = Dosen::where('NIP', $nip)->first();
        }

        if ($anak->where('NIP', $nip)->count() > 0) {
            $datak = UpdateAnak::where('NIP', $nip)->first();
        } else {
            $datak = Dosen::where('NIP', $nip)->first();
        }

        return view('kepegawaian_fakultas.data.edit ',compact('data','datas','datak', 'usulan'));

    }

    public function update(Request $request, $nip)
    {
        // return $request->all();
        $data = UpdatePegawai::where('NIP', $nip)->first();
        $usulan = UsulanPensiun::where('nip',$nip)->first();
        $foto = $request->foto;
        if (isset($data)) {
            // return 'update';
            //update

            if (isset($request->foto)) {
                //jika ada input foto
                //cek foto
                // return 'input foto';
                if (isset($data->foto)) {
                    // return 'foto ada';
                    //jika data sudah ada, hapus file lama
                    // $foto = $pegawai->foto;
                    $hapus_foto = $this->delete_file($data->foto);

                    // $hapus_data = BerkasUsulanPensiun::find($cek_berkas->first()->id)->delete();
                    $berkas_foto = $this->save_file($data->NIP, $foto);
                } else {
                    //jika tdk ada foto
                    // return 'foto baru';
                    $berkas_foto= $this->save_file($data->NIP, $foto);
                }

                //simpat foto di storage

                //buat filed foto

                $data->foto = $berkas_foto;

            }
            // return 'foto kosong';

            $data->NAMA_LENGKAP = $request->NAMA_LENGKAP;
            $data->NIP = $request->NIP;
            $data->Tempat_Lahir = $request->Tempat_Lahir;
            $data->Tgl_Lahir = $request->Tgl_Lahir;
            $data->JAB_FUNGSIONAL = $request->JAB_FUNGSIONAL;
            $data->NIDN = $request->NIDN;
            $data->ALAMAT_EMAIL = $request->ALAMAT_EMAIL;
            $data->Gol = $request->Gol;
            $data->Jurusan = $request->Jurusan;
            $data->Program_Studi = $request->Program_Studi;
            $data->pangkat = $request->pangkat;
            $data->tmt = $request->tmt;
            $data->gaji_pokok_terakhir = $request->gaji_pokok_terakhir;
            $data->masa_kerja_kp_terakhir = $request->masa_kerja_kp_terakhir;
            $data->masa_kerja_golongan = $request->masa_kerja_golongan;
            $data->masa_kerja_pns = $request->masa_kerja_pns;
            $data->masa_kerja_pensiun = $request->masa_kerja_pensiun;
            $data->cltn = $request->cltn;
            $data->peninjauan_masa_kerja = $request->peninjauan_masa_kerja;
            $data->pendidikan_dasar = $request->pendidikan_dasar;
            $data->lulus_tahun = $request->lulus_tahun;
            $data->npwp = $request->npwp;
            $data->no_hp = $request->no_hp;
            $data->alamat_jl = $request->alamat_jl;
            $data->alamat_rt = $request->alamat_rt;
            $data->alamat_rw = $request->alamat_rw;
            $data->alamat_kelurahan = $request->alamat_kelurahan;
            $data->alamat_kecamatan = $request->alamat_kecamatan;
            $data->alamat_kota = $request->alamat_kota;
            $data->alamat_provinsi = $request->alamat_provinsi;
            $data->masa_kerja_golongan_tahun = $request->masa_kerja_golongan_tahun;
            $data->masa_kerja_golongan_bulan = $request->masa_kerja_golongan_bulan;
            $data->masa_kerja_golongan_kalender = $request->masa_kerja_golongan_kalender;
            $data->masa_kerja_pensiun_tahun = $request->masa_kerja_pensiun_tahun;
            $data->masa_kerja_pensiun_bulan = $request->masa_kerja_pensiun_bulan;
            $data->mulai_masuk_pns = $request->mulai_masuk_pns;
            $data->belum_pns_dari = $request->belum_pns_dari;
            $data->belum_pns_sampai_dengan = $request->belum_pns_sampai_dengan;


            $data->update();

        } else {
            ///create
            // return 'create';

            $input = [
                'usulan_pensiun_id'=> $usulan->id,
                'NAMA_LENGKAP' => $request->get('NAMA_LENGKAP'),
                'NIP' => $request->get('NIP'),
                'Tempat_Lahir' => $request->get('Tempat_Lahir'),
                'Tgl_Lahir' => $request->get('Tgl_Lahir'),
                'JK' => $request->get('JK'),
                'JAB_FUNGSIONAL' => $request->get('JAB_FUNGSIONAL'),
                'NIDN' => $request->get('NIDN'),
                'ALAMAT_EMAIL' => $request->get('ALAMAT_EMAIL'),
                'Gol' => $request->get('Gol'),
                'Jurusan' => $request->get('Jurusan'),
                'Program_Studi' => $request->get('Program_Studi'),
                'pangkat' => $request->get('pangkat'),
                'tmt' => $request->get('tmt'),
                'gaji_pokok_terakhir' => $request->get('gaji_pokok_terakhir'),
                'masa_kerja_kp_terakhir' => $request->get('masa_kerja_kp_terakhir'),
                'masa_kerja_golongan' => $request->get('masa_kerja_golongan'),
                'masas_kerja_pns' => $request->get('masa_kerja_pns'),
                'masa_kerja_pensiun' => $request->get('masa_kerja_pensiun'),
                'cltn' => $request->get('cltn'),
                'peninjauan_masa_kerja' => $request->get('peninjauan_masa_kerja'),
                'pendidikan_dasar' => $request->get('pendidikan_dasar'),
                'lulus_tahun' => $request->get('lulus_tahun'),
                'no_hp' => $request->get('no_hp'),
                'npwp' => $request->get('npwp'),
                'alamat_jl' => $request->get('alamat_jl'),
                'alamat_rt' => $request->get('alamat_rt'),
                'alamat_rw' => $request->get('alamat_rw'),
                'alamat_kelurahan' => $request->get('alamat_kelurahan'),
                'alamat_kecamatan' => $request->get('alamat_kecamatan'),
                'alamat_kota' => $request->get('alamat_kota'),
                'alamat_provinsi' => $request->get('alamat_provinsi'),
                'masa_kerja_golongan_tahun' => $request->get('masa_kerja_golongan_tahun'),
                'masa_kerja_golongan_bulan' => $request->get('masa_kerja_golongan_bulan'),
                'masa_kerja_golongan_kalender' => $request->get('masa_kerja_golongan_kalender'),
                'masa_kerja_pensiun_tahun' => $request->get('masa_kerja_pensiun_tahun'),
                'masa_kerja_pensiun_bulan' => $request->get('masa_kerja_pensiun_bulan'),
                'mulai_masuk_pns' => $request->get('mulai_masuk_pns'),
                'belum_pns_dari' => $request->get('belum_pns_dari'),
                'belum_pns_sampai_dengan' => $request->get('belum_pns_sampai_dengan'),

            ];

            if (isset($request->foto)) {
                //jika ada input foto
                //simpat foto di storage
                $berkas_foto = $this->save_file($request->get('NIP'), $request->foto);

                //buat filed foto
                $input['foto'] =  $berkas_foto;

            }

            $update_data_baru = UpdatePegawai::create($input);

        }

         return redirect()->route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]);
        // return redirect()->route('kepegawaian-fakultas.beranda');

    }

    public function show($nip)
    {

        $data = UpdatePegawai::where('NIP', $nip)->get();
        $usulan = UsulanPensiun::where('nip', $nip)->first();

        if ($data->count() > 0) {
            $foto = 1;
            $pegawai = $data->first();;
        } else {
            $foto = 0;
            $pegawai = Dosen::where('NIP', $nip)->first();
        }

        $dataKeluarga = UpdateKeluarga::where('NIP', $nip)->get();

        if ($dataKeluarga->count() > 0) {
            $keluarga = $dataKeluarga;
        } else {
            $keluarga = [];
        }

        $dataAnak = UpdateAnak::where('NIP', $nip)->get();

        if ($dataAnak->count() > 0) {
            $anak = $dataAnak;
        } else {
            $anak = [];
        }

        return view('kepegawaian_fakultas.data.show',compact('pegawai', 'usulan', 'keluarga', 'anak', 'foto'));
    }

    public function tambah_keluarga(Request $request)
    {
        // return $request->all();
        $input = [
            'NIP'=> $request->get('NIP'),
            'nik' => $request->get('nik'),
            'nama' => $request->get('nama'),
            'tanggal_lahir' => $request->get('tanggal_lahir'),
            'tanggal_kawin' => $request->get('tanggal_kawin'),
            'tanggal_cerai' => $request->get('tanggal_cerai'),
            'pasangan_keberapa' => $request->get('pasangan_keberapa'),
        ];

        UpdateKeluarga::create($input);
        Alert::success('Data berhasil ditambahkan!');
        return redirect()->back();

    }

    public function update_keluarga(Request $request, $id)
    {
        $datas = UpdateKeluarga::where('id', $id)->first();

        $datas->NIP = $request->NIP;
        $datas->nik = $request->nik;
        $datas->nama = $request->nama;
        $datas->tanggal_lahir = $request->tanggal_lahir;
        $datas->tanggal_kawin = $request->tanggal_kawin;
        $datas->tanggal_cerai = $request->tanggal_cerai;
        $datas->pasangan_keberapa = $request->pasangan_keberapa;

        $datas->update();
        Alert::success('Data berhasil diubah!');
        return redirect()->back();

    }

    // tambah anak
    public function tambah_anak(Request $request)
    {
        $input = [
            'NIP'=> $request->get('NIP'),
            'nik' => $request->get('nik'),
            'nama' => $request->get('nama'),
            'tanggal_lahir' => $request->get('tanggal_lahir'),
            'nama_ayah' => $request->get('nama_ayah'),
            'nama_ibu' => $request->get('nama_ibu'),
            'keterangan' => $request->get('keterangan'),
        ];

        UpdateAnak::create($input);
        Alert::success('Data berhasil ditambahkan!');
        return redirect()->back();

    }

    // update anak
    public function update_anak(Request $request, $id)
    {
        $datak = UpdateAnak::where('id', $id)->first();

        $datak->nik = $request->nik;
        $datak->nama = $request->nama;
        $datak->tanggal_lahir = $request->tanggal_lahir;
        $datak->nama_ayah = $request->nama_ayah;
        $datak->nama_ibu = $request->nama_ibu;
        $datak->keterangan = $request->keterangan;

        $datak->update();
        Alert::success('Data berhasil diubah!');
        return redirect()->back();

    }

    public function destroy_anak($id)
    {
        $anak = UpdateAnak::findOrFail($id);
        $anak->delete();

        return response()->json($anak);
    }

    public function destroy_keluarga($id)
    {
        $keluarga = UpdateKeluarga::findOrFail($id);
        $keluarga->delete();

        return response()->json($keluarga);
    }


    //private function
    private function save_file($nip, $file)
    {
        $name = $nip.'_'.$file->getClientOriginalName();
        $filename = str_replace(' ', '_', $name);
        Storage::putFileAs('public/pegawai/', $file, $filename);
        return $filename;

    }

    private function delete_file($file)
    {
        if (isset($file)) {
            Storage::delete('public/pegawai/'.$file);
        } else {
            return 'file does not exist';
        }

    }

}
