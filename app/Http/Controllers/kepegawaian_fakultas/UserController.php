<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Alert;
use App\User;
use App\Models\InfoFakultas;
use Hash;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index(){
    	$user =  auth()->user();
    	// return $user = Auth()->user()->with('fakultas')->get();
    	// $fakultas_id = auth()->user()->fakultas->id;
        return view('kepegawaian_fakultas.profil.index', compact('user'));

    }

    public function editProfil($id){
    	$user = user::find($id);
    	return view('kepegawaian_fakultas.profil.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required|min:3|max:50',
            'username'  => 'required|min:3|max:50',
            // 'no_hp'     => 'required|min:3|max:50',
            'email'     => 'required|email',
            'password'  => 'confirmed',
        ]);

        // return $request->all();
        $user = User::find($id);
        $user->name     = $request->name;
        $user->username = $request->username;
        $user->no_hp    = $request->no_hp;
        $user->email    = $request->email;

        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
        }

        $user->update();

        if (isset($user->fakultas->infoFakultas->id)) {
            $idInfoFakultas = $user->fakultas->infoFakultas->id;

            $infoFakultas = InfoFakultas::where('id', $idInfoFakultas)->first();
            
            $infoFakultas->nama_lengkap_fakultas    = $request->nama_lengkap_fakultas;
            $infoFakultas->nama_dekan               = $request->nama_dekan;
            $infoFakultas->nip_dekan                = $request->nip_dekan;
            $infoFakultas->pangkat_dekan            = $request->pangkat_dekan;
            $infoFakultas->golongan_dekan           = $request->golongan_dekan;
            $infoFakultas->alamat_fakultas          = $request->alamat_fakultas;
            $infoFakultas->no_telepon_fakultas      = $request->no_telepon_fakultas;
            $infoFakultas->email_fakultas           = $request->email_fakultas;
            $infoFakultas->fax_fakultas             = $request->fax_fakultas;
            $infoFakultas->kode_pos_fakultas        = $request->kode_pos_fakultas;
    
            $infoFakultas->update();
        } else {
            $infoFakultas = new InfoFakultas;
            
            $infoFakultas->fakultas_id              = auth()->user()->fakultas->id;
            $infoFakultas->nama_lengkap_fakultas    = $request->nama_lengkap_fakultas;
            $infoFakultas->nama_dekan               = $request->nama_dekan;
            $infoFakultas->nip_dekan                = $request->nip_dekan;
            $infoFakultas->pangkat_dekan            = $request->pangkat_dekan;
            $infoFakultas->golongan_dekan           = $request->golongan_dekan;
            $infoFakultas->alamat_fakultas          = $request->alamat_fakultas;
            $infoFakultas->no_telepon_fakultas      = $request->no_telepon_fakultas;
            $infoFakultas->email_fakultas           = $request->email_fakultas;
            $infoFakultas->fax_fakultas             = $request->fax_fakultas;
            $infoFakultas->kode_pos_fakultas        = $request->kode_pos_fakultas;
    
            $infoFakultas->save();
        }
        
        Alert::success('Data berhasil di perbaharui');
        return redirect()->route('kepegawaian-fakultas.profil');
    }

}