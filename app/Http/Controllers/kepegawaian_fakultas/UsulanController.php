<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Storage;
use Alert;
use App\Kirim;

use App\User;
use App\Models\UsulanPensiun;
use App\Models\Dosen;
use App\Models\Notifikasi;
use App\Models\Berkas;
use App\Models\BerkasUsulanPensiun;
use App\Models\LogUsulanPensiun;
use App\Models\MasterBerkasPensiun;
use App\Models\NotifikasiWaEmail;
use App\Models\UpdatePegawai;

class UsulanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // return $k = DB::table('dosen')->where('NIP', 196402061988101001)->get();
        $fakultas_id = auth()->user()->fakultas->id;
        $usulans = UsulanPensiun::where('fakultas_id', $fakultas_id)
                                ->whereIn('status_usulan_kepeg_untan',[0,1,2,3,5])
                                ->orderBy('id', 'desc')->get();

        // $pegawai = UpdatePegawai::where('nip', $nip)->first();
        // return $pegawai;

        return view('kepegawaian_fakultas.usulan.index', compact('usulans'));
    }

    public function cari_data_pegawai($nip)
    {
        $fakultas = auth()->user()->fakultas->nama_fakultas;
        $dosen = DB::table('dosen')->where('NIP', $nip)->first();
        if (isset($dosen)) {
            if ($dosen->Unit_Kerja != $fakultas) {
                return 2;
            }
            return response()->json($dosen, 200);
        } else {
            return 0;
        }
    }

    public function tambah_usulan(Request $request)
    {
        // return $request->all();
        $fakultas_id = auth()->user()->fakultas->id;
        $nip_pegawai = $request->nip;
        $jenis_pensiun_id = $request->jenis_pensiun_id;

        $cek_usulan = UsulanPensiun::where('nip', $nip_pegawai)->get();

        if ($cek_usulan->count() > 0) {
            return 0;
        } else {
            $usulan = UsulanPensiun::create([
                'kode_usulan' => 'USUL'.$nip_pegawai,
                'nip' => $nip_pegawai,
                'fakultas_id' => $fakultas_id,
                'jenis_pensiun_id' => $jenis_pensiun_id,
                'status_usulan_kepeg_fakultas' => 0,
                'status_usulan_kepeg_untan' => 0,
            ]);

            //kirim notifikasi ke pegawai atau dosen bersangkutan
            $dosen = Dosen::where('NIP', $nip_pegawai)->first();

            if (isset($dosen->NO_HP)) {
                $no_hp = $dosen->NO_HP;
            } else {
                $bkd = file_get_contents('http://bkd.untan.ac.id/api.php/'.$nip_pegawai);
                $dataBkd = json_decode($bkd, true);
                $no_hp = $dataBkd['alamat_telp'];
            }

            //kirim notif jika terdapat no hp
            if (isset($no_hp)) {
                $pesan = 'Kepegawaian UNTAN \nUsulan pensiun atas nama anda '.$dosen->NAMA_LENGKAP.' telah di tambahkan oleh operator fakultas. Silahkan login ke aplikasi/website silap.untan.ac.id untuk menindaklanjuti usulan tersebut.';
                Kirim::wa($no_hp, $pesan);
            }

            $logUsulanPensiun = $this->logUsulan($usulan->id, 'Tambah usulan dengan jenis usulan '.$usulan->jenisPensiun->nama);

            $route = route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]);

            return response()->json($route, 200);
        }
    }

    public function hapus_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $usulan->delete();

        if ($usulan) {
            return response()->json('deleted');
        } else {
            return response()->json('error');
        }

    }

    public function kelola_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->orderBy('id', 'desc')->first();

        $dataPegawai = UpdatePegawai::where('NIP', $usulan->nip)->get();

        if ($dataPegawai->count() > 0) {
            $foto = 1;
            $dosen = $dataPegawai->first();
        } else {
            $foto = 0;
            $dosen = DB::table('dosen')->where('NIP', $usulan->nip)->first();
        }

        return view('kepegawaian_fakultas.usulan.kelola_usulan', compact('usulan','dosen', 'foto'));

    }

    public function kelola_usulan_berkas($id)
    {
        $usulans = UsulanPensiun::where('id', $id)->with('jenisPensiun', 'berkasUsulanPensiun')->get();
        // $berkas = Berkas::where('id', $usulan->berkas_usulan_pensiun)
        // return $usulans;

        return view('kepegawaian_fakultas.usulan.kelola_berkas', compact('usulans'));
    }

    public function kelola_usulan_berkas_tambah(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'master_berkas_pensiun_id' => 'required',
            'berkas' => 'required|mimes:jpeg,JPG,png,pdf|max:5054',
        ],[
            'master_berkas_pensiun_id.required' => 'master berkas pensiun id tidak boleh kosong!',
            'berkas.required' => 'Berkas tidak boleh kosong!',
            'berkas.mimes' => 'File berkas hanya boleh JPG, JPEG, PDF. Maskimal ukuran 5MB!',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $usulan = UsulanPensiun::where('id', $request->usulan_pensiun_id)->first();
            $file_berkas = $request->berkas;
            $master_berkas_pensiun_id = $request->master_berkas_pensiun_id;
            $user_id = auth()->user()->id;

            $cek_berkas = BerkasUsulanPensiun::where([['usulan_pensiun_id', $usulan->id], ['master_berkas_pensiun_id', $master_berkas_pensiun_id]]);

            if ($cek_berkas->count() > 0) {
                //jika data sudah ada, hapus file lama
                $berkas = $cek_berkas->first()->berkas;
                $hapus_berkas = $this->delete_file($usulan->id, $berkas->nama_berkas);
                // $hapus_data = BerkasUsulanPensiun::find($cek_berkas->first()->id)->delete();
                $nama_berkas = $this->save_file($usulan->id, $master_berkas_pensiun_id, $file_berkas);

                $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
                $direktori = $fakultas.'/'.$usulan->nip;

                $simpan_berkas = Berkas::find($berkas->id)->update([
                    'nama_berkas'               => $nama_berkas,
                    'direktori'                 => $direktori,
                    'uploader_berkas_user_id'   => $user_id,
                ]);

                $berkas_id = $berkas->id;

            } else {
                //jika data baru
                $nama_berkas = $this->save_file($usulan->id, $master_berkas_pensiun_id, $file_berkas);

                $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
                $direktori = $fakultas.'/'.$usulan->nip;

                //simpan data berkas
                $simpan_berkas = Berkas::create([
                    'nama_berkas'               => $nama_berkas,
                    'direktori'                 => $direktori,
                    'uploader_berkas_user_id'   => $user_id,
                ]);

                $berkas_id = $simpan_berkas->id;
            }


            //simpan daftar berkas
            // $berkas_usulan_pensiun = BerkasUsulanPensiun::create([
            //     'usulan_pensiun_id' => $usulan->id,
            //     'master_berkas_pensiun_id' => $master_berkas_pensiun_id,
            //     'berkas_id' => $berkas_id,
            //     'status_verifikasi_kepeg_fakultas' => 0,
            //     'status_verifikasi_kepeg_untan' => 0,
            // ]);

            $berkas_usulan_pensiun = BerkasUsulanPensiun::updateOrCreate(
                [
                    'usulan_pensiun_id' => $usulan->id,
                    'master_berkas_pensiun_id' => $master_berkas_pensiun_id,
                ],[
                    'berkas_id' => $berkas_id,
                    'status_verifikasi_kepeg_fakultas' => 0,
                    'status_verifikasi_kepeg_untan' => 0,
                ]
            );

            // $nama_master_berkas = MasterBerkasPensiun::where('id', $master_berkas_pensiun_id)->first();
            $nama_berkas = $berkas_usulan_pensiun->masterBerkasPensiun->judul_berkas;
            $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator Fakultas mengupload berkas '.$nama_berkas);

            Alert::success('Berkas berhasil di upload!');
            return redirect()->back();

        }
    }

    public function verifikasi_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $status_usulan_fakultas = $usulan->status_usulan_kepeg_fakultas;
        $status_usulan_untan = $usulan->status_usulan_kepeg_untan;

        $berkasJenisPensiun = $usulan->jenisPensiun->berkasJenisPensiun;
        $berkasUpload = $usulan->berkasUsulanPensiun;

        if ($berkasUpload->count() < $berkasJenisPensiun->count()) {
            // return 'berkastidaklengkap';
            return response()->json([
                'status' => 0,
                'pesan' => 'Berkas Belum Lengkap!'
            ]);
        } else {
            if ($berkasUpload->where('status_verifikasi_kepeg_fakultas', 1)->count() < $berkasJenisPensiun->count()) {
                // return 'adaberkasbelumverifikasi';
                return response()->json([
                    'status' => 0,
                    'pesan' => 'Terdapat Berkas Yang Belum Diverifikasi!'
                ]);
            } else {
                if ($status_usulan_untan == 0 || $status_usulan_untan == 5) {
                    if ($status_usulan_fakultas == 0) {

                        //cek data update pegawai
                        $dosen = UpdatePegawai::where('NIP', $usulan->nip)->first();

                        if (empty($dosen)) {
                            //usulan blm bisa di kirm krna data pegawai belum di perbaharui
                            return response()->json([
                                'status' => 0,
                                'pesan' => 'Usulan Tidak Dapat Diverifikasi Karena Data Pegawai Belum Diperbaharui!'
                            ]);
                        }

                        //verifikasi usulan
                        $verifikasi = $usulan->update([
                            'status_usulan_kepeg_fakultas' => 1,
                        ]);

                        //catat log
                        $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator kepegawaian fakultas memverfikasi usulan');

                        // return 1;
                        return response()->json([
                            'status' => 1,
                            'pesan' => 'Usulan Berhasil Diverfikasi!'
                        ]);

                    } elseif ($status_usulan_fakultas == 1) {
                        //usulan telah di verfikasi
                        return response()->json([
                            'status' => 0,
                            'pesan' => 'Usulan Telah Diverifikasi Sebelumnya!'
                        ]);
                    } elseif ($status_usulan_fakultas == 2) {
                        //usulan telah di kirim
                        return response()->json([
                            'status' => 0,
                            'pesan' => 'Usulan Telah Dikirim Sebelumnya!'
                        ]);
                    } else {
                        return response()->json([
                            'status' => 0,
                            'pesan' => 'Ada Masalah Verifikasi Usulan!'
                        ]);
                    }
                }
            }

        }

        return response()->json([
            'status' => 0,
            'pesan' => 'Ada Masalah Dengan Server!'
        ]);
    }

    public function kirim_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $status_usulan_fakultas = $usulan->status_usulan_kepeg_fakultas;
        $status_usulan_untan = $usulan->status_usulan_kepeg_untan;

        if ($status_usulan_untan == 0 || $status_usulan_untan == 5) {
            if ($status_usulan_fakultas == 1) {
                //usulan dapat dikirim
                $verifikasi = $usulan->update([
                    //ubah status fakultas jdi dikirim
                    'status_usulan_kepeg_fakultas' => 2,
                    //ubah status kepeg untan jdi diterima
                    'status_usulan_kepeg_untan' => 1
                ]);

                $link = route('kepegawaian-untan.usulan.kelola_usulan', ['id' => $usulan->id]);

                $notifikasi = Notifikasi::create([
                    'usulan_pensiun_id' => $usulan->id,
                    'dari_users_id' => auth()->user()->id,
                    'untuk_role' => 'operator untan',
                    'fakultas_id' => $usulan->fakultas_id,
                    'nip_dosen' => $usulan->dosen->NIP,
                    'pesan' => 'Terdapat usulan baru dari '.$usulan->fakultas->nama_fakultas. ' Atas nama '.$usulan->dosen->NAMA_LENGKAP,
                    'status' => 0,
                    'link' => $link,
                    'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),


                ]);

                //kirim notifikasi wa ke operator untan
                $this->kirimNotifikasi('Kepegawaian UNTAN \nOperator kepegawaian fakultas mengirimkan usulan atas nama '.$usulan->dataPegawai->NAMA_LENGKAP, 'operator fakultas', $usulan->id);

                //kirim notifikasi ke pegawai atau dosen bersangkutan
                $dosen = UpdatePegawai::where('NIP', $usulan->nip)->first();

                if (isset($dosen->no_hp)) {
                    $no_hp = $dosen->no_hp;
                } else {
                    $bkd = file_get_contents('http://bkd.untan.ac.id/api.php/'.$usulan->nip);
                    $dataBkd = json_decode($bkd, true);
                    $no_hp = $dataBkd['alamat_telp'];
                }

                //kirim notif jika terdapat no hp
                if (isset($no_hp)) {
                    $pesan = 'Kepegawaian UNTAN \nUsulan pensiun atas nama anda '.$dosen->NAMA_LENGKAP.' telah di teruskan ke Kepegawaian UNTAN oleh Operator Kepegawaian Fakultas.';
                    Kirim::wa($no_hp, $pesan);
                }

                //catat log
                $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator fakultas mengirim usulan ke kepegawaian UNTAN');
                $logUsulanPensiun = $this->logUsulan($usulan->id, 'Usulan di terima oleh kepegawaian UNTAN');

                return response()->json([
                    'status' => 1,
                    'pesan' => 'Usulan Berhasil Dikirim Kepegawaian Untan!'
                ]);
            } elseif ($status_usulan_fakultas == 0) {
                //usulan blm bisa di kirm krna blm verifikasi
                return response()->json([
                    'status' => 0,
                    'pesan' => 'Usulan Belum Diverfikasi!'
                ]);
            } elseif ($status_usulan_fakultas == 2) {
                //usulan telah dikirim
                // return 'sent';
                return response()->json([
                    'status' => 0,
                    'pesan' => 'Usulan Telah Dikirim Sebelumnya!'
                ]);
            } else {
                return response()->json([
                    'status' => 0,
                    'pesan' => 'Ada Masalah Dengan Usulan!'
                ]);
            }
        }

        return response()->json([
            'status' => 0,
            'pesan' => 'Ada Masalah Dengan Usulan!'
        ]);

    }

    public function verifikasi_berkas($id)
    {
        $data = BerkasUsulanPensiun::where('id', $id)->first();
        $nama_berkas = $data->masterBerkasPensiun->judul_berkas;

        if ($data->status_verifikasi_kepeg_fakultas == 0) {

            $catatanBerkas = $data->catatanBerkas;

            if ($catatanBerkas->where('status_kepeg_fakultas', 0)->count() > 0) {
                return response()->json([
                    'status' => 0,
                    'pesan' => 'Terdapat Catatan Berkas Yang Belum Selesai!'
                ]);
            } else {
                $verified = $data->update([
                    'status_verifikasi_kepeg_fakultas' => 1
                ]);

                $logUsulanPensiun = $this->logUsulan($data->usulan_pensiun_id, 'Operator fakultas memverifikasi berkas '.$nama_berkas);

                return response()->json([
                    'status' => 1,
                    'pesan' => 'Berkas Berhasil Diverfikasi!'
                ]);
            }

        } elseif ($data->status_verifikasi_kepeg_fakultas == 1) {

            $verified = $data->update([
                'status_verifikasi_kepeg_fakultas' => 0
            ]);

            return response()->json([
                'status' => 2,
                'pesan' => 'Berkas Berhasil Tidak Diverifikasi!'
            ]);

        } else {
            return response()->json([
                'status' => 0,
                'pesan' => 'Ada Kesalahan Pada Status Berkas!'
            ]);
        }
    }

    public function lihat_catatan($usulanId)
    {
        $usulan = UsulanPensiun::with('lihatCatatan')->find($usulanId);
        $data = [
            'status' => 1,
            'pesan' => 'usulan berhasil ditemukan!',
            'data' => $usulan
        ];
        return response()->json($data, 200);
    }

    public function catatan_selesai($usulanId)
    {
        $usulan = UsulanPensiun::with('lihatCatatan')->find($usulanId);
        $catatan = $usulan->lihatCatatan;

        if ($catatan->where('status', 0)->count() > 0) {

            foreach ($catatan as $key => $value) {
                $value->update([
                    'status' => 1
                ]);
            }

            return response()->json([
                'status' => 1,
                'pesan'  => 'Catatan telah selesai di laksanakan',
                'data'   => $catatan
            ], 200);

        } else {
            return response()->json([
                'status' => 0,
                'pesan'  => 'Tidak ada catatan yang harus diselesaikan',
                'data'   => $catatan
            ], 200);
        }
    }

    //private function
    private function save_file($usulan_id, $master_berkas_pensiun_id, $file)
    {
        $usulan         = UsulanPensiun::where('id', $usulan_id)->first();
        $master_berkas  = MasterBerkasPensiun::where('id', $master_berkas_pensiun_id)->first();

        $nama_berkas    = str_replace(' ', '_', $master_berkas->judul_berkas);
        $fakultas       = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);

        $name = $nama_berkas.'_'.$usulan->nip.'_'.md5(date('Ymdhis')).'.'.$file->getClientOriginalExtension();
        $filename = str_replace(str_split('\\/:*?"<>|'), '_', $name);
        Storage::putFileAs('public/pensiun/'.$fakultas.'/'.$usulan->nip, $file, $filename);
        //Storage::disk('berkas')->putFileAs('pensiun/'.$fakultas.'/'.$usulan->nip, $file, $filename);

        return $filename;
    }

    private function delete_file($usulan_id, $file)
    {
        if (isset($file)) {
            $usulan = UsulanPensiun::where('id', $usulan_id)->first();
            $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
            Storage::delete('public/pensiun/'.$fakultas.'/'.$usulan->nip.'/'.$file);
        } else {
            return 'file does not exist';
        }

    }

    private function logUsulan($usulan_pensiun_id, $nama)
    {
        $logUsulanPensiun = LogUsulanPensiun::create([
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nama' => $nama,
        ]);
        return true;
    }

    private function kirimNotifikasi($pesan, $role, $usulan_pensiun_id)
    {
        $usulan = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
        $fakultas_id = $usulan->fakultas_id;
        $nip = $usulan->nip;

        if ($role == 'operator untan') {
            $untuk_user_id  = $this->idOperator($fakultas_id, 'operator fakultas');
            $dari_user_id   = $this->idOperator($fakultas_id, 'operator untan');
            $email          = $this->emailOperator($fakultas_id, 'operator fakultas');
            $no_hp          = $this->noHpOperator($fakultas_id, 'operator fakultas');
        } else {
            $untuk_user_id  = $this->idOperator($fakultas_id, 'operator untan');
            $dari_user_id   = $this->idOperator($fakultas_id, 'operator fakultas');
            $email          = $this->emailOperator($fakultas_id, 'operator untan');
            $no_hp          = $this->noHpOperator($fakultas_id, 'operator untan');
        }

        $data = NotifikasiWaEmail::create([
            'pesan' => $pesan,
            'waktu_kirim' => \Carbon\Carbon::now(),
            'email' => $email,
            'no_hp' => $no_hp,
            'untuk_user_id' => $untuk_user_id,
            'dari_user_id' => $dari_user_id,
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nip' => $nip,
        ]);

        //kirim::wa(tujuan , pesan)
        $kirimWa = Kirim::wa($no_hp, $pesan);

        //kirim email

    }

    private function noHpOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->no_hp;
                    } else {
                        $noHp = 0;
                    }

                }
            }
        }

        return $noHP;
    }

    private function emailOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->email;
                    } else {
                        $noHp = 0;
                    }

                }
            }
        }

        return $noHP;
    }

    private function idOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->id;
                    } else {
                        $noHp = 0;
                    }

                }
            }
        }

        return $noHP;
    }

}
