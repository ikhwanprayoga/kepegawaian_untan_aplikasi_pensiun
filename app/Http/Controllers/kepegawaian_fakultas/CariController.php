<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Dosen;

class CariController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search($input)
    {
        $data = Dosen::select('NIP', 'NAMA_LENGKAP')->where('NIP', 'like', '%' .$input. '%')->orWhere('NAMA_LENGKAP', 'like', '%' .$input. '%')->take(10)->get();
        return response()->json($data, 200);
    }

    public function bpkad($nip)
    {
        $dt = file_get_contents('http://bkd.untan.ac.id/api.php/'.$nip);
        $data = json_decode($dt, true);

        return view('kepegawaian_fakultas.data.show_bpkad', compact('data'));
    }
}
