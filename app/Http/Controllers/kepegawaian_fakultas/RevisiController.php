<?php

namespace App\Http\Controllers\kepegawaian_fakultas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Storage;
use Alert;

use App\Models\UsulanPensiun;
use App\Models\JenisPensiun;
use App\Models\Berkas;
use App\Models\BerkasUsulanPensiun;
use App\Models\LogUsulanPensiun;
use App\Models\MasterBerkasPensiun;
use App\Models\Notifikasi;
use App\Models\Revisi;
use App\Models\CatatanBerkas;

class RevisiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detail_revisi($upi)
    {
        $usulan = UsulanPensiun::where('id', $upi)->with('dosen', 'revisi')->orderBy('id', 'desc')->first();
        // $dosen = Dosen::where('nip', $nip)->first();
        // return $revisi = Revisi::where('usulan_pensiun_id', $upi)->get();

        return view('kepegawaian_fakultas.revisi.detailRevisi', compact('usulan'));
    }

    public function revisi_selesai($id)
    {
        // return 'data id adalah '.$id;

        $CatatanBerkas = CatatanBerkas::where('berkas_usulan_pensiun_id', $id)->get();

        foreach ($CatatanBerkas as $key => $value) {
            if ($value->status_kepeg_fakultas == 0) {
                $value->update([
                    'status_kepeg_fakultas' => 1,
                ]);
            }
        }

        BerkasUsulanPensiun::where('id', $id)->update([
            'status_verifikasi_kepeg_fakultas' => 1
        ]);
        
        $usulan = $CatatanBerkas->first()->berkasUsulanPensiun->usulanPensiun;
        $berkas = $CatatanBerkas->first()->berkasUsulanPensiun->masterBerkasPensiun;
        $link = route('kepegawaian-untan.usulan.kelola_usulan.berkas', ['id' => $usulan->id]);

        $notifikasi = Notifikasi::create([
            'usulan_pensiun_id' => $usulan->id,
            'dari_users_id' => auth()->user()->id,
            'untuk_role' => 'operator untan',
            'fakultas_id' => $usulan->fakultas_id,
            'nip_dosen' => $usulan->dosen->NIP,
            'pesan' => 'Revisi berkas '.$berkas->judul_berkas.' dengan kode usulan '.$usulan->kode_usulan.' dari '.$usulan->fakultas->nama_fakultas.' telah selesai dilaksanakan.',
            'status' => 0,
            'link' =>$link,
            'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
        ]);
        
        //catat log
        $logUsulanPensiun = $this->logUsulan($usulan->id, 'Revisi berkas '.$berkas->judul_berkas.' dengan kode usulan '.$usulan->kode_usulan.' dari '.$usulan->fakultas->nama_fakultas.' telah selesai dilaksanakan.');

        return 1;
    }

    // private function
    private function logUsulan($usulan_pensiun_id, $nama)
    {
        $logUsulanPensiun = LogUsulanPensiun::create([
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nama' => $nama,
        ]);
        return true;
    }
}
