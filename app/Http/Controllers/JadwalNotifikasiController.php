<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use App\Kirim;

use App\User;
use App\Models\Dosen;
use App\Models\Fakultas;
use App\Models\DosenSementara;
use App\Models\NotifikasiPemberitahuanPengajuanPensiun;
use DateTime;

class JadwalNotifikasiController extends Controller
{
    public function kirim_notif()
    {
        $tahun = date('Y-m-d', strtotime('2022-07-04 08:00:00'));
        $data = NotifikasiPemberitahuanPengajuanPensiun::whereDate('waktu_kirim_notifikasi', $tahun)->where('status', 0)->get();

        if ($data->count() > 0) {
            foreach ($data as $key => $value) {
                //kirim notif wa ke dosen
                Kirim::wa($value->no_hp_pegawai, $value->pesan);

                //kirim notif email ke dosen
                $to_name = $value->nama;
                $to_email = $value->email_pegawai;
                $data = array('name'=>$value->nama, "body" => $value->pesan);
                    
                Mail::send('email.tes_email', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                            ->subject('pemberitahuan tentang pemberkasan batas usia pensiuns');
                    $message->from('ikhwanprayoga@untan.ac.id','Ikhwan Prayoga');
                });

                //kirim notif ke operator fakultas
                Kirim::wa($value->no_hp_operator_fakultas, $value->pesan);

                //kirim notif ke operator untan
                Kirim::wa($value->no_hp_operator_untan, $value->pesan);

                //update status kirim notif
                NotifikasiPemberitahuanPengajuanPensiun::where('id', $value->id)->update([
                    'status' => 1
                ]);
            }

        } else {
            return null;
        }
        
        return 'done';
        //return info commantd

    }

    public function atur_jadwal()
    {
        // $bkd = file_get_contents('http://bkd.untan.ac.id/api.php', true);
        // $dataBkd = json_decode($bkd, true);
        $dosen = DosenSementara::where('status', 0)->get()->take(100);

        foreach ($dosen as $key => $value) {

            $bkd = file_get_contents('http://bkd.untan.ac.id/api.php/'.$value->nip);
            $dataBkd = json_decode($bkd, true);

            $tahunLahir = (int)date('Y', strtotime($value->tgl_lahir));
            $split = explode('-', $value->tgl_lahir);

            if ($value->jabatan == 'GB') {
                $tahunPensiun = $tahunLahir + 70;
            } else {
                $tahunPensiun = $tahunLahir + 65;
            }
            
            $pesan = 'Kepegawaian Universitas Tanjungpura \nMohon untuk melengkapi berkas usulan pensiun, karena usia anda akan memasuki batas usia untuk pensiun. Untuk informasi lebih lanjut silahkan masuk ke sistem informasi dan layanan pensiun www.silap.untan.ac.id/pegawai/login';

            $waktuNotif1 = new DateTime((int)$tahunPensiun - 2 .'-'.$split[1].'-'.$split[2].' 08:00:00');
            $waktuNotif2 = $waktuNotif1->modify("+6 month");

            $fakultas       = $value->unit_kerja;
            $unit_kerja = Fakultas::where('nama_fakultas', $fakultas)->first();
            $no_hp_o_untan    = $this->noHpOperatorUntan('operator untan');
            $no_hp_o_fakultas = $this->noHpOperator($unit_kerja->id, 'operator fakultas');

            $atur = NotifikasiPemberitahuanPengajuanPensiun::create([
                'nip'           => $value->nip,
                'nama'          => $value->nama,
                'tgl_lahir'     => $value->tgl_lahir,
                'jabatan'       => $value->jabatan,
                'unit_kerja'    => $value->unit_kerja,
                'pesan'         => $pesan,
                'tahun_pensiun' => $tahunPensiun,
                'email_pegawai' => $dataBkd['email'],
                'no_hp_pegawai' => $dataBkd['alamat_telp'],
                'no_hp_operator_fakultas'   => $no_hp_o_fakultas, 
                'no_hp_operator_untan'      => $no_hp_o_untan,
                'waktu_kirim_notifikasi'    => $waktuNotif2,
                'status' => 0,
            ]);

            $update = DosenSementara::where('id', $value->id)->update([
                'status' => 1
            ]);

            // $data[$key]['nama']     = $value->nama;
            // $data[$key]['nip']      = $value->nip;
            // $data[$key]['jabatan']  = $value->jabatan;
            // $data[$key]['tgl_lahir']  = $value->tgl_lahir;
            // $data[$key]['email']    = $dataBkd['email'];
            // $data[$key]['no_hp']    = $dataBkd['alamat_telp'];

        }

        return $atur;
    }

    public function clone_data()
    {
        $dosen = Dosen::select('NIP', 'NAMA_LENGKAP', 'JAB_FUNGSIONAL', 'Unit_Kerja', 'Tgl_Lahir')->get();

        foreach ($dosen as $key => $value) {
            $clone = DosenSementara::create([
                'nama'      => $value->NAMA_LENGKAP,
                'nip'       => $value->NIP,
                'jabatan'   => $value->JAB_FUNGSIONAL,
                'unit_kerja'=> $value->Unit_Kerja,
                'tgl_lahir' => $value->Tgl_Lahir,
                'no_hp'     => null,
                'email'     => null,
                'status'    => 0,
            ]);
        }
        
        return 'done';
    }

    public function kirim_notif_sementara($nip)
    {
        // $tahun = date('Y-m-d', strtotime('2022-07-04 08:00:00'));
        $dataPegawai = NotifikasiPemberitahuanPengajuanPensiun::where('nip', $nip)->get();

        if ($dataPegawai->count() > 0) {
            foreach ($dataPegawai as $key => $value) {
                //kirim notif wa ke dosen
                Kirim::wa($value->no_hp_pegawai, $value->pesan);

                //kirim notif email ke dosen
                $to_name = $value->nama;
                $to_email = $value->email_pegawai;
                $data = array('name'=>$value->nama, "body" => $value->pesan);
                    
                Mail::send('email.tes_email', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                            ->subject('pemberitahuan tentang pemberkasan batas usia pensiuns');
                    $message->from('kepegawaian@untan.ac.id','Kepegawaian UNTAN');
                });

		$pesan_op = 'Kepegawaian Untan \nPemberitahuan bahwa pegawai atas nama '.$value->nama.' telah memasuki batas usia pensiun. Harap segera menindak lanjuti pemberitahuan ini di aplikasi silap.';

                //kirim notif ke operator fakultas
                Kirim::wa($value->no_hp_operator_fakultas, $pesan_op);

                //kirim notif ke operator untan
                Kirim::wa($value->no_hp_operator_untan, $pesan_op);

                //update status kirim notif
                // NotifikasiPemberitahuanPengajuanPensiun::where('id', $value->id)->update([
                //     'status' => 1
                // ]);
            }

        } else {
            return null;
        }
        
        return $dataPegawai;
        //return info commantd
    }

    private function noHpOperator($fakultas_id, $role)
    {
        // return null;
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == 'operator fakultas') {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->no_hp;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    private function noHpOperatorUntan($role)
    {
        // $user = User::with('roles')->get();
        // foreach ($user as $keys => $value) {
        //     foreach ($value->roles as $key => $val) {
        //         if ($val->name == $role) {
        //             if ($val) {
        //                 // $data['role'] = $val;
        //                 $noHP = $value->no_hp;
        //             } else {
        //                 $noHp = 0;
        //             }
                    
        //         }
        //     }
        // }
        $noHP = '089501942413';
        return $noHP;
    }

    private function emailOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->email;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    private function idOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->id;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

}
