<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;

use App\Models\Dosen;
use App\Models\UsulanPensiun;
use App\Models\UpdatePegawai;
use App\Models\UpdateKeluarga;
use App\Models\UpdateAnak;
use App\Models\WaktuPrintBerkas;
use App\Models\InfoFakultas;

class BerkasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dpcp(Request $request, $nip)
    {
        // return $request->tanggal;
        $dataPegawai = UpdatePegawai::where('NIP', $nip)->first();
        $usulan = UsulanPensiun::where('nip', $nip)->first();

        if (isset($dataPegawai)) {
            $pegawai = $dataPegawai;
        } else {
            $pegawai = Dosen::where('NIP', $nip)->first();
        }

        $keluarga = UpdateKeluarga::where('NIP', $nip)->get();
        $dataAnak = UpdateAnak::where('NIP', $nip)->get();
        if ($request->waktu_lama_dpcp == 'ya') {
            //waktu lama di pilih
            $dataWaktu = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first();
            $tgl = date('d-m-Y', strtotime($dataWaktu->waktu_dpcp));

        } else {
            //waktu baru di masukan
            $dataWaktu = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->get();
            if ( $dataWaktu->count() > 0 ) {
                //jika ada waktu lama maka update data waktu
                $dataWaktuPilih = $dataWaktu->first();
                $k = WaktuPrintBerkas::where('id', $dataWaktuPilih->id)->update([
                    'waktu_dpcp' => $request->tanggal,
                ]);
                
                $tgl = date('d-m-Y', strtotime($request->tanggal));

            } else {

                //create waktu
                WaktuPrintBerkas::create([
                    'usulan_pensiun_id' => $usulan->id,
                    'waktu_dpcp' => $request->tanggal,
                ]);
                $tgl = date('d-m-Y', strtotime($request->tanggal));

            }
        }
        

        return view('berkas.dpcp', compact('pegawai','keluarga','dataAnak','tgl'));
    }

    public function cek_dpcp($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $data = WaktuPrintBerkas::where('usulan_pensiun_id', $id)->first();

        if ( $data ) {
            
            if ($data->waktu_dpcp) {
                return $data->waktu_dpcp;
            } else {
                return 0;
            }

        } else {
            return 0;
        }

        // if ( $k = $data->where('usulan_pensiun_id', $usulan->id)->first() ) {
            
        //     if ($k->waktu_dpcp) {
        //         return $k->waktu_dpcp;
        //     } else {
        //         return 0;
        //     }

        // } else {
        //     return 0;
        // }
        
    }

    public function sp4(Request $request, $nip)
    {
        // $dataPegawai = UpdatePegawai::where('NIP', $nip)->first();
        $usulan = UsulanPensiun::where('nip', $nip)->with('dataPegawai')->first();

        // if (isset($dataPegawai)) {
        //     $pegawai = $dataPegawai;
        // } else {
        //     $pegawai = Dosen::where('NIP', $nip)->first();
        // }

        if ($request->waktu_lama_sp4 == 'ya') {
            //waktu lama di pilih
            $dataWaktu = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first();
            $tgl = date('d-m-Y', strtotime($dataWaktu->waktu_sp4));

        } else {
            //waktu baru di masukan
            $dataWaktu = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->get();
            if ( $dataWaktu->count() > 0 ) {
                //jika ada waktu lama maka update data waktu
                $dataWaktuPilih = $dataWaktu->first();
                WaktuPrintBerkas::where('id', $dataWaktuPilih->id)->update([
                    'waktu_sp4' => $request->tanggal,
                ]);
                $tgl = date('d-m-Y', strtotime($request->tanggal));

            } else {

                //create waktu
                $dataWaktuPilih = $dataWaktu->first();
                WaktuPrintBerkas::where('id', $dataWaktuPilih->id)->update([
                    'waktu_sp4' => $request->tanggal,
                ]);
                $tgl = date('d-m-Y', strtotime($request->tanggal));

            }
        }

        return view('berkas.sp4', compact('usulan','tgl'));
    }

    public function cek_sp4($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $data = WaktuPrintBerkas::get();

        if ( $k = $data->where('usulan_pensiun_id', $usulan->id)->first() ) {
            
            if ($k->waktu_sp4) {
                return $k->waktu_sp4;
            } else {
                return 0;
            }

        } else {
            return 0;
        }
    }

    public function surat_pengantar_bup(Request $request, $nip)
    {
        $usulan = UsulanPensiun::where('nip',$nip)->with('dataPegawai')->first();
        $cekData = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first();
        
        if ($request->nomor_surat_pengantar) {
            $update = WaktuPrintBerkas::where('id', $cekData->id)->update([
                'no_surat_pengantar' => $request->nomor_surat_pengantar
            ]);
        }

        if ($request->waktu_lama_surat_pengantar != 'ya') {
            $update = WaktuPrintBerkas::where('id', $cekData->id)->update([
                'waktu_surat_pengantar' => $request->tanggal
            ]);
        }

        $dataSurat = WaktuPrintBerkas::where('id', $cekData->id)->first();        

        return view ('berkas.surat_pengantar_bup', compact('usulan', 'dataSurat'));
    }

    public function cek_surat_pengantar($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $data = WaktuPrintBerkas::get();

        if ( $k = $data->where('usulan_pensiun_id', $usulan->id)->first() ) {
            
            if ($k->waktu_surat_pengantar) {

                return response()->json([
                    'waktu' => $k->waktu_surat_pengantar,
                    'no_surat' => $k->no_surat_pengantar,
                ]);

            } else {
                return 0;
            }

        } else {
            return 0;
        }
    }

    public function surat_bebas_pidana(Request $request, $nip)
    {
        // return $request->all();
        $cekDataPegawai = UpdatePegawai::where('NIP', $nip)->get();
        $fakultasId = auth()->user()->fakultas->id;
        $cekDataFakultas = InfoFakultas::where('fakultas_id', $fakultasId)->first();

        if (isset($cekDataFakultas)) {
            if ($cekDataPegawai->count() > 0) {
                $usulan = UsulanPensiun::where('nip',$nip)->with('dataPegawai')->first();
                $cekData = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first();
        
                if (empty($cekData)) {
                    //create
                    $create = WaktuPrintBerkas::create([
                        'usulan_pensiun_id' => $usulan->id,
                        'waktu_surat_pidana' => $request->tanggal,
                        'no_surat_pidana' => $request->no_surat_pidana,
                    ]);
                } else {
        
                    if ($request->waktu_lama_pidana != 'ya') {
                        $update = WaktuPrintBerkas::where('id', $cekData->id)->update([
                            'waktu_surat_pidana' => $request->tanggal
                        ]);
                    }

                    if ($request->no_surat_pidana) {
                        $update = WaktuPrintBerkas::where('id', $cekData->id)->update([
                            'no_surat_pidana' => $request->no_surat_pidana
                        ]);
                    }
        
                }
                $dataSurat = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first(); 
                return view ('berkas.surat_bebas_pidana', compact('usulan', 'dataSurat'));
            } else {
                Alert::error('Update Data Pegawai Terlebih Dahulu!');
                return redirect()->back();
            }
        } else {
            Alert::error('Update Data Profil Fakultas Terlebih Dahulu!');
            return redirect()->back();
        }

    }

    public function cek_surat_bebas_pidana($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $data = WaktuPrintBerkas::get();

        if ( $k = $data->where('usulan_pensiun_id', $usulan->id)->first() ) {
            
            if ($k->waktu_surat_pidana) {

                return response()->json([
                    'no_surat' => $k->no_surat_pidana,
                    'waktu' => $k->waktu_surat_pidana,
                ]);

            } else {
                return 0;
            }

        } else {
            return 0;
        }
    }

    public function surat_bebas_hukum(Request $request, $nip)
    {
        $cekDataPegawai = UpdatePegawai::where('NIP', $nip)->get();
        $fakultasId = auth()->user()->fakultas->id;
        $cekDataFakultas = InfoFakultas::where('fakultas_id', $fakultasId)->first();
        if (isset($cekDataFakultas)) {
            if ($cekDataPegawai->count() > 0) {
                $usulan = UsulanPensiun::where('nip',$nip)->with('dataPegawai')->first();
                $cekData = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first();
    
                if (empty($cekData)) {
                    //create
                    $create = WaktuPrintBerkas::create([
                        'usulan_pensiun_id' => $usulan->id,
                        'waktu_surat_hukuman' => $request->tanggal,
                        'no_surat_hukuman' => $request->no_surat_hukuman,
                    ]);
                } else {
    
                    if ($request->waktu_lama_hukum != 'ya') {
                        $update = WaktuPrintBerkas::where('id', $cekData->id)->update([
                            'waktu_surat_hukuman' => $request->tanggal
                        ]);
                    }

                    if ($request->no_surat_hukuman) {
                        $update = WaktuPrintBerkas::where('id', $cekData->id)->update([
                            'no_surat_hukuman' => $request->no_surat_hukuman
                        ]);
                    }
                    
                }
    
                $dataSurat = WaktuPrintBerkas::where('usulan_pensiun_id', $usulan->id)->first(); 
    
                return view ('berkas.surat_bebas_hukum', compact('usulan', 'dataSurat'));
            } else {
                Alert::error('Update Data Pegawai Terlebih Dahulu!');
                return redirect()->back();
            }
        } else {
            Alert::error('Update Data Profil Fakultas Terlebih Dahulu!');
            return redirect()->back();
        }
    }

    public function cek_surat_bebas_hukum($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $data = WaktuPrintBerkas::get();

        if ( $k = $data->where('usulan_pensiun_id', $usulan->id)->first() ) {
            
            if ($k->waktu_surat_hukuman) {

                return response()->json([
                    'no_surat' => $k->no_surat_hukuman,
                    'waktu' => $k->waktu_surat_hukuman,
                ]);

            } else {
                return 0;
            }

        } else {
            return 0;
        }
    }
}
