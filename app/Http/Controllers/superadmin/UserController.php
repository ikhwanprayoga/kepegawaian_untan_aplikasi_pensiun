<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Models\Fakultas;
use Hash;
use DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $data = user::with('roles','fakultas')->get();
        $role = Role::all();
        return view('superadmin.user.index', [
            'data' => $data,
            'role' => $role,
        ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::table('users')->get();
        $role = DB::table('roles')->get();
        $fakultas= DB::table('fakultas')->get();

        return view('superadmin.user.create',[
            'data' => $data,
            'role' => $role,
            'fakultas'=> $fakultas,
        ]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            // 'fakultas_id' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        User::create(array(
            'name' => $request->get('name'),
            'username' => $request->get('username'),
            'fakultas_id' => $request->get('fakultas_id'),
            'email' => $request->get('email'),
            'no_hp' => $request->get('no_hp'),
            'password' => Hash::make($request->get('password'))
        ));

        $id = DB::getPdo()->lastInsertId();
        $user = User::findOrFail($id);
        $user->assignRole($request->role);
   
        return redirect()->route('superadmin.user.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $role = Role::all();
        $cur_role = $data->roles;
        $cur_role_id = 0;
        $fakultas = Fakultas::all();
        $cf= $data->fakultas_id;
       
      


         foreach ($cur_role as $cr) {
            $cur_role_id = $cr->id;
        }
       
        
        // $roles = Role::pluck('name','name')->all();
        //  $userRole = $user->roles->pluck('name','name');
        
        return view('superadmin.user.edit', compact('data', 'role', 'cur_role_id','fakultas','cf'));
        // return $roles;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            // 'fakultas' => 'required',
            'email' => 'required|email',
            // 'password' => 'required|min:6'

        ]);

        $user = User::findOrFail($id);

        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        $user->no_hp    = $request->no_hp;
        $user->fakultas_id = $request->fakultas_id;

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->update();
        $user->roles()->detach();

        $user->assignRole($request->get('roles'));
       
        return redirect()->route('superadmin.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        return redirect()->route('superadmin.user.index');
    }
}
