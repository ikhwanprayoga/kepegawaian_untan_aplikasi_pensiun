<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use Hash;
use DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $permission = DB::table('permissions')->select('id', 'name')->get();
        $rhp = DB::table('role_has_permissions')->select('permission_id', 'role_id')->get();
        $role = Role::all();
        return view('superadmin.permission.index', [
            'role'=> $role,
            'permission' => $permission,
            'rhp' => $rhp,
        ]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            // 'role' => 'required',
            'permission' => 'required'
        ]);

        // Role::create(['name' => $request->get('role')]);
        Permission::create(['name' => $request->get('permission')]);

        return redirect()->route('superadmin.permission.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $permission = Permission::findOrFail($id);
        // $permission = Permission::all();

        return view('superadmin.permission.edit', compact('permission'));
        // return redirect()->route('superadmin.permission.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $validatedData = $request->validate([
                'name' => 'required|max:255',
            
            ]
        );
            Permission::where('id', $id)->update($validatedData);
            
            $permission = Permission::findOrFail($id);
    
            return redirect()->route('superadmin.permission.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Permission::findOrFail($id);
        $role->delete();

        return redirect()->route('superadmin.permission.index');
        //
    }
}
