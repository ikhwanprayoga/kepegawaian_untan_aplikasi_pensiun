<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\Models\MasterBerkasPensiun;

class BerkasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataBerkasPensiun = MasterBerkasPensiun::all();

        return view('superadmin.berkas_pensiun.index', [
            'dataBerkasPensiun' => $dataBerkasPensiun,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.berkas_pensiun.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'judul' => 'required',
        ]);

        if (isset($request->pegawai_dapat_upload)) {
            $uploadBerkasPegawai = 1;
        } else {
            $uploadBerkasPegawai = 0;
        }

        DB::table('master_berkas_pensiun')->insert([
            'judul_berkas' => $request->get('judul'),
            'keterangan' => $request->get('keterangan'),
            'pegawai_dapat_upload' => $uploadBerkasPegawai,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        return redirect()->route('superadmin.berkas-pensiun.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('master_berkas_pensiun')->where('id', $id)->get();

        return view('superadmin.berkas_pensiun.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataBerkasPensiun = DB::table('master_berkas_pensiun')->where('id', $id)->get();

        return view('superadmin.berkas_pensiun.edit', compact('dataBerkasPensiun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'judul' => 'required',
        ],
        [
            'judul.required' => 'Judul berkas pensiun perlu di isi'
        ]);

        if (isset($request->pegawai_dapat_upload)) {
            $uploadBerkasPegawai = 1;
        } else {
            $uploadBerkasPegawai = 0;
        }

        DB::table('master_berkas_pensiun')->where('id', $id)->update([
            'judul_berkas' => $request->get('judul'),
            'keterangan' => $request->get('keterangan'),
            'pegawai_dapat_upload' => $uploadBerkasPegawai,
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        return redirect()->route('superadmin.berkas-pensiun.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataBerkasPensiun = DB::table('master_berkas_pensiun')->where('id', $id);
        $dataBerkasPensiun->delete();

        return redirect()->route('superadmin.berkas-pensiun.index');
    }
}
