<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role_id;
        $role_id_count;

        $role = Role ::with('permissions')->get();
        $permission = DB::table('permissions')->select('id', 'name')->get();
        $rhp = DB::table('role_has_permissions')->select('permission_id', 'role_id')->get();

        foreach ($role as $r) {
            $role_id[] = $r->id;
        }

        $role_id_count = count($role_id);

        // return $rhp;

        return view('superadmin.role.homeRole', [
            'role' => $role,
            'permission' => $permission,
            'rhp' => $rhp,
            'role_id' => $role_id,
            'ric' => $role_id_count,
        ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $role = DB::table('roles')->get();
        $permission = DB::table('permissions')->get();
        return view('superadmin.role.createRole',[
        'role' => $role,
        'permission' => $permission,
        ]);

        // return view('superadmin.role.createRole');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = DB::table('roles')->insert([
            'name'=> $request->get('role'),
            'guard_name'=>'web'
        ]);
       
       $id = DB::getPdo()->lastInsertId();
       $arrayCB = $_POST['cb'];
       $sz = sizeof($arrayCB);
       for ($i=0; $i < $sz; $i++) { 
        $rhpArray[$i] =[
        'role_id'=>$id,
          'permission_id'=>$arrayCB[$i]
        ];
       }
       DB::table('role_has_permissions')->insert($rhpArray);
        
        
        
        // $validateData = $request->validate([
        //     'role' => 'required',
            
        // ]);

        // Role::create(['name' => $request->get('role')]);
        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('superadmin.role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('id',$id)->with('permissions')->first();
        $permission = Permission::all();
        // $rhp = DB::table('role_has_permissions')->select('permission_id', 'role_id')->get();

        
        // $var[] = null;
        // foreach ($permission as $key) {
        //     if (!empty($key)) {
        //         $var[] = $key->permission_id;
        //     } else {
        //         $var = ['null'];
        //     }
        // }
        // $sz=count($permission);
        // foreach ($rhp as $key) {
        //     if (!empty($key)) {
        //         if ($id==$key->role_id) {
        //          $var[] = $key->permission_id;
        //         }
                
        //     } else {
        //         $var = ['null'];
        //     }
        // }

        // return $sz;

        return view('superadmin.role.edit', compact('role','permission'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        {
            $validatedData = $request->validate([
                'name' => 'required|max:255',
            
            ],
            [
               'name.required'=>'nama harus diisi' 
            ]
        );

        DB::table('roles')->where('id', $id)->update([
            'name' => $request->get('name'),
            
        ]);

            $arrayCB = $_POST['cb'];
            $sz = sizeof($arrayCB);
            for ($i=0; $i < $sz; $i++) { 
               $ep[$i] = [
                   'permission_id'=>$arrayCB[$i],
                   'role_id'=>$id,
               ];

            }
            $hp=DB::table('role_has_permissions')->where('role_id',$id);
            $hp->delete();
            DB::table('role_has_permissions')->insert($ep);
            
             $role = Role::findOrFail($id);
            // return $arrayCB;
              return redirect()->route('superadmin.role.index');
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('superadmin.role.index');
        //
    }

    
}
