<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Models\BerkasJenisPensiun;
use App\Models\JenisPensiun;
use App\Models\MasterBerkasPensiun;
use DB;

class JenisPensiunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataJenisPensiun = DB::table('jenis_pensiun')->select('id', 'nama', 'keterangan')->get();

        return view('superadmin.jenis_pensiun.index', [
            'dataJenisPensiun' => $dataJenisPensiun,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dataBerkasPensiun = DB::table('master_berkas_pensiun')->select('id', 'judul_berkas', 'keterangan')->get();
        return view('superadmin.jenis_pensiun.insert', [
            'dataBerkasPensiun' => $dataBerkasPensiun,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
        ]);

        DB::table('jenis_pensiun')->insert([
            'nama' => $request->get('nama'),
            'keterangan' => $request->get('keterangan'),
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        $id = DB::getPdo()->lastInsertId();

        $arrayCB = $_POST['cb'];
        $sz = sizeof($arrayCB);

        for ($i = 0; $i < $sz; $i++) { 
            $berkasJenisPensiunArray[$i] = [
                'jenis_pensiun_id' => $id,
                'master_berkas_pensiun_id' => $arrayCB[$i],
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        }

        DB::table('berkas_jenis_pensiun')->insert($berkasJenisPensiunArray);

        return redirect()->route('superadmin.jenis-pensiun.index');
        // return $berkasJenisPensiunArray;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = JenisPensiun::all()->where('id', $id);
        $dataBerkasJenisPensiun = BerkasJenisPensiun::all()->where('jenis_pensiun_id', $id);

        $var[] = null;

        foreach ($dataBerkasJenisPensiun as $key) {
            if (!empty($key)) {
                $var[] = $key->master_berkas_pensiun_id;
            } else {
                $var = ['null'];
            }
        }
        $varCount = count($var);

        $masterBerkasPensiunData = MasterBerkasPensiun::all();
        return view('superadmin.jenis_pensiun.detail', compact('data', 'dataBerkasJenisPensiun', 'masterBerkasPensiunData', 'dataId', 'var', 'varCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = JenisPensiun::all()->where('id', $id);
        $dataBerkasJenisPensiun = BerkasJenisPensiun::all()->where('jenis_pensiun_id', $id);

        $var[] = null;

        foreach ($dataBerkasJenisPensiun as $key) {
            if (!empty($key)) {
                $var[] = $key->master_berkas_pensiun_id;
            } else {
                $var = ['null'];
            }
        }
        $varCount = count($var);

        $masterBerkasPensiunData = MasterBerkasPensiun::all();
        // return $dataBerkasJenisPensiun;
        return view('superadmin.jenis_pensiun.edit', compact('data', 'dataBerkasJenisPensiun', 'masterBerkasPensiunData', 'dataId', 'var', 'varCount'));

        // return $var;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
        ],
        [
            'nama.required' => 'Nama jenis pensiun perlu di isi'
        ]
    );

        DB::table('jenis_pensiun')->where('id', $id)->update([
            'nama' => $request->get('nama'),
            'keterangan' => $request->get('keterangan'),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        $arrayCB = $_POST['cb'];
        $sz = sizeof($arrayCB);

        for ($i = 0; $i < $sz; $i++) { 
            $berkasJenisPensiunArray[$i] = [
                'jenis_pensiun_id' => $id,
                'master_berkas_pensiun_id' => $arrayCB[$i],
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        }

        $toBeExecute = DB::table('berkas_jenis_pensiun')->where('jenis_pensiun_id', $id);
        $toBeExecute->delete();

        DB::table('berkas_jenis_pensiun')->insert($berkasJenisPensiunArray);

        return redirect()->route('superadmin.jenis-pensiun.index');

        // return $berkasJenisPensiunArray;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataJenisPensiun = DB::table('jenis_pensiun')->where('id', $id);
        $dataBerkasJenisPensiun = DB::table('berkas_jenis_pensiun')->where('jenis_pensiun_id', $id);
        $dataJenisPensiun->delete();
        $dataBerkasJenisPensiun->delete();

        return redirect()->route('superadmin.jenis-pensiun.index');
    }
}
