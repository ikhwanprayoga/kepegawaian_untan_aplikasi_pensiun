<?php

namespace App\Http\Controllers\superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UsulanPensiun;
use App\Models\Fakultas;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use DB;

class BerandaController extends Controller
{
    public function index()
    {
     $fakultas = Fakultas::with('usulanPensiun')->get();
     $usulan =  UsulanPensiun::all();
     $role = Role::with('Users')->get();
     $user = User::all();
     
    //  $user= User::all();
  
       
       return view('superadmin.beranda.index', [
        'fakultas' => $fakultas,
        'usulan' => $usulan,
        'role' => $role,
        'user'=>$user,
        
        // 'user'=>$user,
    ]);
    }
}
