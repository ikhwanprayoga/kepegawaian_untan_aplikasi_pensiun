<?php

namespace App\Http\Controllers\kepala_kepegawaian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\UsulanPensiun;

class TraceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function traceusulan($id)
    {
        $usulan = UsulanPensiun::where('id',$id)->first();
        $tabellog = UsulanPensiun::where('id', $id)-> with('LogUsulanPensiun')->first();
        return view ('kepala_kepegawaian.traceusulan.traceusulan',compact('usulan','tabellog'));
    }
}
