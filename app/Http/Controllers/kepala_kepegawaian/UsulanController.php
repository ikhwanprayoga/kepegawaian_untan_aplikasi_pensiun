<?php

namespace App\Http\Controllers\kepala_kepegawaian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\UsulanPensiun;

class UsulanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $fakultasId = auth()->user()->fakultas->id;
        $usulans = UsulanPensiun::orderBy('id', 'desc')->get();

        return view('kepala_kepegawaian.usulan.index', compact('usulans'));
    }
}
