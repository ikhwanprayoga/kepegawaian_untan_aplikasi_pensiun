<?php

namespace App\Http\Controllers\kepegawaian_untan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Storage;
use Alert;

use App\Models\Dosen;
use App\Models\UsulanPensiun;
use App\Models\UpdatePegawai;
use App\Models\UpdateKeluarga;
use App\Models\UpdateAnak;
use App\Models\BerkasStepAkhir;
use App\Models\LogUsulanPensiun;

class DpcpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($nip)
    {
       $dataPegawai = UpdatePegawai::where('NIP', $nip)->first();

        if (isset($dataPegawai)) {
            $pegawai = $dataPegawai;
        } else {
            $pegawai = Dosen::where('NIP', $nip)->first();
        }

        $keluarga = UpdateKeluarga::where('NIP', $nip)->get();
        $dataAnak = UpdateAnak::where('NIP', $nip)->get();
        $tgl = date('d-m-Y');

        return view('kepegawaian_untan.usulan.dpcp', compact('pegawai','keluarga','dataAnak','tgl'));

    }

    public function cetak($nip)
    {
        $dataPegawai = UpdatePegawai::where('NIP', $nip)->first();

        if (isset($dataPegawai)) {
            $pegawai = $dataPegawai;
        } else {
            $pegawai = Dosen::where('NIP', $nip)->first();
        }

        $pdf = PDF::loadView('kepegawaian_untan.usulan.dpcp',['pegawai'=>$pegawai]);
        return $pdf->stream();
        return $pdf->download('dpcp_'.$nip.'.pdf');
    }
    
    public function upload(Request $request, $usulan_pensiun_id)
    {
        $berkas_dpcp = $request->berkas_dpcp;
        $usulan_pensiun = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
        $cek_berkas = BerkasStepAkhir::where('usulan_pensiun_id', $usulan_pensiun_id)->first();

        if (empty($cek_berkas->file_dpcp)) {
            //upload baru dpcp
            $jenis_berkas = 'berkas_dpcp';
            $file_dpcp = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas_dpcp);

            $simpan_dpcp = BerkasStepAkhir::create([
                'usulan_pensiun_id' => $usulan_pensiun->id,
                'file_dpcp' => $file_dpcp,
            ]);

            $logUsulanPensiun = $this->logUsulan($usulan_pensiun->id, 'Operator Kepegawaian Untan mengupload berkas DPCP');

        } else {
            //update file dpcp
            $hapus_berkas = $this->delete_file($usulan_pensiun->id, $cek_berkas->file_dpcp);

            //upload baru dpcp
            $jenis_berkas = 'berkas_dpcp';
            $file_dpcp = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas_dpcp);

            $update_dpcp = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_dpcp' => $file_dpcp,
            ]);
        }

        Alert::success('Berkas DPCP berhasil di upload!');
        return redirect()->back();
        
    }

    public function sp4_upload(Request $request, $usulan_pensiun_id)
    {
        $berkas_dpcp = $request->berkas_sp4;
        $usulan_pensiun = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
        $cek_berkas = BerkasStepAkhir::where('usulan_pensiun_id', $usulan_pensiun_id)->first();

        if (empty($cek_berkas->file_sp4)) {
            //upload baru dpcp
            $jenis_berkas = 'berkas_sp4';
            $file_sp4 = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas_dpcp);

            $simpan_dpcp = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_sp4' => $file_sp4,
            ]);

            $logUsulanPensiun = $this->logUsulan($usulan_pensiun->id, 'Operator Kepegawaian Untan mengupload berkas DPCP');

        } else {
            //update file dpcp
            $hapus_berkas = $this->delete_file($usulan_pensiun->id, $cek_berkas->file_dpcp);

            //upload baru dpcp
            $jenis_berkas = 'berkas_sp4';
            $file_sp4 = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas_dpcp);

            $update_dpcp = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_sp4' => $file_sp4,
            ]);
        }

        Alert::success('Berkas SP4 berhasil di upload!');
        return redirect()->back();
    }

    public function surat_pengantar_upload(Request $request, $usulan_pensiun_id)
    {
        $berkas = $request->berkas_surat_pengantar;
        $usulan_pensiun = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
        $cek_berkas = BerkasStepAkhir::where('usulan_pensiun_id', $usulan_pensiun_id)->first();

        if (empty($cek_berkas->file_surat_pengantar)) {
            //upload baru dpcp
            $jenis_berkas = 'berkas_surat_pengantar';
            $file_surat_pengantar = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas);

            $simpan = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_surat_pengantar' => $file_surat_pengantar,
            ]);

            $logUsulanPensiun = $this->logUsulan($usulan_pensiun->id, 'Operator Kepegawaian Untan mengupload berkas DPCP');

        } else {
            //update file dpcp
            $hapus_berkas = $this->delete_file($usulan_pensiun->id, $cek_berkas->file_dpcp);

            //upload baru dpcp
            $jenis_berkas = 'berkas_surat_pengantar';
            $file_surat_pengantar = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas);

            $update = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_surat_pengantar' => $file_surat_pengantar,
            ]);
        }

        Alert::success('Berkas Surat Pengantar berhasil di upload!');
        return redirect()->back();
    }
    
    public function sk_upload(Request $request, $usulan_pensiun_id)
    {
        $berkas_sk = $request->sk_pensiun;
        $usulan_pensiun = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
        $cek_berkas = BerkasStepAkhir::where('usulan_pensiun_id', $usulan_pensiun_id)->first();

        if (empty($cek_berkas->file_sk)) {
            //upload baru sk
            $jenis_berkas = 'berkas_sk_pensiun';
            $file_sk = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas_sk);

            $simpan_sk = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_sk' => $file_sk,
            ]);

            $logUsulanPensiun = $this->logUsulan($usulan_pensiun->id, 'Operator Kepegawaian Untan mengupload berkas SK Pensiun');

        } else {
            //update file sk
            $hapus_berkas = $this->delete_file($usulan_pensiun->id, $cek_berkas->file_sk);

            //upload baru sk
            $jenis_berkas = 'berkas_sk_pensiun';
            $file_sk = $this->save_file($usulan_pensiun->nip, $jenis_berkas, $berkas_sk);

            $update_sk = BerkasStepAkhir::where('id', $cek_berkas->id)->update([
                'file_sk' => $file_sk,
            ]);
        }

        Alert::success('Berkas SK Pensiun berhasil di upload!');
        return redirect()->back();
        
    }

    //private function
    private function save_file($nip, $jenis_berkas, $file)
    {
        $usulan         = UsulanPensiun::where('nip', $nip)->first();
        
        $nama_berkas    = $jenis_berkas;
        $fakultas       = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);

        $name = $nama_berkas.'_'.md5(date('Ymdhis')).'.'.$file->getClientOriginalName();
        $filename = str_replace(str_split('\\/:*?"<>|'), '_', $name);
        Storage::putFileAs('public/pensiun/'.$fakultas.'/'.$usulan->nip, $file, $filename);

        return $filename;

        // $name = $nip.'_'.md5(date('Ymdhis')).'_'.$file->getClientOriginalName();
        // $filename = str_replace(' ', '_', $name);
        // Storage::putFileAs('public/berkas_akhir/', $file, $filename);
        // return $filename;

    }

    private function delete_file($usulan_id, $file)
    {
        if (isset($file)) {
            $usulan = UsulanPensiun::where('id', $usulan_id)->first();
            $fakultas = str_replace(' ', '_', $usulan->fakultas->nama_fakultas);
            Storage::delete('public/pensiun/'.$fakultas.'/'.$usulan->nip.'/'.$file);
        } else {
            return 'file does not exist';
        }

    }

    private function logUsulan($usulan_pensiun_id, $nama)
    {
        $logUsulanPensiun = LogUsulanPensiun::create([
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nama' => $nama,
        ]);
        return true;
    }
}
