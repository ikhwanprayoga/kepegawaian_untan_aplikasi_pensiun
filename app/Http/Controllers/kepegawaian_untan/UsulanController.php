<?php

namespace App\Http\Controllers\kepegawaian_untan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Alert;
use App\Kirim;
use App\User;

use App\Models\UsulanPensiun;
use App\Models\LogUsulanPensiun;
use App\Models\BerkasUsulanPensiun;
use App\Models\Dosen;
use App\Models\Notifikasi;
use App\Models\UpdateKeluarga;
use App\Models\UpdateAnak;
use App\Models\UpdatePegawai;
use App\Models\NotifikasiWaEmail;
use App\Models\CatatanUsulan;
use App\Models\IsiCatatanUsulan;

class UsulanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $fakultasId = auth()->user()->fakultas->id;
        $usulans = UsulanPensiun::whereIn('status_usulan_kepeg_untan', [1,2,3,5])
                                // ->where('fakultas_id', $fakultasId)
                                ->orderBy('id', 'desc')
                                ->get();

        return view('kepegawaian_untan.usulan.index', compact('usulans'));
    }

    public function kelola_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->with('jenisPensiun', 'berkasStepAkhir')->first();
        $nip = $usulan->nip;
        $dataDosen = UpdatePegawai::where('NIP', $nip)->get();
        if ($dataDosen->count() > 0) {
            $foto = 1;
            $dosen = $dataDosen->first();
        } else {
            $foto = 0;
            $dosen = DB::table('dosen')->where('NIP', $nip)->first();
        }
        
        // return $dosen->NAMA_LENGKAP;
        return view('kepegawaian_untan.usulan.kelola_usulan', compact('usulan', 'dosen', 'foto'));
    }

    public function verifikasi_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $status_usulan_fakultas = $usulan->status_usulan_kepeg_fakultas;
        $status_usulan_untan = $usulan->status_usulan_kepeg_untan;

        $berkasJenisPensiun = $usulan->jenisPensiun->berkasJenisPensiun;
        $berkasUpload = $usulan->berkasUsulanPensiun;

        if ($berkasUpload->where('status_verifikasi_kepeg_untan', 1)->count() < $berkasJenisPensiun->count()) {
            return response()->json([
                'status' => 0,
                'pesan' => 'Terdapat Berkas Yang Belum Diverifikasi!'
            ]);
        } else {
            if ($status_usulan_fakultas == 2) {
                //boleh di verifikasi
                if ($status_usulan_untan == 1) {
    
                    //verifikasi usulan
                    $verifikasi = $usulan->update([
                        'status_usulan_kepeg_untan' => 2,
                    ]);
    
                    $link = route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]);
    
    
                    $notifikasi = Notifikasi::create([
                        'usulan_pensiun_id' => $usulan->id,
                        'dari_users_id' => auth()->user()->id,
                        // 'untuk_users_id',
                        'fakultas_id' => $usulan->fakultas_id,
                        'nip_dosen' => $usulan->dosen->NIP,
                        'pesan' => 'Usulan '.$usulan->dataPegawai->NAMA_LENGKAP.' telah di verifikasi oleh kepegawaian UNTAN dan berkas DPCP dan SP4 dapat cetak.',
                        'status' => 0,
                        'link' => $link,
                        'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
                        'waktu_notifikasi_dibaca' => \Carbon\Carbon::now(),
                    ]);

                    //kirim notifikasi WA dan Email
                    // kirimNotifikasi($pesan, $role, $usulan_pensiun_id)
                    $pesan = 'Usulan '.$usulan->dataPegawai->NAMA_LENGKAP.' telah di verifikasi oleh kepegawaian UNTAN dan berkas DPCP dan SP4 dapat cetak.';
                    $kirimNotif = $this->kirimNotifikasi($pesan, 'operator untan', $usulan->id);
                    
                    //catat log
                    $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator kepegawaian untan memverifikasi usulan');
    
                    return response()->json([
                        'status' => 1,
                        'pesan' => 'Usulan Berhasil Diverifikasi!'
                    ]);
                } else if ($status_usulan_untan == 2) {
                    //usulan telah diverfikasi
                    return response()->json([
                        'status' => 0,
                        'pesan' => 'Usulan Telah Diverifikasi Sebelumnya!'
                    ]);
                } elseif ($status_usulan_untan == 3) {
                    //usulan telah selesai
                    return response()->json([
                        'status' => 0,
                        'pesan' => 'SK Usulan Telah Diterbitkan!'
                    ]);
                } else {
                    //somethings error
                    return response()->json([
                        'status' => 0,
                        'pesan' => 'Ada Kesalah Dengan Usulan!'
                    ]);
                }
            } else {
                //usulan blm selesai dari opertor fakultas
                return response()->json([
                        'status' => 0,
                        'pesan' => 'Ada Kesalah Dengan Usulan!'
                    ]);
            }
        }
        

    }

    public function kirim_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $status_usulan_fakultas = $usulan->status_usulan_kepeg_fakultas;
        $status_usulan_untan = $usulan->status_usulan_kepeg_untan;

        if ($status_usulan_fakultas == 2) {
            //boleh di verifikasi
            if ($status_usulan_untan == 2) {

                //verifikasi usulan
                $verifikasi = $usulan->update([
                    'status_usulan_kepeg_untan' => 3,
                ]);

                $link = route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]);


                $notifikasi = Notifikasi::create([
                    'usulan_pensiun_id' => $usulan->id,
                    'dari_users_id' => auth()->user()->id,
                    // 'untuk_users_id',
                    'fakultas_id' => $usulan->fakultas_id,
                    'nip_dosen' => $usulan->dosen->NIP,
                    'pesan' => 'Usulan '.$usulan->dataPegawai->NAMA_LENGKAP.' telah di kirim ke Dikti oleh kepegawaian UNTAN.',
                    'status' => 0,
                    'link' => $link,
                    'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
                    'waktu_notifikasi_dibaca' => \Carbon\Carbon::now(),
                ]);

                //kirim notifikasi WA dan Email
                // kirimNotifikasi($pesan, $role, $usulan_pensiun_id)
                $pesan = 'Kepegawaian UNTAN \nUsulan '.$usulan->dataPegawai->NAMA_LENGKAP.' telah di kirim ke Dikti oleh kepegawaian UNTAN. Tunggu pemberitahuan selanjutnya ketika SK sudah Terbit.';
                $kirimNotif = $this->kirimNotifikasi($pesan, 'operator untan', $usulan->id);
                
                //kirim notifikasi ke dosen
                $pesan2 = 'Kepegawaian UNTAN \nUsulan pensiun atas nama anda '.$usulan->dataPegawai->NAMA_LENGKAP.' telah di teruskan atau dikirim ke Dikti oleh kepegawaian UNTAN.';
                Kirim::wa($usulan->dataPegawai->no_hp, $pesan2);

                //catat log
                $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator kepegawaian untan mengirim usulan ke Dikti');

                return 1;
            } else if ($status_usulan_untan == 1) {
                //usulan telah diverfikasi
                return 'not verified';
            } elseif ($status_usulan_untan == 3) {
                //usulan telah selesai
                return 'sended';
            } else {
                //somethings error
                return 'error';
            }
        } else {
            //usulan blm selesai dari opertor fakultas
            return 'error';
        }
    }

    public function kirim_notif($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $pesan = 'Kepegawaian UNTAN \nTerdapat catatan untuk usulan atas nama '.$usulan->dataPegawai->NAMA_LENGKAP.' \nDiharapkan Kepegawaian Fakultas untuk segera melihat atau merevisi usulan tersebut.';
        $kirimNotif = $this->kirimNotifikasi($pesan, 'operator untan', $usulan->id);
        
        return response()->json([
            'status' => 1,
            'pesan' => 'Notifikasi WhatsApp berhasil dikirim ke Operator Kepegawaian fakultas!.'
        ]);
        
    }

    public function sk_terbit_usulan($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->first();
        $status_usulan_fakultas = $usulan->status_usulan_kepeg_fakultas;
        $status_usulan_untan = $usulan->status_usulan_kepeg_untan;

        if ($status_usulan_fakultas == 2) {
            //boleh di verifikasi
            if ($status_usulan_untan == 3) {

                //verifikasi usulan
                $verifikasi = $usulan->update([
                    'status_usulan_kepeg_untan' => 4,
                ]);

                $link = route('kepegawaian-fakultas.usulan.terbit.kelola', ['id' => $usulan->id]);


                $notifikasi = Notifikasi::create([
                    'usulan_pensiun_id' => $usulan->id,
                    'dari_users_id' => auth()->user()->id,
                    // 'untuk_users_id',
                    'fakultas_id' => $usulan->fakultas_id,
                    'nip_dosen' => $usulan->dosen->NIP,
                    'pesan' => 'SK Usulan '.$usulan->dataPegawai->NAMA_LENGKAP.' telah Terbit.',
                    'status' => 0,
                    'link' => $link,
                    'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
                    'waktu_notifikasi_dibaca' => \Carbon\Carbon::now(),
                ]);

                //kirim notifikasi WA dan Email
                // kirimNotifikasi($pesan, $role, $usulan_pensiun_id)
                $pesan = 'Kepegawaian UNTAN \nSK Usulan '.$usulan->dataPegawai->NAMA_LENGKAP.' telah di Terbit. Untuk Informasi selanjutnya silahkan hubungi Kepegawaian UNTAN.';
                $kirimNotif = $this->kirimNotifikasi($pesan, 'operator untan', $usulan->id);

                //kirim notifikasi ke dosen
                $pesan2 = 'Kepegawaian UNTAN \nSK Usulan pensiun atas nama anda '.$usulan->dataPegawai->NAMA_LENGKAP.' telah Terbit, Anda dapat menghubungi kepegawaian UNTAN atau kepegawaian Fakultas untuk informasi lebih lanjut.';
                Kirim::wa($usulan->dataPegawai->no_hp, $pesan2);

                //catat log
                $logUsulanPensiun = $this->logUsulan($usulan->id, 'Sk usulan telah terbit');

                return 1;
            } else if ($status_usulan_untan == 1) {
                //usulan telah diverfikasi
                return 'not verified';
            } elseif ($status_usulan_untan == 3) {
                //usulan telah selesai
                return 'sended';
            } else {
                //somethings error
                return 'error';
            }
        } else {
            //usulan blm selesai dari opertor fakultas
            return 'error';
        }
    }

    public function kelola_usulan_berkas($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->with('jenisPensiun', 'berkasUsulanPensiun')->first();
        
        foreach ($usulan->berkasUsulanPensiun as $berkasUsul) {
            $t[] = $berkasUsul->catatanBerkas->where('berkas_usulan_pensiun_id', $berkasUsul->id)->where('status_kepeg_fakultas', 0)->count();
        }
        $cekCatatan=  array_sum($t);
        
        return view('kepegawaian_untan.usulan.kelola_usulan_berkas', compact('usulan', 'cekCatatan'));
    }

    public function verifikasi_berkas($id)
    {
        $data = BerkasUsulanPensiun::where('id', $id)->first();
        $nama_berkas = $data->masterBerkasPensiun->judul_berkas;

        if ($data->status_verifikasi_kepeg_untan == 0) {

            $catatanBerkas = $data->catatanBerkas;

            if ($catatanBerkas->where('status_kepeg_fakultas', 0)->count() > 0) {
                //ada catatan berkas yg blm selesai
                return response()->json([
                    'status' => 0,
                    'pesan' => 'Terdapat Catatan Berkas Yang Belum Selesai!'
                ]);
            } else {
                //catatan berkas sudah selesai
                $verified = $data->update([
                    'status_verifikasi_kepeg_untan' => 1
                ]);
    
                $logUsulanPensiun = $this->logUsulan($data->usulan_pensiun_id, $nama_berkas.' telah di verfikasi oleh operator Untan');
                // return 1;
                return response()->json([
                    'status' => 1,
                    'pesan' => 'Berkas Berhasil Diverfikasi!'
                ]);
            }



        } elseif ($data->status_verifikasi_kepeg_untan == 1) {

            $verified = $data->update([
                'status_verifikasi_kepeg_untan' => 0
            ]);

            return response()->json([
                'status' => 2,
                'pesan' => 'Berkas Berhasil Tidak Diverifikasi!'
            ]);

        } else {
            return response()->json([
                'status' => 0,
                'pesan' => 'Ada Kesalahan Pada Status Berkas!'
            ]);
        }
    }

    public function lihat_data_pegawai($id, $nip)
    {
        $data = UpdatePegawai::where('NIP', $nip)->get();
        $usulan = UsulanPensiun::where([['id', $id],['nip', $nip]])->first();

        if ($data->count() > 0) {
            $foto = 1;
            $pegawai = $data->first();;
        } else {
            $foto = 0;
            $pegawai = Dosen::where('NIP', $nip)->first();
        }

        $dataKeluarga = UpdateKeluarga::where('NIP', $nip)->get();

        if ($dataKeluarga->count() > 0) {
            $keluarga = $dataKeluarga;
        } else {
            $keluarga = [];
        }
        
        $dataAnak = UpdateAnak::where('NIP', $nip)->get();

        if ($dataAnak->count() > 0) {
            $anak = $dataAnak;
        } else {
            $anak = [];
        }

        return view('kepegawaian_untan.usulan.show',compact('pegawai', 'usulan', 'keluarga', 'anak', 'foto'));
    }

    public function revisiUsulan($nip)
    {
        $usulan = UsulanPensiun::where('nip', $nip)->with('dosen', 'fakultas')->first();

        return view('kepegawaian_untan.usulan.revisi_usulan', compact('usulan'));

        // return $nip;
    }

    public function revisiUsulan_send(Request $request)
    {
        $validatedData = $request->validate([
                'catatanRevisi' => 'required',
            ],
            [
                'catatanRevisi.required' => 'Masukan Catatan Revisi'
            ]
        );

        $usulan = UsulanPensiun::where('id', $request->upi)->first();

        $revisi = Revisi::create([
            'usulan_pensiun_id' => $request->upi,
            'catatan_revisi' => $request->catatanRevisi,
            'status' => $request->status,
        ]);

        $link = route('kepegawaian-fakultas.usulan.kelola.berkas', ['id' => $usulan->id]);

        $notifikasi = Notifikasi::create([
            'usulan_pensiun_id' => $usulan->id,
            'dari_users_id' => auth()->user()->id,
            // 'untuk_users_id',
            'fakultas_id' => $usulan->fakultas_id,
            'nip_dosen' => $usulan->dosen->NIP,
            'pesan' => 'Terdapat revisi untuk usulan dengan kode usulan '.$usulan->kode_usulan,
            'status' => 0,
            'link' =>$link,
            'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
            'waktu_notifikasi_dibaca' => \Carbon\Carbon::now(),
        ]);

        //catat log
        $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator kepegawaian untan memberikan revisi untuk usulan '.$usulan->kode_usulan);

        Alert::success('Submit revisi berhasil');
        return redirect()->route('kepegawaian-untan.usulan.index');
    }

    public function kirim_resi(Request $request, $usulan_id)
    {
        $noResi = $request->no_resi;
        $tanggal = $request->tanggal;
        $jasa_pengiriman = $request->jasa_pengiriman;
        $data = UsulanPensiun::where('id', $usulan_id)->first();

        $data->update([
            'no_resi_pengiriman_berkas' => $noResi,
            'tanggal_kirim_berkas' => $tanggal,
            'jasa_pengiriman' => $jasa_pengiriman,
        ]);

        Alert::success('Nomor Resi Pengiriman Berhasil Disimpan!');
        return redirect()->back();
        
    }

    public function kembalikan_usulan(Request $request, $usulan_id)
    {
        $validatedData = $request->validate(
            [
                'catatanUsulan' => 'required'
            ],
            [
                'catatanUsulan.required' => 'Catatan Usulan Tidak Boleh Kosong!'
            ]
        );

        $usulan = UsulanPensiun::where('id', $usulan_id)->first();
        
        $catatanUsulan = CatatanUsulan::create([
            'usulan_pensiun_id' => $usulan->id,
            'isi_catatan'       => $request->catatanUsulan,
            'status'            => 0
        ]);

        $usulan->update([
            'status_usulan_kepeg_fakultas' => 0,
            'status_usulan_kepeg_untan' => 5
        ]);

        $link = route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]);

        $notifikasi = Notifikasi::create([
            'usulan_pensiun_id' => $usulan->id,
            'dari_users_id' => auth()->user()->id,
            // 'untuk_users_id',
            'fakultas_id' => $usulan->fakultas_id,
            'nip_dosen' => $usulan->dosen->NIP,
            'pesan' => 'Terdapat usulan yang dikembalikan oleh operator Kepegawaian Untan dengan kode usulan '.$usulan->kode_usulan,
            'status' => 0,
            'link' =>$link,
            'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
            'waktu_notifikasi_dibaca' => \Carbon\Carbon::now(),
        ]);

        //catat log
        $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator kepegawaian Untan mengembalikan usulan ke operator Kepegawaian fakultas');

        Alert::success('Usulan Berhasil Dikembalikan!');
        return redirect()->back();
    }

    public function tambah_catatan_baru(Request $request, $usulanId)
    {
        $validatedData = $request->validate(
            [
                'catatanUsulan' => 'required'
            ],
            [
                'catatanUsulan.required' => 'Catatan Usulan Tidak Boleh Kosong!'
            ]
        );
        
        $catatanUsulan = CatatanUsulan::create([
            'usulan_pensiun_id' => $usulanId,
            'isi_catatan'       => $request->catatanUsulan,
            'status'            => 0
        ]);

        //catat log
        $logUsulanPensiun = $this->logUsulan($usulanId, 'Operator kepegawaian Untan menambah catatan baru pengembalikan usulan ke operator Kepegawaian fakultas');

        Alert::success('Catatan Baru Berhasil Dikirim!');
        return redirect()->back();
    }

    public function lihat_catatan($usulanId)
    {
        $usulan = UsulanPensiun::with('lihatCatatan')->find($usulanId);

        $data = [
            'status' => 1,
            'data' => $usulan,
            'pesan' => 'Catatan Ditemukan!',
        ];

        return response()->json($data, 200);
    }

    public function batal_verifikasi($usulan_id)
    {
        $usulan = UsulanPensiun::find($usulan_id);

        if ($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 2) {
            $usulan->update([
                'status_usulan_kepeg_untan' => 1
            ]);

            return response()->json([
                'status' => 1,
                'pesan'  => 'Pembatalan Verifikasi Usulan Berhasil!'   
            ], 200);

        } else {

            return response()->json([
                'status' => 0,
                'pesan'  => 'Proses Pembatalan Verifikasi Usulan Gagal!'   
            ], 200);

        }
    }

    // private function
    private function logUsulan($usulan_pensiun_id, $nama)
    {
        $logUsulanPensiun = LogUsulanPensiun::create([
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nama' => $nama,
        ]);
        return true;
    }

    private function kirimNotifikasi($pesan, $role, $usulan_pensiun_id)
    {
        $usulan = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
        $fakultas_id = $usulan->fakultas_id;
        $nip = $usulan->nip;

        if ($role == 'operator untan') {
            $untuk_user_id  = $this->idOperator($fakultas_id, 'operator fakultas');
            $dari_user_id   = $this->idOperator($fakultas_id, 'operator untan');
            $email          = $this->emailOperator($fakultas_id, 'operator fakultas');
            $no_hp          = $this->noHpOperator($fakultas_id, 'operator fakultas');
        } else {
            $untuk_user_id  = $this->idOperator($fakultas_id, 'operator untan');
            $dari_user_id   = $this->idOperator($fakultas_id, 'operator fakultas');
            $email          = $this->emailOperator($fakultas_id, 'operator untan');
            $no_hp          = $this->noHpOperator($fakultas_id, 'operator untan');
        }
        
        $data = NotifikasiWaEmail::create([
            'pesan' => $pesan,
            'waktu_kirim' => \Carbon\Carbon::now(),
            'email' => $email,
            'no_hp' => $no_hp,
            'untuk_user_id' => $untuk_user_id, 
            'dari_user_id' => $dari_user_id,
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nip' => $nip,
        ]);
        
        //kirim::wa(tujuan , pesan)
        $kirimWa = Kirim::wa($no_hp, $pesan);
        
        //kirim email

    }

    private function noHpOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->no_hp;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    private function emailOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->email;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    private function idOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->id;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    
}
