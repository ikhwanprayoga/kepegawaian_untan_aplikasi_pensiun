<?php

namespace App\Http\Controllers\kepegawaian_untan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\Models\UsulanPensiun;
use App\Models\UpdatePegawai;

class UsulanTerbitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $fakultasId = auth()->user()->fakultas->id;
        $usulans = UsulanPensiun::where('status_usulan_kepeg_untan', 4)
                            // ->where('fakultas_id', $fakultasId)
                            ->orderBy('id', 'desc')
                            ->get();

        return view('kepegawaian_untan.usulan_terbit.index', compact('usulans'));
    }

    public function kelola($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->with('jenisPensiun', 'berkasStepAkhir')->first();
        $nip = $usulan->nip;
        $dataDosen = UpdatePegawai::where('NIP', $nip)->get();
        if ($dataDosen->count() > 0) {
            $foto = 1;
            $dosen = $dataDosen->first();
        } else {
            $foto = 0;
            $dosen = DB::table('dosen')->where('NIP', $nip)->first();
        }
        
        // return $dosen->NAMA_LENGKAP;
        return view('kepegawaian_untan.usulan_terbit.kelola_usulan', compact('usulan', 'dosen', 'foto'));
    }

    public function kelola_usulan_berkas($id)
    {
        $usulan = UsulanPensiun::where('id', $id)->with('jenisPensiun', 'berkasUsulanPensiun')->first();
        
        return view('kepegawaian_untan.usulan_terbit.kelola_usulan_berkas', compact('usulan'));
    }
}
