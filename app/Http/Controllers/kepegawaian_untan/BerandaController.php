<?php

namespace App\Http\Controllers\kepegawaian_untan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UsulanPensiun;
use App\Models\NotifikasiPemberitahuanPengajuanPensiun;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $fakultasId = auth()->user()->fakultas->id;
        $usulans = UsulanPensiun::where('fakultas_id', $fakultasId)->orderBy('id', 'desc')->get();

        $tahunPensiun1 = date('Y') + 1;
        $tahunPensiun2 = $tahunPensiun1 + 1;
        $unitKerja = auth()->user()->fakultas->nama_fakultas;
        $dosenPensiun = NotifikasiPemberitahuanPengajuanPensiun::with(['dataDosen'])
                                                        ->whereHas('dataDosen', function($q) use ($unitKerja) {
                                                            $q->where('Unit_Kerja', $unitKerja);
                                                        })
                                                        ->whereIn('tahun_pensiun', [$tahunPensiun1, $tahunPensiun2])
                                                        ->get();
        // return $dat;

        if ($usulans->count() > 0) {
            foreach ($usulans as $key => $value) {
                $tahun[] = $value->created_at->format('Y');
            }
        } else {
            $date = date('Y');
            $tahun = [$date];
        }

        foreach ($tahun as $key => $value) {
            $usulanc[$value] = UsulanPensiun::where('fakultas_id', $fakultasId)->orderBy('id', 'desc')->whereYear('created_at', $value)->count();
        }

        $y = array_unique($tahun);
        $usulan = implode(',', $usulanc);
        $year = implode(',', $y);
        // $usulandiproses = UsulanPensiun::where('fakultas_id', $fakultasId)->whereIn('status_usulan_kepeg_untan', [1,2,3])->count();
        // $skUsulanTerbit = UsulanPensiun::where('fakultas_id', $fakultasId)->where('status_usulan_kepeg_untan',4)->count();
        $usulandiproses = UsulanPensiun::whereIn('status_usulan_kepeg_untan', [1,2,3])->count();
        $skUsulanTerbit = UsulanPensiun::where('status_usulan_kepeg_untan',4)->count();

        return view('kepegawaian_untan.beranda.index', compact( 'year', 'dosenPensiun', 'usulan','usulandiproses', 'skUsulanTerbit'));
    }
}
