<?php

namespace App\Http\Controllers\kepegawaian_untan;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UsulanPensiun;
Use App\Models\LogUsulanPensiun;
use Psy\Command\WhereamiCommand;

class TraceUsulanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usulans = UsulanPensiun::whereIn('status_usulan_kepeg_untan', [1,2])->with('revisi')->orderBy('id', 'desc')->get();
        return view ('kepegawaian_untan.traceusulan.index', compact('usulans'));
    }

    public function trackingusulan($id)
    {
        $logusulan = UsulanPensiun::where('id', $id)-> with('LogUsulanPensiun')->first();
        return view ('kepegawaian_untan.traceusulan.tracking_usulan', compact ('logusulan'));
    }

    public function traceusulan($id)
    {
        $usulan = UsulanPensiun::where('id',$id)->first();
        $tabellog = UsulanPensiun::where('id', $id)-> with('LogUsulanPensiun')->first();
        return view ('kepegawaian_untan.traceusulan.traceusulan',compact('usulan','tabellog'));
    }

    public function traceindex()
    {
        $usulans = UsulanPensiun::whereIn('status_usulan_kepeg_untan', [1,2])->with('revisi')->orderBy('id', 'desc')->get();
        return view ('kepegawaian_untan.traceusulan.trace_index', compact('usulans'));
    }
}



