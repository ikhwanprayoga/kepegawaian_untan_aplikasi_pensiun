<?php

namespace App\Http\Controllers\kepegawaian_untan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
// use App\User;

use App\Models\LihatProfil as Profil;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('kepegawaian_untan.lihatprofil.kelolaprofil');
    }

    public function updateProfil($id, Request $request)
    {
        $this->validate($request, [
            'name'                  =>'required',
            'email'                 =>'required',
            'username'              =>'required',
            'password'              => 'confirmed',
        ]);

        $user = Profil::find($id);

        if($request->password != ""){
            $user->password = \Hash::make($request->password);
        }
        $user->username = $request->username;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->no_hp    = $request->no_hp;
        
        $user->update();
        
        Alert::success('Data berhasil di perbaharui'); 
        return redirect('/kepegawaian-untan/kelolaprofil');
    }

}

