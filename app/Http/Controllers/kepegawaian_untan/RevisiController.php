<?php

namespace App\Http\Controllers\kepegawaian_untan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
use DB;

use App\Models\CatatanBerkas;
use App\Models\Notifikasi;
use App\Models\LogUsulanPensiun;
use App\Models\BerkasUsulanPensiun;
use App\Models\UsulanPensiun;

class RevisiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendRevision(Request $request)
    {
        // return $request->all();
        $validatedData = $request->validate([
            'catatanRevisi' => 'required',
        ], [
            'catatanRevisi.required' => 'Catatan revisi tidak boleh kosong!',
        ]);

        $catatan_berkas = CatatanBerkas::create([
            'berkas_usulan_pensiun_id' => $request->get('berkas_usulan_pensiun_id'),
            'catatan' => $request->get('catatanRevisi'),
            'status_kepeg_fakultas' => 0,
        ]);

        $statusBerkasFakultas = BerkasUsulanPensiun::where('id', $request->berkas_usulan_pensiun_id)->update([
            'status_verifikasi_kepeg_fakultas' => 0,
            'status_verifikasi_kepeg_untan' => 0,
        ]);

        $usulan = $catatan_berkas->berkasUsulanPensiun->usulanPensiun;
        $berkas = $catatan_berkas->berkasUsulanPensiun->masterBerkasPensiun;
        $link = route('kepegawaian-fakultas.usulan.kelola.berkas', ['id' => $usulan->id]);

        $notifikasi = Notifikasi::create([
            'usulan_pensiun_id' => $usulan->id,
            'dari_users_id' => auth()->user()->id,
            // 'untuk_users_id',
            'fakultas_id' => $usulan->fakultas_id,
            'nip_dosen' => $usulan->dosen->NIP,
            'pesan' => 'Terdapat revisi untuk berkas '.$berkas->judul_berkas.' dari usulan '.$usulan->dataPegawai->NAMA_LENGKAP,
            'status' => 0,
            'link' =>$link,
            'waktu_notifikasi_dikirim' => \Carbon\Carbon::now(),
        ]);

        //kirim notifikasi wa dan email

        //catat log
        $logUsulanPensiun = $this->logUsulan($usulan->id, 'Operator kepegawaian untan memberikan revisi untuk untuk berkas '.$berkas->judul_berkas.' dengan kode usulan '.$usulan->kode_usulan);

        Alert::success('Catatan revisi berhasil dikirim');
        return redirect()->back();
    }

    // private function
    private function logUsulan($usulan_pensiun_id, $nama)
    {
        $logUsulanPensiun = LogUsulanPensiun::create([
            'usulan_pensiun_id' => $usulan_pensiun_id,
            'nama' => $nama,
        ]);
        return true;
    }
}
