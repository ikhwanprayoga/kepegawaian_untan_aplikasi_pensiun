<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;

use App\Http\Controllers\kepegawaian_untan\UsulanController;
use App\User;
use App\Models\BerkasUsulanPensiun;
use App\Models\Dosen;
use App\Models\UsulanPensiun;
use App\Models\UpdatePegawai;

class Helpers {

    public static function tes()
    {
        return 'tes yes';
    }

    public static function tanggalIndo($tanggal, $cetak_hari=false)
    {
        $nama_hari = array('1' => 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu' );
        $nama_bulan = array('1' => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $split = explode('-', $tanggal);
        $tgl_indo = $split[0].' '.$nama_bulan[(int)$split[1]].' '.$split[2];

        if ($cetak_hari) {
            $num = date('N', strtotime($tanggal));
            return $nama_hari[$num].', '.$tgl_indo;
        }
        return $tgl_indo;
    }

    public static function cekCatatanUsulanPensiun($usulan_pensiun_id)
    {
        $berkas = BerkasUsulanPensiun::where('usulan_pensiun_id', $usulan_pensiun_id)->get();
        $data = 0;

        foreach ($berkas as $keys => $val) {
            if ($val->catatanBerkas != null) {

                foreach ($val->catatanBerkas as $key => $value) {
                    if ($value->where('berkas_usulan_pensiun_id', $val->id)->first()->status_kepeg_fakultas == 0) {
                        $data = 1;
                    } else {
                        $data = 0;
                    }
                }
            }
        }
        
        return $data;
    }

    public static function noHpOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->no_hp;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    public static function emailOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->email;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    public static function idOperator($fakultas_id, $role)
    {
        $user = User::where('fakultas_id', $fakultas_id)->with('roles')->get();
        foreach ($user as $keys => $value) {
            foreach ($value->roles as $key => $val) {
                if ($val->name == $role) {
                    if ($val) {
                        // $data['role'] = $val;
                        $noHP = $value->id;
                    } else {
                        $noHp = 0;
                    }
                    
                }
            }
        }
        
        return $noHP;
    }

    public static function jabatanLengkap($nip)
    {
        $data = Dosen::where('NIP', $nip)->first();

        if ($data->JAB_FUNGSIONAL == 'GB') {
            return 'Guru Besar';
        } elseif($data->JAB_FUNGSIONAL == 'LK') {
            return 'Lektor Kepala';
        } elseif($data->JAB_FUNGSIONAL == 'L') {
            return 'Lektor';
        } elseif($data->JAB_FUNGSIONAL == 'AA') {
            return 'Asisten Ahli';
        } elseif($data->JAB_FUNGSIONAL == 'TP') {
            return 'Tenaga Pengajar';
        } else {
            return '';
        }
        
    }

    public static function jabatanLengkapBaru($nip)
    {
        $data = UpdatePegawai::where('NIP', $nip)->first();

        if ($data->JAB_FUNGSIONAL == 'GB') {
            return 'Guru Besar';
        } elseif($data->JAB_FUNGSIONAL == 'LK') {
            return 'Lektor Kepala';
        } elseif($data->JAB_FUNGSIONAL == 'L') {
            return 'Lektor';
        } elseif($data->JAB_FUNGSIONAL == 'AA') {
            return 'Asisten Ahli';
        } elseif($data->JAB_FUNGSIONAL == 'TP') {
            return 'Tenaga Pengajar';
        } else {
            return '';
        }
        
    }

    public static function tahunPensiun($nip, $bulan = false)
    {
        $data = UpdatePegawai::where('NIP', $nip)->first();
        $nama_bulan = array('1' => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tahunLahir = (int)date('Y', strtotime($data->Tgl_Lahir));

        $split = explode('-', $data->Tgl_Lahir);
        $namaBulan = $nama_bulan[(int)$split[1]];

        // if ($bulan) {
        //     $num = date('N', strtotime($tanggal));
        //     return $nama_hari[$num].', '.$tgl_indo;
        // }
        // return $tgl_indo;

        if ($data->JAB_FUNGSIONAL == 'GB') {

            if ($bulan) {
                $tahunPensiun = $tahunLahir + 70;
                return $namaBulan.' '.$tahunPensiun;
            }
            return $tahunLahir + 70;
            
        } else {

            if ($bulan) {
                $tahunPensiun = $tahunLahir + 65;
                return $namaBulan.' '.$tahunPensiun;
            }
            return $tahunLahir + 65;

        }
    }

    public static function tahunPensiunHuruf($nip)
    {
        $data = UpdatePegawai::where('NIP', $nip)->first();

        if ($data->JAB_FUNGSIONAL == 'GB') {
            return '70 (tujuh puluh)';
        } else {
            return '65 (enam puluh lima)';
        }
    }

    public static function cekUsulan($nip)
    {
        $dosen = Dosen::where('NIP', $nip)->orWhere('NIP_Lama', $nip)->first();

        $nipDosen = $dosen->NIP;

        $data = UsulanPensiun::where('nip', $nipDosen)->first();

        if (isset($data)) {
            return 1;
        } else {
            return 0;
        }
        
    }

    public static function cekCatatan($usulanId)
    {
        $usulan = UsulanPensiun::with('lihatCatatan')->find($usulanId);
        $catatan = $usulan->lihatCatatan;

        if ($catatan->where('status', 0)->count() > 0) {
            
            return 1;

        } else {
            return 0;
        }
    }

}