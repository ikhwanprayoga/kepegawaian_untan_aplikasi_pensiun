<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Kirim;

use App\Models\NotifikasiPemberitahuanPengajuanPensiun;

class NotifikasiOtomatis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifikasi:otomatis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command untuk mengirimkan notifikasi otomatis ke pegawai yang akan memasuki batas usia pensiun.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tahun = date('Y-m-d');
        $data = NotifikasiPemberitahuanPengajuanPensiun::whereDate('waktu_kirim_notifikasi', $tahun)->where('status', 0)->get();

        if ($data->count() > 0) {
            foreach ($data as $key => $value) {
                //kirim notif wa ke dosen
                Kirim::wa($value->no_hp_pegawai, $value->pesan);

                //kirim notif email ke dosen
                $to_name = $value->nama;
                $to_email = $value->email_pegawai;
                $data = array('name'=>$value->nama, "body" => $value->pesan);
                    
                Mail::send('email.tes_email', $data, function($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                            ->subject('pemberitahuan tentang pemberkasan batas usia pensiun');
                    $message->from('kepegawaian@untan.ac.id','Kepegawaian UNTAN');
                });

                //kirim notif ke operator fakultas
                Kirim::wa($value->no_hp_operator_fakultas, $value->pesan);

                //kirim notif ke operator untan
                Kirim::wa($value->no_hp_operator_untan, $value->pesan);

                //update status kirim notif
                NotifikasiPemberitahuanPengajuanPensiun::where('id', $value->id)->update([
                    'status' => 1
                ]);
            }

        } else {
            // return null;
            $this->info('tidak ada notifikasi yang dikirim hari ini!');
        }
        
        // return 'done';
        //return info commantd

        $this->info('notifikasi berhasil dikirim!');
    }
}
