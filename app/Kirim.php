<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kirim extends Model
{
    public static function sms($tujuan, $pesan)
    {
        // $pesan = html_entity_decode($pesan, ENT_QUOTES, 'utf-8'); 
        // $pesan = urlencode($pesan);
        
    	$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'user'      => 'smsuntan_api',
                'password'  => '5M0334s',
                'SMSText'   => $pesan,
                'GSM'       => $tujuan
            )
        ));
        $resp = curl_exec($curl);
        if (!$resp) {
            return curl_error($curl) . '" - Code: ' . curl_errno($curl);
        } else {
            header('Content-type: text/xml'); /*if you want to output to be an xml*/
            return $resp;
        }
        curl_close($curl);
    }

    public static function wa($tujuan, $pesan)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendwa/plain',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'username' => 'smsuntan_api',
                'password' => '5M0334s',
                'text' => $pesan,
                'GSM' => $tujuan
            )
        ));
        $resp = curl_exec($curl);
        // if (!$resp) {
        //     die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        // } else {
        //     header('Content-type: text/xml'); /*if you want to output to be an xml*/
        //     echo $resp;
        // }
        curl_close($curl);
    }

    private static function ismscURL($link)
    {
    	$http = curl_init($link);

    	curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
    	$http_result = curl_exec($http);
    	$http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
    	curl_close($http);

    	return $http_result;
    }
}
