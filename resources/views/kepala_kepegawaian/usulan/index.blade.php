@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class="active"><i class="icon-home2 position-left"></i> Usulan</li>
        {{-- <li class="active">Usulan</li> --}}
        {{-- <li class="active">Log Usulan {{ $usulan->first()->kode_usulan }}</li> --}}
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><b>Data Usulan</b>
                    </h5>
                    <div class="heading-elements">
                        {{-- <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button> --}}
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table datatable-show-all datatable-basic">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Usulan</th>
                                <th>Jenis Usulan</th>
                                <th width="">Nama Pengusul</th>
                                <th>NIP</th>
                                <th>Waktu Usulan Diajukan</th>
                                {{-- <th>Status Usulan Untan</th> --}}
                                @role('kepala kepegawaian untan')
                                <th>Trace</th>
                                @endrole
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usulans as $usulan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ $usulan->kode_usulan }}
                                    {{-- <a href="{{ route('kepegawaian-untan.usulan.kelola_usulan', ['id'=>$usulan->id]) }}">{{ $usulan->kode_usulan }}</a> --}}
                                </td>
                                <td>{{ $usulan->jenisPensiun->nama }}</td>
                                <td>{{ $usulan->pegawai->NAMA_LENGKAP }}</td>
                                <td>{{ $usulan->pegawai->NIP }}</td>
                                <td>{{ $usulan->created_at }}</td>
                                {{-- <td>
                                    @if ($usulan->status_usulan_kepeg_untan == 2)
                                    <span class="label bg-success">Verified</span>
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_untan == 1)
                                    <span class="label label-default">Diproses</span>
                                    @if ($usulan->revisi->where('status', 0)->count() > 0)
                                    <span class="label bg-danger">Ada Revisi</span>
                                    @elseif ($usulan->revisi->count() == 0)

                                    @else
                                    <span class="label bg-success">Revisi Selesai</span>
                                    @endif
                                    @endif
                                </td> --}}
                                @role('kepala kepegawaian untan')
                                <td>
                                    <ul class="icons-list">
                                        <a href="{{  route('kepala-kepegawaian.usulan.trace', ['id'=>$usulan->id]) }}"
                                            class="label label-default bg-blue">Trace Usulan</a>
                                    </ul>
                                </td>
                                @endrole
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#usulan').addClass('active');

</script>
{{-- <script>
    function verifUsulan(id) {
        // console.log('verif' + id)
        var url = '{{ url('
        kepegawaian - untan / usulan / verifikasi ') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if (data == 1) {
                    toastr.success('Usulan berhasil di verfikasi!', 'Sukses', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'verified') {
                    toastr.error('Usulan telah di verfikasi sebelumnya!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'finished') {
                    toastr.error('Usulan telah diselesaikan!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'error') {
                    toastr.error('Terjadi kesalahan!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {
                        timeOut: 5000
                    });
                }
            }
        })
    }

</script> --}}
@endpush
