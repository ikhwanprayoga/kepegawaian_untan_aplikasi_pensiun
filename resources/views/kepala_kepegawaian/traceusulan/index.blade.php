@extends('layouts.theme.base')
@push('css')

@endpush
@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Usulan</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection
@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Log Usulan</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Usulan</th>
                            <th>Jenis Usulan</th>
                            <th width="">Nama Pengusul</th>
                            <th>NIP</th>
                            <th>NIDN</th>
                            <th>Unit Kerja</th>
                            <th>Waktu Usulan Diajukan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usulans as $usulan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td><a
                                    href="{{ route('page-tracking-usulan', ['id'=>$usulan->id]) }}">{{ $usulan->kode_usulan }}</a>
                            </td>
                            <td>{{ $usulan->jenisPensiun->nama }}</td>
                            <td>{{ $usulan->pegawai->NAMA_LENGKAP }}</td>
                            <td>{{ $usulan->pegawai->NIP }}</td>
                            <td>{{ $usulan->pegawai->NIDN }}</td>
                            <td>{{ $usulan->pegawai->Unit_Kerja }}</td>
                            <td>{{ $usulan->created_at }}</td>
                            <td class="text-center"></td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>


        </div>
        @include('layouts.theme.footer')
        <!-- /basic datatable -->

        @endsection

        @push('js')
        <script>
            $('#usulan').addClass('active');
        </script>
        <script>
            function verifUsulan(id) {
                // console.log('verif' + id)
                var url = '{{ url('
                kepegawaian - untan / usulan / verifikasi ') }}'
                $.ajax({
                    type: 'POST',
                    url: url + '/' + id,
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (data) {
                        console.log(data)
                        if (data == 1) {
                            toastr.success('Usulan berhasil di verfikasi!', 'Sukses', {
                                timeOut: 5000
                            });
                            location.reload();
                        } else if (data == 'verified') {
                            toastr.error('Usulan telah di verfikasi sebelumnya!', 'Error', {
                                timeOut: 5000
                            });
                            location.reload();
                        } else if (data == 'finished') {
                            toastr.error('Usulan telah diselesaikan!', 'Error', {
                                timeOut: 5000
                            });
                            location.reload();
                        } else if (data == 'error') {
                            toastr.error('Terjadi kesalahan!', 'Error', {
                                timeOut: 5000
                            });
                            location.reload();
                        } else {
                            toastr.error('Terjadi kesalahan!', 'Error', {
                                timeOut: 5000
                            });
                        }
                    }
                })
            }
        </script>
        @endpush
