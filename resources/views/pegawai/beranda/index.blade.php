@extends('layouts.pegawai.base')

@section('title', 'Kepegawaian Untan Beranda')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class="active"><a href="#"><i class="icon-home2 position-left"></i> Beranda</a></li>
        {{-- <li class="active">Dashboard</li> --}}
    </ul>

    {{-- @include('layouts.pegawai.profile') --}}
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        @if ($usulan == null)
        <div class="col-lg-4 col-sm-12">
            <!-- Members online -->
            <div class="panel bg-red-400">

                <div class="panel-body">
                    <h3 class="no-margin"> Belum Terdapat Usulan Dengan NIP Anda</h3>
                    <a href="#" style="color:black;"> {{ session()->get('nip') }}</a>
                    <div class="text-muted text-size-small">status usulan</div>
                </div>

            </div>
            <!-- /members online -->
        </div>
        @else
        <div class="col-lg-6 col-xs-12">
            <!-- Members online -->
            <div class="panel bg-success-800">

                <div class="panel-body">
                    <h3 class="no-margin"> Usulan : {{ $usulan->kode_usulan }}</h3>
                    <a href="#" style="color:white;"> {{ $usulan->jenisPensiun->nama }}</a>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="text-muted text-size-small">Status usulan di kepegawaian fakultas</div>
                            @if ($usulan->status_usulan_kepeg_fakultas == 0)
                            <span class="label bg-white">Usulan di proses</span>
                            @elseif($usulan->status_usulan_kepeg_fakultas == 1)
                            <span class="label bg-blue">Usulan di verifikasi</span>
                            @elseif($usulan->status_usulan_kepeg_fakultas == 2)
                            <span class="label bg-success">Usulan telah teruskan ke kepegawaian UNTAN</span>
                            @else
                            <span class="label bg-danger">ada masalah dengan usulan</span>
                            @endif
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-muted text-size-small">Status usulan di kepegawaian UNTAN</div>
                            @if ($usulan->status_usulan_kepeg_untan == 0)
                                
                            @elseif ($usulan->status_usulan_kepeg_untan == 1)
                            <span class="label bg-white">Usulan di proses</span>
                            @elseif($usulan->status_usulan_kepeg_untan == 2)
                            <span class="label bg-blue">Usulan di verifikasi</span>
                            @elseif($usulan->status_usulan_kepeg_untan == 3)
                            <span class="label bg-success">Usulan telah teruskan ke kepegawaian UNTAN</span>
                            @elseif($usulan->status_usulan_kepeg_untan == 4)
                            <span class="label bg-success">SK Usulan telah terbit</span>
                            @else
                            <span class="label bg-danger">ada masalah dengan usulan</span>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            <!-- /members online -->
        </div>
        @if ($usulan->status_usulan_kepeg_untan == 4)
        <div class="col-lg-4 col-xs-12">
            <div class="panel">
                <div class="panel-body text-center">
                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                    <h5 class="text-semibold">Lihat SK Pensiun</h5>
                    @if($usulan->berkasStepAkhir)
                    @if ($usulan->status_usulan_kepeg_untan != 4)
                    <button type="button" data-toggle="modal" data-target="#upload_sk" class="btn bg-success-400">Upload SK</button>
                    @endif
                        @if ($usulan->berkasStepAkhir->file_sk)
                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_sk) }}">
                            <button type="button" class="btn btn-info">Lihat SK</button>
                        </a>
                        @endif    
                    @endif
                </div>
            </div>
        </div>
        @endif
        @endif

    </div>
    <!-- /main content -->

    @include('layouts.pegawai.footer')

</div>
@endsection

@push('js')
<script>
    $('#beranda').addClass('active');

</script>
@endpush
