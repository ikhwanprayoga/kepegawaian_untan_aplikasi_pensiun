@extends('layouts.pegawai.base')

@section('title', 'Kepegawaian Untan Pegawai Upload Berkas')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ asset('pegawai.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Upload Berkas</li>
    </ul>

    {{-- @include('layouts.pegawai.profile') --}}
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        @foreach ($usulan->jenisPensiun->berkasJenisPensiun as $item)
        @if ($item->masterBerkasPensiun->pegawai_dapat_upload == 1)
        <div class="col-lg-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">{{ $item->masterBerkasPensiun->judul_berkas }}</h6>
                    <small>Hanya boleh file JPG, PNG dan PDF. Maksimal 2 MB.</small>
                </div>
                
                <div class="panel-body">
                    <form action="{{ route('pegawai.berkas.tambah_berkas') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                @foreach ($item->masterBerkasPensiun->berkasUsulanPensiun->where('usulan_pensiun_id', $usulan->id) as $val)
                                {{-- {{$val}} --}}
                                    <p>
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.$val->berkas->direktori.'/'.$val->berkas->nama_berkas) }}">
                                            <button type="button" class="btn btn-info btn-labeled btn-xs"><b><i class="icon-eye"></i></b> Lihat Berkas</button>
                                        </a>

                                        @if ($val->catatanBerkas->count() > 0)
                                        {{-- Modal Button --}}
                                        <button type="button" class="btn btn-primary btn-labeled btn-xs" data-toggle="modal" data-target="#{{ $item->masterBerkasPensiun->id }}">
                                                <b><i class="icon-eye"></i></b> Lihat Revisi
                                        </button>
    
                                        {{-- Modal --}}
                                        <div class="modal fade" id="{{ $item->masterBerkasPensiun->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Revisi {{ $item->masterBerkasPensiun->judul_berkas }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <table class="table">
                                                                <tbody>
                                                                    @foreach ($val->catatanBerkas as $catatan_berkas)
                                                                        <tr>
                                                                            <td scope="row">{{ $loop->iteration }}</td>
                                                                            <td>{!! $catatan_berkas->catatan !!}</td>
                                                                            <td>
                                                                                @if ($catatan_berkas->status_kepeg_fakultas == 1)
                                                                                    <span class="label label-success">Selesai</span>
                                                                                @endif
                                                                                @if ($catatan_berkas->status_kepeg_fakultas == 0)
                                                                                    <span class="label label-warning">Belum Selesai</span>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="modal-footer">
                                                        @if ($val->catatanBerkas->where('status_kepeg_fakultas', 0)->count() > 0)
                                                            <button type="button" onclick="revisiSelesai({{$val->id}})" class="btn btn-success">Catatan Selesai</button>
                                                        @endif
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Modal End --}}
                                        @else
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="" disabled>
                                            Tidak Ada Revisi
                                        </button>
                                        @endif
                                    </p>
                                            <label class="heading-elements" style="margin-top: -26px">
                                                @if ($val->status_verifikasi_kepeg_fakultas == 1)
                                                <span class="label bg-success" style="margin-bottom: 1px;">Terverifikasi fakultas</span> &nbsp;&nbsp;&nbsp;<br>
                                                @else
                                                <span class="label bg-warning" style="margin-bottom: 1px;">Belum Verifikasi fakultas</span> &nbsp;&nbsp;&nbsp;<br>
                                                @endif
                                                {{-- {{ $val->status_verifikasi_kepeg_fakultas == 1 ? print '<span class="label bg-success">Verified</span>' : ''  }} --}}
    
                                                @if ($val->status_verifikasi_kepeg_untan == 1)
                                                <span class="label bg-success">Terverifikas Untan</span> &nbsp;&nbsp;&nbsp;
                                                @else    
                                                <span class="label bg-warning">Belum Terverifikasi Untan</span> &nbsp;&nbsp;&nbsp;
                                                @endif
                                            </label>
                                @endforeach
                            </div>
                        </div>
                        @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                        <div class="row">
                            <div class="col-lg-8">
                                <input type="hidden" name="usulan_pensiun_id" value="{{ $usulan->id }}">
                                <input type="hidden" name="master_berkas_pensiun_id" value="{{ $item->master_berkas_pensiun_id }}">
                                <input type="file" class="file-styled-primary" name="berkas" required>
                            </div>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
    <!-- /main content -->

    @include('layouts.pegawai.footer')

</div>
@endsection

@push('js')
<script>
    $('#berkas').addClass('active');
</script>
@endpush
