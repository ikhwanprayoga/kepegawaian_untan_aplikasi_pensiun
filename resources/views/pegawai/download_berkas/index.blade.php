@extends('layouts.pegawai.base')

@section('title', 'Kepegawaian Untan Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('pegawai.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Download Berkas</li>
    </ul>

    {{-- @include('layouts.theme.profile') --}}
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Daftar Kelengkapan Berkas</h5>
                    {{-- <div class="heading-elements">
                        <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button>
                    </div> --}}
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Berkas</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>SOP Informasi Dan Layanan Pensiun (SILAP)</td>
                                <td>
                                    <a href="{{ asset('berkas_download/sop_silap.pdf') }}" download="">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Syarat Pensiun</td>
                                <td>
                                    <a href="{{ asset('berkas_download/syarat_pensiun.docx') }}" download="">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Berkas Formulir Taspen</td>
                                <td>
                                    <a href="{{ asset('berkas_download/formulir_taspen.pdf') }}" download="">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Berkas Daftar riwayat pekerjaan</td>
                                <td>
                                    <a href="{{ asset('berkas_download/daftar_riwayat_pekerjaan.docx') }}" download="">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>Berkas Daftar susunan keluarga</td>
                                <td>
                                    <a href="{{ asset('berkas_download/daftar_susunan_keluarga.docx') }}" download="">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')

<script>
    $('#download_berkas').addClass('active');
</script>


@endpush
