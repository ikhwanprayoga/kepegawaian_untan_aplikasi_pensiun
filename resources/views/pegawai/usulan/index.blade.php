@extends('layouts.pegawai.base')

@section('title', 'Kepegawaian Untan Usulan')

@push('css')
<style>
    .progressbar {
        counter-reset: step;
    }

    .progressbar li {
        list-style-type: none;
        width: 15%;
        float: left;
        font-size: 12px;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        color: #7d7d7d;
    }

    .progressbar li::before {
        width: 55px;
        height: 55px;
        content: counter(step);
        counter-increment: step;
        line-height: 47px;
        border: 3px solid #7d7d7d;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 53%;
        background-color: white;
    }

    .progressbar li:after {
        width: 100%;
        height: 3px;
        content: '';
        position: absolute;
        background-color: #7d7d7d;
        top: 26px;
        left: -50%;
        z-index: -1;
    }

    .progressbar li:first-child:after {
        content: none;
    }

    .progressbar li.active {
        color: green;
    }

    .progressbar li.active:before {
        border-color: #33a924;
    }

    .progressbar li.active+li:after {
        background-color: #33a924;
    }

    td {
        font-size: 14px;
    }

    th {
        font-size: 14px;
    }

    ul,
    ol {
        padding-left: 166px;
    }

</style>
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('pegawai.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Usulan</li>
    </ul>

    {{-- @include('layouts.pegawai.profile') --}}
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    {{-- <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data Pribadi Pengusul</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="{{asset('profil.png')}}" height="200" width="150" />
                        </div>

                        <div class="col-lg-2">
                            <div id="content">
                                <p>Nama </p>
                                <p>Tempat, Tanggal Lahir</p>
                                <p>NIP</p>
                                <p>NIDN / NIDK </p>
                                <p>Alamat E-mail </p>
                                <p>Jabatan Fungsional </p>
                                <p>Golongan Ruang </p>
                                <p>Jurusan </p>
                                <p>Program Studi </p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <p>:{{$dosen->NAMA_LENGKAP }}</p>
                            <p>: {{$dosen->Tempat_Lahir}},{{$dosen->Tgl_Lahir}} </p>
                            <p>: {{$dosen->NIP }}</p>
                            <p>: {{$dosen->NIDN}}</p>
                            <p>: {{$dosen->ALAMAT_EMAIL }}</p>
                            <p>: {{ $dosen->JAB_FUNGSIONAL}}</p>
                            <p>: {{ $dosen->Gol }}</p>
                            <p>: {{ $dosen->Jurusan}}</p>
                            <p>: {{ $dosen->Program_Studi}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-lg-12 col-xs-12">
            <ul class="progressbar">
                @if ( ($usulan->status_usulan_kepeg_fakultas == 0 && $usulan->status_usulan_kepeg_untan == 0) ||
                ($usulan->status_usulan_kepeg_fakultas == 1 && $usulan->status_usulan_kepeg_untan))
                <li class="active">Kepegawaian Fakultas</li>
                <li>Kepegawaian Untan</li>
                <li>Berkas Diverifikasi</li>
                <li>Berkas Dikirim</li>
                <li>SK Terbit</li>
                @elseif($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 1)
                <li class="active">Kepegawaian Fakultas</li>
                <li class="active">Kepegawaian Untan</li>
                <li>Berkas Diverifikasi</li>
                <li>Berkas Dikirim</li>
                <li>SK Terbit</li>
                @elseif($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 2)
                <li class="active">Kepegawaian Fakultas</li>
                <li class="active">Kepegawaian Untan</li>
                <li class="active">Berkas Diverifikasi</li>
                <li>Berkas Dikirim</li>
                <li>SK Terbit</li>
                @elseif($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 3)
                <li class="active">Kepegawaian Fakultas</li>
                <li class="active">Kepegawaian Untan</li>
                <li class="active">Berkas Diverifikasi</li>
                <li class="active">Berkas Dikirim</li>
                <li>SK Terbit</li>
                @elseif($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 4)
                <li class="active">Kepegawaian Fakultas</li>
                <li class="active">Kepegawaian Untan</li>
                <li class="active">Berkas Diverifikasi</li>
                <li class="active">Berkas Dikirim</li>
                <li class="active">SK Terbit</li>
                @else
            
                @endif
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
    
            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Riwayat usulan</h5>
                </div>
    
                <div class="panel-body">
                    Data log usulan dalam pemroses oleh Kepegawaian Fakultas dan Kepegawaian UNTAN
                </div>
    
                <div class="table-responsive">
                    <table class="table datatable-show-all">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Riwayat usulan</th>
                                <th>Waktu</th>
                                {{-- <th class="text-center">#</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usulan->logUsulanPensiun as $usulan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $usulan->nama }}</td>
                                <td>{{ Helpers::tanggalIndo(date('d-m-Y', strtotime($usulan->created_at)), true) }} <br> Pukul : {{ date('H:i', strtotime($usulan->created_at)) }}</td>
                                {{-- <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>
    
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#" title="Verifikasi Usulan"><i class="icon-file-pdf"></i>
                                                        Verifikasi Usulan</a></li>
                                                <li><a href="#" title="Kirim usulan ke Kepegewaian UNTAN"><i
                                                            class="icon-file-excel"></i> Kirim usulan</a></li>
                                                <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
    
            </div>
            <!-- /latest posts -->
    
        </div>
    </div>
    <!-- /main content -->
</div>

@include('layouts.pegawai.footer')

</div>
@endsection

@push('js')
<script>
    $('#usulan').addClass('active');

</script>
@endpush
