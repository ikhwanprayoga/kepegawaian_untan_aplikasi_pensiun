<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Surat Bebas Hukuman {{ $usulan->dataPegawai->NAMA_LENGKAP }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')

    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal portrait
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 0px solid black;
        }

        .table td, .table th {
            padding: 0.01rem;
            vertical-align: top;
            border-top: 3px;
            font-family:times new roman;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 0px solid #dee2e6;
        }

        p, h3{
            font-family:times new roman;
        }

    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    {{--  <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">  --}}
     <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">
        <div class="row" style="padding: 0px 40px 0px">
        <div class="col-lg-12">
            <table style="width:100%">
                <tr>
                    <td style="width:5%">
                        <img style="width: 80px;" src="{{ asset('assets/images/untan.png') }}" alt="" srcset="">
                    </td>
                    <td align="center" style="width:95%">
                        {{-- <p  class="tag" style="font-size: 16px; padding: 3px 0px 4px 0px;"> --}}
                        <h3 style="font-size: 16px;">KEMENTRIAN PENDIDIKAN DAN KEBUDAYAAN
                            <br><b>UNIVERSITAS TANJUNGPURA</b>
                            <br><b>{{ $usulan->fakultas->infoFakultas->nama_lengkap_fakultas }}</b>
                            <br>Jalan Prof. Dr. H. Hadari Nawawi Pontianak 78124</h3>
                        <h3 colspan="3" style="font-size: 11px;">Telp. {{ $usulan->fakultas->infoFakultas->no_telepon_fakultas }}</
                            <br> Fax. {{ $usulan->fakultas->infoFakultas->fax_fakultas }} Kotak Pos {{ $usulan->fakultas->infoFakultas->kode_pos_fakultas }}
                            <br>e-mail : <u>{{ $usulan->fakultas->infoFakultas->email_fakultas }}</u></h3>
                        {{-- <p class="margin-bottom: -0.5rem;"> </h3> --}}
                    </td>
                </tr>
            </table>
             <hr style="border:1px solid black;">
        </div>
        </div>


        <!-- Write HTML just like a web page -->

        <p align="center" colspan="3" style="font-size: 16px; padding: 3px 0px 4px 0px; font-weight:bold;">SURAT PERNYATAAN <br> TIDAK PERNAH DIJATUHI HUKUMAN DISIPLIN TINGKAT SEDANG/BERAT</p>
        <p align="center" colspan="3" style="font-size: 16px; padding: 3px 0px 4px 0px; font-weight:bold; text-transform: uppercase;">NOMOR : {{ $dataSurat->no_surat_hukuman }}</p>
        <font size="2" style="font-size: 12px;">
        <div class="row" style="margin-bottom: -16px;">
            <div class="col-lg-12">

            </div>
        </div>
            {{-- tabel --}}
        <div class="row" style="padding: 0px 40px 0px">
            <div class="col-lg-12">
                <table class="table" >
                    <tbody>
                        <tr>
                            <td colspan="3">Yang bertanda tangan dibawah ini :</td>
                        </tr>
                        <tr>
                            <td style="width:3%"></td>
                            <td style="width:12%">Nama</td>
                            <td>: {{ $usulan->fakultas->infoFakultas->nama_dekan }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>NIP</td>
                            <td>: {{ $usulan->fakultas->infoFakultas->nip_dekan }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Pangkat/golongan ruang</td>
                            <td>: {{ $usulan->fakultas->infoFakultas->pangkat_dekan }}/{{ $usulan->fakultas->infoFakultas->golongan_dekan }}</td>
                        </tr>
                            <td colspan="3">Dengan ini menyatakan dengan sesungguhnya, bahwa Pegawai Negeri Sipil :</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Nama</td>
                            <td>: {{ $usulan->dataPegawai->NAMA_LENGKAP  }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>NIP</td>
                            <td>: {{ $usulan->dataPegawai->NIP  }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Pangkat/golongan ruang</td>
                            <td>: {{ $usulan->dataPegawai->pangkat}}/{{ $usulan->dataPegawai->Gol}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Jabatan</td>
                            <td>: {{ $usulan->dataPegawai->JAB_FUNGSIONAL}}</td>
                        </tr>
                         <tr>
                            <td></td>
                            <td>Instansi</td>
                            <td>: {{ $usulan->fakultas->nama_fakultas}}</td>
                        </tr>
                </table>
            </div>
                <p align="justify"> Dalam satu tahun terakhir tidak pernah dijatuhi hukuman disiplin tingkat sedang/berat.
                </p>
                <p align="justify">Demikian surat pernyataan ini saya buat dengan sesungguhnya dengan mengingat sumpah jabatan dan apabila di kemudian hari
                    ternyata isi surat pernyataan ini tidak benar yang mengakibatkan kerugian bagi negara maka saya bersedia menanggung kerugian
                    tersebut.</p>

        <div class="row" style="padding: 0px 40px 0px">
            <div class="col-lg-12">
                <table class="table" >
                    <tbody align="center">
                        <tr>
                            <td style="width:7%"></td>
                            <td style="width:31%"></td>
                            <td colspan="3" style="width:17%">Pontianak, {{Helpers::tanggalIndo(date('d-m-Y', strtotime($dataSurat->waktu_surat_hukuman)), false) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>PEJABAT PENGELOLA KEPEGAWAIAN</td>
                        </tr>
                        <tr style="height: 60px;">
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>({{ $usulan->fakultas->infoFakultas->nama_dekan }})</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>NIP. {{ $usulan->fakultas->infoFakultas->nip_dekan }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script>
        window.print()
    </script>

</body>
</html>
