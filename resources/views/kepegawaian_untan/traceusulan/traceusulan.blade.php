@extends('layouts.theme.base')
@push('css')
<style>
    .progressbar {
        counter-reset: step;
    }

    .progressbar li {
        list-style-type: none;
        width: 27%;
        float: left;
        font-size: 12px;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        color: #7d7d7d;
    }

    .progressbar li::before {
        width: 55px;
        height: 55px;
        content: counter(step);
        counter-increment: step;
        line-height: 47px;
        border: 3px solid #7d7d7d;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 53%;
        background-color: white;
    }

    .progressbar li:after {
        width: 100%;
        height: 3px;
        content: '';
        position: absolute;
        background-color: #7d7d7d;
        top: 26px;
        left: -50%;
        z-index: -1;
    }

    .progressbar li:first-child:after {
        content: none;
    }

    .progressbar li.active {
        color: green;
    }

    .progressbar li.active:before {
        border-color: #33a924;
    }

    .progressbar li.active+li:after {
        background-color: #33a924;
    }

    td {
       font-size: 14px;
    }

    th {
        font-size: 14px;
    }

    ul, ol {
        padding-left: 166px;
    }
</style>
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li><a href="{{ route('kepegawaian-untan.usulan.index') }}">Usulan</a></li>
        <li class="active">{{ $usulan->kode_usulan }}</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection


@section('content')
<ul class="progressbar">
    @if ( ($usulan->status_usulan_kepeg_fakultas == 0 && $usulan->status_usulan_kepeg_untan == 0) ||
    ($usulan->status_usulan_kepeg_fakultas == 1 && $usulan->status_usulan_kepeg_untan))
    <li class="active">Kepegawaian Fakultas</li>
    <li>Kepegawaian Untan</li>
    <li>Selesai</li>
    @elseif($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 1)
    <li class="active">Kepegawaian Fakultas</li>
    <li class="active">Kepegawaian Untan</li>
    <li>Selesai</li>
    @elseif($usulan->status_usulan_kepeg_fakultas == 2 && $usulan->status_usulan_kepeg_untan == 2)
    <li class="active">Kepegawaian Fakultas</li>
    <li class="active">Kepegawaian Untan</li>
    <li class="active">Selesai</li>
    @else

    @endif
</ul>
<div class="content">
    <!-- main content -->
    <div class="row" style="margin-top: 140px;">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Log Usulan</h5>
                    <div class="panel-body">
                        <div class="col-lg-2">
                            <p><b>Nama Pengusul</b></p>
                            <p><b>Kode Usulan</b></p>
                            <p><b>Unit Kerja</b></p>
                        </div>
                        <div class="col-lg-6">
                            <p>: {{ $usulan->dosen->NAMA_LENGKAP}}</p>
                            <p>: {{ $usulan->kode_usulan }}</p>
                            <p>: {{ $usulan->dosen->Unit_Kerja }}</p>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr class="bg-blue">
                                    <th>No.</th>
                                    <th>Waktu</th>
                                    <th>Status Usulan</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 0;?>
                            @foreach ($tabellog->logUsulanPensiun as $item)
                            <?php $no++ ;?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $item->created_at->formatLocalized('%d %B %Y') }}
                                <br><small>{{ $item->created_at->format('H:i') }}</small></td>
                                <td>{{ $item->nama }}</td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
                </div>
                @include('layouts.theme.footer')
                @endsection
                </div>
                </div>


@push('js')

@endpush
