@extends('layouts.theme.base')
@push('css')
<style>
    body {
        margin: 0;
        padding: 0;
        /* background-color:#ff6d6d; */
        font-family: arial
    }

    .box {
        margin: 0 10%;
        overflow: hidden;

        padding: 10px 0 40px 60px
    }

    .box ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        position: relative;

        transition: all 0.5s linear;
        top: 0
    }

    .box ul:last-of-type {
        top: 80px
    }

    .box ul:before {
        content: "";
        display: block;
        width: 0;
        height: 100%;
        border: 1px dashed #ff6d6d;
        position: absolute;
        top: 0;
        left: 30px
    }

    .box ul li {
        margin: 20px 60px 60px;
        position: relative;
        padding: 10px 20px;
        background: rgba(255, 255, 255, 0.3);
        background-color: #ff6d6d;
        color: #fff;
        border-radius: 10px;
        line-height: 20px;
        width: 35%
    }


    .box ul li>span {
        content: "";
        display: block;
        width: 0;
        height: 100%;
        border: 1px solid #ff6d6d;
        position: absolute;
        top: 0;
        left: -30px
    }

    .box ul li>span:before,
    .box ul li>span:after {
        content: "";
        display: block;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        background: #ff6d6d;
        border: 2px solid #ff6d6d;
        position: absolute;
        left: -5.5px
    }

    .box ul li>span:before {
        top: -10px
    }

    .box ul li>span:after {
        top: 95%
    }

    .box .title {
        text-transform: uppercase;
        font-weight: 700;
        margin-bottom: 5px
    }

    .box .info:first-letter {
        text-transform: capitalize;
        line-height: 1.7
    }

    .box .name {
        margin-top: 10px;
        text-transform: capitalize;
        font-style: italic;
        text-align: right;
        margin-right: 20px
    }


    .box .time span {
        position: absolute;
        left: -110px;
        color: #000;
        font-size: 80%;
        font-weight: bold;
    }

    .box .time span:first-child {
        top: -14px
    }

    .box .time span:last-child {
        top: 92%
    }


    .arrow {
        position: absolute;
        color: #000;
        top: 105%;
        left: 22%;
        cursor: pointer;
        height: 20px;
        width: 20px
    }

    .arrow:hover {
        -webkit-animation: arrow 1s linear infinite;
        -moz-animation: arrow 1s linear infinite;
        -o-animation: arrow 1s linear infinite;
        animation: arrow 1s linear infinite;
    }

    .box ul:last-of-type .arrow {
        position: absolute;
        top: 105%;
        left: 22%;
        transform: rotateX(180deg);
        cursor: pointer;
    }

    svg {
        width: 20px;
        height: 20px
    }

    @keyframes arrow {

        0%,
        100% {
            top: 105%
        }

        50% {
            top: 106%
        }
    }

    @-webkit-keyframes arrow {

        0%,
        100% {
            top: 105%
        }

        50% {
            top: 106%
        }
    }

    @-moz-keyframes arrow {

        0%,
        100% {
            top: 105%
        }

        50% {
            top: 106%
        }
    }

    @-o-keyframes arrow {

        0%,
        100% {
            top: 105%
        }

        50% {
            top: 106%
        }
    }
</style>
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="kepegawaian-untan.beranda"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Usulan</li>
        <li class="active"></li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                <div class="panel-heading">
                    <h5 class="panel-title">Log Usulan</h5>
                    <div class="heading-elements">
                    </div>
                </div>
                <div class="col-lg-2">
                    <p><b>Nama Pengusul</b></p>
                    <p><b>Kode Usulan</b></p>
                </div>
                <div class="col-lg-6">
                    <p>: {{ $logusulan->dosen->NAMA_LENGKAP}}</p>
                    <p>: {{ $logusulan->kode_usulan }}</p>
                </div>

                </div>
                <div class="box">
                    <ul>
                        @foreach ( $logusulan->logUsulanPensiun as $item)
                        <li>
                            <span></span>
                            <div class="info">{{ $item->nama }}</div>
                            <div class="time">
                                <span>{{ $item->created_at->format('d, M Y') }}</span>
                                <span style = "left:-70px">{{ $item->created_at->format('H:i') }}</span>
                            </div>
                        </li>

                        @endforeach
                    </ul>
                    <ul id="second-list"></ul>
                    <script src="JavaScript/timeline-V2.js"></script>
                </div>
            </div>

        </div>
    </div>
    @include('layouts.theme.footer')
</div>

@endsection

@push('js')
<script>
    var downArrow = document.getElementById("btn1");
    var upArrow = document.getElementById("btn2");

    downArrow.onclick = function () {
        'use strict';
        document.getElementById("first-list").style = "top:-620px";
        document.getElementById("second-list").style = "top:-620px";
        downArrow.style = "display:none";
        upArrow.style = "display:block";
    };

    upArrow.onclick = function () {
        'use strict';
        document.getElementById("first-list").style = "top:0";
        document.getElementById("second-list").style = "top:80px";
        upArrow.style = "display:none";
        downArrow.style = "display:block";
    };


    // creating my image link

    var link = document.createElement("a");
    document.body.appendChild(link);

    link.href = "https://codepen.io/mo7hamed/pens/public";
    link.target = "_blank";

    var photo = document.createElement("img");
    link.appendChild(photo);

    photo.onmouseover = function () {
        this.style.transform = "scale(1.1,1.1)";
        this.style.boxShadow = "5px 5px 15px #000";
    };

    photo.onmouseout = function () {
        this.style.transform = "scale(1,1)";
        this.style.boxShadow = "none";
    };
</script>
@endpush
