@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Usulan')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Usulan</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Revisi Usulan</h5>
                    <div class="heading-elements">
                        {{-- <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button> --}}
                    </div>
                </div>

                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    <form action="{{ route('kepegawaian-untan.usulan.revisi-send') }}" method="post">
                        @csrf
                        <input type="hidden" name="dui" value="{{ auth()->user()->id }}"> {{-- User id yang membuat revisi --}}
                        {{-- <input type="hidden" name="uui" value="{{ $var_ui }}"> User id sesuai fakultas --}}
                        <input type="hidden" name="upi" value="{{ $usulan->id }}"> {{-- Usulan pensiun id --}}
                        <input type="hidden" name="nip" value="{{ $usulan->nip }}"> {{-- NIP --}}
                        <input type="hidden" name="status" value="0">
                        <div class="form-group">
                            <label for="idPengirim">Fakultas Tujuan</label>
                            <input type="text" class="form-control" id="idPengirim" aria-describedby="idPengirimHelp" value="{{ $usulan->fakultas->nama_fakultas }}" disabled>
                            <small id="idPengirimHelp" class="form-text text-muted">Fakultas Tujuan Revisi Dikirim</small>
                        </div>
                        <div class="form-group">
                            <label for="kodeUsulan">Kode Usulan</label>
                            <input type="text" class="form-control" id="kodeUsulan" aria-describedby="kodeUsulanHelp" value="{{ $usulan->kode_usulan }}" disabled>
                            <small id="kodeUsulanHelp" class="form-text text-muted">Kode Usulan Dari Usulan Yang Akan Direvisi</small>
                        </div>
                        {{-- <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" name="status" id="status">
                                <option value="0">Proses</option>
                                <option value="1">Verified</option>
                                <option value="2">Telah Dikirim Ke Untan</option>
                            </select>
                        </div> --}}
                        <div class="form-group">
                            <label for="cr">Catatan Revisi</label>
                            <textarea class="form-control" name="catatanRevisi" id="cr"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')

@endpush