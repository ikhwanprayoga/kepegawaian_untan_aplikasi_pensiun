@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Kelola Usulan')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-untan.usulan.index') }}"> Usulan</a></li>
        <li class="active">Kelola Usulan {{ $usulan->first()->kode_usulan }}</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- data pribadi pengusul -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data pribadi pengusul</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <ul class="icons-list">
                                <a href="{{ route('kepegawaian-untan.usulan.lihat_data_pegawai', ['id' => $usulan->id, 'nip' => $dosen->NIP]) }}" class="btn btn-primary btn-sm">Lihat Data Pegawai</a>
                            </ul>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3" style="text-align:center;">
                            @if ($foto == 1 && $dosen->foto != null)
                                <img src="{{ Storage::url('public/pegawai/'.$dosen->foto) }}" style="width: 200px; height: 100%;" />
                            @else
                                <img src="{{asset('assets/images/allgender.png')}}" style="width: 200px; height: 100%;" />
                            @endif
                        </div>

                        <div class="col-lg-2 col-xs-2">
                            <div id="content">
                                <p>Nama </p>
                                <p>Tempat, Tanggal Lahir</p>
                                <p>NIP</p>
                                <p>NIDN / NIDK </p>
                                <p>Alamat E-mail  </p>
                                <p>Jabatan Fungsional </p>
                                <p>Golongan Ruang </p> 
                                <p>Jurusan </p>
                                <p>Program Studi </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-6">
                            <div>
                                <p>:{{ $dosen->NAMA_LENGKAP }}</p>
                                <p>: {{ $dosen->Tempat_Lahir }}, &nbsp;{{ date('d-m-Y', strtotime($dosen->Tgl_Lahir)) }} </p>
                                <p>: {{ $dosen->NIP }}</p>
                                <p>: {{ $dosen->NIDN }}</p>
                                <p>: {{ $dosen->ALAMAT_EMAIL }}</p>
                                <p>: {{ Helpers::jabatanLengkap($dosen->NIP) }}</p>
                                <p>: {{ $dosen->Gol }}</p>
                                <p>: {{ $dosen->Jurusan }}</p>
                                <p>: {{ $dosen->Program_Studi}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /data pribadi pengusul -->

            <!-- daftar formulir -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Opsi Lanjutan Usulan Pensiun</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="panel-body">
					<div class="row">
                        <div class="col-md-12" style="text-align:center">
                            {{-- <button type="button" onclick="verifUsulan({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-eye"></i></b> Verifikasi Usulan</button> --}}
                            @if ($usulan->status_usulan_kepeg_untan == 1)
                            <button type="button" onclick="kembalikanUsulan({{$usulan->id}})" class="btn btn-primary btn-labeled" title="Kembalikan Usulan Ke Operator Fakultas"><b><i class="icon-undo2"></i></b> Kembalikan Usulan</button>
                            <button type="button" onclick="verifUsulan({{$usulan->id}})" class="btn btn-primary btn-labeled" title="Verifikasi Usulan"><b><i class="icon-file-check"></i></b> Verifikasi Usulan</button>
                            @elseif($usulan->status_usulan_kepeg_untan == 2)
                            <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-file-check"></i></b>Usulan Telah Verifikasi</button>
                            <button type="button" onclick="batalVerfikasiUsulan({{$usulan->id}})" class="btn btn-warning btn-labeled" title="Batalkan Verifikasi Usulan"><b><i class="icon-undo2"></i></b>Batal Verifikasi Usulan</button>
                                @if ($usulan->berkasStepAkhir)
                                    @if ($usulan->berkasStepAkhir->file_surat_pengantar)
                                    <button type="button" onclick="kirimUsulan({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-paperplane"></i></b>Tandai Usulan Telah Dikirim</button>
                                    @endif
                                @endif
                            @elseif($usulan->status_usulan_kepeg_untan == 5 )
                            <button type="button" class="btn btn-warning btn-labeled" title="Usulan Telah Dikembalikan Ke Fakultas" disabled><b><i class="icon-file-check"></i></b> Usulan Dikembalikan Ke Fakultas</button>
                            <button type="button" class="btn btn-primary btn-labeled" onclick="lihatCatatan({{$usulan->id}})" title="Lihat catatan usulan"><b><i class="icon-file-check"></i></b> Lihat Catatan</button>
                            @else
                            <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-file-check"></i></b>Usulan Telah Dikirim Ke Dikti</button>
                            @if(isset($usulan->berkasStepAkhir->file_sk))
                            <button type="button" onclick="skTerbit({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-file-check"></i></b>Tandai SK Usulan Telah Terbit</button>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data berkas</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
					<div class="row">
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Kelengkapan Pemberkasan</h5>
                                    <p class="mb-15">Kelengkapan berkas untuk jenis usulan pensiun {{ $usulan->jenisPensiun->nama }}</p>
                                    <a href="{{ route('kepegawaian-untan.usulan.kelola_usulan.berkas', ['id'=>$usulan->id]) }}" class="btn bg-success-400">Lihat Daftar Berkas</a>
                                </div>
                            </div>
                        </div>

                        @if ($usulan->status_usulan_kepeg_untan == 2 )
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Berkas DPCP / SP4</h5>
                                    <p class="mb-15">Berkas dapat di download atau lansung di cetak</p>
                                    <button type="button" onclick="cetak_dpcp({{$usulan->id}})" class="btn bg-success-400">Cetak DPCP</button>
                                    <button type="button" onclick="cetak_sp4({{$usulan->id}})" class="btn bg-success-400">Cetak SP4</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        @if ($usulan->status_usulan_kepeg_untan == 3)
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Lihat Berkas Pengantar</h5>
                                    <p class="mb-15">Berkas dapat di lihat disini</p>
                                    {{-- <button type="button" data-toggle="modal" data-target="#upload_dpcp" class="btn bg-success-400">Upload DPCP</button> --}}
                                    @if($usulan->berkasStepAkhir)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_dpcp) }}">
                                            <button type="button" class="btn btn-info">Lihat DPCP</button>
                                        </a>
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_sp4) }}">
                                            <button type="button" class="btn btn-info">Lihat SP4</button>
                                        </a>
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_surat_pengantar) }}">
                                            <button type="button" class="btn btn-info">Lihat Surat Pengantar</button>
                                        </a>
                                        {{-- <button type="button" data-toggle="modal" data-target="#surat_pengantar" class="btn bg-success-400">Cetak Surat Pengantar</button> --}}
                                    @endif
                                </div>
                            </div>
                        </div> 
                        @endif

                    </div>
                </div>
            </div>

            @if ($usulan->status_usulan_kepeg_untan == 2 && $usulan->status_usulan_kepeg_fakultas == 2)
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Pengarsipan Berkas DPCP</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Upload Berkas DPCP dan SP4</h5>
                                    <p class="mb-15">Berkas DPCP yang telah di acc pimpinan dapat di Upload disini</p>
                                    <p class="mb-15">Berkas SP$ yang telah di cap 3 jari oleh pegawai dapat di Upload disini</p>
                                    <button type="button" data-toggle="modal" data-target="#upload_dpcp" class="btn bg-success-400">Upload DPCP</button>
                                    @if($usulan->berkasStepAkhir)
                                    @if ($usulan->berkasStepAkhir->file_dpcp)
                                    <button type="button" data-toggle="modal" data-target="#upload_sp4" class="btn bg-success-400">Upload SP4</button>
                                    @endif
                                    <div style="padding-top:1%;">
                                        @if ($usulan->berkasStepAkhir->file_dpcp)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_dpcp) }}">
                                            <button type="button" class="btn btn-info">Lihat DPCP</button>
                                        </a>
                                        @endif
                                        @if ($usulan->berkasStepAkhir->file_sp4)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_sp4) }}">
                                            <button type="button" class="btn btn-info">Lihat SP4</button>
                                        </a>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if ($usulan->berkasStepAkhir)
                        @if ($usulan->berkasStepAkhir->file_dpcp && $usulan->berkasStepAkhir->file_sp4)
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Cetak Surat Pengantar</h5>
                                    <p class="mb-15">Surat Dapat lansung di cetak atau didownload</p>
                                    <div style="padding-bottom:1%;">
                                        <button type="button" onclick="cetak_surat_pengantar({{$usulan->id}})" class="btn bg-success-400">Cetak Surat Pengantar</button>
                                    </div>
                                    <button type="button" data-toggle="modal" data-target="#upload_surat_pengantar" class="btn bg-success-400">Upload Surat Pengantar</button>
                                    @if($usulan->berkasStepAkhir->file_surat_pengantar)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_surat_pengantar) }}">
                                            <button type="button" class="btn btn-info">Lihat Surat Pengantar</button>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
            @endif

            @if ($usulan->status_usulan_kepeg_untan == 3 || $usulan->status_usulan_kepeg_untan == 4)
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Arsip Berkas SK Pensiun</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        @if ($usulan->status_usulan_kepeg_untan == 3 || $usulan->status_usulan_kepeg_untan == 4)
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Input Nomor Resi Pengiriman Berkas Pensiun</h5>
                                    <p class="mb-15">Nomor Resi Pengiriman Berkas Pensiun Ke Dikti</p>
                                    @if($usulan->berkasStepAkhir)
                                    @if ($usulan->status_usulan_kepeg_untan != 4)
                                    <button type="button" data-toggle="modal" data-target="#input_resi_pengiriman" class="btn bg-success-400">Input No. Resi</button>
                                    @endif
                                    @if ($usulan->no_resi_pengiriman_berkas)
                                    <button type="button" data-toggle="modal" data-target="#lihat_resi_pengiriman" class="btn btn-info">Lihat No. Resi</button>
                                    @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Upload SK Pensiun</h5>
                                    <p class="mb-15">SK dapat di arsipkan di form ini</p>
                                    @if($usulan->berkasStepAkhir)
                                    @if ($usulan->status_usulan_kepeg_untan != 4)
                                    <button type="button" data-toggle="modal" data-target="#upload_sk" class="btn bg-success-400">Upload SK</button>
                                    @endif
                                        @if ($usulan->berkasStepAkhir->file_sk)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_sk) }}">
                                            <button type="button" class="btn btn-info">Lihat SK</button>
                                        </a>
                                        @endif    
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <!-- /main content -->

    {{-- modal untuk menambah catatan ngembalikan usulan --}}
    <div id="modal_kembalikan_usulan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tuliskan Catatan Untuk Pengembalian Usulan</h4>
                </div>
                <form action="{{ route('kepegawaian-untan.usulan.kembalikan', ['id'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            {{-- <label for="cr">Catatan Revisi</label> --}}
                            <textarea class="wysihtml5 wysihtml5-min form-control" cols="18" rows="18" name="catatanUsulan" id="cr" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Kembalikan Usulan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk menambah catatan baru ngembalikan usulan --}}
    <div id="modal_tambah_catatan_baru" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tuliskan Catatan Baru Untuk Pengembalian Usulan</h4>
                </div>
                <form action="{{ route('kepegawaian-untan.usulan.tambah_catatan_baru', ['id'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            {{-- <label for="cr">Catatan Revisi</label> --}}
                            <textarea class="wysihtml5 wysihtml5-min form-control" cols="18" rows="18" name="catatanUsulan" id="cr" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Tambah Catatan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk melihat catatan usulan yg di kembalikan --}}
    <div id="modal_lihat_catatan_usulan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Catatan Pengembalian Usulan</h4>
                </div>
                <form action="#" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="table-responsive pre-scrollable">
							<table class="table" id="tableLihatCatatan">
								<thead>
									<tr>
										<th>Catatan</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="button" id="tombolTambahCatatanBaru" class="btn btn-primary">Tambah Catatan Baru</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk input tgl dpcp print --}}
    <div class="modal fade" id="modal_dpcp" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Berkas DPCP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form target="_blank" action="{{ route('dpcp', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form_tanggal_lama_dpcp">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="" id="waktu_lama_dpcp" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_dpcp" id="pilih_waktu_lama_dpcp" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_dpcp">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_dpcp" class="form-control" placeholder="" aria-describedby="helpId" required>
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary cetak">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk input tgl dan no surat sp4 --}}
    <div class="modal fade" id="modal_sp4" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Berkas SP4</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form target="_blank" action="{{ route('sp4', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form_tanggal_lama_sp4">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="" id="waktu_lama_sp4" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_sp4" id="pilih_waktu_lama_sp4" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_sp4">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_sp4" class="form-control" placeholder="" aria-describedby="helpId">
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary cetak">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk upload dpcp --}}
    <div class="modal fade" id="upload_dpcp" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload Berkas DPCP yang telah di ACC Pimpinan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="{{ route('kepegawaian-untan.dpcp.upload', ['nip'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      {{-- <label for="">Tanggal</label> --}}
                      <input type="file" class="file-styled-primary" name="berkas_dpcp" required><br>
                      <small>Hanya boleh file PDF. Maksimal 2 MB.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk upload sp4 --}}
    <div class="modal fade" id="upload_sp4" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload Berkas SP4 yang telah di cap 3 jari pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="{{ route('kepegawaian-untan.sp4.upload', ['nip'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      {{-- <label for="">Tanggal</label> --}}
                      <input type="file" class="file-styled-primary" name="berkas_sp4" required><br>
                      <small>Hanya boleh file PDF. Maksimal 2 MB.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk upload surat_pengantar --}}
    <div class="modal fade" id="upload_surat_pengantar" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload Berkas Surat Pengantar Untuk Arsip</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="{{ route('kepegawaian-untan.surat-pengantar.upload', ['nip'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      {{-- <label for="">Tanggal</label> --}}
                      <input type="file" class="file-styled-primary" name="berkas_surat_pengantar" required><br>
                      <small>Hanya boleh file PDF. Maksimal 2 MB.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk cetak surat pengantar --}}
    <div class="modal fade" id="modal_surat_pengantar" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Surat Pengantar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form target="_blank" action="{{ route('surat_pengantar_bup', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form-nomor-surat">
                        <label for="">Nomor Surat</label>
                        <input type="text" name="nomor_surat_pengantar" id="nomor_surat_pengantar" class="form-control" placeholder="" aria-describedby="helpId">
                        <small id="helpId" class="text-muted">Nomor surat pengantar</small>
                    </div>
                    <div class="form-group form_tanggal_lama_pengantar">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="" id="waktu_lama_surat_pengantar" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_surat_pengantar" id="pilih_waktu_lama_surat_pengantar" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_pengantar">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_surat_pengantar_baru" class="form-control" placeholder="" aria-describedby="helpId" required>
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk upload sk --}}
    <div class="modal fade" id="upload_sk" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload SK Pensiun</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('kepegawaian-untan.sk.upload', ['usulan_pensiun_id'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      {{-- <label for="">Tanggal</label> --}}
                      <input type="file" class="file-styled-primary" name="sk_pensiun" required><br>
                      <small>Hanya boleh file PDF. Maksimal 2 MB.</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk input no resi --}}
    <div class="modal fade" id="input_resi_pengiriman" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Input No Resi Pengiriman Berkas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('kepegawaian-untan.usulan.kirim-resi', ['usulan_pensiun_id'=>$usulan->id]) }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Nomor Resi Pengiriman</label>
                      <input type="text" name="no_resi" id="" class="form-control" placeholder="" aria-describedby="helpId" required>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Tanggal Pengiriman Berkas</label>
                      <input type="date" name="tanggal" id="" class="form-control" placeholder="" aria-describedby="helpId" required>
                      <small id="helpId" class="text-muted">Tanggal Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Jasa Pengiriman Yang Digunakan</label>
                      <input type="text" name="jasa_pengiriman" id="" class="form-control" placeholder="" aria-describedby="helpId" required>
                      <small id="helpId" class="text-muted">Jasa Pengiriman Berkas Yang Digunakan</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk lihat no resi --}}
    <div class="modal fade" id="lihat_resi_pengiriman" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nomor Resi Pengiriman Berkas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Nomor Resi Pengiriman Berkas</label>
                      <input type="text" id="" value="{{ $usulan->no_resi_pengiriman_berkas ? $usulan->no_resi_pengiriman_berkas: 'No Resi belum di masukan' }}" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Tanggal Pengiriman Berkas</label>
                      <input type="text" id="" value="{{ $usulan->tanggal_kirim_berkas ? date('d-m-Y', strtotime($usulan->tanggal_kirim_berkas)) : 'Berkas Belum Dikirim' }}" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Jasa Pengiriman Yang Digunakan</label>
                      <input type="text" value="{{ $usulan->jasa_pengiriman ? $usulan->jasa_pengiriman: 'Tidak Ada Data Yang Diinputkan' }}" id="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Jasa Pengiriman Berkas Yang Digunakan</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')

<script>
    $('#usulan').addClass('active');
</script>
<script>
    function verifUsulan(id) {
        var url = '{{ url('kepegawaian-untan/usulan/verifikasi') }}'
        swal({
            title: "Verifikasi Usulan?",
            text: "Verifikas Usulan Ini",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url + '/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                    success: function(data) {
                        if (data.status == 1) {
                            swal(data.pesan, {
                                icon: "success",
                            });
                            location.reload();
                        } else {
                            swal(data.pesan, {
                                icon: "error",
                            });
                        }
                    },
                    error: function(errors) {
                        swal("Maaf Ada Kesalahan!", {
                            icon: "warning",
                        });
                    },
                })

        } else {
            swal("Usulan batal diverfikasi!");
        }
    });
    }

    function kembalikanUsulan(usulanId) {
        // console.log('usulan '+usulanId)\
        $('#modal_kembalikan_usulan').modal('show')
    }

    function batalVerfikasiUsulan(usulanId) {
        // console.log('usulan '+usulanId)
        var url = '{{ url('kepegawaian-untan/usulan') }}'
        swal({
            title: "Batal Verifikasi Usulan?",
            text: "Membatalkan Verifikasi Usulan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((bataVerif) => {
            if (bataVerif) {
                $.ajax({
                    type: 'POST',
                    url: url + '/' + usulanId + '/batalverifikasi',
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                        success: function(data) {
                            console.log(data)
                            if (data.status == 0) {
                                swal(data.pesan, {
                                    icon: "warning",
                                });
                            } else {
                                swal(data.pesan, {
                                    icon: "success",
                                });
                                location.reload();
                            }
                        },
                        error: function(errors) {
                            swal("Upss.. Ada yang salah!", {
                                icon: "error",
                            });
                        },
                    })

            } else {
                swal("Pembatalan Verifikasi Tidak Dilakukan");
            }
        });
    }

    function lihatCatatan(usulanId) {
        // console.log(usulanId)
        $('#modal_lihat_catatan_usulan').modal('show')
        var url = '{{ url('kepegawaian-untan/usulan/') }}'
        $.getJSON(url+"/"+usulanId+"/lihat-catatan",function (data) {
                // console.log(data.data.lihat_catatan)
                $('#tableLihatCatatan > tbody').empty()
                $.each(data.data.lihat_catatan, function (indexInArray, valueOfElement) { 
                    console.log(valueOfElement)
                    if (valueOfElement.status == '0') {
                        $('#tableLihatCatatan > tbody').append('<tr><td>'+valueOfElement.isi_catatan+'</td><td><span class="label label-danger">Belum Selesai</span></td></tr>')
                    } else {
                        $('#tableLihatCatatan > tbody').append('<tr><td>'+valueOfElement.isi_catatan+'</td><td><span class="label label-flat border-success text-success-600 position-right">Selesai</span></td></tr>')
                    }
                });
                // $('#tableLihatCatatan > tbody > tr').each(function (data, index) { 
                //     console.log(index)
                // })
            }
        );
    }
    
    $('#tombolTambahCatatanBaru').click(function (e) { 
        e.preventDefault();
        $('#modal_lihat_catatan_usulan').modal('toggle')
        $('#modal_tambah_catatan_baru').modal('show')
    });

    function kirimUsulan(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-untan/usulan/kirim') }}'
        swal({
            title: "Kirim Usulan?",
            text: "Tandai Usulan Telah Dikirim Ke Dikti",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'POST',
                    url: url + '/' + id,
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                        success: function(data) {
                            swal("Data berhasil dikirim!", {
                                icon: "success",
                            });
                            location.reload();
                        },
                        error: function(errors) {
                            swal("Usulan Gagal Dikirim!", {
                                icon: "warning",
                            });
                        },
                    })

            } else {
                swal("Usulan Tidak Dikirim");
            }
        });
    }

    function skTerbit(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-untan/usulan/sk-terbit') }}'
        swal({
            title: "SK Telah Terbit?",
            text: "Tandai usulan bahwa SK telah terbit",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url + '/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                    success: function(data) {
                        swal("SK Telah Terbit!", {
                            icon: "success",
                        });
                        window.location.replace("{{ route('kepegawaian-untan.usulan.terbit.index') }}");
                        // location.reload();
                    },
                    error: function(errors) {
                        swal("Gagal!", {
                            icon: "warning",
                        });
                    },
                })

        } else {
            swal("Gagal");
        }
    });
}

    function cetak_surat_pengantar(id) {
        var url_cek = '{{ url('cek/surat_pengantar_bup') }}'
        $.get( url_cek + '/' + id, function( data ) {
            // console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_pengantar').addClass('hidden')
                $('#nomor_surat_pengantar').prop('required',true)
                $('#tanggal_surat_pengantar_baru').prop('required',true)
            } else {
                //jika ada recent waktu
                $('#nomor_surat_pengantar').val(data.no_surat)
                $('#waktu_lama_surat_pengantar').val(data.waktu)
            }
        });

        $('#modal_surat_pengantar').modal('show')

        $('#pilih_waktu_lama_surat_pengantar').on('change', function () {
            $('.form_tanggal_baru_pengantar').addClass('hidden')
            $('#tanggal_surat_pengantar_baru').removeAttr('required')
        })
    }

    function cetak_dpcp(id) {
        var cekDpcp = '{{ url('cek/dpcp') }}'
        $.get( cekDpcp + '/' + id, function( data ) {
            console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_dpcp').addClass('hidden')
                $('#tanggal_dpcp').prop('required',true);
            } else {
                //jika ada recent waktu
                $('#waktu_lama_dpcp').val(data);
            }
        });
        $('#modal_dpcp').modal('show')

        $('#pilih_waktu_lama_dpcp').on('change', function () {
            $('.form_tanggal_baru_dpcp').addClass('hidden')
            $('#tanggal_dpcp').removeAttr('required');
        })
    }

    function cetak_sp4(id) {
        var cekDpcp = '{{ url('cek/sp4') }}'
        $.get( cekDpcp + '/' + id, function( data ) {
            // console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_sp4').addClass('hidden')
                $('#tanggal_sp4').prop('required',true);
            } else {
                //jika ada recent waktu
                $('#waktu_lama_sp4').val(data);
            }
        });
        $('#modal_sp4').modal('show')

        $('#pilih_waktu_lama_sp4').on('change', function () {
            $('.form_tanggal_baru_sp4').addClass('hidden')
        })
    }
</script>
@endpush