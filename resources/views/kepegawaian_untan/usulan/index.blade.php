@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Usulan</li>
        {{-- <li class="active">Log Usulan {{ $usulan->first()->kode_usulan }}</li> --}}
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><b>Data Usulan Unit Kerja {{ auth()->user()->fakultas->nama_fakultas }}</b>
                    </h5>
                    <div class="heading-elements">
                        {{-- <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button> --}}
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table datatable-show-all datatable-basic">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Usulan</th>
                                <th>Jenis Usulan</th>
                                <th width="">Nama Pengusul</th>
                                <th>NIP</th>
                                <th>Unit Kerja</th>
                                <th>Waktu Usulan Diajukan</th>
                                <th>Status Usulan Untan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usulans as $usulan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a
                                        href="{{ route('kepegawaian-untan.usulan.kelola_usulan', ['id'=>$usulan->id]) }}">{{ $usulan->kode_usulan }}</a>
                                </td>
                                <td>{{ $usulan->jenisPensiun->nama }}</td>
                                <td>{{ $usulan->pegawai->NAMA_LENGKAP }}</td>
                                <td>{{ $usulan->pegawai->NIP }}</td>
                                <td>{{ $usulan->pegawai->Unit_Kerja }}</td>
                                <td>{{ $usulan->created_at }}</td>
                                <td>
                                    @if ($usulan->status_usulan_kepeg_untan == 5)
                                    <span class="label bg-warning">Dikembalikan</span>
                                    @endif

                                    @if ($usulan->status_usulan_kepeg_untan == 3)
                                    <span class="label bg-success">Dikirim Ke Dikti</span>
                                    @endif

                                    @if ($usulan->status_usulan_kepeg_untan == 2)
                                    <span class="label bg-success">Diverfikasi</span>
                                    @endif

                                    @if ($usulan->status_usulan_kepeg_untan == 1)
                                    <span class="label label-default">Diproses</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#usulan').addClass('active');
</script>
<script>
    function verifUsulan(id) {
        console.log('verif' + id)
        var url = '{{ url('kepegawaian-untan/usulan/verifikasi') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if (data == 1) {
                    toastr.success('Usulan berhasil di verfikasi!', 'Sukses', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'verified') {
                    toastr.error('Usulan telah di verfikasi sebelumnya!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'finished') {
                    toastr.error('Usulan telah diselesaikan!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'error') {
                    toastr.error('Terjadi kesalahan!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {
                        timeOut: 5000
                    });
                }
            }
        })
    }

    function kirimUsulan(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-untan/usulan/kirim') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if (data == 1) {
                    toastr.success('Usulan berhasil di tandai kirim!', 'Sukses', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'sended') {
                    toastr.error('Usulan telah di tandai kirim sebelumnya!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'not verified') {
                    toastr.error('Usulan belum diverifikasi!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else if (data == 'error') {
                    toastr.error('Terjadi kesalahan!', 'Error', {
                        timeOut: 5000
                    });
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {
                        timeOut: 5000
                    });
                }
            }
        })
    }

</script>
@endpush
