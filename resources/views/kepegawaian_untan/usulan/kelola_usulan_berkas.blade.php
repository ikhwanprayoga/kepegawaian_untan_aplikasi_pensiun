@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Berkas')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
            <li class=""><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class=""><a href="{{ route('kepegawaian-untan.usulan.index') }}"> Usulan</a></li>
            <li class=""><a href="{{ route('kepegawaian-untan.usulan.kelola_usulan', ['id' => $usulan->id]) }}"> Kelola Usulan {{ $usulan->kode_usulan }}</a></li>
            <li class="active">Kelola Berkas Usulan </li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    @if ($cekCatatan > 0)  
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><b>Catatan Untuk Usulan {{$usulan->dosen->NAMA_LENGKAP}}</b></h5>
                </div>
                <div class="panel-body">
                    <div class="row">
                        @foreach ($usulan->berkasUsulanPensiun->where('status_verifikasi_kepeg_fakultas', 0) as $bup)
                        <div class="col-lg-12" style="padding-bottom: 6px;">
                            <div class="media-body">
                                <table class="table">
                                    <tr>
                                        <td colspan="2" style=" background-color: #dfe1e2;">
                                            <p style="margin-bottom: 1px; font-size: 14px"><b>{{ $bup->masterBerkasPensiun->judul_berkas }}</b></p>
                                        </td>
                                    </tr>
                                    @foreach ($bup->catatanBerkas->where('status_kepeg_fakultas', 0) as $cb)
                                    <tr>
                                        <td><span class="label label-warning">Belum Selesai</span></td>
                                        <td style="padding-left: 5px"><span class="display-block text-muted">&nbsp; {{ $cb->catatan }}</span></td>
                                    </tr>
                                    @endforeach
                                    
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <button type="submit" onclick="kirimPesan({{$usulan->id}})" class="btn btn-primary">Kirim Notifikasi Ke Operator Kepegawaian Fakultas</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        @foreach ($usulan->jenisPensiun->berkasJenisPensiun as $item)
        <div class="col-lg-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">{{ $item->masterBerkasPensiun->judul_berkas }}</h6>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @foreach ($item->masterBerkasPensiun->berkasUsulanPensiun->where('usulan_pensiun_id', $usulan->id) as $val)
                                <p>
                                    <a target="_blank" href="{{ Storage::url('public/pensiun/'.$val->berkas->direktori.'/'.$val->berkas->nama_berkas) }}">
                                        <button type="button" class="btn btn-info btn-labeled btn-xs"><b><i class="icon-eye"></i></b> Lihat</button>
                                    </a>
                                    {{-- Modal Button --}}
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#{{ $item->masterBerkasPensiun->id }}">
                                        + Catatan
                                    </button>

                                    {{-- Modal --}}
                                    <div class="modal fade" id="{{ $item->masterBerkasPensiun->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <form action="{{ route('kepegawaian-untan.usulan.send') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="berkas_usulan_pensiun_id" value="{{ $val->id }}">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Catatan Untuk {{ $item->masterBerkasPensiun->judul_berkas }}</h4>
                                                    </div>

                                                    @if ($usulan->status_usulan_kepeg_untan == 2 || $usulan->status_usulan_kepeg_untan == 3)
                                                        
                                                    @else
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            {{-- <label for="cr">Catatan Revisi</label> --}}
                                                            <textarea class="wysihtml5 wysihtml5-min form-control" cols="18" rows="18" name="catatanRevisi" id="cr"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Kirim Catatan</button>
                                                    </div>
                                                    @endif
                                                    
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <table class="table">
                                                                <tbody>
                                                                    @foreach ($val->catatanBerkas as $item)
                                                                    <tr>
                                                                        <td scope="row">{{ $loop->iteration }}</td>
                                                                        <td>{!! $item->catatan !!}</td>
                                                                        <td>
                                                                            @if ($item->status_kepeg_fakultas == 1)
                                                                            <span class="label label-success">Selesai</span>
                                                                            @else
                                                                            <span class="label label-warning">Belum Selesai</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- Modal End --}}
                                    <label class="heading-elements">
                                        @if ($val->status_verifikasi_kepeg_fakultas == 1)
                                        <span class="label bg-success" style="margin-bottom: 9px;">Terverifikasi fakultas</span> &nbsp;&nbsp;&nbsp;<br>
                                        @else
                                        <span class="label bg-warning" style="margin-bottom: 9px;">Belum Verifikasi fakultas</span> &nbsp;&nbsp;&nbsp;<br>
                                        @endif
                                        {{-- {{ $val->status_verifikasi_kepeg_fakultas == 1 ? print '<span class="label bg-success">Verified</span>' : ''  }} --}}
                                        @if ($usulan->status_usulan_kepeg_untan == 2 || $usulan->status_usulan_kepeg_untan == 3)
                                        <span class="label bg-success">Terverifikas Untan</span> &nbsp;&nbsp;&nbsp;
                                        @else    
                                        <input type="checkbox" class="switchery pull-right" onclick="verfikasi_berkas({{$val->id}})" {{ $val->status_verifikasi_kepeg_untan == 1 ? 'checked="checked"' : ''  }} >
                                        Verifikasi Berkas
                                        @endif
                                    </label>
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>

<script>
    $('#usulan').addClass('active');
</script>
<script>
    function verfikasi_berkas(id) {
        // console.log('blade ' + id)
        var url = '{{ url('kepegawaian-untan/usulan/kelola/berkas/verifikasi') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if(data.status == 1){
                    toastr.success(data.pesan, 'Sukses', {timeOut: 5000});
                    location.reload();
                } else if (data.status == 2) {
                    toastr.success(data.pesan, 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error(data.pesan, 'Error', {timeOut: 5000});
                    location.reload();
                }
            }
        })
    }

    function kirimPesan(id) {
        console.log('blade ' + id)
        var url = '{{ url('kepegawaian-untan/usulan/kirim-notif') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if(data.status == 1){
                    toastr.success(data.pesan, 'Sukses', {timeOut: 5000});
                    // location.reload();
                } else {
                    toastr.error(data.pesan, 'Error', {timeOut: 5000});
                    // location.reload();
                }
            }
        })
    }
</script>

@endpush