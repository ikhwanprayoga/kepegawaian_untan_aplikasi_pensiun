<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>

    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')
    
    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal landscape
        }

    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="padding-10mm">

        <!-- Write HTML just like a web page -->

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" width="1209px">
            <tbody>
                <tr>
                {{-- <td colspan="30" style="width: 877.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="99.65928449744463%">
                    </span><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;color:white;">ANAK LAMPIRAN I</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;color:white;">KEPUTUSAN KEPALA BADAN&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;color:white;">KEPEGAWAIAN NEGARA</span></p>
                </td> --}}
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="30" style="width: 877.25pt;padding: 0in 5.4pt;height: 14.9pt;vertical-align: top;" valign="top" width="99.65928449744463%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;color:white;">&nbsp;2003</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="30" style="width: 877.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="99.65928449744463%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong>badan kepegawaian negara</strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">instansi</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: &nbsp;</span><strong><span style="font-size:11px;">KEMENTERIAN &nbsp;RISET, &nbsp;TEKNOLOGI, &nbsp;DAN PENDDIKAN TINGGI</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">provinsi</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: &nbsp;<strong>KALIMANTAN BARAT</strong></span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">kab/kota</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: &nbsp;<strong>PONTIANAK</strong></span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">unit kerja</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: &nbsp;<strong>universitas&nbsp;</strong></span><strong><span style="font-size:11px;">&nbsp;</span></strong><strong><span style="font-size:11px;">tanjungpura</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">pembayaran</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: &nbsp;<strong>PT. TASPEN (PERSERO) CABANG&nbsp;</strong></span><strong><span style="font-size:11px;">&nbsp;</span></strong><strong><span style="font-size:11px;">PONTIANAK</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">b</span></strong><strong><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">up</span></strong></p>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">:&nbsp;</span><span style="font-size:11px;">&nbsp;</span><strong><span style="font-size:11px;">58</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="3" style="width: 68.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="7.836456558773424%">
                    <h1 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:16px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-size:11px;font-weight:normal;">&nbsp;</span></h1>
                </td>
                <td colspan="27" style="width: 808.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="91.82282793867121%">
                    <h1 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:16px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-size:11px;font-family:&quot;Times New Roman&quot;,serif;font-weight:normal;">&nbsp;</span></h1>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="30" style="width: 877.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="99.65928449744463%">
                    <h1 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:16px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-family:&quot;Times New Roman&quot;,serif;">DATA PERORANGAN CALON PENERIMA PENSIUN (DPCP) PEGAWAI NEGERI SIPIL YANG MENCAPAI BATAS USIA PENSIU</span><span style="font-family:&quot;Times New Roman&quot;,serif;">N</span></h1>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="5" style="width: 131.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.906303236797275%">
                    <h1 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:16px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-size:11px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</span></h1>
                </td>
                <td colspan="25" style="width: 746pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="84.75298126064736%">
                    <h1 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:16px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-size:11px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</span></h1>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><strong><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">1</span></strong><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">.</span></p>
                </td>
                <td colspan="5" style="width: 148.6pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="16.86541737649063%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">KETERANGAN PRIBADI</span></strong></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">2.</span></strong></p>
                </td>
                <td colspan="16" style="width:467.9pt;padding:0in 5.4pt 0in 5.4pt;" width="53.15161839863714%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">KETERANGAN KELUARGA</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.216538789428815%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">A.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.492753623188406%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">N A M A</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.06393861892583%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">:&nbsp;</span><span style="font-size:11px;">ahmad</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">a.</span></strong></p>
                </td>
                <td colspan="15" style="width:446.6pt;padding:0in 5.4pt 0in 5.4pt;" width="50.72463768115942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">ISTRI/
                        <s>SUAMI</s>
                        </span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.3410059676044331%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">B.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">N I P</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: 131654337</span><span style="font-size:11px;">&nbsp;/</span><span style="font-size:11px;">&nbsp;196207051987011001</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td rowspan="2" style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" rowspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">nO</span></strong><strong><span style="font-size:10px;">.</span></strong></p>
                </td>
                <td colspan="3" rowspan="2" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">NIK</span></strong></p>
                </td>
                <td rowspan="2" style="width:70.95pt;padding:0in 5.4pt 0in 5.4pt;" width="8.09199318568995%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">NAMA</span></strong></p>
                </td>
                <td colspan="3" rowspan="2" style="width:70.85pt;padding:0in 5.4pt 0in 5.4pt;" width="8.006814310051107%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">TGL.LAHIR</span></strong></p>
                </td>
                <td colspan="3" rowspan="2" style="width:70.9pt;padding:0in 5.4pt 0in 5.4pt;" width="8.09199318568995%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">TGL.KAWIN</span></strong></p>
                </td>
                <td colspan="2" rowspan="2" style="width:75.75pt;padding:0in 5.4pt 0in 5.4pt;" width="8.603066439522998%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">TGL.CERAI/MD</span></strong></p>
                </td>
                <td rowspan="2" style="width:51.8pt;padding:0in 5.4pt 0in 5.4pt;" width="5.877342419080068%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:10px;">ISTERI KE</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="4.7272727272727275%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="5.090909090909091%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">C.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="30.90909090909091%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">TEMPAT/TANGGAL LAHIR</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="53.45454545454545%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: PONTIANAK / 5 JULI 1962</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="5.090909090909091%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.7272727272727273%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">D.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">JABATAN</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: PENGADMINISTRASI UMUM</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">1.</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">-</span></p>
                </td>
                <td style="width:70.95pt;padding:0in 5.4pt 0in 5.4pt;" width="8.09199318568995%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">habibah</span></p>
                </td>
                <td colspan="3" style="width:70.85pt;padding:0in 5.4pt 0in 5.4pt;" width="8.006814310051107%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">10-05-1960</span></p>
                </td>
                <td colspan="3" style="width:70.9pt;padding:0in 5.4pt 0in 5.4pt;" width="8.09199318568995%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">-</span></p>
                </td>
                <td colspan="2" style="width:75.75pt;padding:0in 5.4pt 0in 5.4pt;" width="8.603066439522998%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">12-09-2017</span></p>
                </td>
                <td style="width:51.8pt;padding:0in 5.4pt 0in 5.4pt;" width="5.877342419080068%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">1</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.216538789428815%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">E.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.492753623188406%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">PANGKAT/GOL. RUANG</span><span style="font-size:11px;">/TMT</span></p>
                </td>
                <td colspan="3" style="width: 120.55pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="13.72549019607843%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: PENGATUR TK. I/ II/d</span></p>
                </td>
                <td style="width: 40.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="4.518329070758738%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;"><span style="font-size:11px;">TMT</span></p>
                </td>
                <td colspan="2" style="width: 59.6pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="6.734867860187554%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: &nbsp;01-04-2015</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.239556692242114%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">2.</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.86615515771526%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">6112014402690013</span></p>
                </td>
                <td style="width:70.95pt;padding:0in 5.4pt 0in 5.4pt;" width="8.098891730605285%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">jusmiati</span></p>
                </td>
                <td colspan="3" style="width:70.85pt;padding:0in 5.4pt 0in 5.4pt;" width="8.013640238704177%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">01-02-1969</span></p>
                </td>
                <td colspan="3" style="width:70.9pt;padding:0in 5.4pt 0in 5.4pt;" width="8.098891730605285%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">01-10-2018</span></p>
                </td>
                <td colspan="2" style="width:75.75pt;padding:0in 5.4pt 0in 5.4pt;" width="8.610400682011935%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">-</span></p>
                </td>
                <td style="width:51.8pt;padding:0in 5.4pt 0in 5.4pt;" width="5.882352941176471%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">2</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.3410059676044331%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">F.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">GAJI POKOK TERAKHIR</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: RP. 3.480.700,-</span></p>
                </td>
                <td colspan="2" style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:70.95pt;padding:0in 5.4pt 0in 5.4pt;" width="8.09199318568995%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:70.85pt;padding:0in 5.4pt 0in 5.4pt;" width="8.006814310051107%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:70.9pt;padding:0in 5.4pt 0in 5.4pt;" width="8.09199318568995%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:75.75pt;padding:0in 5.4pt 0in 5.4pt;" width="8.603066439522998%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;margin-left:-10.7pt;text-align:center;text-indent:10.7pt;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:51.8pt;padding:0in 5.4pt 0in 5.4pt;" width="5.877342419080068%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.216538789428815%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">G.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.492753623188406%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">MASA KERJA&nbsp;</span><span style="font-size:11px;">KP terakhir</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.06393861892583%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: 22 TAHUN 03 BULAN</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">b.</span></strong></p>
                </td>
                <td colspan="15" style="width:446.6pt;padding:0in 5.4pt 0in 5.4pt;" width="50.72463768115942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">anak kandung</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.3410059676044331%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">h.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">masa kerja golongan</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: 2</span><span style="font-size:11px;">7 TAHUN 06 BULAN</span></p>
                </td>
                <td colspan="2" style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">nO</span></strong><strong><span style="font-size:11px;">.</span></strong></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">NIK</span></strong></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">NAMA</span></strong></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">TGL.LAHIR</span></strong></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">nama ayah / ibu</span></strong></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><strong><span style="font-size:11px;">keterangan</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">i</span><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">MASA KERJA&nbsp;</span><span style="font-size:11px;">pns</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: 33&nbsp;</span><span style="font-size:11px;">T</span><span style="font-size:11px;">AHUN 07 BULAN</span></p>
                </td>
                <td colspan="2" style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">1.</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">6112015203900014</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">sya’banniah</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">12-03-1990</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">ahmad/habibah</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">anak kandung</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">j</span><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">MASA KERJA&nbsp;</span><span style="font-size:11px;">pensiun</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: 33&nbsp;</span><span style="font-size:11px;">T</span><span style="font-size:11px;">AHUN 07</span><span style="font-size:11px;">&nbsp;</span><span style="font-size:11px;">BULAN</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">k.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">CLTN</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: -</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">l.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">peninjauan masa kerja</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">: -</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">m.</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">pendidikan dasar</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">pengan</span><span style="font-size:11px;">G</span><span style="font-size:11px;">katan pertama</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">:</span><span style="font-size:11px;">&nbsp;</span><span style="font-size:11px;">sd &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; lulus tahun : &nbsp;1985</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;height: 4.6pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;height: 4.6pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;height: 4.6pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;height: 4.6pt;vertical-align: top;" valign="top" width="25.04258943781942%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;height: 4.6pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width:21.3pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:28.35pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="3.2367972742759794%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:78.0pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:106.3pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="12.095400340715502%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:56.7pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="6.473594548551959%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width:99.2pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:78.05pt;padding:0in 5.4pt 0in 5.4pt;height:4.6pt;" width="8.858603066439523%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;height: 18.75pt;vertical-align: top;" valign="top" width="2.216538789428815%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:  &quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;height: 18.75pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;height: 18.75pt;vertical-align: top;" valign="top" width="14.492753623188406%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="6" style="width: 220.25pt;padding: 0in 5.4pt;height: 18.75pt;vertical-align: top;" valign="top" width="25.06393861892583%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;height: 18.75pt;vertical-align: top;" valign="top" width="2.3870417732310316%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">3.</span></strong></p>
                </td>
                <td colspan="5" style="width: 99.3pt;padding: 0in 5.4pt;height: 18.75pt;vertical-align: top;" valign="top" width="11.253196930946292%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">alamat sesudah &nbsp;pensiun</span></strong></p>
                </td>
                <td colspan="11" style="width:368.6pt;padding:0in 5.4pt 0in 5.4pt;height:18.75pt;" width="41.85848252344416%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">jl. adisucipto nurul huda &nbsp;gg. h.saleh &nbsp; &nbsp; ii &nbsp;RT.003/RW. 005 KELURAHAN parit baru&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.3410059676044331%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 110.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="12.52129471890971%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width: 74.75pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="8.517887563884157%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width: 35.4pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="4.003407155025553%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">&nbsp;</span></strong></p>
                </td>
                <td colspan="5" style="width:99.3pt;padding:0in 5.4pt 0in 5.4pt;" width="11.243611584327088%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="7" style="width:233.85pt;padding:0in 5.4pt 0in 5.4pt;" width="26.57580919931857%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;kecamatan &nbsp;</span><span style="font-size:11px;">sungai &nbsp;raya kabupaten &nbsp;kubu raya</span></p>
                </td>
                <td colspan="4" style="width:134.75pt;padding:0in 5.4pt 0in 5.4pt;" width="15.332197614991482%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">provinsi KALIMANTAN BARAT</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width: 99.4pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="11.328790459965928%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="5" style="width: 120.85pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="13.713798977853493%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width: 21.3pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">4.</span></strong></p>
                </td>
                <td colspan="16" style="width:467.9pt;padding:0in 5.4pt 0in 5.4pt;" width="53.15161839863714%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong><span style="font-size:11px;">demikian dpcp ini dibuat dengan sebenarnya dipergunakan sebagaimana mestinya</span></strong></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td style="width: 19.2pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.2146507666098807%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;line-height:150%;"><span style="font-size:11px;line-height:150%;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td style="width: 21.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="2.385008517887564%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;font-family:&quot;Arial&quot;,sans-serif;">&nbsp;</span></p>
                </td>
                <td colspan="4" style="width: 127.5pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.480408858603067%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width: 99.4pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="11.328790459965928%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="5" style="width: 120.85pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="13.713798977853493%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="width: 11.8pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="1.362862010221465%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="3" style="width: 52.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="5.877342419080068%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="2" style="width:14.2pt;padding:0in 5.4pt 0in 5.4pt;" width="1.6183986371379897%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="12" style="width:411.1pt;padding:0in 5.4pt 0in 5.4pt;" width="46.67802385008518%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td style="border:none;padding:0in 0in 0in 0in;" width="0.34071550255536626%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</p>
                </td>
                </tr>
                <tr>
                <td colspan="4" style="width: 128.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.565587734241909%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="8" style="width: 259.95pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="29.557069846678022%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="10" style="width: 238.8pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="27.086882453151617%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">MENGETAHUI :</span></p>
                </td>
                <td colspan="9" style="width: 253.15pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="28.79045996592845%">
                    <h3 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:11px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-family:&quot;Times New Roman&quot;,serif;">PONTIANAK, 29 APRIL&nbsp;</span><span style="font-family:&quot;Times New Roman&quot;,serif;">201</span><span style="font-family:&quot;Times New Roman&quot;,serif;">9</span></h3>
                </td>
                </tr>
                <tr>
                <td colspan="4" style="width: 128.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.565587734241909%">
                    <h3 style="margin:0in;margin-bottom:.0001pt;text-align:center;font-size:11px;font-family:&quot;Arial&quot;,sans-serif;"><span style="font-family:&quot;Times New Roman&quot;,serif;">&nbsp;</span></h3>
                </td>
                <td colspan="8" style="width: 259.95pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="29.557069846678022%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="10" style="width: 238.8pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="27.086882453151617%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">PEJABAT&nbsp;</span><span style="font-size:11px;">pengelola kepegawaian</span></p>
                </td>
                <td colspan="9" style="width: 253.15pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="28.79045996592845%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">PEGAWAI NEGERI SIPIL YANG BERSANGKUTAN</span></p>
                </td>
                </tr>
                <tr>
                <td colspan="4" style="width: 128.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.565587734241909%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="8" style="width: 259.95pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="29.557069846678022%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="10" style="width: 238.8pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="27.086882453151617%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">KEPALA&nbsp;</span><span style="font-size:11px;">&nbsp;biro umum dan keuangan</span></p>
                </td>
                <td colspan="9" style="width: 253.15pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="28.79045996592845%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                </tr>
                <tr>
                <td colspan="4" style="width: 128.1pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="14.565587734241909%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="8" style="width: 259.95pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="29.557069846678022%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                </td>
                <td colspan="10" style="width: 238.8pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="27.086882453151617%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">(</span><span style="font-size:11px;">d</span><span style="font-size:11px;">ra.&nbsp;</span><span style="font-size:11px;">HERILASTI PUJININGSIH</span><span style="font-size:11px;">, m.</span><span style="font-size:11px;">M</span><span style="font-size:11px;">)</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">NIP196809171987022001</span></p>
                </td>
                <td colspan="9" style="width: 253.15pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="28.79045996592845%">
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">&nbsp;</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">(</span><span style="font-size:11px;">&nbsp;</span><span style="font-size:11px;">ahmad</span><span style="font-size:11px;">&nbsp;</span><span style="font-size:11px;">)</span></p>
                    <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;"><span style="font-size:11px;">NIP196207051987011001</span></p>
                </td>
                </tr>
            </tbody>
        </table>

    </section>

</body>

</html>
