@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Kelola Usulan')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-untan.usulan.terbit.index') }}"> SK Usulan Terbit</a></li>
        <li class="active">Kelola Usulan {{ $usulan->first()->kode_usulan }}</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- data pribadi pengusul -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data pribadi pengusul</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <ul class="icons-list">
                                <a href="{{ route('kepegawaian-untan.usulan.lihat_data_pegawai', ['id' => $usulan->id, 'nip' => $dosen->NIP]) }}" class="btn btn-primary btn-sm">Lihat Data Pegawai</a>
                            </ul>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3" style="text-align:center;">
                            @if ($foto == 1 && $dosen->foto != null)
                                <img src="{{ Storage::url('public/pegawai/'.$dosen->foto) }}" style="width: 200px; height: 100%;" />
                            @else
                                <img src="{{asset('assets/images/allgender.png')}}" style="width: 200px; height: 100%;" />
                            @endif
                        </div>

                        <div class="col-lg-2">
                            <div id="content">
                                <p>Nama </p>
                                <p>Tempat, Tanggal Lahir</p>
                                <p>NIP</p>
                                <p>NIDN / NIDK </p>
                                <p>Alamat E-mail  </p>
                                <p>Jabatan Fungsional </p>
                                <p>Golongan Ruang </p> 
                                <p>Jurusan </p>
                                <p>Program Studi </p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div>
                                <p>:{{ $dosen->NAMA_LENGKAP }}</p>
                                <p>: {{ $dosen->Tempat_Lahir }},{{ $dosen->Tgl_Lahir }} </p>
                                <p>: {{ $dosen->NIP }}</p>
                                <p>: {{ $dosen->NIDN }}</p>
                                <p>: {{ $dosen->ALAMAT_EMAIL }}</p>
                                <p>: {{ $dosen->JAB_FUNGSIONAL}}</p>
                                <p>: {{ $dosen->Gol }}</p>
                                <p>: {{ $dosen->Jurusan }}</p>
                                <p>: {{ $dosen->Program_Studi}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /data pribadi pengusul -->

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Informasi Usulan Pensiun</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
					<div class="row">
                        <div class="col-md-12" style="text-align:center">

                            @if ($usulan->status_usulan_kepeg_untan == 4)
                            <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class=" icon-file-text"></i></b>SK Usulan Telah Terbit</button>
                            @endif

                        </div>
                    </div>
                </div>
            </div>

            <!-- daftar formulir -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data berkas</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
					<div class="row">
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Kelengkapan Pemberkasan</h5>
                                    <p class="mb-15">Kelengkapan berkas untuk jenis usulan pensiun {{ $usulan->jenisPensiun->nama }}</p>
                                    <a href="{{ route('kepegawaian-untan.usulan.terbit.kelola_berkas', ['id'=>$usulan->id]) }}" class="btn bg-success-400">Lihat Daftar Berkas</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Lihat Berkas DPCP</h5>
                                    <p class="mb-15">Berkas DPCP yang telah di acc pimpinan dapat di lihat disini</p>
                                    {{-- <button type="button" data-toggle="modal" data-target="#upload_dpcp" class="btn bg-success-400">Upload DPCP</button> --}}
                                    @if($usulan->berkasStepAkhir)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_dpcp) }}">
                                            <button type="button" class="btn btn-info">Lihat DPCP</button>
                                        </a>
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_sp4) }}">
                                            <button type="button" class="btn btn-info">Lihat SP4</button>
                                        </a>
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_surat_pengantar) }}">
                                            <button type="button" class="btn btn-info">Lihat Surat Pengantar</button>
                                        </a>
                                        {{-- <button type="button" data-toggle="modal" data-target="#surat_pengantar" class="btn bg-success-400">Cetak Surat Pengantar</button> --}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            @if ($usulan->status_usulan_kepeg_untan == 3 || $usulan->status_usulan_kepeg_untan == 4)
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Arsip Berkas SK Pensiun</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        @if ($usulan->status_usulan_kepeg_untan == 3 || $usulan->status_usulan_kepeg_untan == 4)
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Informasi Nomor Resi Pengiriman Berkas Pensiun</h5>
                                    <p class="mb-15">Nomor Resi Pengiriman Berkas Pensiun Ke Dikti</p>
                                    @if($usulan->berkasStepAkhir)
                                    @if ($usulan->status_usulan_kepeg_untan != 4)
                                    <button type="button" data-toggle="modal" data-target="#input_resi_pengiriman" class="btn bg-success-400">Input No. Resi</button>
                                    @endif
                                    @if ($usulan->no_resi_pengiriman_berkas)
                                    <button type="button" data-toggle="modal" data-target="#lihat_resi_pengiriman" class="btn btn-info">Lihat No. Resi</button>
                                    @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Lihat SK Pensiun</h5>
                                    <p class="mb-15">SK dapat di lihat disini</p>
                                    @if($usulan->berkasStepAkhir)
                                    @if ($usulan->status_usulan_kepeg_untan != 4)
                                    <button type="button" data-toggle="modal" data-target="#upload_sk" class="btn bg-success-400">Upload SK</button>
                                    @endif
                                        @if ($usulan->berkasStepAkhir->file_sk)
                                        <a target="_blank" href="{{ Storage::url('public/pensiun/'.str_replace(' ', '_', $usulan->fakultas->nama_fakultas).'/'.$usulan->nip.'/'.$usulan->berkasStepAkhir->file_sk) }}">
                                            <button type="button" class="btn btn-info">Lihat SK</button>
                                        </a>
                                        @endif    
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <!-- /main content -->

    {{-- modal untuk lihat no resi --}}
    <div class="modal fade" id="lihat_resi_pengiriman" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nomor Resi Pengiriman Berkas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Nomor Resi Pengiriman Berkas</label>
                      <input type="text" id="" value="{{ $usulan->no_resi_pengiriman_berkas ? $usulan->no_resi_pengiriman_berkas: 'No Resi belum di masukan' }}" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Tanggal Pengiriman Berkas</label>
                      <input type="text" id="" value="{{ $usulan->tanggal_kirim_berkas ? date('d-m-Y', strtotime($usulan->tanggal_kirim_berkas)) : 'Berkas Belum Dikirim' }}" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Jasa Pengiriman Yang Digunakan</label>
                      <input type="text" value="{{ $usulan->jasa_pengiriman ? $usulan->jasa_pengiriman: 'Tidak Ada Data Yang Diinputkan' }}" id="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Jasa Pengiriman Berkas Yang Digunakan</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')

<script>
    $('#usulan_terbit').addClass('active');
</script>
<script>
function skTerbit(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-untan/usulan/sk-terbit') }}'
        swal({
            title: "SK Telah Terbit?",
            text: "Tandai usulan bahwa SK telah terbit",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url + '/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                    success: function(data) {
                        swal("SK Telah Terbit!", {
                            icon: "success",
                        });
                        location.reload();
                    },
                    error: function(errors) {
                        swal("Gagal!", {
                            icon: "warning",
                        });
                    },
                })

        } else {
            swal("Gagal");
        }
    });
}
</script>
@endpush