@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Berkas')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
            <li class=""><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li class=""><a href="{{ route('kepegawaian-untan.usulan.terbit.index') }}"> Usulan</a></li>
            <li class=""><a href="{{ route('kepegawaian-untan.usulan.terbit.kelola', ['id' => $usulan->id]) }}"> Kelola Usulan {{ $usulan->kode_usulan }}</a></li>
            <li class="active">Kelola Berkas Usulan </li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        @foreach ($usulan->jenisPensiun->berkasJenisPensiun as $item)
        <div class="col-lg-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">{{ $item->masterBerkasPensiun->judul_berkas }}</h6>
                </div>
                
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @foreach ($item->masterBerkasPensiun->berkasUsulanPensiun->where('usulan_pensiun_id', $usulan->id) as $val)
                                <p>
                                    <a target="_blank" href="{{ Storage::url('public/pensiun/'.$val->berkas->direktori.'/'.$val->berkas->nama_berkas) }}">
                                        <button type="button" class="btn btn-info btn-labeled btn-xs"><b><i class="icon-eye"></i></b> Lihat</button>
                                    </a>
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>

<script>
    $('#usulan_terbit').addClass('active');
</script>
<script>
    function verfikasi_berkas(id) {
        // console.log('blade ' + id)
        var url = '{{ url('kepegawaian-untan/usulan/kelola/berkas/verifikasi') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                // console.log(data)
                if(data == 1){
                    toastr.success('Berkas berhasil di verfikasi!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else if (data == 2) {
                    toastr.success('Berkas tidak di verfikasi!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});
                    
                }
            }
        })
    }
</script>

@endpush