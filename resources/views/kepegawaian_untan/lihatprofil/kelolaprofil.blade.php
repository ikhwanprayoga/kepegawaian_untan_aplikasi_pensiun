@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Kelola Profil</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">
<div class="panel panel-flat">
<div class="panel-heading">
    <form class="form-horizontal"  action="/kepegawaian-untan/kelolaprofil/{{ Auth::user()->id }}" method="POST"> 
        <input type="hidden" name="_method" value="PUT">
        @csrf
        <fieldset class="content-group">
        <legend class="text-bold">Kelola Profil</legend>
        <!-- gimane ni? wkwk udah ke? -->
        <div class="form-group">
            <label class="control-label col-lg-2">Name</label>
            <div class="col-lg-10"> 
                <input type="text" name="name"  class="form-control"value="{{ Auth::user()->name }}">
                <span style="color:red;">{{$errors->has('name') ? $errors->first('name', 'form nama wajib diisi!') : ''}}</span>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">Username</label>
            <div class="col-lg-10">
                <input type="text" name="username"  class="form-control" value="{{ Auth::user()->username }}">
                <span style="color:red;">{{$errors->has('username') ? $errors->first('username', 'form nama username diisi!') : ''}}</span>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">Password</label>
            <div class="col-lg-10">
                <input type="password" name="password" class="form-control" >
                <span style="color:red;">{{$errors->has('password') ? $errors->first('password', 'form password tidak cocok!') : ''}}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-2">Konfirmasi Password</label>
            <div class="col-lg-10">
                <input type="password" name="password_confirmation" class="form-control">
            </div>            
        </div>


        <div class="form-group">
            <label class="control-label col-lg-2">Email</label>
            <div class="col-lg-10">
                <input type="email" name="email"  class="form-control" value="{{ Auth::user()->email }}" >
                <span style="color:red;">{{ $errors->has('email') ? $errors->first('email', 'form nama email diisi!') : ''}}</span>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">No HP</label>
            <div class="col-lg-10">
                <input type="text" name="no_hp"  class="form-control" value="{{ Auth::user()->no_hp }}" >
                <span style="color:red;">{{ $errors->has('no_hp') ? $errors->first('no_hp', 'form nama email diisi!') : ''}}</span>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </div>
        </div>
        </form>

    @include('layouts.theme.footer')

@endsection

@push('js')
<script>
    // $('#beranda').addClass('active');
</script>
@endpush