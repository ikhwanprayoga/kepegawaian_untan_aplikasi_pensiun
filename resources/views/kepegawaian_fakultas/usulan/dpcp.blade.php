<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')

    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal landscape
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 0px solid black;
        }

        .table td, .table th {
            padding: 0.01rem;
            vertical-align: top;
            border-top: 0px solid #dee2e6;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 0px solid #dee2e6;
        }

    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">

        <!-- Write HTML just like a web page -->
        <font size="2" style="font-size: 12px;">
        <div class="row" style="margin-bottom: -16px;">
            <div class="col-lg-12">
                <table class="table">
                    <tbody style="font-weight:bold">
                        <tr>
                            <td colspan="2">
                                {{-- <img style="width: 89px;" src="{{ asset('assets/images/garuda.PNG') }}" alt="" srcset=""> --}}
                            </td>
                            <td>
                                <img style="width: 89px;" src="{{ asset('assets/images/garuda.PNG') }}" alt="" srcset="">

                            </td>
                        <tr>
                            <td>INSTANSI INDUK</td>
                            <td>: KEMENTRIAN RISET, TEKNOLOGI, DAN PENDIDIKAN TINGGI</td>
                        </tr>
                        <tr>
                            <td>PROVINSI</td>
                            <td>: KALIMANTAN BARAT</td>
                        </tr>
                        <tr>
                            <td>KAB/KOTA</td>
                            <td>: PONTIANAK</td>
                        </tr>
                        <tr>
                            <td>UNIT KERJA</td>
                            <td>: UNIVERSITAS TANJUNGPURA</td>
                        </tr>
                        <tr>
                            <td>PEMBAYARAN</td>
                            <td>: PT. TASPEN (PERSERO) CABANG PONTIANAK</td>
                        </tr>
                        <tr>
                            <td>BUP</td>
                            <td>: 68</td>
                        </tr>
                        <tr align="center">
                            <td colspan="2" style="font-size: 18px; padding: 7px 0px 0px 0px;">DATA PERORANGAN CALON PENERIMA PENSIUN (DPCP) PEGAWAI NEGERI SIPIL YANG MENCAPAI BATAS USIA PENSIUN</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        {{-- keterangan pribadi --}}
        <div class="row" style="padding: 0px 40px 0px 0px;">
            <div class="col-lg-6">
                <table class="table">
                    <tbody>
                        <tr style="font-weight:bold">
                            <td>1.</td>
                            <td colspan="3">KETERANGAN PRIBADI</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>A.</td>
                            <td>NAMA</td>
                            <td>: {{ $pegawai->NAMA_LENGKAP }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>B.</td>
                            <td>NIP</td>
                            <td>: {{ $pegawai->NIDN }} / {{ $pegawai->NIP }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>C.</td>
                            <td>TEMPAT/TANGGAL LAHIR</td>
                            <td>: {{ $pegawai->Tempat_Lahir }}, {{ date('d-m-Y',strtotime($pegawai->Tgl_Lahir)) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>D.</td>
                            <td>JABATAN</td>
                            <td>: {{ $pegawai->JAB_FUNGSIONAL }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>E.</td>
                            <td>PANGKAT/GOL. RUANG/TMT</td>
                            <td>: {{ $pegawai->pangkat }}/{{ $pegawai->Gol}} &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; TMT : {{ $pegawai->tmt }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>F.</td>
                            <td>GAJI POKOK TERAKHIR</td>
                            <td>: {{ $pegawai->gaji_pokok_terakhir }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>G.</td>
                            <td>MASA KERJA KP TERAKHIR</td>
                            <td>: {{ $pegawai->masa_kerja_kp_terakhir }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>H.</td>
                            <td>MASA KERJA GOLONGAN</td>
                            <td>: {{ $pegawai->masa_kerja_golongan }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>I.</td>
                            <td>MASA KERJA PNS</td>
                            <td>: {{ $pegawai->masa_kerja_pns }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>J.</td>
                            <td>MASA KERJA PENSIUN</td>
                            <td>: {{ $pegawai->masa_kerja_pensiun }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>K.</td>
                            <td>MASA KERJA CLTN</td>
                            <td>: {{ $pegawai->cltn }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>L.</td>
                            <td>PENINJAUAN MASA KERJA</td>
                            <td>: {{ $pegawai->peninjauan_masa_kerja }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>M.</td>
                            <td>PENDIDIKAN DASAR</td>
                            <td>: SD {{ $pegawai->pendidikan_dasar }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>PENGANGKATAN PERTAMA</td>
                            <td>: LULUS TAHUN : {{ $pegawai->lulus_tahun }} </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table">
                        <thead >
                            <tr>
                                <th>2.</th>
                                <th colspan="8">KETERANGAN KELUARGA</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>A.</th>
                                <th colspan="6">ISTRI/SUAMI</th>
                            </tr>
                            <tr align="center">
                                <th></th>
                                <th></th>
                                <th>NO.</th>
                                <th>NAMA</th>
                                <th>TGL. LAHIR</th>
                                <th>TGL. KAWIN</th>
                                <th>ISTRI/SUAMI KE</th>
                            </tr>
                        </thead>
                            <tbody>
                            @foreach ($keluarga as $data)
                            <tr align="center">
                                <td></td>
                                <td></td>
                                <td>{{ $data->id }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ date('d-m-Y',strtotime($data->tanggal_lahir ))}}</td>
                                <td>{{ date('d-m-Y',strtotime($data->tanggal_kawin ))}}</td>
                                <td>{{ $data->pasangan_keberapa }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                    </table>
            </div>
            {{-- data keluarga --}}
            <div class="col-lg-6">
                <div class="row">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 17px;"></th>
                                <th>B.</th>
                                <th colspan="6">ANAK-ANAK</th>
                            </tr>
                            <tr align="center">
                                <th></th>
                                <th></th>
                                <th>NO.</th>
                                <th>NAMA</th>
                                <th>TGL. LAHIR</th>
                                <th>KANDUNG</th>
                                <th>TIRI</th>
                                <th>ANGKAT</th>
                                <th>NAMA AYAH/IBU</th>
                            </tr>
                        </thead>
                        <tbody  align="center">
                        @foreach ($dataAnak as $data)
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{ $data->id }}</td>
                                <td>{{ $data->nik }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ date('d-m-Y',strtotime($data->tanggal_lahir))}}</td>
                                <td>{{ $data->nama_ayah }}/{{ $data->nama_ibu }}</td>
                                <td>ANAK KANDUNG</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <table class="table">
                        <thead>
                            <tr style="font-weight:bold">
                                <th style="width: 17px;"> 3.</td>
                                <th colspan="3">ALAMAT</th>
                            </tr>
                            <tr>
                                <td style="width: 17px;"></td>
                                <td style="width: 17px;">A.</td>
                                <td>ALAMAT SEKARANG : </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="width: 17px;"></td>
                                <td style="width: 17px;"></td>
                                <td>KECAMATAN</td>
                                <td>KAB/KOTA</td>
                                <td>PROPINSI</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>B.</td>
                                <td>ALAMAT SESUDAH PENSIUN : </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td style="width: 17px;"></td>
                                <td style="width: 17px;"></td>
                                <td>KECAMATAN</td>
                                <td>KAB/KOTA</td>
                                <td>PROPINSI</td>
                            </tr>
                        </thead>
                    </table>

                    <table class="table">
                        <thead>
                            <tr style="font-weight:bold">
                                <td style="width: 17px;">4.</td>
                                <td colspan="8">DENGAN INI SAYA MENYATAKAN AKAN MENGEMBALIKAN SELURUH BARANG INVENTARIS MILIK NEGARA SETELAH
                                DIBERHENTIKAN DENGAN HORMAT SEBAGAI PEGAWAI NEGERI SIPIL DAN APABILA SAYA TIDAK MEMATUHINYA SAYA BERSEDIA
                                DIAMBIL TINDAKAN SESUAI PERATURAN PERUNDANG-UNDANGAN YANG BERLAKU</td>
                            </tr>
                        </thead>
                    </table>

                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 17px;">5.</th>
                                <th colspan="8">DEMIKIAN DATA INI DBUAT DENGAN SEBENARNYA </th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6"></div>
            <div class="col-lg-6">
                <div class="row">
                    {{-- <table class="table">
                        <thead>
                        <tr style="font-weight:bold">
                            <td>3.</td>
                            <td colspan="3">ALAMAT</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>A.</td>
                            <td>ALAMAT SEKARANG</td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>B.</td>
                            <td>ALAMAT SESUDAH PENSIUN</td>
                            <td>: </td>
                        </tr>
                        </thead>
                    </table> --}}
                    {{-- <table class="table">
                        <thead>
                            <tr>
                                <th>4.</th>
                                <th colspan="8">DEMIKIAN CPCP INI DIBUAT DENGAN SEBENARNYA DIPERGUNAKAN SEBAGAIMANA MESTINYA</th>
                            </tr>
                        </thead>
                    </table> --}}
                    <table class="table">
                        <tbody align="center">
                            <tr >
                                <td>MENGETAHUI</td>
                                <td>PONTIANAK, {{ Carbon\Carbon::parse(date('Y-m-d'))->formatLocalized('%d %B %Y')}}</td>
                            </tr>
                            <tr>
                                <td>PEJABAT PENGELOLA KEPEGAWAIAN</td>
                                <td>PEGAWAI NEGERI SIPIL YANG BERSANGKUTAN</td>
                            </tr>
                            <tr>
                                <td>KEPALA BIRO UMUM DAN KEUANGAN</td>
                                <td></td>
                            </tr>
                            <tr style="height: 60px;">
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td>(Dra. HERLILASTI PURJININGSIH, M.M)</td>
                                <td>({{ $pegawai->NAMA_LENGKAP  }})</td>
                            </tr>
                            <tr>
                                <td>NIP. 19289282828282</td>
                                <td>NIP. {{ $pegawai->NIP }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script>
        {{-- window.print() --}}
    </script>

</body>

</html>
