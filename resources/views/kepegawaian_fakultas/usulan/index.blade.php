@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Usulan</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Daftar Usulan Pensiun</h5>
                    <div class="heading-elements">
                        <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table datatable-show-all datatable-basic">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Usulan</th>
                                <th>Jenis Usulan</th>
                                <th width="">Nama Pengusul</th>
                                <th>NIP</th>
                                <th>Waktu Usulan Diajukan</th>
                                <th>Status Usulan Fakultas</th>
                                <th>Status Usulan Untan</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usulans as $usulan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('kepegawaian-fakultas.usulan.kelola', ['id'=>$usulan->id]) }}">{{ $usulan->kode_usulan }}</a></td>
                                <td>{{ $usulan->jenisPensiun->nama }}</td>
                                <td>{{ $usulan->pegawai->NAMA_LENGKAP }}</td>
                                <td>{{ $usulan->pegawai->NIP }}</td>
                                <td>{{ $usulan->created_at }}</td>
                                <td>
                                    @if ($usulan->status_usulan_kepeg_fakultas == 0)
                                        <span class="label label-default">Proses</span>
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_fakultas == 1)
                                        <span class="label bg-primary">Verified</span>
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_fakultas == 2 )
                                        <span class="label bg-primary">Verified</span>
                                        <span class="label bg-success">Dikirim</span>
                                    @endif
                                    {{-- @if ($usulan->status_usulan_kepeg_fakultas == 3)
                                        <span class="label bg-warning">Dikembalikan</span>
                                    @endif --}}
                                </td>
                                <td>
                                    @if ($usulan->status_usulan_kepeg_untan == 1)
                                        <span class="label bg-primary">Diterima</span>

                                        @if (Helpers::cekCatatanUsulanPensiun($usulan->id) == 1)
                                        <a href="{{ route('kepegawaian-fakultas.usulan.kelola.berkas', ['id'=>$usulan->id]) }}">
                                            <span class="label bg-danger">Terdapat Catatan</span>
                                        </a>
                                        @endif
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_untan == 2)
                                        <span class="label bg-success">Diverfikasi</span>
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_untan == 3)
                                        <span class="label bg-success">Dikirim Ke Dikti</span>
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_untan == 5)
                                        <span class="label bg-warning">Dikembalikan</span>
                                        <span class="label bg-primary" data-id="{{$usulan->id}}" id="lihatCatatan" title="Klik untuk melihat catatan kenapa usulan dikembalikan">Lihat Catatan</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                {{-- <li><a href="#" onclick="verifFakultas({{$usulan->id}})" title="Verifikasi Usulan"><i class="icon-file-pdf"></i> Verifikasi Usulan</a></li> --}}
                                                {{-- <li><a href="#" onclick="kirimUsulan({{$usulan->id}})" title="Kirim usulan ke Kepegewaian UNTAN"><i class="icon-file-excel"></i> Kirim usulan</a></li> --}}
                                                @if ($usulan->status_usulan_kepeg_fakultas == 0)
                                                <li><a href="#" onclick="hapusUsulan({{$usulan->id}})" title="Hapus usulan"><i class="icon-file-excel"></i> Hapus usulan</a></li>
                                                @elseif ($usulan->status_usulan_kepeg_fakultas == 1)
                                                <li><a href="#" title="Usulan Telah Diverfikasi Oleh Kepegawaian Fakultas"><i class="icon-file-excel"></i> Usulan Diverfikasi</a></li>
                                                @else
                                                <li><a href="#" title="Usulan Telah Dikirim Ke Kepegawaian Untan"><i class="icon-file-excel"></i> Usulan Dikirim</a></li>
                                                @endif
                                                {{-- <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li> --}}
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /main content -->

    <!-- tambah usulan modal -->
    <div id="tambahUsulanModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Tambah Usulan Baru</h5>
                </div>

                <div class="modal-body">
                    <h6 class="text-semibold">Masukkan NIP atau Nama Pegawai</h6>
                    <div class="form-group">
                        <div class="col-lg-10">
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" list="hasilData" class="form-control" placeholder="NIP atau Nama" id="nipPegawai" autofocus>
                                <datalist id="hasilData">
                                </datalist>
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-base"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="btn btn-primary" id="cariDataPegawai">Cari</button>
                        </div>
                    </div>
                    <br><br>
                    <h6 class="text-semibold">Data Singkat Pegawai</h6>
                    <div class="form-group" id="dataSingkatPegawai">
                        <div class="row">
                            <div class="col-lg-4 col-xs-6">
                                <label for="">Nama</label>
                            </div>
                            <div class="col-lg-8 col-xs-6">
                                <label for="" id="namaPegawai"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-6">
                                <label for="">NIP</label>
                            </div>
                            <div class="col-lg-8 col-xs-6">
                                <label for="" id="NIPPegawai"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-6">
                                <label for="">NIDN</label>
                            </div>
                            <div class="col-lg-8 col-xs-6">
                                <label for="" id="nidnPegawai"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-6">
                                <label for="" >Tempat Tanggal Lahir</label>
                            </div>
                            <div class="col-lg-8 col-xs-6">
                                <label for="" id="tempatTanggalLahirPegawai"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-6">
                                <label for="">Unit Kerja</label>
                            </div>
                            <div class="col-lg-8 col-xs-6">
                                <label for="" id="unitKerjaPegawai"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-6">
                                <label for="">Alamat</label>
                            </div>
                            <div class="col-lg-8 col-xs-6">
                                <label for="" id="alamatPegawai"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Pensiun</label>
                        <select data-placeholder="Pilih jenis pensiun..." class="select-size-xs " id="jenisPensiun" disabled>
                            <option></option>
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal" id="tutupModal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="tambahUsulan">Tambah Usulan Baru</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

    {{-- modal untuk melihat catatan usulan yg di kembalikan --}}
    <div id="modal_lihat_catatan_usulan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Catatan Pengembalian Usulan</h4>
                </div>
                <form action="#" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="table-responsive pre-scrollable">
                            <table class="table" id="tableLihatCatatan">
                                <thead>
                                    <tr>
                                        <th>Catatan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        {{-- <button type="button" id="tombolTambahCatatanBaru" class="btn btn-primary">Tambah Catatan Baru</button> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>

<script>
    $('#usulan').addClass('active');

    // $(document).ready(function() {
    //     $('#tableUsulan').DataTable( {
    //         "scrollX": true
    //     } );
    // } );
    $("#tableUsulans").dataTable( {
        "responsive" : true
        // "sScrollX": '100%'
    } );
</script>
<script>
    //cari data pegewai dengan keyup nip dan nama pegawai
    var url_cari = '{{ url('kepegawaian-fakultas/search') }}';
    $('#nipPegawai').keyup(function () {
        var data = this.value
        $.get(url_cari + '/' + data, function (data) {
            $("#hasilData").empty();
            $.each(data, function (index, value) {
                $('#hasilData').append('<option value="'+value.NIP+'">'+value.NAMA_LENGKAP+'</option>');
            })
        })
    })
</script>
<script>
    var urlCariDataPegawai = '{{ url('kepegawaian-fakultas/usulan/cari-data-pegawai') }}';

    //tampilkan tambah usulan baru
    $('#tombolTambahUsulanBaru').on('click', function (e) {
        e.preventDefault();
        $('#tambahUsulanModal').modal('show');
    });

    $('#tutupModal').on('click', function () {
        $('#namaPegawai').text("");
        $('#NIPPegawai').text("");
        $('#nidnPegawai').text("");
        $('#tempatTanggalLahirPegawai').text("");
        $('#unitKerjaPegawai').text("");
        $('#alamatPegawai').text("");
        $('#jenisPensiun').prop("disabled", true);
    });

    $('#cariDataPegawai').on('click', function () {
        var nipPegawai = $('#nipPegawai').val();
        $.get(urlCariDataPegawai + '/' + nipPegawai, function (data) {
            console.log(data)
            if (data == 0) {
                // console.log('Data tidak ditemukan!')
                toastr.error('Data tidak ditemukan!', 'Error', {timeOut: 5000});
                $('#namaPegawai').text("");
                $('#NIPPegawai').text("");
                $('#nidnPegawai').text("");
                $('#tempatTanggalLahirPegawai').text("");
                $('#unitKerjaPegawai').text("");
                $('#alamatPegawai').text("");
            } else if(data == 2) {
                toastr.error('NIP pegawai yang anda masukan tidak berasal dari fakultas anda!', 'Error', {timeOut: 5000});
                $('#namaPegawai').text("");
                $('#NIPPegawai').text("");
                $('#nidnPegawai').text("");
                $('#tempatTanggalLahirPegawai').text("");
                $('#unitKerjaPegawai').text("");
                $('#alamatPegawai').text("");
            } else {
                $.get('{{ url('jenis-pensiun') }}', function (e) {
                    // console.log(e)
                    $('#jenisPensiun').prop("disabled", false);
                    $('#jenisPensiun').find('option').remove().end().append('<option></option>');
                    $.each(e, function (index, value) {
                        $('#jenisPensiun').append('<option value="'+value.id+'">'+value.nama+'</option>');
                    })
                });

                // $.each(data, function (index, value) {
                    // console.log(value)
                    toastr.success('Data berhasil ditemukan!', 'Sukses', {timeOut: 5000});
                    $('#namaPegawai').text("").text(data.NAMA_LENGKAP);
                    $('#NIPPegawai').text("").text(data.NIP);
                    $('#nidnPegawai').text("").text(data.NIDN);
                    $('#tempatTanggalLahirPegawai').text("").text(data.Tempat_Lahir + ', ' + data.Tgl_Lahir);
                    $('#unitKerjaPegawai').text("").text(data.Unit_Kerja);
                    $('#alamatPegawai').text("").text(data.ALAMAT);
                // });
            }
        });
    });

    $('#tambahUsulan').on('click', function () {
        var nip = $('#NIPPegawai').text();
        var jenisPensiun = $('#jenisPensiun').val();
        if (jenisPensiun == "") {
            toastr.error('Jenis pensiun tidak boleh kosong!', 'Error', {timeOut: 5000});
        } else {
            $.ajax({
                type: 'post',
                url: '{{ route('kepegawaian-fakultas.usulan.tambah') }}',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'nip' : nip,
                    'jenis_pensiun_id' : $('#jenisPensiun').val(),
                },
                success: function(data) {
                    // console.log(data)
                    if (data == 0) {
                        toastr.error('Sudah ada ajuan dengan nip yang sama!', 'Error', {timeOut: 5000});
                    } else {
                        if(data.errors){
                            toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});

                        } else {
                            toastr.success('Usulan berhasil ditambahkan!', 'Sukses', {timeOut: 5000});
                            var rUrl = data;
                            window.location.replace(rUrl);
                            window.location.href = rUrl;
                            // console.log(data);
                        }
                    }
                },
            })
        }
    });

    function verifFakultas(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-fakultas/usulan/verifikasi') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if(data == 1){
                    toastr.success('Usulan berhasil di verfikasi!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else if (data == 'verified') {
                    toastr.error('Usulan telah di verfikasi sebelumnya!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else if (data == 'sent') {
                    toastr.error('Usulan telah di kirim sebelumnya!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});
                }
            }
        })
    }

    function kirimUsulan(id) {
        // console.log('kirim' + id)
        var url = '{{ url('kepegawaian-fakultas/usulan/kirim') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if(data == 1){
                    toastr.success('Usulan berhasil di kirim ke kepegawaian UNTAN!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else if (data == 'sent') {
                    toastr.error('Usulan telah di kirim sebelumnya!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else if (data == 'not verified') {
                    toastr.error('Usulan belum di verfikasi sebelumnya!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});

                }
            }
        })
    }

    $('#lihatCatatan').click(function () {
        console.log('di clik')
        $('#modal_lihat_catatan_usulan').modal('show')
        var usulanId = $(this).data("id")
        var url = '{{ url('kepegawaian-fakultas/usulan/') }}'
        $.getJSON(url+"/"+usulanId+"/lihat-catatan",function (data) {
                // console.log(data.data.lihat_catatan)
                $('#tableLihatCatatan > tbody').empty()
                $.each(data.data.lihat_catatan, function (indexInArray, valueOfElement) {
                    console.log(valueOfElement)
                    if (valueOfElement.status == '0') {
                        $('#tableLihatCatatan > tbody').append('<tr><td>'+valueOfElement.isi_catatan+'</td><td><span class="label label-danger">Belum Selesai</span></td></tr>')
                    } else {
                        $('#tableLihatCatatan > tbody').append('<tr><td>'+valueOfElement.isi_catatan+'</td><td><span class="label label-flat border-success text-success-600 position-right">Selesai</span></td></tr>')
                    }
                });
                // $('#tableLihatCatatan > tbody > tr').each(function (data, index) {
                //     console.log(index)
                // })
            }
        );
    })

    function hapusUsulan(id) {
        console.log('hapus' + id)
        var url = '{{ url('kepegawaian-fakultas/usulan/hapus') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if (data == 'deleted') {
                    toastr.success('Usulan berhasil di hapus!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});
                }
            }
        })
    }
</script>
@endpush
