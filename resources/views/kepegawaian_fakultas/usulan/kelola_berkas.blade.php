@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Berkas')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class="active"><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"> Usulan</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulans->first()->id]) }}"> Kelola Usulan {{ $usulans->first()->kode_usulan }}</a></li>
        <li class="active">Kelola Berkas Usulan</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        @foreach ($usulans as $usulan)
            @foreach ($usulan->jenisPensiun->berkasJenisPensiun as $item)
            <div class="col-lg-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">{{ $item->masterBerkasPensiun->judul_berkas }}</h6>
                        <small>Hanya boleh file PDF. Maksimal 2 MB.</small>
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('kepegawaian-fakultas.usulan.kelola.berkas.tambah', ['id'=>$usulan->id]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    @foreach ($item->masterBerkasPensiun->berkasUsulanPensiun->where('usulan_pensiun_id', $usulan->id) as $val)
                                    {{-- {{$val}} --}}
                                        <p>
                                            <a target="_blank" href="{{ Storage::url('public/pensiun/'.$val->berkas->direktori.'/'.$val->berkas->nama_berkas) }}">
                                                <button type="button" class="btn btn-info btn-labeled btn-xs"><b><i class="icon-eye"></i></b> Lihat Berkas</button>
                                            </a>
                                            @if ($val->catatanBerkas->count() > 0)
                                            {{-- Modal Button --}}
                                            <button type="button" class="btn btn-primary btn-labeled btn-xs" data-toggle="modal" data-target="#{{ $item->masterBerkasPensiun->id }}">
                                                    <b><i class="icon-eye"></i></b> Lihat Catatan
                                            </button>

                                            {{-- Modal --}}
                                            <div class="modal fade" id="{{ $item->masterBerkasPensiun->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Catatan Untuk {{ $item->masterBerkasPensiun->judul_berkas }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div>
                                                                <table class="table">
                                                                    <tbody>
                                                                        @foreach ($val->catatanBerkas as $catatan_berkas)
                                                                            <tr>
                                                                                <td scope="row">{{ $loop->iteration }}</td>
                                                                                <td>{!! $catatan_berkas->catatan !!}</td>
                                                                                <td>
                                                                                    @if ($catatan_berkas->status_kepeg_fakultas == 1)
                                                                                    <span class="label label-success">Selesai</span>
                                                                                    @else
                                                                                    <span class="label label-warning">Belum Selesai</span>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            @if ($val->catatanBerkas->where('status_kepeg_fakultas', 0)->count() > 0)
                                                                <button type="button" onclick="revisiSelesai({{$val->id}})" class="btn btn-success">Catatan Selesai</button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- Modal End --}}
                                            @else
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="" disabled>
                                                Tidak Ada Revisi
                                            </button>
                                            @endif

                                            @if ($usulan->status_usulan_kepeg_untan == 0 ||$usulan->status_usulan_kepeg_untan == 1)
                                                @if ($usulan->berkasUsulanPensiun->where('usulan_pensiun_id', $usulan->id)->where('master_berkas_pensiun_id', $item->master_berkas_pensiun_id)->first()->status_verifikasi_kepeg_fakultas == 0)
                                                <label class="heading-elements">
                                                    <input type="checkbox" class="switchery pull-right" onclick="verfikasi_berkas({{$val->id}})" {{ $val->status_verifikasi_kepeg_fakultas == 1 ? 'checked="checked"' : ''  }} >
                                                    Verifikasi Berkas
                                                </label>
                                                @else
                                                <label class="heading-elements">
                                                    Batalkan Verifikasi
                                                    <input type="checkbox" class="switchery pull-right" onclick="verfikasi_berkas({{$val->id}})" {{ $val->status_verifikasi_kepeg_fakultas == 1 ? 'checked="checked"' : ''  }} >
                                                </label>
                                                @endif
                                            @else
                                                <label class="heading-elements">
                                                    Berkas Terverifikasi
                                                    <input type="checkbox" class="switchery pull-right" checked="checked" disabled>
                                                </label>
                                            @endif
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <input type="hidden" name="usulan_pensiun_id" value="{{ $usulan->id }}">
                                    <input type="hidden" name="master_berkas_pensiun_id" value="{{ $item->master_berkas_pensiun_id }}">
                                    @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                                    <input type="file" class="file-styled-primary" name="berkas" required>
                                    @endif
                                </div>
                                <div class="col-lg-4">
                                    @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        @endforeach
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>

<script>
    $('#usulan').addClass('active');
</script>
<script>
    function verfikasi_berkas(id) {
        // console.log('blade ' + id)
        var url = '{{ url('kepegawaian-fakultas/usulan/kelola/berkas/verifikasi') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                // console.log(data)
                if(data.status == 1 || data.status == 2){
                    toastr.success(data.pesan, 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error(data.pesan, 'Error', {timeOut: 5000});
                    location.reload();
                }
            }
        })
    }

    function revisiSelesai(id) {
        // console.log(id)
        var url = '{{ url('kepegawaian-fakultas/revisiSelesai') }}'
        $.ajax({
            type: 'POST',
            url: url + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function (data) {
                console.log(data)
                if (data == 1) {
                    toastr.success('Catatan berhasil dilaksanakan! \n Harap verifikasi berkas lagi!', 'Sukses', {timeOut: 5000});
                    location.reload();
                } else {
                    toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});
                }
            }
        })
    }
</script>

@endpush
