@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"> Usulan</a></li>
        <li class="active">{{ $dosen->NAMA_LENGKAP }}</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- data pribadi pengusul -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data Pribadi Pengusul</h6>
                    <div class="heading-elements">
                        @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                        <ul class="icons-list">
                            <a href="{{ route('kepegawaian-fakultas.data.editdata', $dosen->NIP) }}" class="btn btn-warning btn-sm">Perbaharui Data Pegawai</a>
                        </ul>
                        @endif
                        <ul class="icons-list">
                            <a href="{{ route('kepegawaian-fakultas.data.show', $dosen->NIP) }}" class="btn btn-primary btn-sm">Lihat Data Pegawai</a>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3 col-xs-4" style="text-align:center;">
                            @if ($foto == 1 && $dosen->foto != null)
                            <img src="{{ Storage::url('public/pegawai/'.$dosen->foto) }}" style="width: 150px; height: 100%;" />
                            @else
                            <img src="{{asset('assets/images/allgender.png')}}" style="width: 150px; height: 100%;" />
                            @endif
                        </div>

                        <div class="col-lg-3 col-xs-2">
                            <div id="content">
                                <p>Nama </p>
                                <p>Tempat, Tanggal Lahir</p>
                                <p>NIP</p>
                                <p>NIDN / NIDK </p>
                                <p>Alamat E-mail  </p>
                                <p>Jabatan Fungsional </p>
                                <p>Golongan Ruang </p>
                                <p>Jurusan </p>
                                <p>Program Studi </p>
                            </div>
                        </div>

                        <div class="col-lg-6 col-xs-6">
                            <p>:{{$dosen->NAMA_LENGKAP }}</p>
                            <p>: {{$dosen->Tempat_Lahir}}, {{ date('d-m-Y', strtotime ($dosen->Tgl_Lahir))}} </p>
                            <p>: {{$dosen->NIP }}</p>
                            <p>: {{$dosen->NIDN}}</p>
                            <p>: {{$dosen->ALAMAT_EMAIL }}</p>
                            <p>: {{ Helpers::jabatanLengkap($dosen->NIP)}}</p>
                            <p>: {{ $dosen->Gol }}</p>
                            <p>: {{ $dosen->Jurusan}}</p>
                            <p>: {{ $dosen->Program_Studi}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- data pribadi pengusul -->

            <!-- opsi usulan -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Pilihan Lanjutan Usulan Pensiun dan Status Usulan</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
					<div class="row">
                        <div class="col-md-12" style="text-align:center">
                            {{-- <button type="button" onclick="verifUsulan({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-eye"></i></b> Verifikasi Usulan</button> --}}
                            @if ( $usulan->status_usulan_kepeg_fakultas == 0 && $usulan->status_usulan_kepeg_untan == 0 )
                            <button type="button" onclick="verifFakultas({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-file-check"></i></b> Verifikasi Usulan</button>
                            @endif

                            @if ( $usulan->status_usulan_kepeg_fakultas == 0 && $usulan->status_usulan_kepeg_untan == 5 )
                            <button type="button" class="btn btn-warning btn-labeled" disabled><b><i class="icon-undo2"></i></b> Usulan Dikembalikan</button>
                            <button type="button" onclick="lihatCatatan({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-file-eye"></i></b> Lihat Catatan</button>
                                @if (Helpers::cekCatatan($usulan->id) == 0)
                                <button type="button" onclick="verifFakultas({{$usulan->id}})" class="btn btn-primary btn-labeled"><b><i class="icon-file-check"></i></b> Verifikasi Usulan</button>
                                @endif
                            @endif

                            @if ( $usulan->status_usulan_kepeg_fakultas == 1 )
                            <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-file-check"></i></b>Usulan Telah Verifikasi</button>
                            <button type="button" onclick="kirimFakultas({{$usulan->id}})" class="btn btn-primary btn-labeled"> <b><i class="icon-paperplane"></i></b>Kirim Usulan Ke Kepegawaian Untan</button>
                            @endif

                            @if ( $usulan->status_usulan_kepeg_fakultas == 2 )
                            <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-file-check"></i></b>Usulan Telah Verifikasi</button>
                            <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-paperplane"></i></b>Usulan Telah Dikiri Ke Kepegawaian Untan</button>
                                @if ($usulan->status_usulan_kepeg_untan == 1)
                                <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-stack-text"></i></b>Usulan Diproses Oleh Kepegawaian Untan</button>
                                @elseif($usulan->status_usulan_kepeg_untan == 2)
                                <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-stack-text"></i></b>Usulan Diverfikasi Oleh Kepegawaian Untan</button>
                                @elseif($usulan->status_usulan_kepeg_untan == 3)
                                <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-stack-text"></i></b>Usulan Dikirim Ke Dikti</button>
                                @else
                                <button type="button" class="btn btn-primary btn-labeled" disabled><b><i class="icon-stack-text"></i></b>SK Usulan Telah Terbit</button>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <!-- daftar formulir -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data berkas</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <!-- Info blocks -->
					<div class="row">
                        <div class="col-md-4">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Kelengkapan Pemberkasan</h5>
                                    <p class="mb-15">BUP</p>
                                    <a href="{{ route('kepegawaian-fakultas.usulan.kelola.berkas', ['id'=>$usulan->id]) }}" class="btn bg-success-400">Lihat Daftar Berkas</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Kelengkapan Berkas</h5>
                                    <p class="mb-15">Kelengkapan berkas yang dapat di print secara otomatis</p>
                                    <a href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.index', ['usulan_id'=>$usulan->id]) }}" class="btn bg-success-400">Lihat</a>
                                </div>
                            </div>
                        </div>

                        @if ($usulan->status_usulan_kepeg_untan == 2 && $usulan->status_usulan_kepeg_fakultas == 2)
                        <div class="col-md-4">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Cetak DPCP dan SP4</h5>
                                    <p class="mb-15">Cetak Berkas DPCP dan SP4</p>
                                    <button type="button" onclick="cetak_dpcp({{$usulan->id}})" class="btn bg-success-400">Cetak DPCP</button>
                                    <button type="button" onclick="cetak_sp4({{$usulan->id}})" class="btn bg-success-400">Cetak SP4</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /info blocks -->
                </div>
            </div>
            <!-- /daftar formulir -->

            @if ($usulan->status_usulan_kepeg_untan == 3)
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data Arsip Berkas</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
					<div class="row">
                        @if ($usulan->status_usulan_kepeg_untan == 2 )
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Berkas DPCP / SP4</h5>
                                    <p class="mb-15">Berkas dapat di download atau lansung di cetak</p>
                                    <button type="button" onclick="cetak_dpcp({{$usulan->id}})" class="btn bg-success-400">Cetak DPCP</button>
                                    <button type="button" onclick="cetak_sp4({{$usulan->id}})" class="btn bg-success-400">Cetak SP4</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        @if ($usulan->status_usulan_kepeg_untan == 3)
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body text-center">
                                    <div class="icon-object border-success text-success"><i class="icon-book"></i></div>
                                    <h5 class="text-semibold">Lihat Berkas Pengantaran</h5>
                                    <p class="mb-15">Berkas dapat di lihat disini</p>
                                    {{-- <button type="button" data-toggle="modal" data-target="#upload_dpcp" class="btn bg-success-400">Upload DPCP</button> --}}
                                    @if($usulan->berkasStepAkhir)
                                        <a target="_blank" href="{{ Storage::url('public/berkas_akhir/'.$usulan->berkasStepAkhir->file_dpcp) }}">
                                            <button type="button" class="btn btn-info">Lihat DPCP</button>
                                        </a>
                                        <a target="_blank" href="{{ Storage::url('public/berkas_akhir/'.$usulan->berkasStepAkhir->file_sp4) }}">
                                            <button type="button" class="btn btn-info">Lihat SP4</button>
                                        </a>
                                        <a target="_blank" href="{{ Storage::url('public/berkas_akhir/'.$usulan->berkasStepAkhir->file_surat_pengantar) }}">
                                            <button type="button" class="btn btn-info">Lihat Surat Pengantar</button>
                                        </a>
                                        @if ($usulan->no_resi_pengiriman_berkas)
                                            <button type="button" data-toggle="modal" data-target="#lihat_resi_pengiriman" class="btn btn-info">Lihat No. Resi</button>
                                        @endif
                                        {{-- <button type="button" data-toggle="modal" data-target="#surat_pengantar" class="btn bg-success-400">Cetak Surat Pengantar</button> --}}
                                    @endif
                                </div>
                            </div>
                        </div> 
                        @endif

                    </div>
                </div>
            </div>
            @endif

        </div>

    </div>
    <!-- /main content -->

    {{-- modal untuk input tgl dpcp print --}}
    <div class="modal fade" id="modal_dpcp" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Berkas DPCP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form target="_blank" action="{{ route('dpcp', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form_tanggal_lama_dpcp">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="" id="waktu_lama_dpcp" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_dpcp" id="pilih_waktu_lama_dpcp" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_dpcp">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_dpcp" class="form-control" placeholder="" aria-describedby="helpId" required>
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary cetak">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk input tgl dan no surat sp4 --}}
    <div class="modal fade" id="modal_sp4" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Berkas SP4</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form target="_blank" action="{{ route('sp4', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form_tanggal_lama_sp4">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="" id="waktu_lama_sp4" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_sp4" id="pilih_waktu_lama_sp4" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_sp4">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_sp4" class="form-control" placeholder="" aria-describedby="helpId">
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary cetak">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk lihat no resi --}}
    <div class="modal fade" id="lihat_resi_pengiriman" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nomor Resi Pengiriman Berkas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label for="">Nomor Resi Pengiriman Berkas</label>
                      <input type="text" id="" value="{{ $usulan->no_resi_pengiriman_berkas ? $usulan->no_resi_pengiriman_berkas: 'No Resi belum di masukan' }}" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                    <div class="form-group">
                      <label for="">Tanggal Pengiriman Berkas</label>
                      <input type="text" id="" value="{{ $usulan->tanggal_kirim_berkas ? date('d-m-Y', strtotime($usulan->tanggal_kirim_berkas)) : 'Berkas Belum Dikirim' }}" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                      <small id="helpId" class="text-muted">Nomor Resi Pengiriman Berkas Pensiun</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    {{-- modal untuk melihat catatan usulan yg di kembalikan --}}
    <div id="modal_lihat_catatan_usulan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Catatan Pengembalian Usulan</h4>
                </div>
                <form action="#" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="table-responsive pre-scrollable">
                            <table class="table" id="tableLihatCatatan">
                                <thead>
                                    <tr>
                                        <th>Catatan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="button" onclick="catatanSelesai({{$usulan->id}})" class="btn btn-primary">Catatan Selesai</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>

<script>
    $('#usulan').addClass('active');
</script>
<script>
    function lihatCatatan(usulanId) {
        $('#modal_lihat_catatan_usulan').modal('show')
        var url = '{{ url('kepegawaian-fakultas/usulan/') }}'
        $.getJSON(url+"/"+usulanId+"/lihat-catatan",function (data) {
                // console.log(data)
                $('#tableLihatCatatan > tbody').empty()
                $.each(data.data.lihat_catatan, function (indexInArray, valueOfElement) { 
                    // console.log(valueOfElement)
                    if (valueOfElement.status == '0') {
                        $('#tableLihatCatatan > tbody').append('<tr><td>'+valueOfElement.isi_catatan+'</td><td><span class="label label-danger">Belum Selesai</span></td></tr>')
                    } else {
                        $('#tableLihatCatatan > tbody').append('<tr><td>'+valueOfElement.isi_catatan+'</td><td><span class="label label-flat border-success text-success-600 position-right">Selesai</span></td></tr>')
                    }
                });
            }
        );
    }

    function catatanSelesai(usulanId) {
        var url = '{{ url('kepegawaian-fakultas/usulan/') }}'
        swal({
            title: "Catatan Sudah Selesai?",
            text: "Tandai catatan sudah diselesaikan",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willVerif) => {
            if (willVerif) {
                $.ajax({
                    type: 'POST',
                    url: url + '/' + usulanId + "/catatan-selesai",
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function(data) {
                        console.log(data)
                        if (data.status == 0) {
                            swal(data.pesan, {
                                icon: "warning",
                            });
                        } else {
                            swal(data.pesan, {
                                icon: "success",
                            });
                            location.reload();
                        }
                        },
                        error: function(errors) {
                            swal("Usulan Gagal Diverifikasi!", {
                                icon: "warning",
                            });
                        },
                    })

            } else {
                swal("Usulan Gagal Diverifikasi!");
            }
        });
    }

    function verifFakultas(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-fakultas/usulan/verifikasi') }}'
        swal({
            title: "Verifikasi Usulan?",
            text: "Verifikasi Usulan Ini Ke Kepegawaian Untan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willVerif) => {
            if (willVerif) {
                $.ajax({
                    type: 'POST',
                    url: url + '/' + id,
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function(data) {
                        console.log(data)
                        if (data.status == 0) {
                            swal(data.pesan, {
                                icon: "warning",
                            });
                        } else {
                            swal(data.pesan, {
                                icon: "success",
                            });
                            location.reload();
                        }
                        },
                        error: function(errors) {
                            swal("Usulan Gagal Diverifikasi!", {
                                icon: "warning",
                            });
                        },
                    })

            } else {
                swal("Usulan Gagal Diverifikasi!");
            }
        });
    }

    function kirimFakultas(id) {
        // console.log('verif' + id)
        var url = '{{ url('kepegawaian-fakultas/usulan/kirim') }}'
        swal({
            title: "Kirim Usulan?",
            text: "Kirim Usulan Ini Ke Kepegawaian Untan!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: url + '/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    console.log(data)
                    if (data.status == 0) {
                        swal(data.pesan, {
                            icon: "warning",
                        });
                    } else {
                        swal(data.pesan, {
                            icon: "success",
                        });
                        location.reload();
                    }
                    },
                    error: function(errors) {
                        swal("Usulan Gagal Dikirim!", {
                            icon: "warning",
                        });
                    },
                })

        } else {
            swal("Usulan Tidak Jadi Dikirim!");
        }
    });
    }

    // function kirimUsulan(id) {
    //     // console.log('kirim' + id)
    //     var url = '{{ url('kepegawaian-fakultas/usulan/kirim') }}'
    //     $.ajax({
    //         type: 'POST',
    //         url: url + '/' + id,
    //         data: {
    //             '_token': $('input[name=_token]').val(),
    //         },
    //         success: function (data) {
    //             console.log(data)
    //             if(data == 1){
    //                 toastr.success('Usulan berhasil di kirim ke kepegawaian UNTAN!', 'Sukses', {timeOut: 5000});
    //                 location.reload();
    //             } else if (data == 'sent') {
    //                 toastr.error('Usulan telah di kirim sebelumnya!', 'Sukses', {timeOut: 5000});
    //                 location.reload();
    //             } else if (data == 'not verified') {
    //                 toastr.error('Usulan belum di verfikasi sebelumnya!', 'Sukses', {timeOut: 5000});
    //                 location.reload();
    //             } else {
    //                 toastr.error('Terjadi kesalahan!', 'Error', {timeOut: 5000});

    //             }
    //         }
    //     })
    // }

    function cetak_dpcp(id) {
        var cekDpcp = '{{ url('cek/dpcp') }}'
        console.log(cekDpcp + '/' + id)
        $.get( cekDpcp + '/' + id, function( data ) {
            console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_dpcp').addClass('hidden')
                $('#tanggal_dpcp').prop('required',true);
            } else {
                //jika ada recent waktu
                $('#waktu_lama_dpcp').val(data);
            }
        });
        $('#modal_dpcp').modal('show')

        $('#pilih_waktu_lama_dpcp').on('change', function () {
            $('.form_tanggal_baru_dpcp').addClass('hidden')
            $('#tanggal_dpcp').removeAttr('required');
        })
    }

    function cetak_sp4(id) {
        var cekDpcp = '{{ url('cek/sp4') }}'
        $.get( cekDpcp + '/' + id, function( data ) {
            console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_sp4').addClass('hidden')
                $('#tanggal_sp4').prop('required',true);
            } else {
                //jika ada recent waktu
                $('#waktu_lama_sp4').val(data);
            }
        });
        $('#modal_sp4').modal('show')

        $('#pilih_waktu_lama_sp4').on('change', function () {
            $('.form_tanggal_baru_sp4').addClass('hidden')
        })
    }

    // $('.cetak').on('click', function () {
    //     location.reload();
    // })

</script>
@endpush
