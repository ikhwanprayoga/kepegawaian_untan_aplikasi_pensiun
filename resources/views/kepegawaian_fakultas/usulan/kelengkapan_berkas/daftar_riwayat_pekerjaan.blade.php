<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')

    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal landscape
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 0px solid black;
        }

        .table td, .table th {
            padding: 0.01rem;
            vertical-align: top;
            border-top: 0px solid #dee2e6;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 0px solid #dee2e6;
        }

    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">

        <!-- Write HTML just like a web page -->
        <p colspan="3" style="font-size: 18px; padding: 3px 0px 4px 0px; font-weight:bold;">DAFTAR RIWAYAT PEKERJAAN</p>
        <font size="2" style="font-size: 12px;">
        <div class="row" style="margin-bottom: -16px;">
            <div class="col-lg-3">
                <table class="table">
                    <tbody >
                        <tr>
                            <td>1. Nama</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td>2. NIP</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td>3. Tempat dan tanggal lahir</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td>4. Pangkat/golongan ruang/TMT</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td>5. Jabatan/Eselon </td>
                            <td>: </td>
                        </tr>
                        <tr>
                            <td>6. Agama</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td>7. Status perkawinan</td>
                            <td>:</td>
                        </tr>
                    </div>
                    </tbody>
                </table>
            </div>
        </div>
            {{-- tabel --}}
            <div class="col-lg-12">
                <div class="row">
                    <table class="table">
                        <thead >
                            <tr align="center">
                                <th>NO.</th>
                                <th>DAFTAR RIWAYAT PEKERJAAN</th>
                                <th>DARI TGL/BLN/THN S/D TGL/BLN/THN</th>
                                <th>GOL. RUANG</th>
                                <th>INSTANSI INDUK</th>
                                <th>KETERANGAN</th>
                            </tr>
                        </thead>
                            <tbody>

                            <tr align="center">
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>

                            </tbody>
                    </table>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6"></div>
            <div class="col-lg-6">
                <div class="row">

                    <table class="table">
                        <tbody align="center">
                            <tr>
                                <td></td>
                                <td>Pimpinan Lembaga/Unit Kerja</td>
                            </tr>
                            <tr>

                                <td></td>
                            </tr>
                            <tr style="height: 60px;">
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>( )</td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>NIP.  </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script>
        {{-- window.print() --}}
    </script>

</body>

</html>
