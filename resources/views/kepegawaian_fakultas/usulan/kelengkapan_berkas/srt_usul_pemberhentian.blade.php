<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')

    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal portrait
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 0px solid black;
        }

        .table td, .table th {
            padding: 0.01rem;
            vertical-align: top;
            border-top: 3px;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 0px solid #dee2e6;
        }
        .tag {
            margin-bottom: -0.5rem;

        }

        p, th, td, h3 {
            font-family: "Times New Roman";
        }

    </style>
</head>

<body class="">
    <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">
        <div class="row" style="padding: 0px 40px 0px">
        <div class="col-lg-12">
            <table style="width:100%">
                <tr>
                    <td style="width:5%">
                        <img style="width: 80px;" src="{{ asset('assets/images/untan.png') }}" alt="" srcset="">
                    </td>
                    <td align="center" style="width:95%">
                        {{-- <p  class="tag" style="font-size: 16px; padding: 3px 0px 4px 0px;"> --}}
                        <h3 style="font-size: 16px;">KEMENTRIAN RISET,
                            TEKNOLOGI,
                            DAN PENDIDIKAN TINGGI
                            <br><b>UNIVERSITAS TANJUNGPURA</b>
                            <br>Jalan Prof. Dr. H. Hadari Nawawi Pontianak 78124</h3>
                        <h3 colspan="3" style="font-size: 11px;">Telp. (0561) 736033,
                            739630,
                            739636, 739637, 739638, 740189, 743466 dan Sentral 736439, 743464
                            <br> Faximili (0561) 739630, 739636, 739637, 743466 Kotak Pos 1049
                            <br>e-mail :<u> untan_59@untan.ac.id</u> Website: <u>http://www.untan.ac.id</u></h3>
                        {{-- <p class="margin-bottom: -0.5rem;"> </h3> --}}
                    </td>
                </tr>
            </table>
                    <hr style="border:1px solid black;">
        </div>
        </div>


        <div class="row" style="padding: 0px 40px 0px">
        <font size="2" style="font-size: 12px;">
        <div>
            <div class="col-lg-12">
                <table class="table" >
                    <tbody>
                        <tr>
                            <td></td>
                            <td>Nomor</td>
                            <td>: </td>
                            <td>4118/UN22/KP/2019</td>
                            <td>{{ Carbon\Carbon::parse(date('Y-m-d'))->formatLocalized('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Lampiran</td>
                            <td>: </td>
                            <td>Satu Berkas</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Hal</td>
                            <td>:</td>
                            <td>Usul Pemberitahuan dengan hormat sebagai <br>
                                    Pegawai Negeri Sipil dengan hak pensiun <br>
                                    dan kenaikan pangkat pengabdian <br>
                                    a.n {{ $usulan->dataPegawai->NAMA_LENGKAP }} <br>
                                    NIP {{ $usulan->dataPegawai->NIP}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
                <p align="justify">Yth. Menteri Riset, Teknologi, dan Pendidikan Tinggi<br>
                u.p. Kepala Biro Sumber Daya Manusia<br>
                Kementrian Riset, Teknologi, dan Pendidikan Tinggi<br>
                Jakarta</p>
                <p align="justify">Bersama ini kami sampaikan usul pemberhentian dengan hormat sebagai Pegawai Negeri Sipil dengan hak
                pensiun dan kenaikan pangkat pengabdian atas nama Saudara/i {{ $usulan->dataPegawai->NAMA_LENGKAP }} NIP {{ $usulan->dataPegawai->NIP }}.
                lahir pada tanggal {{ $usulan->dataPegawai->Tgl_Lahir }} pangkat {{ $usulan->dataPegawai->pangkat }} {{ $usulan->dataPegawai->JAB_FUNGSIONAL }} golongan ruang {{ $usulan->dataPegawai->Gol }} jabatan terakhir {{ $usulan->dataPegawai->JAB_FUNGSIONAL }}
                pada {{ $usulan->fakultas->nama_fakultas}} Universitas Tanjungpura, sesuai dengan Undang-Undang Nomor 14 Tahun 2005 pasal 67 ayat (4):
                "Pemberhentian dosen karena batas usia pensiun sebagaimana dimaksud pada ayat (1) huruf b dilakukan pada usia 65
                (enam puluh lima) tahun", pada akhir bulan Desember 2019 telah mencapai batas usia pensiun.</p>
                <p>Sebagai bahan pertimbangan bersama ini kami lampirkan : </p>
                <p>1. DPCP;
                <br>2. SP4. A;
                <br>3.	Foto copy Konversi NIP;
                    <br>4.	Foto copy KARPEG;
                    <br>5.	Foto copy SK Pengangkatan Pertama (CPNS);
                    <br>6.	Foto copy SK PNS;
                    <br>7.	Foto copy SK Pangkat Terakhir;
                    <br>8.	Foto copy SK Berkala Terakhir ;
                    <br>9.	Foto copy Surat Nikah yang disahkan oleh KUA;
                    <br>10.	Daftar Susunan Keluarga disahkan  oleh Camat;
                    <br>11.	Foto copy Akta Kelahiran anak yang dilegalisir pejabat yang berwenang;
                    <br>12.	Surat Pernyataan tidak pernah dijatuhi hukuman disiplin berat atau sedang;
                    <br>13.	Surat Pernyataan tidak sedang menjalani proses pidana atau pernah dipidana penjara;
                    <br>14.	Daftar Riwayat Pekerjaan;
                    <br>15.	PPKP  2 tahun terakhir;
                    <br>16.	Pas foto ukuran 3 x 4 cm sebanyak 7 lembar;
                    <br>17.	Foto copy Rekening Tabungan;
                    <br>18.	Foto copy NPWP;
                    <br>19.	Formulir Permintaan Pembayaran Taspen.
                </p>
                <p>Demikian untuk mendapat penyelesaian sebagaimana mestinya.</p>

        <div class="row" style="padding: 0px 40px 0px">
            <div class="col-lg-12">
                <table class="table" >
                    <tbody align="left">
                        <tr>
                            <td style="width:7%"></td>
                            <td style="width:31%"></td>
                            <td colspan="3" style="width:17%">a.n. Rektor</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Wakil Rektor Bidang Umum dan Keuangan,</td>
                        </tr>
                        <tr style="height: 60px;">
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Dr. Rini Sulistiawati , S.E., M.Si.</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>NIP. 19289282828282</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p> Tembusan :
        <br>1. Menteri Riset, Teknologi, dan Pendidikan Tinggi
        <br>2. Sekretaris Jendral Kemenristekdikti di Jakarta
        <br>3. Kepala BKN Jakarta
        <br>4. Kepala Bagian Mutasi, Disiplin, dan Pemberhentian Kemenristekdikti
        <br>5. Dekan FKIP Untan. </p>

        </div>
    </section>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script>
        {{-- window.print() --}}
    </script>

</body>
</html>
