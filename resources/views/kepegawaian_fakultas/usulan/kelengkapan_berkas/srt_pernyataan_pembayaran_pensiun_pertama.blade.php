<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')

    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal portrait;
        }

        table {
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        .table td,
        .table th {
            padding: 0.01rem;
            vertical-align: top;
            border-top: 3px;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 0px solid #dee2e6;
        }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">

        <font size="2" style="font-size: 12px;">
            <div class="row" style="margin-bottom: -16px; padding: 0px 40px 0px">
                <div class="col-md-4 col-sm-4"></div>
                <div class="col-md-4 col-sm-4">
                    <div style="width: 350px; border: 0px solid black; margin: 0px; padding:5px;">
                        <p align="right">Lampiran IV</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div style="width: 350px; border: 0px solid black; margin: 0px; padding:5px 5px 5px 80px;">
                        Keputusan Kepala Badan<br>
                        Administrasi kepegawaian<br>
                        Negara<br>
                        Nomor : 18 Tahun 1992<br>
                        Tanggal : 06 Maret 1992
                        <hr style="border: 1px solid black;">
                    </div>
                </div>
            </div>
        </font>

        <p align="center" colspan="3" style="font-size: 18px; padding: 3px 0px 4px 0px; font-weight:bold;">SURAT
            PERMINTAAN PEMBAYARAN PENSIUN PERTAMA</p>
        <font size="2" style="font-size: 12px;">

            <div class="row" style="margin-bottom: -16px; padding: 0px 40px 0px">
                <div class="col-md-3 col-sm-3">
                    <div style="width: 350px; border: 1px solid black; margin: 0px; padding:5px;">
                        <p>Surat penerimaan pembayaran Pensiun/Tunjangan<br>Pertama dan Tabungan Hari Tua X)</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3"></div>
                <div class="col-md-3 col-sm-3"></div>
                <div class="col-md-3 col-sm-3">
                    <div style="width: 350px; border: 0px solid black; margin: 0px;">
                        <p>Kepada Yth.<br>Kepala Kantor Cabang<br>PT. TASPEN (PERSERO)<br>Di Pontianak</p>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: -16px; padding: 0px 40px 0px">
                <div class="col-lg-6">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>PNS</td>
                                <td>PEJABAT NEGARA</td>
                                <td>ABRI</td>
                                <td>PKRI</td>
                                <td>VETERAN</td>
                                <td>U.T</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- </div> --}}

            <p style="padding: 0px 40px ">Tulis dengan huruf cetak</p>

            <div class="row" style="padding: 0px 40px 0px">
                <div class="col-lg-12">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Nama Lengkap Penerima Pensiun</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->NAMA_LENGKAP }}</td>
                            </tr>
                            <tr>
                                <td>Tempat dan Tanggal Lahir</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->Tempat_Lahir }}, {{ date('d-m-Y', strtotime($usulan->dataPegawai->Tgl_Lahir)) }}</td>
                            </tr>
                            <tr>
                                <td>Nama yang berhak menerima Pensiun</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->NAMA_LENGKAP }}</td>
                            </tr>
                            <tr>
                                <td>NIP/ NPP/ NPV/ KRP</td>
                                <td>{{ $usulan->dataPegawai->NIP }}</td>
                            </tr>
                            <tr>
                                <td>Pangkat/ Golongan</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->pangkat }}/{{ $usulan->dataPegawai->Gol }}</td>
                            </tr>
                            <tr>
                                <td>Gaji Pokok Terakhir</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->gaji_pokok_terakhir}}</td>
                            </tr>
                            <tr>
                                <td>Diberhentikan/ Meninggal Dunia pada Tanggal</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Penerima Pensiun/ Saat ini/ Akan/ <br> Pernah Menerima Pensiun Rangkap<br> Dengan
                                    Pensiun</td>
                                <td>Pejabar Negara Edukatif/ Non Edukatif, <br> Perintis Kemerdekaan, PNS/ ABRI/Sendiri
                                    atau<br>Gaji PNS/ABRI</td>
                            </tr>
                            <tr>
                                <td>Nomor Pensiun Rangkap (bagi penerima pensiun rangkap)</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Alamat setelah Pensiun
                                    <br>Kelurahan/Desa
                                    <br>Kecamatan
                                    <br>Kotamadya/ Kabupaten</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->alamat_jl}} {{ $usulan->dataPegawai->alamat_rt }} {{ $usulan->dataPegawai->alamat_rw }}
                                    <br>{{ $usulan->dataPegawai->alamat_kelurahan }}
                                    <br>{{ $usulan->dataPegawai->alamat_kecamatan }}
                                    <br>{{ $usulan->dataPegawai->alamat_kota }}
                                </td>
                            </tr>
                            {{--  <tr>
                                <td>Kelurahan/Desa</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->alamat_kelurahan }}</td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->alamat_kecamatan }}</td>
                            </tr>
                            <tr>
                                <td>Kotamadya/ Kabupaten</td>
                                <td style="text-transform: uppercase;">{{ $usulan->dataPegawai->alamat_kota }}</td>
                            </tr>  --}}
                            <tr>
                                <td>Uang Pensiun Diminta untuk dibayarkan Melalui **)</td>
                                <td>a. BANK ..............di............<br>
                                    b. Kantor Cabang PT. Taspen (Persero) di <br>
                                    c. Kantor Pos dan Giro di <br>
                                    d. Rekening No ..................pada Bank.......<br>
                                    e. Rekening No ..................Pada Sentral Giro.......<br>
                                    f. Rekening No ..................Pada Sentral Giro.......<br>
                                    g. Kantor POS dan Giro di...............
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <p style="padding: 0px 40px 0px"> Dengan ini mengajukan permintaan pembayaran Pensiun Pertama dan Tabungan
                Hari Tua (THT),
                apabila keterangan yang saya berikan ini tidak benar, saya bersedia dituntut berdasarkan ketentuan
                yang berlaku dan bersedia mengganti kerugian PT. TASPEN (PERSERO). </p>

            <div class="row" style="padding: 0px 40px 0px">
                <div class="col-md-4 col-sm-4">
                    <div style="width: 386px; border: 1px solid black; padding: 2 px; margin: 0px;">
                        <p><u>Catatan Penerimaan Surat Keputusan Pensiun :</u></p>
                        <br>Kepada nama tersebut di atas telah diberikan Hak Pensiun
                        <br>Berdasarkan SK Tanggal ………………. No. …………..
                        <br>Mulai Bulan ……… Pensiun Pokok Sebesar Rp…………..
                        <p align="center" style="height:96px">PEJABAT PENERBIT SK PENSIUN</p>
                        <p align="center">(……………………………………)</p>
                    </div>
                    <br>Coret yang tidak perlu :
                    <br>Isi salah satu yang diinginkan
                    <br>THT hanya dibayarkan kepada peserta yang mempunyai hak</td>
                </div>
                <div class="col-md-4 col-sm-4"></div>
                <div class="col-md-4 col-sm-4">
                    <div style="width: 250px; border: 0px solid black; margin: 20px;">
                        <p align="center" style="height:96px">Pontianak, 1 April 2019<br>
                            Pemohon, <br> </p>
                        <p align="center">DRS. ABAS YUSUF, M.S<br>
                            NIP 195503211983031005<br></p>
                        <p align="center">Nama Jelas tanda tangan<br>
                            Cap tiga jari tangan kiri</p>
                    </div>
                </div>
            </div>
    </section>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script>
        {
        window.print()
        }
    </script>

</body>

</html>
