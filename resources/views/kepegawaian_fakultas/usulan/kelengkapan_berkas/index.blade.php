@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Berkas')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class="active"><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"> Usulan</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]) }}"> Kelola Usulan {{ $usulan->kode_usulan }}</a></li>
        <li class="active">Kelengkapan Berkas</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Daftar Kelengkapan Berkas</h5>
                    {{-- <div class="heading-elements">
                        <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button>
                    </div> --}}
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Berkas</th>
                                <th>Download</th>
                                <th>Cetak</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Surat tidak pernah dijatuhi hukuman disiplin tingkat sedang/berat</td>
                                <td>
                                    <a target="_blank" href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.srt_pernyataan_bebas_hukuman', ['usulan_id'=>$usulan->id]) }}">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                                <td>
                                    <button type="button" onclick="cetak_hukum({{$usulan->id}})" class="btn btn-primary btn-xs">Cetak</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Surat pernyataan tidak sedang menjalani proses pidana atau pernah dipidana penjara berdasarkan putusan pengadilan yang telah berkekuatan hukum tetap</td>
                                <td>
                                    <a target="_blank" href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.srt_pernyataan_tidak_pernah_pidana', ['usulan_id'=>$usulan->id]) }}">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                                <td>
                                    <button type="button" onclick="cetak_pidana({{$usulan->id}})" class="btn btn-primary btn-xs">Cetak</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Daftar riwayat pekerjaan</td>
                                <td>
                                    <a target="_blank" href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.daftar_riwayat_pekerjaan', ['usulan_id'=>$usulan->id]) }}">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Daftar susunan keluarga</td>
                                <td>
                                    <a target="_blank" href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.susunan_keluarga', ['usulan_id'=>$usulan->id]) }}">
                                        <button type="button" class="btn btn-primary btn-xs">Download Berkas</button>
                                    </a>
                                </td>
                                <td></td>
                            </tr>
                             {{-- <tr>
                                <td>Surat pernyataan pembayaran pensiun pertama<td>
                                <td><a target="_blank" href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.srt_pernyataan_pembayaran_pensiun_pertama', ['usulan_id'=>$usulan->id]) }}">Lihat</a></td>
                            </tr> --}}
                            {{-- <tr>
                                <td>Surat Usul Pemberhentian<td>
                                <td><a target="_blank" href="{{ route('kepegawaian-fakultas.Kelengkapan_berkas.usul_pemberhentian', ['usulan_id'=>$usulan->id]) }}">Lihat</a></td>
                            </tr> --}}

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- /main content -->

    {{-- modal untuk input tgl pidana print --}}
    <div class="modal fade" id="modal_pidana" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Berkas Bebas Pidana</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form target="_blank" action="{{ route('surat_bebas_pidana', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form-nomor-surat">
                        <label for="">Nomor Surat</label>
                        <input type="text" name="no_surat_pidana" id="no_surat_pidana" class="form-control" placeholder="" aria-describedby="helpId">
                        <small id="helpId" class="text-muted">Nomor surat pidana</small>
                    </div>
                    <div class="form-group form_tanggal_lama_pidana">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="waktu_lama_pidana" id="waktu_lama_pidana" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_pidana" id="pilih_waktu_lama_pidana" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_pidana">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_pidana" class="form-control" placeholder="" aria-describedby="helpId" required>
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary cetak">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal untuk input tgl hukum print --}}
    <div class="modal fade" id="modal_hukum" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Berkas Bebas Pidana</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form target="_blank" action="{{ route('surat_bebas_hukum', ['nip'=>$usulan->nip]) }}" method="get">
                <div class="modal-body">
                    <div class="form-group form-nomor-surat">
                        <label for="">Nomor Surat</label>
                        <input type="text" name="no_surat_hukuman" id="no_surat_hukuman" class="form-control" placeholder="" aria-describedby="helpId">
                        <small id="helpId" class="text-muted">Nomor surat bebas hukuman</small>
                    </div>
                    <div class="form-group form_tanggal_lama_hukum">
                        <label for="">Tanggal Print Berkas Lama</label>
                        <input type="text" name="" id="waktu_lama_hukum" value="" class="form-control" placeholder="" aria-describedby="helpId" disabled>
                        <input type="checkbox" name="waktu_lama_hukum" id="pilih_waktu_lama_hukum" value="ya">
                        <small id="helpId" class="text-muted">Gunakan waktu yang pernah di pilih</small>
                    </div>
                    <div class="form-group form_tanggal_baru_hukum">
                        <label for="">Tanggal</label>
                        <input type="date" name="tanggal" id="tanggal_hukum" class="form-control" placeholder="" aria-describedby="helpId" required>
                        <small id="helpId" class="text-muted">Tanggal untuk kepentingan TTD</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary cetak">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/form_checkboxes_radios.js') }}"></script>

<script>
    $('#usulan').addClass('active');
</script>

<script>
    function cetak_pidana(id) {
        console.log(id)
        var cekPidana = '{{ url('cek/surat_bebas_pidana') }}'
        $.get( cekPidana + '/' + id, function( data ) {
            console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_pidana').addClass('hidden')
                $('#tanggal_pidana').prop('required',true);
                $('#no_surat_pidana').prop('required',true);
            } else {
                //jika ada recent waktu
                $('#waktu_lama_pidana').val(data.waktu);
                $('#no_surat_pidana').val(data.no_surat);
            }
        });
        $('#modal_pidana').modal('show')

        $('#pilih_waktu_lama_pidana').on('change', function () {
            $('.form_tanggal_baru_pidana').addClass('hidden')
            $('#tanggal_pidana').removeAttr('required')
        })
    }

    function cetak_hukum(id) {
        console.log('hukum '+id)
        var cekHukum = '{{ url('cek/surat_bebas_hukum') }}'
        $.get( cekHukum + '/' + id, function( data ) {
            console.log(data)
            if (data == 0) {
                $('.form_tanggal_lama_hukum').addClass('hidden')
                $('#tanggal_hukum').prop('required',true);
                $('#no_surat_hukuman').prop('required',true);
            } else {
                //jika ada recent waktu
                $('#waktu_lama_hukum').val(data.waktu);
                $('#no_surat_hukuman').val(data.no_surat);
            }
        });
        $('#modal_hukum').modal('show')

        $('#pilih_waktu_lama_hukum').on('change', function () {
            $('.form_tanggal_baru_hukum').addClass('hidden')
            $('#tanggal_hukum').removeAttr('required')
        })
    }
</script>

@endpush
