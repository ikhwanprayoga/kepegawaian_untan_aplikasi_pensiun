<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Normalize or reset CSS with your favorite library -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.normalize')

    <!-- Load paper.css for happy printing -->
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css"> --}}
    @include('kepegawaian_untan.usulan.css_dpcp.paper')

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page {
            size: legal portrait
        }

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 0px solid black;
        }

        .table td, .table th {
            padding: 0.01rem;
            vertical-align: top;
            border-top: 3px;
            font-family: times new roman;
            font-size:12px;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 0px solid #dee2e6;
        }

        p, h3{
           font-family: times new roman;
        }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="">

        <section class="padding-60mm" style="padding: 20px 17px 10px 40px;">
        <div class="row" style="padding: 0px 40px 0px">
        <div class="col-lg-12">
            <table style="width:100%">
                <tr>
                    <td style="width:5%">
                        <img style="width: 80px;" src="{{ asset('assets/images/untan.png') }}" alt="" srcset="">
                    </td>
                    <td align="center" style="width:95%">
                        {{-- <p  class="tag" style="font-size: 16px; padding: 3px 0px 4px 0px;"> --}}
                        <h3 style="font-size: 16px;">KEMENTRIAN RISET,
                            TEKNOLOGI,
                            DAN PENDIDIKAN TINGGI
                            <br><b>UNIVERSITAS TANJUNGPURA</b>
                            <br>Jalan Prof. Dr. H. Hadari Nawawi Pontianak 78124</h3>
                        <h3 colspan="3" style="font-size: 11px;">Telp. (0561) 736033,
                            739630,
                            739636, 739637, 739638, 740189, 743466 dan Sentral 736439, 743464
                            <br> Faximili (0561) 739630, 739636, 739637, 743466 Kotak Pos 1049
                            <br>e-mail :<u> untan_59@untan.ac.id</u> Website: <u>http://www.untan.ac.id</u></h3>
                        {{-- <p class="margin-bottom: -0.5rem;"> </h3> --}}
                    </td>
                </tr>
            </table>
             <hr style="border:1px solid black;">
        </div>
        </div>

        <!-- Write HTML just like a web page -->
        <div class="row" style="padding: 0px 40px 0px">
        <div class="col-lg-12">
        <p align="center" colspan="3"
            style="font-size: 16px; padding: 3px 0px 4px 0px; font-weight:bold; font-family:times new roman;">SURAT
            PERNYATAAN TIDAK SEDANG MENJALANI PROSES PIDANA ATAU <br>PERNAH DIPIDANA PENJARA BERDASARKAN PUTUSAN
            PENGADILAN YANG <br>TELAH BERKEKUATAN HUKUM TETAP</p>
        <p align="center" colspan="3"
            style="font-size: 16px; padding: 3px 0px 4px 0px; font-weight:bold; font-family:times new roman;">NOMOR :
        </p>
        </div>
        </div>


        <font size="2" style="font-size: 12px;">
            <div class="row" style="margin-bottom: -16px;">
                <div class="col-lg-12"></div>
        </div>
            {{-- tabel --}}
        <div class="row" style="padding: 0px 40px 0px">
            <div class="col-lg-12">
                <table class="table" >
                    <tbody>
                        <tr>
                            <td colspan="3">Yang bertanda tangan dibawah ini :</td>
                        </tr>
                        <tr>
                            <td style="width:3%"></td>
                            <td style="width:12%">Nama</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>NIP</td>
                            <td>:</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Pangkat/golongan ruang</td>
                            <td>:</td>
                        </tr>
                            <td colspan="3">Dengan ini menyatakan dengan sesungguhnya, bahwa Pegawai Negeri Sipil :</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Nama</td>
                            <td>: {{ $usulan->dataPegawai->NAMA_LENGKAP  }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>NIP</td>
                            <td>: {{ $usulan->dataPegawai->NIP  }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Pangkat/golongan ruang</td>
                            <td>: {{ $usulan->dataPegawai->pangkat}}/{{ $usulan->dataPegawai->Gol}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Jabatan</td>
                            <td>: {{ $usulan->dataPegawai->JAB_FUNGSIONAL}}</td>
                        </tr>
                         <tr>
                            <td></td>
                            <td>Instansi</td>
                            <td>: {{ $usulan->fakultas->nama_fakultas}} UNIVERSITAS TANJUNGPURA</td>
                        </tr>
                </table>
            </div>
                <p align="justify">Tidak sedang dalam menjalani proses pidana atau pernah dipidana penjara berdasarkan putusan pengadilan yang telah
                    berkekuatan hukum tetap karena melakukan tindak pidana kejahatan jabatan atau tindak pidana kejahatan yang ada
                    hubungannya dengan jabatan dan/atau pidana umum
                </p>
                <p align="justify">Demikian surat pernyataan ini saya buat dengan sesungguhnya dengan mengingat sumpah jabatan dan apabila di kemudian hari
                    ternyata isi surat pernyataan ini tidak benar yang mengakibatkan kerugian bagi negara maka saya bersedia menanggung kerugian
                    negara sesuai dengan ketentuan peraturan perundang-undangan.</p>

        <div class="row" style="padding: 0px 40px 0px">
            <div class="col-lg-12">
                <table class="table" >
                    <tbody align="center">
                        <tr>
                            <td style="width:7%"></td>
                            <td style="width:31%"></td>
                            <td colspan="3" style="width:17%">Pontianak,</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>PEJABAT PENGELOLA KEPEGAWAIAN</td>
                        </tr>
                        <tr style="height: 60px;">
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>(Dra. HERLILASTI PURJININGSIH, M.M)</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>NIP. 19289282828282</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script>
        window.print()
    </script>

</body>
</html>
