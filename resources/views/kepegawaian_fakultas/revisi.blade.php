@extends('layouts.theme.base')

@section('title', 'Kedosenan Untan dt')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Revisi</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Revisi</h5>
                    <div class="heading-elements">
                        {{-- <button type="button" class="btn btn-primary btn-sm" id="tombolTambahdtBaru">Tambah dt Baru</button> --}}
                    </div>
                </div>

                <div class="panel-body">
                    Usulan yang memiliki revisi
                </div>

                <div class="table-responsive">
                    <table class="table datatable-show-all">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Usulan</th>
                                <th>Jenis Usulan</th>
                                <th>Nama Pengusul</th>
                                <th>NIP</th>
                                <th>NIDN</th>
                                <th>Unit Kerja</th>
                                {{-- <th>Isi Revisi</th>
                                <th>Waktu Revisi Dibuat</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @foreach ($usulan as $dt)
                            @if ($dt->revisi->count() > 0 && $dt->revisi->status == 0)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('kepegawaian-fakultas.usulan.detail', $dt->id) }}">{{ $dt->kode_usulan }}</a></td>
                                <td>{{ $dt->jenisPensiun->nama }}</td>
                                <td>{{ $dt->dosen->NAMA_LENGKAP }}</td>
                                <td>{{ $dt->dosen->NIP }}</td>
                                <td>{{ $dt->dosen->NIDN }}</td>
                                <td>{{ $dt->dosen->Unit_Kerja }}</td>
                                <td>{!! $dt->revisi->catatan_revisi !!}</td>
                                <td>{{ $dt->revisi->created_at }}</td>
                            </tr>
                            @else
                                
                            @endif
                            @endforeach --}}
                            @if ($hasil == 0)
                                <td class="text-center" colspan="7">Tidak ada revisi</td>
                            @else
                                @if ($pengecek_status == 'nilainya 1')
                                    <td class="text-center" colspan="7">Tidak ada revisi</td>
                                @else
                                    @foreach ($usulan as $dt)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><a href="{{ route('kepegawaian-fakultas.usulan.detail', $dt->id) }}">{{ $dt->kode_usulan }}</a></td>
                                            <td>{{ $dt->jenisPensiun->nama }}</td>
                                            <td>{{ $dt->dosen->NAMA_LENGKAP }}</td>
                                            <td>{{ $dt->dosen->NIP }}</td>
                                            <td>{{ $dt->dosen->NIDN }}</td>
                                            <td>{{ $dt->dosen->Unit_Kerja }}</td>
                                            {{-- <td>{!! $dt->revisi->catatan_revisi !!}</td>
                                            <td>{{ $dt->revisi->created_at }}</td> --}}
                                        </tr>                                        
                                    @endforeach
                                @endif
                            @endif
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
    <script>
        $('#revisi').addClass('active');
    </script>
@endpush
