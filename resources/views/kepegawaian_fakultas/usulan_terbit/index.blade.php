@extends('layouts.theme.base')

@section('title', 'Kepegawaian Untan Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">SK Usulan Terbit</li>
        {{-- <li class="active">Log Usulan {{ $usulan->first()->kode_usulan }}</li> --}}
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><b>Data Usulan SK Terbit Unit Kerja {{ auth()->user()->fakultas->nama_fakultas }}</b>
                    </h5>
                    <div class="heading-elements">
                        {{-- <button type="button" class="btn btn-primary btn-sm" id="tombolTambahUsulanBaru">Tambah Usulan Baru</button> --}}
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table datatable-show-all datatable-basic">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Usulan</th>
                                <th>Jenis Usulan</th>
                                <th width="">Nama Pengusul</th>
                                <th>NIP</th>
                                <th>Waktu Usulan Diajukan</th>
                                <th>Status Usulan</th>
                                {{-- <th class="text-center">#</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usulans as $usulan)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a
                                        href="{{ route('kepegawaian-fakultas.usulan.terbit.kelola', ['id'=>$usulan->id]) }}">{{ $usulan->kode_usulan }}</a>
                                </td>
                                <td>{{ $usulan->jenisPensiun->nama }}</td>
                                <td>{{ $usulan->pegawai->NAMA_LENGKAP }}</td>
                                <td>{{ $usulan->pegawai->NIP }}</td>
                                <td>{{ Helpers::tanggalIndo(date('d-m-Y', strtotime($usulan->created_at)), false) }}</td>
                                <td>
                                    @if ($usulan->status_usulan_kepeg_untan == 3)
                                    <span class="label label-success">Dikirim</span>
                                    @endif
                                    @if ($usulan->status_usulan_kepeg_untan == 4)
                                    <span class="label label-success">SK Terbit</span>
                                    @endif
                                </td>
                                {{-- <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                @if ($usulan->status_usulan_kepeg_untan != 2 && $usulan->status_usulan_kepeg_untan != 3)
                                                    <li><a href="#" onclick="verifUsulan({{$usulan->id}})" title="Verifikasi Usulan"><i class="icon-file-pdf"></i> Verifikasi Usulan</a></li>
                                                @endif
                                                @if ($usulan->status_usulan_kepeg_untan == 2)
                                                    <li><a href="#" onclick="kirimUsulan({{$usulan->id}})" title="Tandai bahwa usulan telah di kirim ke Dikti "><i class="icon-file-pdf"></i> Tandai Telah Dikirim</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    </ul>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /latest posts -->

        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#usulan_terbit').addClass('active');
</script>
@endpush
