@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Beranda')

@push('css')

@endpush

@section('breadcrumb')

<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class="active"><a href="#"><i class="icon-home2 position-left"></i> Beranda</a>
        </li>
    </ul>
    @include('layouts.theme.profile')
</div>

@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-6">
            <div class="panel bg-pink-400">
                <div class="panel-body">
                    <div class="heading-elements">
                        <ul class="icons-list">
                            {{-- <li><a data-action="reload"></a></li> --}}
                        </ul>
                    </div>
                    <h3 class="no-margin">{{$jumlahProses}} Usulan</h3>
                    <a href="/kepegawaian-fakultas/usulan" style="color:white;"> Usulan Diproses</a>
                    <div class="text-muted text-size-small"></div>
                </div>

                <div id="today-revenue"></div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel bg-blue-400">
                <div class="panel-body">
                    <div class="heading-elements">
                        <ul class="icons-list">
                            {{-- <li><a data-action="reload"></a></li> --}}
                        </ul>
                    </div>
                    <h3 class="no-margin">{{$skUsulanTerbit}} Usulan</h3>
                    <a href="/kepegawaian-fakultas/usulan" style="color:white;">SK Usulan Terbit</a>
                    <div class="text-muted text-size-small"></div>
                </div>
                <div id="today-revenue"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Data Pegawai</h5>
                    {{-- <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div> --}}
                </div>

                <div class="panel-body">
                    Data pegawai yang akan pensiun dalam 2 tahun kedepan di Unit Kerja {{ auth()->user()->fakultas->nama_fakultas }}.
                </div>

                <div class="table-responsive">
                    <table class="table datatable-show-all datatable-basic" id="tableDx">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Tanggal Lahir</th>
                                <th>Tahun Pensiun</th>
                                <th>Jabatan</th>
                                <th>Status Usulan</th>
                                <th>Status Notif</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dosenPensiun as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ date('d-m-Y', strtotime($item->tgl_lahir)) }}</td>
                                <td>{{ $item->tahun_pensiun }}</td>
                                @if ($item->dataDosen->JAB_FUNGSIONAL == 'GB')
                                <td>Guru Besar</td>
                                @elseif ($item->dataDosen->JAB_FUNGSIONAL == 'LK')
                                <td>Lektor Kepala</td>
                                @elseif ($item->dataDosen->JAB_FUNGSIONAL == 'L')
                                <td>Lektor</td>
                                @elseif ($item->dataDosen->JAB_FUNGSIONAL == 'AA')
                                <td>Asisten Ahli</td>
                                @elseif ($item->dataDosen->JAB_FUNGSIONAL == 'TP')
                                <td>Tenaga Pengajar</td>
                                @else
                                <td></td>
                                @endif

                                @if ($item->usulan != null)
                                    @if ($item->usulan->status_usulan_kepeg_untan == 0)
                                    <td><span class="label bg-primary">Diproses</span></td>
                                    @elseif ($item->usulan->status_usulan_kepeg_untan == 1)
                                    <td>
                                        <span class="label bg-primary">Diproses Kepeg Untan</span>
                                    </td>
                                    @elseif ($item->usulan->status_usulan_kepeg_untan == 2)
                                    <td>
                                        <span class="label bg-primary">Diverifikasi</span>
                                    </td>
                                    @elseif ($item->usulan->status_usulan_kepeg_untan == 3)
                                    <td>
                                        <span class="label bg-primary">Dikirim Ke Dikti</span>
                                    </td>
                                    @elseif ($item->usulan->status_usulan_kepeg_untan == 4)
                                    <td>
                                        <span class="label bg-success">SK Terbit</span>
                                    </td>
                                    @else
                                    <td>
                                        <span class="label bg-primary">Belum Diproses</span>
                                    </td>
                                    @endif
                                @else
                                    <td>
                                        <span class="label bg-primary">Belum Diproses</span>
                                    </td>
                                @endif

                                {{-- <td>{{ $item->statusKirimNotifikasi }}</td> --}}
                                @if ($item->statusKirimNotifikasi != null)
                                <td>{!! $item->statusKirimNotifikasi->status == 1 ? '<span class="label bg-success">Sudah Terkirim</span>' : '<span class="label bg-primary">Belum Dikirim</span>' !!}</td>
                                @else
                                <td>Belum</td>
                                @endif

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Usulan Pertahun</h5>
                        <div class="heading-elements">
                            <form class="heading-form" action="#">
                                <div class="form-group">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="chart-container">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('layouts.theme.footer')
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>
    $('#beranda').addClass('active');
    $('#tableD tr:last').remove()
</script>
<script type="text/javascript">
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [{{ $year }}],
        datasets: [{
            label: 'Jumlah Pegawai Pensiun',
            data: [{{$usulan}}]  ,
            // data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                 'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
@endpush
