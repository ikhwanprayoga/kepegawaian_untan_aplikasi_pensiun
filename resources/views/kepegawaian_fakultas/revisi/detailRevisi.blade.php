@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Usulan')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"> Usulan</a></li>
        <li class="active">Detail Revisi</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- data pribadi pengusul -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data Pribadi Pengusul</h6>
                    {{-- <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div> --}}
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="{{asset('profil.png')}}" height="200" width="150" />
                        </div>

                        <div class="col-lg-2">
                            <div id="content">
                                <p>Nama </p>
                                <p>Tempat, Tanggal Lahir</p>
                                <p>NIP</p>
                                <p>NIDN / NIDK </p>
                                <p>Alamat E-mail  </p>
                                <p>Jabatan Fungsional </p>
                                <p>Golongan Ruang </p> 
                                <p>Jurusan </p>
                                <p>Program Studi </p>
                            </div>
                        </div>
                           
                            <div class="col-lg-6">
                                <p>: {{$usulan->dosen->NAMA_LENGKAP }}</p>
                                <p>: {{$usulan->dosen->Tempat_Lahir}},{{$usulan->dosen->Tgl_Lahir}} </p>
                                <p>: {{$usulan->dosen->NIP }}</p>
                                <p>: {{$usulan->dosen->NIDN}}</p>
                                <p>: {{$usulan->dosen->ALAMAT_EMAIL }}</p>
                                <p>: {{ $usulan->dosen->JAB_FUNGSIONAL}}</p>
                                <p>: {{ $usulan->dosen->Gol }}</p>
                                <p>: {{ $usulan->dosen->Jurusan}}</p>
                                <p>: {{ $usulan->dosen->Program_Studi}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /data pribadi pengusul -->

            <!-- daftar formulir -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="heading-elements">
                        
                    </div>
                    <h6 class="panel-title">Catatan Revisi</h6>
                    {{-- <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div> --}}
                </div>

                <div class="panel-body">
                    <!-- Info blocks -->
                    @foreach ($usulan->revisi as $r)
                        <hr>
                        <h3>No. {{ $loop->iteration }}</h3>
                        <p>Dibuat Saat : {{ $r->created_at }}</p>
                        <p><h5>Pesan :</h5><br>{!! $r->catatan_revisi !!}</p>
                        @if ($r->status == 1)
                            <p>Revisi Selesai</p>
                        @else
                            <a href="{{ route('kepegawaian-fakultas.usulan.selesai', $r->id) }}" class="btn btn-primary">Revisi Selesai</a>
                        @endif
                    @endforeach
                    <!-- /info blocks -->
                </div>
            </div>
            <!-- /daftar formulir -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>

<script>
    $('#usulan').addClass('active');
</script>
@endpush