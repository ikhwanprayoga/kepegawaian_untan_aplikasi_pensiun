@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Beranda')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Profil Akun</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">
            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Profil Operator Fakultas dan Informasi Fakultas</h6>
                    <div class="heading-elements">
                        {{-- <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul> --}}
                    </div>
                </div>
                    <!-- Form horizontal -->
                <div class="panel-body">
                    <form class="form-horizontal">
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Nama</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" id="name" value="{{$user->name}}" >
                                </div>
                            </div>

                                <div class="form-group">
                                <label class="control-label col-lg-2">Username</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" id="username" value="{{$user->username}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" id="email" value="{{$user->email}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">No HP</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" id="no_hp" value="{{$user->no_hp}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-12" style="font-size: larger;">Informasi Fakultas</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Nama Lengkap Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->nama_lengkap_fakultas) ? $user->fakultas->infoFakultas->nama_lengkap_fakultas : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Nama Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->nama_dekan) ? $user->fakultas->infoFakultas->nama_dekan : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Nip Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->nip_dekan) ? $user->fakultas->infoFakultas->nip_dekan : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Pangkat Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->pangkat_dekan) ? $user->fakultas->infoFakultas->pangkat_dekan : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Golongan Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->golongan_dekan) ? $user->fakultas->infoFakultas->golongan_dekan : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Alamat Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->alamat_fakultas) ? $user->fakultas->infoFakultas->alamat_fakultas : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">No Telepon Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->no_telepon_fakultas) ? $user->fakultas->infoFakultas->no_telepon_fakultas : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Email Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->email_fakultas) ? $user->fakultas->infoFakultas->email_fakultas : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Fax Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->fax_fakultas) ? $user->fakultas->infoFakultas->fax_fakultas : '' }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Kode Pos Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" readonly="readonly" value="{{ isset($user->fakultas->infoFakultas->kode_pos_fakultas) ? $user->fakultas->infoFakultas->kode_pos_fakultas : '' }}">
                                </div>
                            </div>

                        </fieldset>
                        <div class="text-right">
                            <a href="{{ route('kepegawaian-fakultas.profil.editprofil', ['id' => $user->id]) }}">
                                <button type="button" class="btn btn-primary">Ubah</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <!-- /main content -->
    </div>
    @include('layouts.theme.footer')

</div>
@endsection

@push('js')

<script>
    // $('#beranda').addClass('active');
</script>
@endpush
