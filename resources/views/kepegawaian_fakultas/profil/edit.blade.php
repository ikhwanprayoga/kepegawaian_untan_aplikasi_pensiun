@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.profil') }}"> Profil Akun</a></li>
        <li class="active">Edit Profil Akun</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Edit Profil Operator Fakultas dan Informasi Fakultas</h6>
                    {{-- <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div> --}}
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{url ('kepegawaian-fakultas/profil/update/' .$user->id)}}" method="post">
                        @csrf
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Nama</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="Masukkan nama user" required>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-lg-2">Username</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="username" id="username" value="{{$user->username}}" placeholder="Masukkan nama user" required>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="email" id="email" value="{{$user->email}}" placeholder="Masukkan email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">No HP</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="no_hp" id="no_hp" value="{{$user->no_hp}}" placeholder="Masukkan no hp" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Password Baru</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" name="password" value="">
                                    <small>kosongkan field password jika tidak ingin mengubah password</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Ulangi Password</label>
                                <div class="col-lg-10">
                                    <input type="password" name="password_confirmation" class="form-control" id="password" value="">
                                    <small>kosongkan field password jika tidak ingin mengubah password</small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-12" style="font-size: larger;">Informasi Fakultas</label>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Nama Lengkap Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="nama_lengkap_fakultas" value="{{ isset($user->fakultas->infoFakultas->nama_lengkap_fakultas) ? $user->fakultas->infoFakultas->nama_lengkap_fakultas : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Nama Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="nama_dekan" value="{{ isset($user->fakultas->infoFakultas->nama_dekan) ? $user->fakultas->infoFakultas->nama_dekan : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Nip Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="nip_dekan" value="{{ isset($user->fakultas->infoFakultas->nip_dekan) ? $user->fakultas->infoFakultas->nip_dekan : '' }}" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-lg-2">Pangkat Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="pangkat_dekan" value="{{ isset($user->fakultas->infoFakultas->pangkat_dekan) ? $user->fakultas->infoFakultas->pangkat_dekan : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Golongan Dekan Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="golongan_dekan" value="{{ isset($user->fakultas->infoFakultas->golongan_dekan) ? $user->fakultas->infoFakultas->golongan_dekan : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Alamat Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="alamat_fakultas" value="{{ isset($user->fakultas->infoFakultas->alamat_fakultas) ? $user->fakultas->infoFakultas->alamat_fakultas : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">No Telepon Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="no_telepon_fakultas" value="{{ isset($user->fakultas->infoFakultas->no_telepon_fakultas) ? $user->fakultas->infoFakultas->no_telepon_fakultas : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Email Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="email_fakultas" value="{{ isset($user->fakultas->infoFakultas->email_fakultas) ? $user->fakultas->infoFakultas->email_fakultas : '' }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Fax Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="fax_fakultas" value="{{ isset($user->fakultas->infoFakultas->fax_fakultas) ? $user->fakultas->infoFakultas->fax_fakultas : '' }}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2">Kode Pos Fakultas</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="kode_pos_fakultas" value="{{ isset($user->fakultas->infoFakultas->kode_pos_fakultas) ? $user->fakultas->infoFakultas->kode_pos_fakultas : '' }}" >
                                </div>
                            </div>

                        </fieldset>
                        <div class="text-right">

                            <a href="{{ route('kepegawaian-fakultas.profil', ['id' => $user->id]) }}">
                                <button type="button" class="btn btn-default">Batal</button>
                            </a>

                            <button type="submit" class="btn btn-primary">Simpan</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')

<script>
    // $('#beranda').addClass('active');
</script>
@endpush