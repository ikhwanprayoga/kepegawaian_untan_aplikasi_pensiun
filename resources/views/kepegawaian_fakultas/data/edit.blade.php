@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i>
                Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"> Usulan</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]) }}"> Kelola Usulan
                {{ $usulan->kode_usulan }}</a></li>
        <li class="active">{{ $data->NAMA_LENGKAP }}</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Form update data baru untuk kelengkapan DPCP</h5>
            <div class="heading-elements">
            </div>
        </div>

        <div class="panel-body">

            <form class="form-horizontal" method="post"
                action="{{ route('kepegawaian-fakultas.data.update', $data->NIP) }}" enctype="multipart/form-data">
                @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Harap isi dengan data yang sebenar-benarnya</legend>

                    <input type="hidden" class="form-control" class="form-control" name="JK" value="{{ $data->JK }}">
                    <div class="form-group">
                        <label class="control-label col-lg-2">Upload Foto</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-styled-primary" name="foto">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="NAMA_LENGKAP"
                                value="{{ $data->NAMA_LENGKAP }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">NIP</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="NIP"
                                value="{{ $data->NIP }}"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Tempat Lahir</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="Tempat_Lahir"
                                value="{{ $data->Tempat_Lahir }}"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Tanggal Lahir {{ $data->Tgl_Lahir }}</label>
                        <div class="col-lg-3">
                            <input type="date" class="form-control" class="form-control" name="Tgl_Lahir"
                                {{-- value="{{ date('YYYY-MM-DD', strtotime($data->Tgl_Lahir)) }}"> --}}
                                value="{{ $data->Tgl_Lahir }}"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">No HP WA</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control" class="form-control" name="no_hp"
                                {{-- value="{{ date('YYYY-MM-DD', strtotime($data->Tgl_Lahir)) }}"> --}}
                                value="{{ $data->no_hp }}" placeholder="Contoh '089598675435'"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Jabatan</label>
                        <div class="col-lg-10">
                            {{-- <input type="text" class="form-control" class="form-control" name="JAB_FUNGSIONAL"
                                value="{{ }}"> --}}
                            <select class="select" name="JAB_FUNGSIONAL" required> 
                                <option value="GB" {{ $data->JAB_FUNGSIONAL == 'GB' ? 'selected' : '' }} >GB (GURU BESAR)</option>
                                <option value="LK" {{ $data->JAB_FUNGSIONAL == 'LK' ? 'selected' : '' }} >LK (LEKTOR KEPALA)</option>
                                <option value="L" {{ $data->JAB_FUNGSIONAL == 'L' ? 'selected' : '' }} >L (LEKTOR)</option>
                                <option value="AA" {{ $data->JAB_FUNGSIONAL == 'AA' ? 'selected' : '' }} >AA (ASISTEN AHLI)</option>
                                <option value="TP" {{ $data->JAB_FUNGSIONAL == 'TP' ? 'selected' : '' }} >TP (TENAGA PENGAJAR)</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">NIDN</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="NIDN"
                                value="{{ $data->NIDN}}"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat Email</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="ALAMAT_EMAIL"
                                value="{{ $data->ALAMAT_EMAIL}}" placeholder="Contoh 'saya@gmail.com'"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Golongan</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="Gol"
                                value="{{ $data->Gol}}"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="Jurusan"
                                value="{{ $data->Jurusan}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Program Studi</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="Program_Studi"
                                value="{{ $data->Program_Studi}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Pangkat</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="pangkat"
                                value="{{$data->pangkat}}" placeholder="Contoh 'Pembina'"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">TMT</label>
                        <div class="col-lg-3">
                            <input type="date" class="form-control" class="form-control" name="tmt"
                                value="{{$data->tmt}}" required >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Gaji Pokok Terakhir</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="gaji_pokok_terakhir"class="form-control" name="gaji_pokok_terakhir"
                                value="{{$data->gaji_pokok_terakhir}}" placeholder="Rp. 3.500.000"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">No NPWP</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="npwp"
                                value="{{$data->npwp}}" placeholder="Contoh '65443555'">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Masa Kerja Golongan</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" value="{{$data->masa_kerja_golongan_tahun}}"  name="masa_kerja_golongan_tahun">
                                            </div>
                                            <div class="col-md-8">
                                                <label class="control-label col-lg-12">Tahun</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" value="{{$data->masa_kerja_golongan_bulan}}" name="masa_kerja_golongan_bulan">
                                            </div>
                                            <div class="col-md-8">
                                                <label class="control-label col-lg-12">Bulan.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label col-lg-12">Pada Tanggal</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="date" class="form-control" value="{{$data->masa_kerja_golongan_kalender}}"  name="masa_kerja_golongan_kalender">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Masa Kerja Pensiun</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" value="{{$data->masa_kerja_pensiun_tahun}}"  name="masa_kerja_pensiun_tahun">
                                            </div>
                                            <div class="col-md-8">
                                                <label class="control-label col-lg-12">Tahun</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="number" class="form-control" value="{{$data->masa_kerja_pensiun_bulan}}"  name="masa_kerja_pensiun_bulan">
                                            </div>
                                            <div class="col-md-8">
                                                <label class="control-label col-lg-12">Bulan.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Masa Kerja Sebelum Diangkat Sebagai PNS</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label col-lg-12">Dari</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="date" class="form-control" value="{{$data->belum_pns_dari}}"  name="belum_pns_dari">
                                            </div>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label col-lg-12">S/D</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="date" class="form-control" value="{{$data->belum_pns_sampai_dengan}}"  name="belum_pns_sampai_dengan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Mulai Masuk PNS</label>
                        <div class="col-lg-2">
                            <input type="date" class="form-control" class="form-control" name="mulai_masuk_pns"
                                value="{{$data->mulai_masuk_pns}}"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Pendidikan Dasar</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="pendidikan_dasar"
                                value="{{$data->pendidikan_dasar}}" placeholder="Contoh 'SPG PENDIDIKAN SD'" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Lulus Tahun</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" class="form-control" name="lulus_tahun"
                                value="{{$data->lulus_tahun}}" placeholder="Contoh '1992'" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat Jalan</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_jl"
                            placeholder="Contoh 'Jalan Tanjung Sari Gg. Amanah 3 No. 3'"    
                            value="{{$data->alamat_jl}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat RT</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_rt"
                            placeholder="Contoh 'RT 2'"      
                            value="{{$data->alamat_rt}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat RW</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_rw"
                            placeholder="Contoh 'RW 4'"     
                            value="{{$data->alamat_rw}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat Kelurahan</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_kelurahan"
                            placeholder="Contoh 'Kelurahan Bansir Laut'"     
                            value="{{$data->alamat_kelurahan}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat Kecamatan</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_kecamatan"
                            placeholder="Contoh 'Pontianak Tenggara'"     
                            value="{{$data->alamat_kecamatan}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat Kota</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_kota"
                            placeholder="Contoh 'Pontianak'"     
                            value="{{$data->alamat_kota}}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Alamat Provinsi</label>
                        <div class="col-lg-10">
                            <input type="text" rows="4" class="form-control" class="form-control" name="alamat_provinsi"
                            placeholder="Contoh 'Kalimantan Barat'"     
                            value="{{$data->alamat_provinsi}}" required>
                        </div>
                    </div>

                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i
                            class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>

    @include('layouts.theme.footer')
</div>
<!-- /main content -->


</div>
@endsection

@push('js')
<script>
    $('#usulan').addClass('active');
    var rupiah = document.getElementById("gaji_pokok_terakhir");
    rupiah.addEventListener("keyup", function(e) {
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, "Rp. ");
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? "." : "";
        rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
    }

</script>
@endpush
