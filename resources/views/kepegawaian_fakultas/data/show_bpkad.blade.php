@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Data Pegawai BPKAD')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i>
                Beranda</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.data.show', $data['nip']) }}"> Data</a></li>
        {{-- <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]) }}"> Kelola Usulan {{ $usulan->kode_usulan }}</a></li> --}}
        <li class="active">{{ $data['nama_lengkap'] }}</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- data pribadi pengusul -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data pegawai dari BPKAD</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <a href="{{ route('kepegawaian-fakultas.data.show', $data['nip']) }}" class="btn btn-warning btn-sm">Kembali </a>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        {{-- <div class="col-lg-3" style="text-align:center;">
                            @if ($foto == 1 && $data->foto != null)
                            <img src="{{ Storage::url('public/pegawai/'.$data->foto) }}" style="width: 200px; height: 100%;" />
                            @else
                            <img src="{{asset('assets/images/allgender.png')}}" style="width: 200px; height: 100%;" />
                            @endif
                        </div> --}}
                        <div class="col-lg-2">
                            <div id="content">
                                <p>Nama Lengkap </p>
                                <p>NIP</p>
                                <p>NIDN</p>
                                <p>Tempat Lahir</p>
                                <p>Tanggal Lahir</p>
                                <p>Golongan </p>
                                <p>Fakultas </p>
                                <p>Bidang </p>
                                <p>S1 </p>
                                <p>S2 </p>
                                <p>S3 </p>
                                <p>Email </p>
                                <p>No HP</p>
                                <p>Alamat Jalan</p>
                                <p>Alamat RT</p>
                                <p>Alamat RW</p>
                                <p>Alamat Kelurahan</p>
                                <p>Alamat Kecamatan</p>
                                <p>Alamat Kota</p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <p>: {{$data['nama_lengkap'] }}</p>
                            <p>: {{$data['nip']}}</p>
                            <p>: {{$data['nidn']}}</p>
                            <p>: {{$data['tempat_lahir']}}</p>
                            <p>: {{$data['tanggal_lahir']}}</p>
                            <p>: {{$data['golongan']}}</p>
                            <p>: {{$data['fakultas']}}</p>
                            <p>: {{$data['bidang']}}</p>
                            <p>: {{$data['s1']}}</p>
                            <p>: {{$data['s2']}}</p>
                            <p>: {{$data['s3']}}</p>
                            <p>: {{$data['email']}}</p>
                            <p>: {{$data['alamat_telp']}}</p>
                            <p>: {{$data['alamat_jl']}}</p>
                            <p>: {{$data['alamat_rt']}}</p>
                            <p>: {{$data['alamat_rw']}}</p>
                            <p>: {{$data['alamat_kelurahan']}}</p>
                            <p>: {{$data['alamat_kecamatan']}}</p>
                            <p>: {{$data['alamat_kota']}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /data pribadi pengusul -->
    @include('layouts.theme.footer')
</div>

</div>
<!-- /main content -->

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>

<script>
    $('#usulan').addClass('active');
</script>
@endpush
