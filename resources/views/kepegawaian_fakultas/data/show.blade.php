@extends('layouts.theme.base')

@section('title', 'Kepegawaian Fakultas Kelola Usulan')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li class=""><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home2 position-left"></i>
                Beranda</a></li>
        @if ($usulan->status_usulan_kepeg_untan == 4)
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.terbit.index') }}"> Usulan Terbit</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.terbit.kelola', ['id' => $usulan->id]) }}"> Kelola Usulan Terbit {{ $usulan->kode_usulan }}</a></li>
        <li class="active">{{ $pegawai->NAMA_LENGKAP }}</li>
        @else
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"> Usulan</a></li>
        <li class=""><a href="{{ route('kepegawaian-fakultas.usulan.kelola', ['id' => $usulan->id]) }}"> Kelola Usulan {{ $usulan->kode_usulan }}</a></li>
        <li class="active">{{ $pegawai->NAMA_LENGKAP }}</li>
        @endif
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- data pribadi pengusul -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Data pegawai untuk kelengkapan DPCP</h6>
                    <div class="heading-elements">
                        @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                        <ul class="icons-list">
                            <a target="_blank" href="{{ route('kepegawaian-fakultas.search.bpkad', $pegawai->NIP) }}" class="btn btn-warning btn-sm">Lihat data BKD </a>
                        </ul>
                        <ul class="icons-list">
                            <a href="{{ route('kepegawaian-fakultas.data.editdata', $pegawai->NIP) }}" class="btn btn-warning btn-sm">Perbaharui data pegawai </a>
                        </ul>
                        @endif
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3 col-xs-4" style="text-align:center;">
                            @if ($foto == 1 && $pegawai->foto != null)
                            <img src="{{ Storage::url('public/pegawai/'.$pegawai->foto) }}" style="width: 200px; height: 100%;" />
                            @else
                            <img src="{{asset('assets/images/allgender.png')}}" style="width: 200px; height: 100%;" />
                            @endif
                        </div>
                        <div class="col-lg-2 col-xs-2">
                            <div id="content">
                                <p>Nama </p>
                                <p>NIP</p>
                                <p>Tempat Lahir</p>
                                <p>Tanggal Lahir</p>
                                <p>No HP </p>
                                <p>Jabatan </p>
                                <p>Pangkat </p>
                                <p>TMT </p>
                                <p>Gaji Pokok Terakhir </p>
                                <p>No NPWP </p>
                                <p>Mulai Masuk Pns</p>
                                {{-- <p>Masa kerja Kp terakhir </p> --}}
                                <p>Masa Kerja Golongan </p>
                                <p>Masa Kerja Pensiun</p>
                                <p>CLTN</p>
                                {{-- <p>Peninjauan Masa Kerja</p> --}}
                                <p>Pendidikan Dasar</p>
                                <p>Lulus Tahun</p>
                                <p>Alamat</p>

                            </div>
                        </div>

                        <div class="col-lg-6 col-xs-6">
                            <p>: {{$pegawai->NAMA_LENGKAP }}</p>
                            <p>: {{$pegawai->NIP}}</p>
                            <p>: {{$pegawai->Tempat_Lahir}}</p>
                            <p>: {{ date('d-m-Y', strtotime ($pegawai->Tgl_Lahir))}}</p>
                            <p>: {{$pegawai->no_hp}}</p>
                            <p>: {{ Helpers::jabatanLengkap($pegawai->NIP)}}</p>
                            <p>: {{$pegawai->pangkat}}</p>
                            <p>: {{date('d-m-Y', strtotime($pegawai->tmt))}}</p>
                            <p>: {{$pegawai->gaji_pokok_terakhir }}</p>
                            <p>: {{$pegawai->npwp }}</p>
                            <p>: {{date('d-m-Y', strtotime($pegawai->mulai_masuk_pns))}}</p>
                            {{-- <p>: {{$pegawai->masa_kerja_kp_terakhir }}</p> --}}
                            <p>: {{$pegawai->masa_kerja_golongan_tahun}} Tahun  {{$pegawai->masa_kerja_golongan_bulan}} Bulan Pada TGL {{date('d-m-Y', strtotime($pegawai->masa_kerja_golongan_kalender))}}</p>
                            <p>: {{$pegawai->masa_kerja_pensiun_tahun}} Tahun  {{$pegawai->masa_kerja_pensiun_bulan}} Bulan</p>
                            <p>: {{$pegawai->cltn}}</p>
                            {{-- <p>: {{$pegawai->peninjauan_masa_kerja}}</p> --}}
                            <p>: {{$pegawai->pendidikan_dasar}}</p>
                            <p>: {{$pegawai->lulus_tahun}}</p>
                            <p>: {{$pegawai->alamat_jl }} {{$pegawai->alamat_rt }} {{$pegawai->alamat_rw }} Kelurahan {{$pegawai->alamat_kelurahan }} Kecamatan {{$pegawai->alamat_kecamatan }} Kabupaten/Kota {{$pegawai->alamat_kota }} Provinsi {{$pegawai->alamat_provinsi }}</p>
                        </div>
                    </div>

                    <hr>

                    <div>
                        <div id="content">
                            <div class="panel-heading">
                                <h6 class="panel-title">Data {{ $pegawai->JK == 'L' ? 'istri' : 'suami' }}</h6>
                                <div class="heading-elements">
                                    @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                                    <td><button type="button" data-toggle="modal" data-target="#modal_form_horizontal2" class="btn btn-primary btn-sm">Tambah data suami/istri </i></button></td>
                                    @endif
                                </div>
                            </div>
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Tanggal Kawin</th>
                                        <th>Tanggal Cerai</th>
                                        <th>{{ $pegawai->JK == 'L' ? 'Istri Ke' : 'Suami Ke' }}</th>
                                        @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                                        <th>Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($keluarga as $kl)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$kl->nik}}</td>
                                        <td>{{$kl->nama}}</td>
                                        <td>{{date('d-m-Y', strtotime($kl->tanggal_lahir))}}</td>
                                        <td>{{$kl->tanggal_kawin}}</td>
                                        <td>{{$kl->tanggal_cerai}}</td>
                                        <td>{{$kl->pasangan_keberapa}}</td>
                                        @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                                        <td>
                                            <button type="button" data-toggle="modal" data-target="#editData_pasangan{{$kl->id}}" class="btn btn-success btn-sm" class="right">Edit</i></button>
                                            <div id="editData_pasangan{{$kl->id}}" class="modal fade">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-primary">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h5 class="modal-title">Ubah Data Suami/Istri</h5>
                                                        </div>

                                                        <div class="modal-body">
                                                            <form method="post" action="{{ route('kepegawaian-fakultas.data.update_keluarga', $kl->id) }}" class="form-horizontal">
                                                                @csrf
                                                                <legend class="text-bold">*Isi Data Dengan Benar</legend>
                                                                <input type="hidden" class="form-control" class="form-control" name="NIP" value="{{ $kl->NIP }}">

                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">Nama*</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control" class="form-control" name="nama" value="{{ $kl->nama }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">NIK</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="text" class="form-control" class="form-control" name="nik" value="{{ $kl->nik }} ">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">Tanggal Lahir*</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="date" class="form-control" class="form-control" name="tanggal_lahir" value="{{ $kl->tanggal_lahir }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">Tanggal Kawin</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="date" class="form-control" class="form-control" name="tanggal_kawin" value="{{ $kl->tanggal_kawin }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">Tanggal Cerai</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="date" class="form-control" class="form-control" name="tanggal_cerai" value="{{ $kl->tanggal_cerai }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label col-lg-2">Suami/Isri Ke*</label>
                                                                    <div class="col-lg-10">
                                                                        <input type="number" class="form-control" class="form-control" name="pasangan_keberapa" value="{{ $kl->pasangan_keberapa }}" required>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                                                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-danger" type="submit" onclick="deleteKeluarga({{$kl->id}})" >Hapus</button>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <hr>

                    <div class="panel-heading">
                        <h6 class="panel-title">Data Anak</h6>
                        <div class="heading-elements">
                            @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                            <td><button type="button" data-toggle="modal" data-target="#modal_form_horizontal1" class="btn btn-success btn-sm" class="right">Tambah Data Anak </i></button></td>
                            @endif
                        </div>
                    </div>
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Tanggal Lahir</th>
                                <th>Nama Ayah</th>
                                <th>Nama Ibu</th>
                                <th>Keterangan</th>
                                @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)
                                <th>Aksi</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($anak as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->nik}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{date('d-m-Y', strtotime($item->tanggal_lahir))}}</td>
                                <td>{{$item->nama_ayah}}</td>
                                <td>{{$item->nama_ibu}}</td>
                                <td>{{$item->keterangan}}</td>
                                @if ($usulan->status_usulan_kepeg_untan == 0 || $usulan->status_usulan_kepeg_untan == 1)  
                                <td>
                                <button type="button" data-toggle="modal" data-target="#editData_anak{{$item->id}}" class="btn btn-success btn-sm" class="right">Edit</i></button>
                                <div id="editData_anak{{$item->id}}" class="modal fade">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h5 class="modal-title">Edit Data Anak</h5>
                                            </div>

                                            <div class="modal-body">
                                                <form class="form-horizontal" method="post" action="{{ route('kepegawaian-fakultas.data.update_anak', $item->id) }}">
                                                    @csrf
                                                    <legend class="text-bold">*Isi Data Dengan Benar</legend>
                                                    <input type="hidden" class="form-control" class="form-control" name="NIP" value="{{ $item->NIP }}">

                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Nama*</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control" class="form-control" name="nama" value="{{ $item->nama }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">NIK</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control" class="form-control" name="nik" value="{{ $item->nik }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Tanggal Lahir*</label>
                                                        <div class="col-lg-10">
                                                            <input type="date" class="form-control" class="form-control" name="tanggal_lahir" value="{{ $item->tanggal_lahir }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Nama Ayah*</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control" class="form-control" name="nama_ayah" value="{{ $item->nama_ayah }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Nama Ibu*</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" class="form-control" class="form-control" name="nama_ibu" value="{{ $item->nama_ibu }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label col-lg-2">Keterangan</label>
                                                        <div class="col-lg-10">
                                                            {{-- <input type="text" class="form-control" class="form-control" name="keterangan" value="{{ $item->keterangan}}"> --}}
                                                            <select class="select" name="keterangan">
                                                                <option value="Anak Kandung" {{ $item->keterangan == "Anak Kandung" ? "selected" : ""  }} >Anak Kandung</option>
                                                                <option value="Anak Tiri" {{ $item->keterangan == "Anak Tiri" ? "selected" : ""  }} >Anak Tiri</option>
                                                                <option value="Anak Angkat {{ $item->keterangan == "Anak Angkat" ? "selected" : ""  }}" >Anak Angkat</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                                        <button type="submit" class="btn btn-primary">Edit Data</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                    <button class="btn btn-danger" type="submit" onclick="deleteAnak({{$item->id}})" >Hapus</button>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            </td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /data pribadi pengusul -->
    @include('layouts.theme.footer')
</div>

<!-- /main content -->
{{-- modal anak --}}
<div id="modal_form_horizontal1" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Tambah Data Anak</h5>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('kepegawaian-fakultas.data.tambah_anak') }}"
                    class="form-horizontal">
                    @csrf
                    <input type="hidden" class="form-control" class="form-control" name="NIP" value="{{ $pegawai->NIP }}">
                    <legend class="text-bold">*Isi Data Dengan Benar</legend>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama*</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="nama" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">NIK</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="nik">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Tanggal Lahir*</label>
                        <div class="col-lg-10">
                            <input type="date" class="form-control" class="form-control" name="tanggal_lahir" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Ayah*</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="nama_ayah" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama Ibu*</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="nama_ibu" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Keterangan</label>
                        <div class="col-lg-10">
                            <select class="select" name="keterangan">
                                <option value="Anak Kandung" >Anak Kandung</option>
                                <option value="Anak Tiri" >Anak Tiri</option>
                                <option value="Anak Angkat" >Anak Angkat</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- modal pasangan --}}
<div id="modal_form_horizontal2" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Tambah Data Suami/Istri</h5>
            </div>

            <div class="modal-body">
                <form method="post" action="{{ route('kepegawaian-fakultas.data.tambah_keluarga') }}"
                    class="form-horizontal">
                    @csrf
                    <input type="hidden" class="form-control" class="form-control" name="NIP" value="{{ $pegawai->NIP }}">
                    <legend class="text-bold">*Isi Data Dengan Benar</legend>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Nama*</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="nama" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">NIK</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" class="form-control" name="nik">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Tanggal Lahir*</label>
                        <div class="col-lg-10">
                            <input type="date" class="form-control" class="form-control" name="tanggal_lahir" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Tanggal Kawin*</label>
                        <div class="col-lg-10">
                            <input type="date" class="form-control" class="form-control" name="tanggal_kawin">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Tanggal Cerai</label>
                        <div class="col-lg-10">
                            <input type="date" class="form-control" class="form-control" name="tanggal_cerai">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">{{ $pegawai->JK == 'L' ? 'Istri Ke' : 'Suami Ke' }}</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" class="form-control" name="pasangan_keberapa" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambah Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')

@if ($usulan->status_usulan_kepeg_untan != 4)                                       
<script>
    $('#usulan').addClass('active');
</script>
@else
<script>
    $('#usulan_terbit').addClass('active');
</script>
@endif
<script>
function deleteKeluarga(id) {
    var url = '{{ url('kepegawaian-fakultas/data/delete/keluarga') }}'
    // console.log(url+'/'+id)
    swal({
        title: "Apakah anda yakin?",
        text: "ingin menghapus data ini",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
    if (willDelete) {
        $.ajax({
            type: 'delete',
            url: url+'/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                swal("Data berhasil dihapus!", {
                    icon: "success",
                });
                location.reload();
            },
            error: function(errors) {
                swal("Data gagal dihapus!", {
                    icon: "warning",
                });
            },
        })

    } else {
        swal("Data tidak terhapus");
    }
    });
}
function deleteAnak(id) {
    var url = '{{ url('kepegawaian-fakultas/data/delete/anak') }}'
    // console.log(url+'/'+id)
    swal({
        title: "Apakah anda yakin?",
        text: "ingin menghapus data ini",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
    if (willDelete) {
        $.ajax({
            type: 'delete',
            url: url+'/'+id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                swal("Data berhasil dihapus!", {
                    icon: "success",
                });
                location.reload();
            },
            error: function(errors) {
                swal("Data gagal dihapus!", {
                    icon: "warning",
                });
            },
        })

    } else {
        swal("Data tidak terhapus");
    }
    });
}
</script>
@endpush
