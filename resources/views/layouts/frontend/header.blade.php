<header class="header_area">
    <div class="main_menu">
        {{-- <div class="search_input" id="search_input_box">
            <div class="container">
                <form class="d-flex justify-content-between" method="" action="">
                    <input type="text" class="form-control" id="search_input" placeholder="Search Here" />
                    <button type="submit" class="btn"></button>
                    <span class="ti-close" id="close_search" title="Close Search"></span>
                </form>
            </div>
        </div> --}}

        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="index.html"><img src="img/logonew.png" alt="" /></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="icon-bar"></span> <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li id="beranda" class="nav-item">
                            <a class="nav-link" href="{{ route('beranda') }}">Beranda</a>
                        </li>
                        <li id="tentang" class="nav-item">
                            <a class="nav-link" href="{{ route('tentang') }}">Tentang</a>
                        </li>
                        <li id="panduan" class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Panduan</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('panduan-aplikasi') }}">Panduan Aplikasi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('panduan-berkas') }}">Panduan Berkas</a>
                                </li>
                            </ul>
                        </li>
                        <li id="layanan" class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Layanan</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('layanan-sop') }}">SOP</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('layanan-produk-hukum') }}">Produk Hukum</a>
                                </li>
                            </ul>
                        </li>
                        <li id="kontak" class="nav-item">
                            <a class="nav-link" href="{{ route('kontak') }}">Kontak</a>
                        </li>
                        <!-- <li class="nav-item">
                  <a class="nav-link" href="contact.html"><button class="primary-btn2 mb-3 mb-sm-0 small">Login</button></a>
                </li> -->
                        <!-- login button -->
                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link search" id="search">
                                <i class="ti-search"></i>
                            </a>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
