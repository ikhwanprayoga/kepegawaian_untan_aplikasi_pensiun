<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 single-footer-widget">
                <ul>
                    <li><img src="{{ asset('frontend/img/ristekdikti.png') }}" alt="">&emsp;<img src="{{ asset('frontend/img/ristekdikti untan.png') }}" alt="">
                    </li>
                    <li>
                        <p>Universitas Tanjungpura Pontianak</p>
                        <p>Kementerian Riset, Teknologi, Dan Pendidikan Tinggi Republik Indonesia
                            (Ministry of Research, Technology, and Higher Education of the Republic of Indonesia)
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6 single-footer-widget">
                <h4>Tentang SILAP</h4>
                <p> SILAP (Sistem Informasi dan Layanan Pensiun) merupakan aplikasi online yang disiapkan oleh Bagian Hukum dan Kepegawaian
                    Universitas Tanjungpura dalam rangka melayani proses usulan Pensiun/Pemberhentian Dosen
                    di Lingkungan Universitas Tanjungpura
                    agar menjadi lebih mudah dan cepat.
                </p>
            </div>
            <div class="col-lg-4 col-md-6 single-footer-widget">
                <h4>Alamat / Kontak</h4>
                <p>Universitas Tanjungpura</p>
                <p>Bagian Hukum dan Kepegawaian Biro Umum dan Keuangan</p>
                <p>Jalan Prof. Dr. H. Hadari Nawawi, Pontianak Kalimantan Barat</p>
                <p>Email : Kepegawaian@untan.ac.id</p>
                Link Terkait :<a target="_blank" href={{ "kepegawaian.untan.ac.id" }} style="color : #f1c40f"> Website Kepegawaian</a>
            </div>
        </div>
        <div class="row footer-bottom d-flex justify-content-between">
            <p class="col-lg-8 col-sm-12 footer-text m-0 text-white">
                Copyright &copy;<script>
                    document.write(new Date().getFullYear());

                </script> Kepegawaian Universitas Tanjungpura</p>
        </div>
    </div>
</footer>
