<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('frontend/img/favicon.png') }}" type="image/png" />
    <title>Kepegawaian UNTAN - @yield('title')</title>
    <meta name="_token" content="{{ csrf_token() }}" />

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
        type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/toastr.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    @stack('css')

</head>

<body>

    <!-- Main navbar -->
    <div class="navbar navbar-inverse bg-indigo">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><img src="{{ asset('logo_silap.png') }}" style="height: 138%; margin-top: -4%; width: auto;" alt=""></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a> </li>
            </ul>

            <div class="navbar-right">

                <ul class="nav navbar-nav">

                    @role('operator fakultas')
                    @php
                    $notif = DB::table('notifikasi')
                                ->where('fakultas_id', auth()->user()->fakultas_id)
                                ->where('status', 0)
                                ->orderBy('id', 'desc')
                                ->get();
                    $jmlh_notif = $notif->count();
                    @endphp
                    @endrole

                    @role('operator untan')
                    @php
                    $notif = DB::table('notifikasi')
                                ->where('status', 0)
                                ->where('untuk_role', 'operator untan')
                                ->orderBy('id', 'desc')
                                ->get();
                    $jmlh_notif = $notif->count();
                    @endphp
                    @endrole

                    @role('superadmin')
                    @php
                    $notif = [];
                    $jmlh_notif = 0;
                    @endphp
                    @endrole

                    @hasanyrole('operator fakultas|operator untan|superadmin')
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-bell2"></i>
                            <span class="visible-xs-inline-block position-right">Notifikasi</span>
                            <span class=""><sup>{{ $jmlh_notif }}</sup></span>
                        </a>
                        <div class="dropdown-menu dropdown-content" style="height: 390px; overflow:auto;">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="dropdown-content-heading">
                                        Notifikasi
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    @role('operator fakultas')
                                    <div class="dropdown-content-heading"><a href="{{route('notifikasi-baca-semua',['fakultas_id'=>auth()->user()->fakultas->id])}}"><small>Tandai pesan dibaca</small></a></div>
                                    @endrole
        
                                    @role('operator untan')
                                    <div class="dropdown-content-heading"><a href="{{route('notifikasi-baca-semua-operatoruntan',['fakultas_id'=>auth()->user()->fakultas->id])}}"><small>Tandai pesan dibaca</small></a></div>
                                    @endrole
                                </div>
                            </div>
                            @if (count($notif) > 0)
                            @foreach ($notif as $item)
                            <ul class="media-list dropdown-content-body width-350">
                                <li class="media">
                                    <div class="media-left">
                                        <a href="#" class="btn bg-warning-400 btn-rounded btn-icon btn-xs"><i
                                                class="icon-bell2"></i></a>
                                    </div>
                                    <div class="media-body"> <a
                                            href="{{ route('notifikasi-baca', ['id' => $item->id]) }}">
                                            {{$item->pesan}}</a>
                                        <div class="media-annotation">{{$item->waktu_notifikasi_dikirim}}</div>

                                    </div>
                                </li>
                            </ul>
                            @endforeach
                            
                            @else
                            <ul class="media-list dropdown-content-body width-350">
                                <li class="media">
                                    <div class="media-body"> <a href="#">Tidak ada notifikasi</a>
                                    </div>
                                </li>
                            </ul>
                            @endif
                        </div>
                    </li>
                    @endrole
                </ul>
            </div>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            @include('layouts.theme.sidebar')
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">

                    @yield('breadcrumb')

                </div>
                <!-- /page header -->


                <!-- Content area -->
                @yield('content')
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_select2.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/wysihtml5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/toolbar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/parsers.js') }}"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @include('sweet::alert')
    
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_inputs.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ asset('assets/js/pages/editor_wysihtml5.js') }}"></script> --}}

    <script>
    $(function() {

        // Default initialization
        $('.wysihtml5-default').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["{{ asset('assets/css/components.css') }}"]
        });


        // Simple toolbar
        $('.wysihtml5-min').wysihtml5({
            parserRules:  wysihtml5ParserRules,
            stylesheets: ["{{ asset('assets/css/components.css') }}"],
            "font-styles": true, // Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, // Italics, bold, etc. Default true
            "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, // Button which allows you to edit the generated HTML. Default false
            "link": true, // Button to insert a link. Default true
            "image": false, // Button to insert an image. Default true,
            "action": false, // Undo / Redo buttons,
            "color": true // Button to change color of font
        });


        // Editor events
        $('.wysihtml5-init').on('click', function() {
            $(this).off('click').addClass('disabled');
            $('.wysihtml5-events').wysihtml5({
                parserRules:  wysihtml5ParserRules,
                stylesheets: ["{{ asset('assets/css/components.css') }}"],
                events: {
                    load: function() { 
                        $.jGrowl('Editor has been loaded.', { theme: 'bg-slate-700', header: 'WYSIHTML5 loaded' });
                    },
                    change_view: function() {
                        $.jGrowl('Editor view mode has been changed.', { theme: 'bg-slate-700', header: 'View mode' });
                    }
                }
            });
        });


        // Style form components
        $('.styled').uniform();

    });
    </script>

    @stack('js')

</body>

</html>
