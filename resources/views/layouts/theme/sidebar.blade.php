<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">

            <div class="category-content">
                <div class="sidebar-user-material-content">
                    <a href="#"><img src="{{ asset('assets/images/placeholder.jpg') }}"
                            class="img-circle img-responsive" alt=""></a>

                    {{-- @if (auth()->user()->hasAnyRole(Role::all()))
                        <h6>{{ auth()->user()->name }}</h6>
                    @else
                    <h6>Pegawai</h6>
                    @endif --}}
                    @hasanyrole('superadmin|operator fakultas|operator untan')
                    <h6>{{ ucwords(auth()->user()->name) }}</h6>
                    @else
                    <h6>{{ session('nama') }}</h6>
                    @endhasanyrole
                    <span class="text-size-small">Universitas Tanjungpura</span>
                </div>

            </div>

        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    @role('superadmin')
                    <li class="navigation-header"><span>Super Admin</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li id="beranda"><a href="{{ route('superadmin.beranda') }}"><i class="icon-home4"></i> <span>Beranda</span></a></li>
                    <li>
                        <a href="#"><i class="icon-stack2"></i> <span>Master Data</span></a>
                        <ul>
                            <li id="jenis_pensiun"><a href="{{ route('superadmin.jenis-pensiun.index') }}">Jenis Pensiun</a></li>
                            <li id="berkas_pensiun"><a href="{{ route('superadmin.berkas-pensiun.index') }}">Berkas Pensiun</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stack2"></i> <span>Manajemen User</span></a>
                        <ul>
                            <li id="user"><a href="{{route('superadmin.user.index')}}">User</a></li>
                            <li id="role"><a href="{{route('superadmin.role.index')}}">Role</a></li>
                            <li id="permission"><a href="{{route('superadmin.permission.index')}}">Permission</a></li>
                        </ul>
                    </li>
                    {{-- <li id="log_usulan">
                        <a href="#"><i class="icon-width"></i> <span>Log Usulan</span></a>
                    </li> --}}
                    @endrole

                    @role('operator untan')
                    <li class="navigation-header"><span>Kepegawaian UNTAN</span> <i class="icon-menu"  title="Main pages"></i></li>
                    <li class="" id="beranda"><a href="{{ route('kepegawaian-untan.beranda') }}"><i class="icon-home4"></i> <span>Beranda</span></a></li>
                    <li class="" id="usulan"><a href="{{ route('kepegawaian-untan.usulan.index') }}"><i class="icon-file-text2"></i> <span>Usulan</span></a></li>
                    <li class="" id="usulan_terbit"><a href="{{ route('kepegawaian-untan.usulan.terbit.index') }}"><i class="icon-file-check"></i> <span>SK Usulan Terbit</span></a></li>
                    @endrole

                    @role('kepala kepegawaian untan')
                    <li class="navigation-header"><span>Kepegawaian UNTAN</span> <i class="icon-menu"  title="Main pages"></i></li>
                    <li class="" id="usulan"><a href="{{ route('kepala-kepegawaian.usulan.index') }}"><i class="icon-file-text2"></i> <span>Usulan</span></a></li>
                    @endrole

                    @role('operator fakultas')
                    <li class="navigation-header"><span>Kepegawaian Fakultas</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="" id="beranda"><a href="{{ route('kepegawaian-fakultas.beranda') }}"><i class="icon-home4"></i> <span>Beranda</span></a></li>
                    <li class="" id="usulan"><a href="{{ route('kepegawaian-fakultas.usulan.index') }}"><i class="icon-file-text2"></i> <span>Usulan</span></a></li>
                    <li class="" id="usulan_terbit"><a href="{{ route('kepegawaian-fakultas.usulan.terbit.index') }}"><i class="icon-file-check"></i> <span>SK Usulan Terbit</span></a></li>
                    @endrole

                </ul>
            </div>
        </div>

    </div>
</div>
