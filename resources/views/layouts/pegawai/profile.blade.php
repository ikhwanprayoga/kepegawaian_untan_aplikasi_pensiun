<ul class="breadcrumb-elements">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-gear position-left"></i>
            Account
            <span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="{{ route('kepegawaian-fakultas.profil') }}">

                <i class="icon-user-lock"></i> Profile</a></li>

                


            <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
            <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
            <li class="divider"></li>
            
            <li><a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="icon-user-lock"></i>
                    {{ __('Logout') }}
                </a>


                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </li>
</ul>