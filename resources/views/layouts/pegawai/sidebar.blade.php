<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">

            <div class="category-content">
                <div class="sidebar-user-material-content">
                    <a href="#"><img src="{{ asset('assets/images/placeholder.jpg') }}"
                            class="img-circle img-responsive" alt=""></a>

                    {{-- @if (auth()->user()->hasAnyRole(Role::all()))
                        <h6>{{ auth()->user()->name }}</h6>
                    @else
                    <h6>Pegawai</h6>
                    @endif --}}
                    @hasanyrole('superadmin|operator fakultas|operator untan')
                    <h6>{{ auth()->user()->name }}</h6>
                    @else
                    <h6>{{ session('nama') }}</h6>
                    @endhasanyrole
                    <span class="text-size-small">Universitas Tanjungpura</span>
                </div>

            </div>

        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- pegawai-->
                    <li class="navigation-header"><span>Kepegawaian UNTAN</span> <i class="icon-menu"
                            title="Main pages"></i></li>
                    <li class="" id="beranda"><a href="{{ route('pegawai.beranda') }}"><i class="icon-home4"></i> <span>Beranda</span></a></li>
                    @if (Helpers::cekUsulan(session()->get('nip')) == 1)
                    <li class="" id="berkas"><a href="{{ route('pegawai.berkas.index') }}"><i class="icon-file-upload"></i> <span>Upload Berkas</span></a></li>
                    <li class="" id="usulan"><a href="{{ route('pegawai.usulan') }}"><i class="icon-stack"></i> <span>Log Usulan</span></a></li>
                    @endif
                    <li class="" id="download_berkas"><a href="{{ route('pegawai.download-berkas.index') }}"><i class="icon-file-download"></i> <span>Download Berkas</span></a></li>
                    <li><a href="{{ route('pegawai.logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="icon-exit"></i>
                            {{ __('Logout') }}
                        </a>


                        <form id="logout-form" action="{{ route('pegawai.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <!-- /main -->

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
