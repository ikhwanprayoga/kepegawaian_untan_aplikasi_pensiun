@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - PANDUAN BERKAS</title>
@endsection

@push('css')

@endpush

@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="banner_content text-center">
                        <h2>Standar Operasional Prosedur (SOP)</h2>
                        <div class="page_link">
                            <a href="{{ route('beranda') }}">Beranda</a>
                            <a href="#">SOP</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color">Standar Operasional Prosedur</h3>
        <iframe src="https://drive.google.com/file/d/1KDhgs2T8ZXBg3VcMq1AaLnjToYYTLkDZ/preview?usp=sharing" width="100%"
            height="700"></iframe>
        <!-- <p class="sample-text" style="text-align: justify">
                  
              </p> -->
    </div>
</section>
@endsection

@push('script')
<script>
    $('#layanan').addClass('active')
    $('.header_area').addClass('white-header')

</script>
@endpush
