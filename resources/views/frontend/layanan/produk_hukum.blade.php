@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - PANDUAN BERKAS</title>
@endsection

@push('css')

@endpush

@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="banner_content text-center">
                        <h2>Produk Hukum</h2>
                        <div class="page_link">
                            <a href="{{ route('beranda') }}">Beranda</a>
                            <a href="#">Produk Hukum</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 course_details_left">
                <div class="content_wrapper">
                    <h4 class="title">Produk Hukum</h4>
                    <hr>
                    <div class="content">
                        <ul class="course_list">
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Undang-Undang Republik Indonesia Nomor 11 Tahun 1969 Tentang Pensiun Pegawai dan
                                        Pensiun Janda/Duda Pegawai</p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/open?id=1pmLq-FWOfzE8vGhT-PIr8tR8nrywdp-k">Download</a>
                                </div>
                            </li>
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Peraturan Pemerintah RI Nomor 70 Tahun 2015 Tentang Jaminan Kecelakaan Kerja dan
                                        Jaminan Kematian Bagi Pegawai Aparatur Sipil Negara</p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/file/d/1tVgTWFAjf18wB0mw1FTj_bdy-kRNocX3/view?usp=sharing">Download</a>
                                </div>
                            </li>
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Peraturan Pemerintah RI Nomor 11 Tahun 2017 Tentang Manajemen Pegawai Negeri
                                        Sipil</p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/open?id=1XMIJEEaq-jv5U20i1j6Lt_Efxu-xttiW">Download</a>
                                </div>
                            </li>
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Pedoman Kriteria Penetapan Kecelakaan Kerja, Cacat dan Penyakit Akibat Kerja
                                        Serta Kriteria Penetapan Tewas Bagi Pegawai Apartur Sipil Negara </p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/open?id=1oHwas6Xwv2a62i45SJkG029bk0haVs_9">Download</a>
                                </div>
                            </li>
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Tata Cara Masa Persiapan Pensiun</p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/open?id=1jLlFQzcRrSl3QxRr36vOqQHVMOQJctWp">Download</a>
                                </div>
                            </li>
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Panduan Pemberian Pertimbangan Teknis Pensiun Pegawai Negeri Sipil dan Pensiun
                                        Janda/Duda Pegawai Negeri Sipil</p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/open?id=1fZz3Y_ZB7hN8el0nKV3FGf0xSlGChA-B">Download</a>
                                </div>
                            </li>
                            <li class="justify-content-between d-flex">
                                <div class="col-md-8">
                                    <p>Surat Edaran 2026/A2/SE/2018 Tentang Kelengkapan Usul Pemberhentian Pegawai
                                        Negeri Sipil DI Lingkungan Kementrian Riset, Teknologi, dan Pendidikan Tinggi
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <a class="primary-btn text-uppercase"
                                        target="_blank" href="https://drive.google.com/open?id=1_ohN_g79lmT6BcpUbCthmYqBVxR0QfzW">Download</a>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('script')
<script>
    $('#layanan').addClass('active')
    $('.header_area').addClass('white-header')

</script>
@endpush
