@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - BERANDA</title>
@endsection

@push('css')
    
@endpush

@section('content')
<!--================ Start Home Banner Area =================-->
<section class="home_banner_area">
    <div class="banner_inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner_content text-center">
                        <p class="text-uppercase">
                            Hello, Selamat Datang Di
                        </p>
                        <h2 class="text-uppercase mt-4 mb-5">
                            Sistem informasi dan layanan pensiun
                        </h2>
                        <div>
                            <!-- <a href="#" class="primary-btn2 mb-3 mb-sm-0">Login Disini</a> -->
                            <a href="{{ route('pegawai.login.index') }}" class="primary-btn ml-sm-3 ml-0">Login Pegawai</a>
                            <a href="{{ route('login') }}" class="primary-btn ml-sm-3 ml-0">Login Operator</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Home Banner Area =================-->

<!--================ Start Feature Area =================-->
<section class="feature_area section_gap_top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="main_title">
                    <h2 class="mb-3">Keunggulan SILAP</h2>
                    <p>
                        Beberapa Keunggulan yang dimiliki Aplikasi SILAP.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single_feature" align='center'>
                    <!-- <div class="icon"><img src="img/progress.png" alt=""><span class="flaticon-student"></span></div> -->
                    <div class="icon" ><img src="{{ asset('frontend/img/clock.png') }}" alt=""></div>
                    <div class="desc">
                        <h4 class="mt-3 mb-2">EWS (EARLY WARNING SYSTEM)</h4>
                        <p>
                            Ews membantu user untuk mempercepat mendapatkan informasi dan melengkapi dokumen pada proses pensiun
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single_feature" align='center'>
                    <div class="icon"><img src="{{ asset('frontend/img/training.png') }}" alt=""></div>
                    <!-- <div class="icon"><span class="flaticon-book"></span></div> -->
                    <div class="desc">
                        <h4 class="mt-3 mb-2">AKSES INFORMASI LEBIH MUDAH</h4>
                        <p>
                            Dengan SILAP user akan lebih mudah mendapatkan informasi karena dapat diakses dimanapun dan kapanpun secara online.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="single_feature" align='center'>
                    <div class="icon"><img src="{{ asset('frontend/img/attached.png') }}" alt=""></div>
                    <!-- <div class="icon"><span class="flaticon-earth"></span></div> -->
                    <div class="desc">
                        <h4 class="mt-3 mb-2">TERSIMPAN ARSIP SECARA DIGITAL</h4>
                        <p>
                            Arsip tersimpan secara digital sehingga mengurangi peluang kehilangan atau kerusakan data dan memudahkan pencarian.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single_feature" align='center'>
                    <div class="icon"><img src="{{ asset('frontend/img/overtime.png') }}" alt=""></div>
                    <!-- <div class="icon"><span class="flaticon-earth"></span></div> -->
                    <div class="desc">
                        <h4 class="mt-3 mb-2">MONITORING USULAN ONLINE</h4>
                        <p>
                            Dengan SILAP user dapat melihat/memonitoring progres usulan pensiun secara online dimanapun dan kapanpun dengan cepat.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Feature Area =================-->

<!--================ Start About Area =================-->
<section class="about_area section_gap" style="margin-top: -120px">
    <div class="container">
        <div class="row h_blog_item">
            <div class="col-lg-6">
                <div class="h_blog_img">
                    <img class="img-fluid" src="{{ asset('frontend/img/prof.jpg') }}" alt="" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="h_blog_text">
                    <div class="h_blog_text_inner left right">
                        <h4>Selamat Datang Di SILAP</h4>
                        <p style="text-align: justify">
                            Sistem Informasi dan Layanan Pensiun (SILAP)
                            SILAP (Sistem Informasi dan Layanan Pensiun) merupakan aplikasi online yang disiapkan
                            oleh Bagian Hukum dan Kepegawaian
                            Universitas Tanjungpura dalam rangka melayani proses usulan Pensiun/Pemberhentian Dosen
                            di Lingkungan Universitas Tanjungpura
                            agar menjadi lebih mudah dan cepat.
                        </p>
                        <a class="primary-btn" href="{{ route('tentang') }}">
                            Baca Selengkapnya <i class="ti-arrow-right ml-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End About Area =================-->
<!--================  Start Popular Courses Area =================-->
<div class="events_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="main_title mb-3">
                    <h2 class="mb-3 text-white">Alur Aplikasi Silap</h2>
                    <p>Informasi mengenai alur aplikasi SILAP</p>
                </div>
                <div class="gambar" align="center">
                    <img src="{{ asset('frontend/img/silap.png') }}" alt=""> <br>
                    <a class="primary-btn" href="{{ route('layanan-sop') }}">Lihat SOP Disini</i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--================ Start Testimonial Area =================-->
<div class="testimonial_area section_gap">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="main_title">
                    <h2 class="mb-3">Pendapat Mereka</h2>
                    <p>
                        Pesan dan Kesan Mereka Terhadap Aplikasi SILAP
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="testi_slider owl-carousel">
                <div class="testi_item">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <img src="{{ asset('frontend/img/testimonials/profhamid.jpg') }}" alt="" />
                        </div>
                        <div class="col-lg-8">
                            <div class="testi_text">
                                <h4>Prof. Ir. H. Abdul Hamid, M.Eng</h4>
                                <h5>(Dosen Fakultas Teknik UNTAN)</h5>
                                <p align="justify">
                                 "Banyak keuntungan dari sistem ini, khususnya untuk dosen yang akan pensiun diantaranya kecepatan pelayanan dalam menyampaikan informasi kepada dosen dan fakultas maupun universitas sendiri sehingga dengan adanya sistem ini dosen dari jauh hari sudah bisa menyiapkan diri dan berkas atau dokumen yang diperlukan dalam usul pensiun"
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testi_item">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <img src="{{ asset('frontend/img/testimonials/warek.jpg') }}" alt="" />
                        </div>
                        <div class="col-lg-8">
                            <div class="testi_text">
                                <h4>Dr. Rini Sulistiawati, S.E., M.Si</h4>
                                <h5>(Wakil Rektor Bidang Umum dan Keuangan UNTAN)</h5>
                                <p align="justify">
                                "Sudah saatnya semua layanan beralih  dari manual ke sistem aplikasi, sehingga tidak ada lagi yang namanya berkas atau arsip yang menumpuk, selain itu layanan juga bisa menjadi lebih efektif dan efisien"
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--================ End Testimonial Area =================-->
@endsection

@push('script')
<script>
    $('#beranda').addClass('active')
</script>
@endpush
