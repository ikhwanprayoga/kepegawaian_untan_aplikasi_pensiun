@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - KONTAK</title>
@endsection

@push('css')

@endpush

@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="banner_content text-center">
                        <h2>Kontak Kami</h2>
                        <div class="page_link">
                            <a href="{{ route('beranda') }}">Beranda</a>
                            <a href="#">kontak</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!--================Contact Area =================-->
<section class="contact_area section_gap">
    <div class="container">
        <div class="row">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3989.81615795495!2d109.34361206475324!3d-0.05965239995647324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1suntan%20map!5e0!3m2!1sid!2sid!4v1566957286985!5m2!1sid!2sid"
                width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div><br><br>
        <div class="row">
            <div class="col-lg-3">
                <div class="contact_info">
                    <div class="info_item">
                        <i class="ti-home"></i>
                        <h6>Universitas Tanjungpura</h6>
                        <p>Jl. Prof. Dr. H. Hadari Nawawi</p>
                    </div>
                    <div class="info_item">
                        <i class="ti-headphone"></i>
                        <h6><a href="#">(0561) 739630</a></h6>
                        <p>Senin Sampai Dengan Jum'at</p>
                    </div>
                    <div class="info_item">
                        <i class="ti-email"></i>
                        <h6><a href="#">kepegawaian@untan.ac.id</a></h6>
                        <p>Kirimkan pertanyaan anda!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Contact Area =================-->
@endsection

@push('script')
<script>
    $('#kontak').addClass('active')
    $('.header_area').addClass('white-header')

</script>
@endpush
