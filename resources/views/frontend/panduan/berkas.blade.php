@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - PANDUAN BERKAS</title>
@endsection

@push('css')

@endpush

@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="banner_content text-center">
                        <h2>Panduan</h2>
                        <div class="page_link">
                            <a href="{{ route('beranda') }}">Beranda</a>
                            <a href="#">Panduan Berkas</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sample-text-area">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Berkas Usul Pensiun BUP</a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Berkas Usul Pensiun Janda/Duda/Anak</a>
                </div>
            </div>
            <div class="col-8">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <div class="col-lg-12 course_details_left">
                            <div class="content_wrapper">
                                <h4 class="title">Persyaratan Berkas Usul Pensiun BUP</h4>
                                <hr>
                                <div class="content">
                                    <ul class="course_list">
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                1. Surat Dari Unit Kerja
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                2. Foto copy KTP
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                3. Foto copy SK Konversi NIP baru
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                4. Foto copy KARPEG
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                5. Foto copy SK CPNS
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                6. Foto copy SK PNS
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                7. Foto copy SK Pangkat terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                8. Foto copy SK Jabatan terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                9. Foto copy SK Kenaikan Gaji Berkala terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                10. Foto copy Akte Perkawinan yang telah disahkan Capil / Surat Nikah yang disahkan oleh KUA
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                11. Foto copy akte kelahiran anak yang disahkan di Catatan Sipil
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                12. SKP dan P2KP 2 tahun terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                13. Pas Photo berwarna ukuran 3 x 4 cm 7 lembar
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                14. Fotocopy rekening Bank yang akan digunakan untuk menerima pensiun
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                15. Fotocopy NPWP
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                16. Daftar susunan keluarga yang disahkan oleh camat 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/file/d/1xg7WUHdSCwaHbN6ljAZbLsSO3adP9Xd6/view?usp=sharing">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                17. Surat Pernyataan tidak pernah mendapat hukuman disiplin 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/file/d/1sNUJOGxavsDdjZS2PTJ_enGwudDrA__L/view?usp=sharing">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                18. Surat Pernyataan tidak sedang menjalani proses pidana atau pernah dipidana penjara berdasarkan putusan pengadilan yang telah berkekuatan hukum tetap 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=11LNaerYqZdQcPztjoxlCs_5nRonvBO7P">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                19. Riwayat Pekerjaan 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1uKhDCIkCuSaJhUiEebjPAIf1HlNT7t_Z">Download</a>
                                            </div>
                                        </li> 
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                20. Formulir pembayaran Taspen (contoh terlampir); 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1SvVUukgoJInrFyBh-7KuBgTiqo88dDQm">Download</a>
                                            </div>
                                        </li>                                   
                                    </ul>
                                    <span style="font-weight : bold"><i>*Keterangan : Semua kelengkapan berkas dibuat 3 rangkap</i> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <div class="col-lg-12 course_details_left">
                            <div class="content_wrapper">
                                <h4 class="title">Persyaratan Berkas Usul Pensiun BUP</h4>
                                <hr>
                                <div class="content">
                                    <ul class="course_list">
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                1. Surat Usulan Dari Unit Kerja
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                2. Surat Permintaan Pensiun
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1HlmzLg3UcYs6GfWRZqW4HUlEaW4c52pW">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                3. Surat Permintaan Pembayaran Pensiun Pertama
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1_8lYNAran9dbaax8NgzbSAVHZuiX6hq0">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                4. Foto copy KTP
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                5. Foto copy Akta Kematian yang disahkan oleh pejabat yang berwenang
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                6. Foto copy SK Konversi NIP baru
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                7. Foto copy KARPEG
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                8. Foto copy SK CPNS
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                9. Foto copy SK PNS
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                10. Foto copy SK Pangkat terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                11. Foto copy SK Jabatan terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                12. Foto copy SK Kenaikan Gaji Berkala terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                13. Foto copy Akte Perkawinan yang telah disahkan Capil / Surat Nikah yang disahkan oleh KUA
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                14. Daftar susunan keluarga yang disahkan oleh camat 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1xg7WUHdSCwaHbN6ljAZbLsSO3adP9Xd6">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                15. Foto copy akte kelahiran anak yang disahkan di Catatan Sipil
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                16. Surat Keterangan Janda/Duda dari Kelurahan
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                17. Surat Pernyataan tidak pernah mendapat hukuman disiplin 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1sNUJOGxavsDdjZS2PTJ_enGwudDrA__L">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                18. Surat Pernyataan tidak sedang menjalani proses pidana atau pernah dipidana penjara berdasarkan putusan pengadilan yang telah berkekuatan hukum tetap 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=11LNaerYqZdQcPztjoxlCs_5nRonvBO7P">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                19. Fotocopy rekening Bank yang akan digunakan untuk menerima pensiun
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1uKhDCIkCuSaJhUiEebjPAIf1HlNT7t_Z">Download</a>
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                20. SKP dan P2KP 2 tahun terakhir
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                21. Pas Photo berwarna ukuran 3 x 4 cm 7 lembar
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                22. Fotocopy rekening Bank yang akan digunakan untuk menerima pensiun
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                23. Fotocopy NPWP 
                                            </div>
                                        </li>
                                        <li class="justify-content-between d-flex">
                                            <div class="col-md-10">
                                                24. Formulir pembayaran Taspen (contoh terlampir); 
                                            </div>
                                            <div class="col-md-2">
                                                <a class="primary-btn text-uppercase" target="_blank" href="https://drive.google.com/open?id=1SvVUukgoJInrFyBh-7KuBgTiqo88dDQm">Download</a>
                                            </div>
                                        </li>                                   
                                    </ul>
                                    <span style="font-weight : bold"><i>*Keterangan : Semua kelengkapan berkas dibuat 3 rangkap</i> </span>
                                </div>
                            </div>
                        </div>
                </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!--================End Home Banner Area =================-->
@endsection

@push('script')
<script>
    $('#panduan').addClass('active')
    $('.header_area').addClass('white-header')

</script>
@endpush
