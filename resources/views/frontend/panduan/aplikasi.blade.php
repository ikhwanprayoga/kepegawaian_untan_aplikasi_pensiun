@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - PANDUAN BERKAS</title>
@endsection

@push('css')

@endpush

@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="banner_content text-center">
                        <h2>Panduan</h2>
                        <div class="page_link">
                            <a href="{{ route('beranda') }}">Beranda</a>
                            <a href="#">Panduan Aplikasi</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->


<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container" align="center">
        <h3 class="text-heading title_color">Modul Untuk Operator Universitas</h3>
        <iframe src="https://drive.google.com/file/d/1oP8_nsfHCCmL77ClyabJ_GvGdnvosPf4/preview?usp=sharing" width="100%"
            height="500"></iframe>
            <a class="primary-btn text-uppercase" href="https://drive.google.com/file/d/1oP8_nsfHCCmL77ClyabJ_GvGdnvosPf4/view?usp=sharing">Klik Untuk Download</a>
        <!-- <p class="sample-text" style="text-align: justify">
                  
              </p> -->
    </div>
</section>

<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container" align="center">
        <h3 class="text-heading title_color">Modul Untuk Pegawai</h3>
        <iframe src="https://drive.google.com/file/d/1MwsKwfyWK8y2_SXZfZ9IbHp2RWq0E4ky/preview?usp=sharing" width="100%"
            height="500"></iframe>
            <a class="primary-btn text-uppercase" href="https://drive.google.com/file/d/1MwsKwfyWK8y2_SXZfZ9IbHp2RWq0E4ky/view?usp=sharing">Klik Untuk Download</a>
        <!-- <p class="sample-text" style="text-align: justify">
                  
              </p> -->
    </div>
</section>

<section class="sample-text-area">
    <div class="container" align="center">
        <h3 class="text-heading title_color">Modul Untuk Operator Fakultas</h3>
        <iframe src="https://drive.google.com/file/d/1NGdbMfCKDNTLjr51BGF9Gp5C5pe3xnV1/preview?usp=sharing" width="100%"
            height="500"></iframe>
            <a class="primary-btn text-uppercase" href="https://drive.google.com/file/d/1NGdbMfCKDNTLjr51BGF9Gp5C5pe3xnV1/view?usp=sharing">Klik Untuk Download</a>
        <!-- <p class="sample-text" style="text-align: justify">
                  
              </p> -->
    </div>
</section>





<!-- Start Sample Area -->
panduan aplikasi
<!-- Start Sample Area -->
@endsection

@push('script')
<script>
    $('#panduan').addClass('active')
    $('.header_area').addClass('white-header')

</script>
@endpush
