@extends('layouts.frontend.base')

@section('header')
<title>SILAP UNTAN - KONTAK</title>
@endsection

@push('css')

@endpush

@section('content')
<!--================Home Banner Area =================-->
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="banner_content text-center">
                        <h2>Tentang Kami</h2>
                        <div class="page_link">
                            <a href="{{ route('beranda') }}">Beranda</a>
                            <a href="#">tentang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Home Banner Area =================-->

<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color">Selamat Datang Di SILAP</h3>
        <p class="sample-text" style="text-align: justify">
            Sistem Informasi dan Layanan Pensiun (SILAP)
            SILAP (Sistem Informasi dan Layanan Pensiun) merupakan aplikasi online yang disiapkan oleh Bagian Hukum dan
            Kepegawaian Universitas Tanjungpura dalam rangka melayani proses usulan Pensiun/Pemberhentian Dosen di
            Lingkungan Universitas Tanjungpura agar menjadi lebih mudah dan cepat.

            <br><br>
            Pensiun memiliki pengertian keadaan seorang pegawai negeri sipil yang sudah tidak bekerja lagi baik karena
            masa kerjanya sudah berakhir/memasuki batas usia pensiun atau karena alasan-alasan lainnya sesuai dengan
            ketentuan dan peraturan yang berlaku.
            <br><br>
            Berdasarkan Peraturan Pemerintah Nomor 11 Tahun 2017 tentang Manajemen Pegawai Negeri Sipil ada beberapa
            jenis pensiun/pemberhentian dengan rincian sebagai berikut :
            <ol type="A">
                <li>Pemberhentian Karena Atas Permintaan Sendiri</li>
                <li>Pemberhentian Karena Mencapai Batas Usia Pensiun (BUP)</li>
                <li>Pemberhentian Karena Meninggal Dunia atau Hilang</li>
                <li>Pemberhentian Karena Tidak Cakap Jasmani dan Rohani</li>
                <li>Pemberhentian Karena Cacad Karena Dinas</li>
                <li>Pemberhentian Tidak Dengan Hormat sebagai PNS (Pasal 87 UU ASN)</li>
                <li>Pemberhentian Karena Penyederhanaan Organisasi</li>
                <li>Pemberhentian Karena Hal-Hal Lain :</li>
                <ol>
                    <li>Habis Menjalankan Cuti Di Luar Tanggungan Negara</li>
                    <li>Menjadi Pejabat Negara</li>
                    <li>Menjadi Anggota Dan/Atau Pengurus Parpol, DPD, KPU, Hakim Mahkamah Konstitusi, dan Lain
                        Sejenisnya</li>
                </ol>
            </ol>
            Untuk mengoptimalisasi Layanan Kepegawaian khususnya Layanan Pensiun/Pemberhentian, Sistem Informasi dan
            Layanan Pensiun (SILAP) hadir dan diharapkan mampu menjadikan layanan pensiun lebih efektif dan efisien,
            dapat menerapkan inovasi dan budaya paperless (sedikit menggunakan kertas dalam proses) sekaligus mendukung
            program go green.

            SILAP dilengkapi dengan Sistem Peringatan Dini (Early Warning System) yang ditujukan kepada Dosen yang akan
            memasuki masa purna tugas secara otomatis, notifikasi ketika Surat Keputusan Pensiun sudah terbit, tersedia
            juga SOP dan Panduan Penggunaan Aplikasi sehingga membantu proses usulan menjadi lebih mudah dan cepat,
            selain itu sistem ini juga dilengkapi dengan sistem monitoring sehingga dosen dapat memonitoring
            perkembangan usulan pensiun/pemberhentiannya.

        </p>
    </div>
</section>
@endsection

@push('script')
<script>
    $('#tentang').addClass('active')
    $('.header_area').addClass('white-header')
</script>
@endpush
