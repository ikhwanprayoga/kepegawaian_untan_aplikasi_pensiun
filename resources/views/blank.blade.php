@extends('layouts.theme.base')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Marketing campaigns -->
            <div class="panel panel-flat">
            </div>
            <!-- /marketing campaigns -->

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Latest posts</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="media-list content-group">
                                <li class="media stack-media-on-mobile">
                                    <div class="media-left">
                                        <div class="thumb">
                                            <a href="#">
                                                <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
                                                <span class="zoom-image"><i class="icon-play3"></i></span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="media-body">
                                        <h6 class="media-heading"><a href="#">Up unpacked friendly</a></h6>
                                        <ul class="list-inline list-inline-separate text-muted mb-5">
                                            <li><i class="icon-book-play position-left"></i> Video tutorials</li>
                                            <li>14 minutes ago</li>
                                        </ul>
                                        The him father parish looked has sooner. Attachment frequently gay terminated son...
                                    </div>
                                </li>

                                <li class="media stack-media-on-mobile">
                                    <div class="media-left">
                                        <div class="thumb">
                                            <a href="#">
                                                <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
                                                <span class="zoom-image"><i class="icon-play3"></i></span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="media-body">
                                        <h6 class="media-heading"><a href="#">It allowance prevailed</a></h6>
                                        <ul class="list-inline list-inline-separate text-muted mb-5">
                                            <li><i class="icon-book-play position-left"></i> Video tutorials</li>
                                            <li>12 days ago</li>
                                        </ul>
                                        Alteration literature to or an sympathize mr imprudence. Of is ferrars subject as enjoyed...
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="col-lg-6">
                            <ul class="media-list content-group">
                                <li class="media stack-media-on-mobile">
                                    <div class="media-left">
                                        <div class="thumb">
                                            <a href="#">
                                                <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
                                                <span class="zoom-image"><i class="icon-play3"></i></span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="media-body">
                                        <h6 class="media-heading"><a href="#">Case read they must</a></h6>
                                        <ul class="list-inline list-inline-separate text-muted mb-5">
                                            <li><i class="icon-book-play position-left"></i> Video tutorials</li>
                                            <li>20 hours ago</li>
                                        </ul>
                                        On it differed repeated wandered required in. Then girl neat why yet knew rose spot...
                                    </div>
                                </li>

                                <li class="media stack-media-on-mobile">
                                    <div class="media-left">
                                        <div class="thumb">
                                            <a href="#">
                                                <img src="assets/images/placeholder.jpg" class="img-responsive img-rounded media-preview" alt="">
                                                <span class="zoom-image"><i class="icon-play3"></i></span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="media-body">
                                        <h6 class="media-heading"><a href="#">Too carriage attended</a></h6>
                                        <ul class="list-inline list-inline-separate text-muted mb-5">
                                            <li><i class="icon-book-play position-left"></i> FAQ section</li>
                                            <li>2 days ago</li>
                                        </ul>
                                        Marianne or husbands if at stronger ye. Considered is as middletons uncommonly...
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
@endpush