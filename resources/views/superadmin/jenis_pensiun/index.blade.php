@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Jenis Pensiun</h6>
                    <a href="{{ route('superadmin.jenis-pensiun.create') }}" class="btn">Tambah Jenis Pensiun</a>
                    <div class="heading-elements">
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table datatable-show-all datatable-basic">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Jenis Pensiun</th>
                                <th>Keterangan</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dataJenisPensiun as $djp)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $djp->nama }}</td>
                                <td>{{ $djp->keterangan }}</td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><form action="{{ route('superadmin.jenis-pensiun.show', $djp->id) }}" method="post">
                                                        @csrf
                                                        @method('GET')
                                                        <button class="btn btn-primary btn-block" type="submit">Detail</button>
                                                    </form>
                                                </li>
                                                <li><form action="{{ route('superadmin.jenis-pensiun.edit', $djp->id) }}" method="post">
                                                        @csrf
                                                        @method('GET')
                                                        <button class="btn btn-warning btn-block" type="submit">Edit</button>
                                                    </form>
                                                </li>
                                                <li><form action="{{ route('superadmin.jenis-pensiun.destroy', $djp->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-block" type="submit">Delete</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#jenis_pensiun').addClass('active')
</script>
@endpush