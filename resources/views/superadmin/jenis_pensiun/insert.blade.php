@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('superadmin.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Jenis Pensiun</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Marketing campaigns -->
            <div class="panel panel-flat">
            </div>
            <!-- /marketing campaigns -->

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Tambah Jenis Pensiun</h6>
                    <a href="{{ route('superadmin.jenis-pensiun.index') }}" class="btn">Kembali</a>
                </div>

                <div class="panel-body">
                    <form action="{{ route('superadmin.jenis-pensiun.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="namaInput">Nama Jenis Pensiun:</label>
                            <input type="tetx" class="form-control" id="namaInput" aria-describedby="namaHelp" name="nama" placeholder="Jenis Pensiun">
                            {{-- <small id="namaHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>
                        <div class="form-group">
                            <label for="keteranganInput">Keterangan</label>
                            <textarea class="form-control" id="keteranganInput" name="keterangan"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="checkBox">Berkas Terkait</label>
                            <div style="height: 100px; height: 150px; overflow:auto;">
                                @foreach ($dataBerkasPensiun as $dbp)
                                <div class="checkbox">
                                    <label><input type="checkbox" name="cb[]" value="{{ $dbp->id }}">{{ $dbp->judul_berkas }}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Masukan Data</button>
                    </form>
                </div>
            </div>
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#jenis_pensiun').addClass('active')
</script>
@endpush