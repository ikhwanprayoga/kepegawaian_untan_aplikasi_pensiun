@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Marketing campaigns -->
            <div class="panel panel-flat">
            </div>
            <!-- /marketing campaigns -->

            <!-- Latest posts -->
            @foreach ($data as $data)
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Detail Jenis Pensiun</h6>
                    <div class="row">
                        <a href="{{ route('superadmin.jenis-pensiun.index') }}" class="btn">Kembali</a>
                        <a href="{{ route('superadmin.jenis-pensiun.edit', $data->id) }}" class="btn">Edit Data</a>
                    </div>
                </div>

                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                    <div class="container">
                        <h1>Jenis Pensiun:</h1>
                        <h2>{{ $data->nama }}</h2>
                        <hr>
                        <h3>Keterangan</h3>
                        <p>{{ $data->keterangan }}</p>
                        <hr>
                        <h3>Berkas Terkait</h3>
                        <div>
                            @foreach ($masterBerkasPensiunData as $item)
                                {{-- {{ $dmbp }} --}}
                                <div class="checkbox">
                                    <label><input type="checkbox" name="cb[]" value="{{ $item->id }}" 
                                        @for ($i = 0; $i < $varCount; $i++)
                                            @if ($item->id == $var[$i])
                                                checked
                                            @else
                                                
                                            @endif
                                        @endfor
                                    disabled>{{ $item['judul_berkas'] }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
@endpush