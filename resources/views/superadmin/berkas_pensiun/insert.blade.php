@extends('layouts.theme.base')

@section('title', 'Super Admin Master Berkas Pensiun')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('superadmin.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li><a href="{{ route('superadmin.berkas-pensiun.index') }}"> Berkas Pensiun</a></li>
        <li class="active">Tambah Berkas Pensiun</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Tambah Berkas Pensiun</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <a href="{{ route('superadmin.berkas-pensiun.index') }}" class="btn btn-primary btn-sm"> Kembali</a>
                        </ul>
                    </div>
                    
                </div>

                <div class="panel-body">
                    <form action="{{ route('superadmin.berkas-pensiun.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="judulInput">Judul Berkas</label>
                            <input type="tetx" class="form-control" id="judulInput" aria-describedby="judulInput" name="judul" placeholder="Judul Berkas">
                            {{-- <small id="judulInput" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        </div>
                        <div class="form-group">
                            <label for="keteranganInput">Keterangan</label>
                            <textarea class="form-control" id="keteranganInput" name="keterangan" placeholder="Keterangan tambahan untuk berkas pensiun"></textarea>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="pegawai_dapat_upload">
                                Pegawai Dapat Mengupload Sendiri ?
                            </label>
                        </div><br>
                        <button type="submit" class="btn btn-primary">Masukan Data</button>
                    </form>
                </div>
            </div>
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#berkas_pensiun').addClass('active')
</script>
@endpush