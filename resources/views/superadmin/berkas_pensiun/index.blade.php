@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="{{ route('superadmin.beranda') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
        <li class="active">Berkas Pensiun</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Berkas Pensiun</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <a href="{{ route('superadmin.berkas-pensiun.create') }}" class="btn btn-primary btn-sm">Tambah Berkas Pensiun</a>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table datatable-basic">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Judul Berkas</th>
                                <th>Pegawai Dapat Upload Sendiri</th>
                                <th>Keterangan</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dataBerkasPensiun as $dbp)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $dbp->judul_berkas }}</td>
                                <td >{{ ($dbp->pegawai_dapat_upload == 1) ? 'Ya' : 'Tidak' }}</td>
                                <td >{!! $dbp->keterangan !!}</td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-menu9"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><form action="{{ route('superadmin.berkas-pensiun.show', $dbp->id) }}" method="post">
                                                        @csrf
                                                        @method('GET')
                                                        <button class="btn btn-primary btn-block" type="submit">Detail</button>
                                                    </form>
                                                </li>
                                                <li><form action="{{ route('superadmin.berkas-pensiun.edit', $dbp->id) }}" method="post">
                                                        @csrf
                                                        @method('GET')
                                                        <button class="btn btn-warning btn-block" type="submit">Edit</button>
                                                    </form>
                                                </li>
                                                <li><form action="{{ route('superadmin.berkas-pensiun.destroy', $dbp->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-block" type="submit">Delete</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#berkas_pensiun').addClass('active')
</script>
@endpush