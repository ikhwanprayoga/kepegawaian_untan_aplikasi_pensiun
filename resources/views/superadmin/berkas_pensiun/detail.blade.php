@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Marketing campaigns -->
            <div class="panel panel-flat">
            </div>
            <!-- /marketing campaigns -->

            <!-- Latest posts -->
            @foreach ($data as $data)
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Detail Berkas Pensiun</h6>
                    <div class="row">
                        <a href="{{ route('superadmin.berkas-pensiun.index') }}" class="btn">Kembali</a>
                        <a href="{{ route('superadmin.berkas-pensiun.edit', $data->id) }}" class="btn">Edit Data</a>
                    </div>
                </div>

                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                    <div class="container">
                        <h1>Judul Berkas Pensiun: {{ $data->judul_berkas }}</h1>
                        <hr>
                        <h3>Keterangan</h3>
                        <p>{{ $data->keterangan }}</p>
                        <h3>Pegawai dapat mengupload berkas sendiri?</h3>
                        <p>{{ ($data->pegawai_dapat_upload == 1) ? 'Ya' : 'Tidak' }}</p>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
@endpush