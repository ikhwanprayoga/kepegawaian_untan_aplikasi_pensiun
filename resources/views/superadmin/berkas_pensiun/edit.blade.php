@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Marketing campaigns -->
            <div class="panel panel-flat">
            </div>
            <!-- /marketing campaigns -->

            <!-- Latest posts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Edit Berkas Pensiun</h6>
                    <a href="{{ route('superadmin.berkas-pensiun.index') }}" class="btn">Kembali</a>
                </div>

                <div class="panel-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                    
                    @foreach ($dataBerkasPensiun as $dbp)
                        <form action="{{ route('superadmin.berkas-pensiun.update', $dbp->id) }}" method="post">
                            <div class="form-group">
                                @csrf
                                @method('PATCH')
                                <label for="judulInput">Judul Berkas Pensiun:</label>
                                <input type="tetx" class="form-control" id="judulInput" name="judul" value="{{ $dbp->judul_berkas }}">
                            </div>
                            <div class="form-group">
                                <label for="keteranganInput">Keterangan</label>
                                <textarea class="form-control" id="keteranganInput" name="keterangan">{{ $dbp->keterangan }}</textarea>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="pegawai_dapat_upload" {{ ($dbp->pegawai_dapat_upload == 1) ? 'checked' : '' }}>
                                    Pegawai Dapat Mengupload Sendiri ?
                                </label>
                            </div><br>
                            <button type="submit" class="btn btn-primary">Edit Data</button>
                        </form>
                    @endforeach
                </div>
            </div>
            <!-- /latest posts -->

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
@endpush