@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> User</a></li>
        <li class="active">Edit User</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <form method="post" action="{{ route('superadmin.user.update', $data->id) }}" class="form-horizontal" >
                            @csrf
                            @method('PATCH')
                            <fieldset class="content-group">
                                <legend class="text-bold">Masukan data User yang Ingin di Edit</legend>
                
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Nama</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" class="form-control" name="name" value="{{ $data->name }}">
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label class="control-label col-lg-2">username</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" class="form-control" name="username" value="{{ $data->username }}">
                                    </div>
                                </div>
                
                                <div class="form-group">
                                    <label class="control-label col-lg-2">E-mail</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" class="form-control" name="email" value="{{ $data->email }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-2 control-label">No Hp</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" class="form-control" name="no_hp" value="{{ $data->no_hp }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                        <label class="control-label col-lg-2">fakultas</label>
                                        <div class="col-lg-10">
                                            <select  class="form-control" name="fakultas_id"> 
                                                    
                                                    
                                                        @foreach ($fakultas as $fak)
                                                        
                                                        @if ($fak->id == $cf)
                                                        <option value="{{ $fak->id }}" class="text-danger">{{ $fak->nama_fakultas }} (Fakultas Saat Ini)</option>
                                                        @else
                                                        
                                                        @endif
                                                
                                                        @endforeach 
                                                        @foreach ($fakultas as $fak)
                                                    <option value="{{$fak->id}}">{{$fak->nama_fakultas}}</option>
                                                            
                                                        @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                
                                
                                <div class="form-group">
                                    <label class="control-label col-lg-2">password</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" class="form-control" name="password">  
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-2">Role</label>
                                    <div class="col-lg-10">
                                        <select  class="form-control" name="roles"> 
                                                
                                                
                                                    @foreach ($role as $rl)
                                                    
                                                    @if ($rl->id == $cur_role_id)
                                                    <option value="{{ $rl->id }}" class="text-danger">{{ $rl->name }} (Role Saat Ini)</option>
                                                    @else
                                                    {{-- <option value="{{ $rl->id }}">{{ $rl->name }}</option> --}}
                                                    @endif
                                            
                                                    @endforeach
                                                    @foreach ($role as $r)
                                                <option value="{{$r->id}}">{{$r->name}}</option>
                                                        
                                                    @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                
                                
                            

                
                                
                        <legend class="text-bold">*Kosongkan password jika tidak ingin mengganti password</legend>
                
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                
                            </fieldset>
                
                            
                
                            
                
                        </form>
                    </div>
                </div>
        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#user').addClass('active')
</script>
@endpush