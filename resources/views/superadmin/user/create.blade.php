@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> USER</a></li>
        <li class="active">TAMBAH USER</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5>Tambah User Baru</h5>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                </div>
                <form method="post" action="{{ route('superadmin.user.store') }}" class="form-horizontal">
                    @csrf
                    <div class="panel panel-flat">

                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Nama</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" class="form-control" name="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Username</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" class="form-control" name="username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">E-mail</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" class="form-control" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">No Hp</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" class="form-control" name="no_hp">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Fakultas</label>
                                <div class="col-lg-9">
                                    <select id="fakultas" name="fakultas_id" class="form-control">
                                        @foreach ($fakultas as $fak)
                                        <option value="{{ $fak->id }}">{{ $fak->nama_fakultas }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-3 control-label">Password</label>
                                <div class="col-lg-9">
                                    <input type="password" class="form-control" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">role</label>
                                <div class="col-lg-9">
                                    <select id="role" name="role" class="form-control">
                                        @foreach ($role as $r)
                                            <option value="{{ $r->name }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Tambah User</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
@endpush
