@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> INDEX</a></li>
        <li class="active">USER</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">INDEX USER</h5>
                    <div class="heading-elements">
                            @hasrole('superadmin')
                            <span></span>
                            <a href="{{ route('superadmin.user.create') }}" class="btn btn-warning">Masukan Data Baru</a>
                        @else
                            Guest Panel
                        @endhasrole
                       
                    </div>
                    <div class="heading-elements">
                        
                       
                    </div>
                </div>

                <div class="panel-body">
                        
                </div>

                <table class="table datatable-basic">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Fakultas</th>
                            <th>Peran</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->username }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ ($dt->fakultas != null) ? $dt->fakultas->nama_fakultas : ''  }}</td>
                            <td >@foreach ($dt->roles as $role)
                                <label class="badge badge-success"> {{ $role->name }}</label>   
                                @endforeach 
                            </td>
                           <td><a href="{{ route('superadmin.user.edit', $dt->id) }}" class="btn btn-primary">Edit</a>
                                
                                <form action="{{ route('superadmin.user.destroy', $dt->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger"  type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach 
                    </tbody>
                </table>
            </div>

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#user').addClass('active')
</script>
@endpush