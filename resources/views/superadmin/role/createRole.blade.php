@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> USER</a></li>
        <li class="active">TAMBAH ROLE</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
            
                    <div class="heading-elements">
                        <h5>TAMBAH ROLE</h5>
                        <ul class="icons-list">
                           
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif
                </div>

                <form method="post" action="{{ route('superadmin.role.store') }}">
                        @csrf
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        
                                    </ul>
                                </div>
                            </div>
                            
            {{-- inputan --}}
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-3 control-label">Role:</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="role"/>
                    </div>
                </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Permission:</label>
                            <div class="col-lg-9">
                                @foreach ($permission as $pm)
                                <div class="checkbox">
                                
                                <label><input type="checkbox" name="cb[]" value="{{$pm->id  }}" >{{$pm->name}}</label>  
                                @endforeach
                                
                            </div>
                        </div>


                  <div>
                
                    <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                </div>
                </div>
        
                           
                        </div>
                    </form>
                       
            </div>
           

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
@endpush