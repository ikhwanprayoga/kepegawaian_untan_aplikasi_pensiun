@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">EDIT ROLE</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                </div>

                <form method="post" action="{{ route('superadmin.role.update', $role->id) }}">
                    @csrf
                    @method('PATCH')
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"></h5>
                            <div class="heading-elements">
                                
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                @csrf
                                @method('PATCH')
                                <label for="nama">Nama Role:</label>
                                <input type="text" class="form-control" name="name" value="{{ $role->name }}" />


                            </div>
                            {{-- <div class="panel-body">
                                <label for="nama">Permission:</label>
                                @foreach ($permission as $pm)
                                <div class="checkbox">
                                    <input type="text" class="form-control" name="name" value="{{ $pm->name }}"/>
                                    <label><input type="checkbox" name="cb[]" value="{{$pm->id  }}" @for ($i=0; $i <
                                            $sz; $i++) @if ($pm->id == $var[$i])
                                        checked
                                        @else

                                        @endif
                                        @endfor
                                        >{{ $pm->name }}</label>
                                    @endforeach
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i
                                            class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </div> --}}
                            <div class="panel-body">
                                    <label for="nama">Permission:</label>
                                    <div class="checkbox">
                                        @foreach ($permission as $prm)
                                        <label for=""><input type="checkbox" name="cb[]"
                                            @foreach ($role->permissions as $item)
                                            @if ($prm->id == $item->id)
                                                checked
                                            @endif    
                                            @endforeach
                                        value="{{$prm->id}}"> {{ $prm->name }}</label><br>
                                        @endforeach
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Submit form <i
                                                class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                </form>
            </div>

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
@endpush