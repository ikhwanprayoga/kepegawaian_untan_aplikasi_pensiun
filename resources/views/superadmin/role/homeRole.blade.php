@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">ROLE</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">INDEX ROLE</h5>
                    <div class="heading-elements">
                            @hasrole('superadmin')
                            <span></span>
                            {{-- <a href="{{ route('superadmin.role.create') }}" class="btn btn-warning">Masukan Data Baru</a> --}}
                            <td><button type="button"  data-toggle="modal" data-target="#modal_form_horizontal" class="btn btn-primary btn-sm">Tambah Role </i></button></td>   
                            
						{{-- <td><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal_form_inline">permission <i class="icon-play3 position-right"></i></button></td> --}}
										
                            @else
                            Guest Panel
                        @endhasrole
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    
                </div>

                <table class="table datatable-basic">
                    <thead>
                    <tr>
                        <th>Role Id</th>
                        
                        <th>Role</th>
                        <th>Permission</th>
                        
                        {{-- <th>permission</th> --}}
                     <th colspan="2">Aksi</th>
                     <th></th>
                     </tr>
                    </thead>
                    <tbody>
                            @foreach ($role as $r)
                        <tr>
                                <td>{{$r->id}}</td>
                                
                                <td>{{ $r->name }}</td>
                                <td >
                                @foreach ($r->permissions as $rpm)
                                <label class="badge badge-success">  {{$rpm->name}}</label>   
                              
                                @endforeach
                               
                                </td>
                                
                                 <td></td>
                               
                                
                                    
                                <td><a href="{{ route('superadmin.role.edit', $r->id) }}" class="btn btn-primary">Edit</a></td>
                                <td>
                                    <form action="{{ route('superadmin.role.destroy', $r->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>
                        </tr>
                            @endforeach

  
                    
                        </tbody>
                </table>
                
            </div>

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
   <!-- Horizontal form modal -->
   <div id="modal_form_horizontal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div  class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Tambah Role</h5>
            </div>

            <form method="post" action="{{ route('superadmin.role.store') }}" class="form-horizontal">
               @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Role</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="role"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">Permission</label>
                        <div class="col-sm-9">
                            @foreach ($permission as $pm)
                            <div class="checkbox">
                            
                            <label><input type="checkbox" name="cb[]" value="{{$pm->id  }}" >{{$pm->name}}</label>  
                            @endforeach
                              
                        </div>
                    </div>

                
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah Role</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /horizontal form modal -->

@endsection

@push('js')
<script>
$('#role').addClass('active')
</script>
@endpush