@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')
    
@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>    
@endsection

@section('content')
<div class="content">

    <!-- main content -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Index Role</h5>
                    <div class="heading-elements">
                            @hasrole('superadmin')
                            <span></span>
                            {{-- <a href="{{ route('superadmin.role.create') }}" class="btn btn-warning">Masukan Data Baru</a> --}}
                            
                            
                            <td><button type="button" class="btn btn-primary btn-sm"  class="btn btn-warning" data-toggle="modal" data-target="#modal_form_horizontal">Tambah Permission</button></td>
                            
                            @else
                            Guest Panel
                        @endhasrole
                        <ul class="icons-list">
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                        <div id="modal_form_horizontal" class="modal fade">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div  class="modal-header bg-primary">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title">TAMBAH PERMISSION</h5>
                                        </div>
        
                                        <form method="post" action="{{ route('superadmin.permission.store') }}" class="form-horizontal">
                                           @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3">Nama permission</label>
                                                    <div class="col-sm-9">    
                                                    <input type="text" placeholder="Permission" class="form-control" name="permission">
                                                    </div>
                                                </div>
        
                                        
                                                
                                            </div>
        
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit form</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                </div>

                {{-- modal edit --}}
               
                 {{-- modal edit --}}

                <table class="table datatable-basic">
                    <thead>
                    <tr>
                        
                        <th>permission id</th>
                        <th></th>
                        <th>permission</th>
                       <th></th>
                         <th >Edit</th>
                        <th>Delete</th>
                     </tr>
                    </thead>
                    <tbody>
                          
                        @foreach ($permission as $pm)
                         <td>{{$pm->id}}</td>
                         <td></td>
                        <td>{{$pm->name}}</td>
                        <td></td> 
                       
                               
                       
                                 
                                    
                    <td><a href="{{ route('superadmin.permission.edit', $pm->id) }}" class="btn btn-primary">Edit</a></td>
                    
                      <td><form action="{{ route('superadmin.permission.destroy', $pm->id) }}" method="post" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal_edit">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                   
                        </tr>
                            @endforeach
                        </td>
                        
                    
                        </tbody>
                </table>
            </div>

        </div>

    </div>
    <!-- /main content -->

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#permission').addClass('active')
</script>
@endpush