@extends('layouts.theme.base')

@section('title', 'Super Admin Beranda')

@push('css')

@endpush

@section('breadcrumb')
<div class="breadcrumb-line" style="padding-top: 6px;">
    <ul class="breadcrumb">
        <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ul>

    @include('layouts.theme.profile')
</div>
@endsection

@section('content')
<div class="content">


    {{-- JUMLAH TOTAL --}}
    <div class="col-lg-4">
        <div class="panel bg-green-400">
            <div class="panel-body">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>
                <h1>JUMLAH USER </h1>
                <h1>{{$user->count( )}}</h1>
            </div>
        </div>

        <div id="server-load"></div>
    </div>
    {{-- user --}}
    <div class="col-lg-4">
        <div class="panel bg-pink-400">
            <div class="panel-body">
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>
                <h1>JUMLAH ROLE </h1>
                <h1>{{$role->count( )}}</h1>
            </div>
        </div>

        <div id="server-load"></div>
    </div>

    {{-- total usulan --}}
    <div class="col-lg-4">
        <div class="panel bg-primary-300">
            <div class="panel-body">

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="reload"></a></li>
                    </ul>
                </div>
                <h1>JUMLAH USULAN FAKULTAS </h1>
                <h1>{{$usulan->count( )}}</h1>
            </div>
        </div>

        <div id="server-load"></div>
    </div>

    {{-- JUMLAH TOTAL --}}

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading"> USULAN FAKULTAS</h5>

                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>

                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table datatable-basic">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Fakultas</th>
                                <th class="hidden-xs">Jumlah Usulan</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($fakultas as $dt)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$dt->nama_fakultas}}</td>
                                <td>
                                    @php
                                    $tt = $dt->usulanPensiun->where('fakultas_id', $dt->id)->count();
                                    @endphp
                                    {{ $tt }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
            <!-- /latest posts -->
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                </div>
                <table class="table datatable-basic">
                    <thead>

                        <tr>
                            <th>No</th>
                            <th>Role</th>
                            <th>Jumlah </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($role as $r)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$r->name}}</td>
                            <td>
                                {{ $r->Users->count() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- menampilkan Role --}}

    @include('layouts.theme.footer')

</div>
@endsection

@push('js')
<script>
    $('#beranda').addClass('active');
</script>
@endpush
