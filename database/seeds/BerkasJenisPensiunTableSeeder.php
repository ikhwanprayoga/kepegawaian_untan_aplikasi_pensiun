<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class BerkasJenisPensiunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('berkas_jenis_pensiun')->insert([
            //jenis bup
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>1, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>2, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>3, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>4, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>5, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>6, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>7, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>8, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>9, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>10, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>11, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>12, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>13, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>14, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>15, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>16, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>29, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>30, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>31, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>32, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>33, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>1, 'master_berkas_pensiun_id'=>34, 'created_at'=>$create, 'updated_at'=>$create],

            // //jenis janda duda
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>1, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>2, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>3, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>4, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>5, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>6, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>7, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>8, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>9, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>10, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>11, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>12, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>13, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>14, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>15, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>16, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>17, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>2, 'master_berkas_pensiun_id'=>18, 'created_at'=>$create, 'updated_at'=>$create],

            // //tewas
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>1, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>2, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>3, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>4, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>5, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>6, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>7, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>8, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>9, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>10, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>11, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>12, 'created_at'=>$create, 'updated_at'=>$create],
            // ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>13, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>14, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>15, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>16, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>17, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>18, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>19, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>23, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>26, 'created_at'=>$create, 'updated_at'=>$create],
            ['jenis_pensiun_id'=>3, 'master_berkas_pensiun_id'=>28, 'created_at'=>$create, 'updated_at'=>$create],
        ]);
    }
}
