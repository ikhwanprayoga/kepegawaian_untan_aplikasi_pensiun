<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('roles')->insert([
            ['name'=>'superadmin', 'guard_name'=>'web', 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'operator untan', 'guard_name'=>'web', 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'operator fakultas', 'guard_name'=>'web', 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'kepala kepegawaian untan', 'guard_name'=>'web', 'created_at'=>$create, 'updated_at'=>$create],
        ]);
    }
}
