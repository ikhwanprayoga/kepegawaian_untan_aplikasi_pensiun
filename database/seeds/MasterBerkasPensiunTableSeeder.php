<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class MasterBerkasPensiunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('master_berkas_pensiun')->insert([
            ['judul_berkas'=>'Surat Usul pimpinan Unit Kerja', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat Permohonan PNS Ybs/Ahli Waris yang sah***)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'DPCP (Daftar Perorangan Calon Penerima Pensiun)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'SK CPNS', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'SK PNS', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'SK Pangkat terakhir', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'SK Jabatan (fungsional)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Kartu Pegawai (karpeg) / KPE', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Akta/Surat Nikah/Cerai', 'keterangan'=>'-Apabila cerai = akta cerai <br> -Apabile meninggal dunia = akta MD <br> -Apabila menikah kembali = akta nikah istri ke II', 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Akta/Surat Kenal Lahir Anak < 25 tahun masih dalam tanggungan', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            
            ['judul_berkas'=>'Kartu Keluarga', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Pas foto berwarna', 'keterangan'=>'- BUP dan Pensiun Janda/Duda?anak = uk. 3x4 cm <br> - Pensiun Dini/Keuzuran = uk. 4 x6 cm', 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Formulir Permintaan Pembayaran Pensiun (SP4)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Penilaian Prestasi Kerja 1 tahun terakhir', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat Pernyataan Tidak Pernah Dijatuhi Hukuman Disiplin Tingkat Sedang/Berat 1 tahun terakhir', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat Pernyataan  tidak sedang menjalani proses pidana atau pernah dipidana penjara berdasarkan putusan pengadilan yang telah berkekuatan hukum tetap', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Salinan Sah SK Pengalaman kerja sebelum diangkat CPNS (Peninjauan Masa Kerja)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Akta/Surat keterangan Kematian***)', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat Keterangan Janda/Duda/anak/Orang Tua', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Kronologis kejadian Bagi PNS yang Tewas/Cacad karena Dinas/Hilang***)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            
            ['judul_berkas'=>'Surat Keterangan Tim Penguji Kesehatan bagi PNS yang Uzur Jasmani/Rohani atau Cacad karena dinas dan lain sejenisnya', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Keputusan Pengangkatan dan Pelantikan sebagai Pejabat Negara/KPU/Hakim Mahkamah Konstitusi dan Lain sejenisnya***)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat usul Pimpinan PT TASPEN', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat Keterangan Kepolisian/Pejabat berwenang', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Salinan Sah SK Pensiun yang hilang', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Visum bagi PNS yang tewas atau cacad karena dinas***)', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Data yang sah sebagai dasar perbaikan SK', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Surat Perintah untuk melaksanakan tugas', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Fotokopi SK Gaji Berkala Terakhir', 'keterangan'=>null, 'pegawai_dapat_upload' => '0', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Fotokopi NPWP', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            
            ['judul_berkas'=>'Fotokopi rekening BANK yang akan digunakan untuk menerima pensiun', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Daftar Susunan Keluarga yang disahkan', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'KTP', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
            ['judul_berkas'=>'Formulir Taspen', 'keterangan'=>null, 'pegawai_dapat_upload' => '1', 'created_at'=>$create, 'updated_at'=>$create],
        ]);
    }
}
