<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfoFakultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('info_fakultas')->insert([
            [
                'fakultas_id' => 8,
                'nama_lengkap_fakultas' => 'Fakultas Matematika Dan Ilmu Pengetahuan Alam',
                'nama_dekan' => 'Afghani Jayuska, S.Si., M.Si.',
                'nip_dekan' => '197107072000121001',
                'pangkat_dekan' => '',
                'golongan_dekan' => 'IV/a',
                'alamat_fakultas' => 'JL. Prof. Dr. H. Hadari Nawawi',
                'no_telepon_fakultas' => '(0561) 577963',
                'email_fakultas' => 'info@fmipa.untan.ac.id',
                'fax_fakultas' => '(0561) 740187',
                'kode_pos_fakultas' => '78124',
            ],
            [
                'fakultas_id' => 4,
                'nama_lengkap_fakultas' => 'Fakultas Teknik',
                'nama_dekan' => 'Dr.rer.nat. Ir. R. M. Rustamaji, M.T.',
                'nip_dekan' => '196801161994031003',
                'pangkat_dekan' => '',
                'golongan_dekan' => 'IV/a',
                'alamat_fakultas' => 'JL. Prof. Dr. H. Hadari Nawawi',
                'no_telepon_fakultas' => '(0561) 7053252',
                'email_fakultas' => 'teknik@untan.ac.id',
                'fax_fakultas' => '(0561) 740187',
                'kode_pos_fakultas' => '78124',
            ]
        ]);
    }
}
