<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class JenisPensiunTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('jenis_pensiun')->insert([
            ['nama'=>'BUP', 'kode'=>'bup', 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Janda/Duda/Anak/Orang Tua', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Tewas', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Cacad karena Dinas', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Keuzuran Jasmani/Rohani', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Atas Permintaan Sendiri, Parpol, Pejabat Negara KPU dan lain sejenisnya', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Petikan Ke II SK Pensiun Yang Hilang', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Perbaikan SK Pensiun', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Pernyataan PNS yang hilang', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
            ['nama'=>'Surat dari instansi terkait/Pengaduan/Kelengkapan data/dan lain sejenisnya', 'kode'=>null, 'keterangan'=>null, 'created_at'=>$create, 'updated_at'=>$create],
        ]);
    }
}
