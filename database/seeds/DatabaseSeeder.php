<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FakultasTableSeeder::class,
            UsersTableSeeder::class,
            RolesTableSeeder::class,
            ModelHasRolesTableSeeder::class,
            JenisPensiunTableSeeder::class,
            MasterBerkasPensiunTableSeeder::class,
            BerkasJenisPensiunTableSeeder::class,
            InfoFakultasTableSeeder::class,
        ]);
    }
}
