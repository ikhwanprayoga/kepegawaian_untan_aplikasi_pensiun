<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class FakultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('fakultas')->insert([
            ['nama_fakultas' => 'Fakultas Hukum', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'FEB', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'Fakultas Pertanian', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'Fakultas Teknik', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'FISIP', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'FKIP', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'Fakultas Kehutanan', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'FMIPA', 'created_at'=>$create, 'updated_at'=>$create],
            ['nama_fakultas' => 'Fakultas Kedokteran', 'created_at'=>$create, 'updated_at'=>$create],
        ]);
    }
}
