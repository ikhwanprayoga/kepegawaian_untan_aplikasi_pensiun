<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('users')->insert([
            ['name'=>'super admin', 'username'=>'superadmin', 'email'=>'superadmin@email.com', 'no_hp' => null, 'fakultas_id' => null,'password'=>bcrypt('123qwe'), 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'operator kepegawaian untan mipa', 'username'=>'untanmipa', 'email'=>'kepegawaianuntan@email.com', 'no_hp' => null, 'fakultas_id' => 8,'password'=>bcrypt('123qwe'), 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'operator fakultas mipa', 'username'=>'fakultasmipa', 'email'=>'kepegawaianmipa@email.com', 'no_hp' => null, 'fakultas_id' => 8,'password'=>bcrypt('123qwe'), 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'kepala kepegawaian untan', 'username'=>'kepalakepegawaian', 'email'=>'kepalakepegawaian@email.com', 'no_hp' => null, 'fakultas_id' => null,'password'=>bcrypt('123qwe'), 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'operator fakultas teknik', 'username'=>'fakultasteknik', 'email'=>'fakultasteknik@email.com', 'no_hp' => null, 'fakultas_id' => 4,'password'=>bcrypt('123qwe'), 'created_at'=>$create, 'updated_at'=>$create],
            ['name'=>'operator kepegawaian untan teknik', 'username'=>'untanteknik', 'email'=>'kepegawaianuntanteknik@email.com', 'no_hp' => null, 'fakultas_id' => 4,'password'=>bcrypt('123qwe'), 'created_at'=>$create, 'updated_at'=>$create],
        ]);
    }
}
