<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaktuPrintBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waktu_print_berkas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usulan_pensiun_id');
            $table->date('waktu_dpcp')->nullable();
            $table->date('waktu_sp4')->nullable();
            $table->date('waktu_surat_pengantar')->nullable();
            $table->date('waktu_surat_pidana')->nullable();
            $table->date('waktu_surat_hukuman')->nullable();
            $table->string('no_surat_pengantar')->nullable();
            $table->string('no_surat_pidana')->nullable();
            $table->string('no_surat_hukuman')->nullable();
            $table->timestamps();

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waktu_print_berkas');
    }
}
