<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usulan_pensiun_id');
            $table->text('catatan_revisi');
            $table->integer('status')->nullable();
            $table->timestamps();

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revisi');
    }
}
