<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifikasiSosmedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasi_sosmed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('alamat_sosmed_dosen_id');
            $table->text('pesan');
            $table->string('no_hp')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();

            $table->foreign('alamat_sosmed_dosen_id')
                ->references('id')
                ->on('alamat_sosmed_dosen')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifikasi_sosmed');
    }
}
