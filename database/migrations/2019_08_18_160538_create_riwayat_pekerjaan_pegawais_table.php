<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatPekerjaanPegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pekerjaan_pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('NIP');
            $table->text('riwayat_pekerjaan')->nullable();
            $table->date('dari_tgl')->nullable();
            $table->date('sampai_tgl')->nullable();
            $table->string('golongan_ruang')->nullable();
            $table->string('instansi_induk')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pekerjaan_pegawai');
    }
}
