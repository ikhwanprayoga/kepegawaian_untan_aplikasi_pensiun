<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifikasiWaEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasi_wa_email', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('pesan')->nullable();
            $table->dateTime('waktu_kirim')->nullable();
            $table->string('email')->nullable();
            $table->string('no_hp')->nullable();
            $table->unsignedBigInteger('untuk_user_id')->nullable();
            $table->unsignedBigInteger('dari_user_id')->nullable();
            $table->unsignedBigInteger('usulan_pensiun_id')->nullable();
            $table->string('nip')->null();
            $table->timestamps();

            $table->foreign('untuk_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('dari_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifikasi_wa_email');
    }
}
