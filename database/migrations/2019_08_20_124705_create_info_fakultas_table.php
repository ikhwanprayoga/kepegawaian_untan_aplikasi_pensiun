<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoFakultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_fakultas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('fakultas_id');
            $table->string('nama_lengkap_fakultas')->nullable();
            $table->string('nama_dekan')->nullable();
            $table->string('nip_dekan')->nullable();
            $table->string('pangkat_dekan')->nullable();
            $table->string('golongan_dekan')->nullable();
            $table->string('alamat_fakultas')->nullable();
            $table->string('no_telepon_fakultas')->nullable();
            $table->string('email_fakultas')->nullable();
            $table->string('fax_fakultas')->nullable();
            $table->string('kode_pos_fakultas')->nullable();
            $table->timestamps();

            $table->foreign('fakultas_id')
                ->references('id')
                ->on('fakultas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_fakultas');
    }
}
