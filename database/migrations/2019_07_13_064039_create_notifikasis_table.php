<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Notifications\Notification;

class CreateNotifikasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usulan_pensiun_id')->nullable();
            $table->unsignedBigInteger('dari_users_id')->nullable();
            $table->unsignedBigInteger('untuk_users_id')->nullable();
            $table->unsignedBigInteger('fakultas_id')->nullable();
            $table->string('untuk_role')->nullable();
            $table->string('nip_dosen')->nullable();
            $table->text('pesan')->nullable();
            $table->text('link')->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('waktu_notifikasi_dikirim')->nullable();
            $table->dateTime('waktu_notifikasi_dibaca')->nullable();
            $table->timestamps();

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');

            $table->foreign('fakultas_id')
                ->references('id')
                ->on('fakultas')
                ->onDelete('set null');

            $table->foreign('dari_users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('untuk_users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifikasi');
    }
}
