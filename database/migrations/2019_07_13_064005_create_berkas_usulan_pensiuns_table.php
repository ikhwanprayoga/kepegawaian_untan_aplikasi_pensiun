<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBerkasUsulanPensiunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berkas_usulan_pensiun', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usulan_pensiun_id');
            $table->unsignedBigInteger('master_berkas_pensiun_id');
            $table->unsignedBigInteger('berkas_id');
            $table->integer('status_verifikasi_kepeg_fakultas')->nullable();
            $table->integer('status_verifikasi_kepeg_untan')->nullable();
            $table->timestamps();

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');

            $table->foreign('master_berkas_pensiun_id')
                ->references('id')
                ->on('master_berkas_pensiun')
                ->onDelete('cascade');

            $table->foreign('berkas_id')
                ->references('id')
                ->on('berkas')
                ->onDelete('cascade');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berkas_usulan_pensiun');
    }
}
