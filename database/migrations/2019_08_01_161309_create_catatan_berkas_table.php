<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatatanBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catatan_berkas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('berkas_usulan_pensiun_id');
            $table->text('catatan')->nullable();
            $table->integer('status_kepeg_fakultas')->nullable();
            $table->timestamps();

            $table->foreign('berkas_usulan_pensiun_id')
                ->references('id')
                ->on('berkas_usulan_pensiun')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catatan_berkas');
    }
}
