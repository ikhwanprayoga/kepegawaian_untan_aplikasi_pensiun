<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berkas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('nama_berkas')->nullable();
            $table->string('direktori')->nullable();
            $table->unsignedBigInteger('uploader_berkas_user_id')->nullable();
            $table->string('uploader_berkas_pegawai_nip')->nullable();
            $table->timestamps();

            $table->foreign('uploader_berkas_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berkas');
    }
}
