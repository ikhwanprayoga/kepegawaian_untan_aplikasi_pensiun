<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBerkasJenisPensiunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berkas_jenis_pensiun', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('jenis_pensiun_id');
            $table->unsignedBigInteger('master_berkas_pensiun_id');
            $table->timestamps();

            $table->foreign('jenis_pensiun_id')
                ->references('id')
                ->on('jenis_pensiun')
                ->onDelete('cascade');
                
            $table->foreign('master_berkas_pensiun_id')
                ->references('id')
                ->on('master_berkas_pensiun')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berkas_jenis_pensiun');
    }
}
