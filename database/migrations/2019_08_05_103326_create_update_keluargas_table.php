<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdateKeluargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_keluarga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('NIP')->nullable();
            $table->string('nik')->nullable();
            $table->string('nama')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->string('tanggal_kawin')->nullable();
            $table->string('tanggal_cerai')->nullable();
            $table->string('pasangan_keberapa')->nullable();
            $table->timestamps();

            // $table->foreign('NIP')
            // ->references('NIP')
            // ->on('update_pegawai')
            // ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_keluarga');
    }
}
