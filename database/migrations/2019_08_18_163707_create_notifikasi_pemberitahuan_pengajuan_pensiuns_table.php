<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifikasiPemberitahuanPengajuanPensiunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifikasi_pemberitahuan_pengajuan_pensiun', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nip');
            $table->string('nama')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('unit_kerja')->nullable();
            $table->string('tahun_pensiun')->nullable();
            $table->dateTime('waktu_kirim_notifikasi')->nullable();
            $table->text('pesan')->nullable();
            $table->string('email_pegawai')->nullable();
            $table->string('no_hp_pegawai')->nullable();
            $table->string('no_hp_operator_fakultas')->nullable();
            $table->string('no_hp_operator_untan')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifikasi_pemberitahuan_pengajuan_pensiun');
    }
}
