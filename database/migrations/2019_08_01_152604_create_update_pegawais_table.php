<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdatePegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usulan_pensiun_id')->nullable();
            $table->string('NAMA_LENGKAP')->nullable();
            $table->string('Tempat_Lahir')->nullable();
            $table->date('Tgl_Lahir')->nullable();
            $table->string('Agama')->nullable();
            $table->string('JK')->nullable();
            $table->string('NIP')->nullable();
            $table->string('NIDN')->nullable();
            $table->string('No_Karpeg')->nullable();
            $table->string('Status_Perkawinan')->nullable();
            $table->string('ALAMAT_EMAIL')->nullable();
            $table->string('JAB_FUNGSIONAL')->nullable();
            $table->string('Gol')->nullable();
            $table->string('Jurusan')->nullable();
            $table->string('Program_Studi')->nullable();
            $table->string('pangkat')->nullable();
            $table->date('tmt')->nullable();
            $table->string('gaji_pokok_terakhir')->nullable();
            $table->string('masa_kerja_kp_terakhir')->nullable();
            $table->string('masa_kerja_golongan')->nullable();
            $table->string('masa_kerja_pns')->nullable();
            $table->string('masa_kerja_pensiun')->nullable();
            $table->string('cltn')->nullable();
            $table->string('peninjauan_masa_kerja')->nullable();
            $table->string('pendidikan_dasar')->nullable();
            $table->string('lulus_tahun')->nullable();
            $table->string('foto')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('npwp')->nullable();
            $table->string('masa_kerja_golongan_tahun')->nullable();
            $table->string('masa_kerja_golongan_bulan')->nullable();
            $table->date('masa_kerja_golongan_kalender')->nullable();
            $table->string('masa_kerja_pensiun_tahun')->nullable();
            $table->string('masa_kerja_pensiun_bulan')->nullable();
            $table->date('mulai_masuk_pns')->nullable();
            $table->date('belum_pns_dari')->nullable();
            $table->date('belum_pns_sampai_dengan')->nullable();
            $table->string('alamat_jl')->nullable();
            $table->string('alamat_rt')->nullable();
            $table->string('alamat_rw')->nullable();
            $table->string('alamat_kelurahan')->nullable();
            $table->string('alamat_kecamatan')->nullable();
            $table->string('alamat_kota')->nullable();
            $table->string('alamat_provinsi')->nullable();
            $table->timestamps();

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_pegawai');
    }
}
