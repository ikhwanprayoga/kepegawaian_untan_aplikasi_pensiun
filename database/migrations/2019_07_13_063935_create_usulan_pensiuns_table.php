<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsulanPensiunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usulan_pensiun', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_usulan');
            $table->string('nip');
            $table->unsignedBigInteger('fakultas_id')->nullable();
            $table->unsignedBigInteger('jenis_pensiun_id');
            $table->integer('status_usulan_kepeg_fakultas')->nullable();
            $table->integer('status_usulan_kepeg_untan')->nullable();
            $table->string('no_resi_pengiriman_berkas')->nullable();
            $table->date('tanggal_kirim_berkas')->nullable();
            $table->string('jasa_pengiriman')->nullable();
            $table->timestamps();

            $table->foreign('fakultas_id')
                ->references('id')
                ->on('fakultas')
                ->onDelete('set null');

            $table->foreign('jenis_pensiun_id')
                ->references('id')
                ->on('jenis_pensiun')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usulan_pensiun');
    }
}
