<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatatanUsulansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catatan_usulan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usulan_pensiun_id');
            $table->text('isi_catatan');
            $table->integer('status');
            $table->timestamps();

            $table->foreign('usulan_pensiun_id')
                ->references('id')
                ->on('usulan_pensiun')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catatan_usulan');
    }
}
