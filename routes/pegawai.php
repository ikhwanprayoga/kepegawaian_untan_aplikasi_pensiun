<?php

use Symfony\Component\HttpFoundation\Request;

Route::get('pegawai/login', 'pegawai\LoginController@index')->name('pegawai.login.index');
Route::post('pegawai/login', 'pegawai\LoginController@login')->name('pegawai.login.post');

Route::group(['prefix' => 'pegawai', 'namespace' => 'pegawai'], function () {
    Route::get('beranda', 'BerandaController@index')->name('pegawai.beranda');
    Route::get('usulan', 'UsulanController@index')->name('pegawai.usulan');
    Route::group(['prefix' => 'berkas'], function () {
        Route::get('/', 'BerkasController@index')->name('pegawai.berkas.index');
        Route::post('tambah-berkas', 'BerkasController@tambah_berkas')->name('pegawai.berkas.tambah_berkas');
    });
    Route::get('download-berkas', 'DownloadBerkasController@index')->name('pegawai.download-berkas.index');
    Route::post('logout', function (Request $request) {
        Session::forget(['nip', 'nama']);
        return redirect()->route('pegawai.login.index');;
    })->name('pegawai.logout');
});