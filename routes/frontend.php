<?php

Route::get('beranda', 'FrontEndController@beranda')->name('beranda');
Route::get('tentang', 'FrontEndController@tentang')->name('tentang');
Route::get('panduan-aplikasi', 'FrontEndController@panduan_aplikasi')->name('panduan-aplikasi');
Route::get('panduan-berkas', 'FrontEndController@panduan_berkas')->name('panduan-berkas');
Route::get('layanan-sop', 'FrontEndController@layanan_sop')->name('layanan-sop');
Route::get('layanan-produk-hukum', 'FrontEndController@layanan_produk_hukum')->name('layanan-produk-hukum');
Route::get('kontak', 'FrontEndController@kontak')->name('kontak');