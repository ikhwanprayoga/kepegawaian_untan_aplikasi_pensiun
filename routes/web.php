<?php
use Illuminate\Support\Facades\DB;
use App\Kirim;
// use Mail;
use App\User;
use App\Models\UsulanPensiun;
use App\Models\NotifikasiWaEmail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('beranda');
    // return view('welcome');
});

// Auth::routes();
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('tahunPensiun', function () {
    $data = Helpers::tahunPensiun(196706161994121001, true);
    return $data;
});

Route::get('tes-email', function () {
    $to_name = 'Ikhwan Prayoga';
    $to_email = 'yoga.seke@gmail.com';
    $pesan2 = 'Kepegawaian Universitas Tanjungpura \nMohon untuk melengkapi berkas usulan pensiun, karena usia anda akan memasuki batas usia untuk pensiun. Untuk informasi lebih lanjut silahkan masuk ke sistem informasi dan layanan pensiun www.silap.untan.ac.id/pegawai/login';
    $data = array('name'=>"ikhwan prayoga", "body" => $pesan2);

    Mail::send('email.tes_email', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('pemberitahuan tentang pemberkasan batas usia pensiun');
        $message->from('kepegawaian@untan.ac.id','Kepegawaian UNTAN');
    });
});

//route clone data dosen dan atur jadwal pengiriman notifikasi
Route::get('atur-jadwal', 'JadwalNotifikasiController@atur_jadwal');
Route::get('clone-data-dosen', 'JadwalNotifikasiController@clone_data');
Route::get('kirim-notif', 'JadwalNotifikasiController@kirim_notif');
Route::get('kirim-notif-sementara/{nip}', 'JadwalNotifikasiController@kirim_notif_sementara');

Route::get('/helpers', function () {
    // return Helpers::idOperator(4, 'operator fakultas');
    $role = 'operator untan';
    $usulan_pensiun_id = 3;
    $pesan = 'tes tok e ss';

    $usulan = UsulanPensiun::where('id', $usulan_pensiun_id)->first();
    $fakultas_id = $usulan->fakultas_id;
    $nip = $usulan->nip;

    if ($role == 'operator untan') {
        $untuk_user_id  = Helpers::idOperator($fakultas_id, 'operator fakultas');
        $dari_user_id   = Helpers::idOperator($fakultas_id, 'operator untan');
        $email          = Helpers::emailOperator($fakultas_id, 'operator fakultas');
        $no_hp          = Helpers::noHpOperator($fakultas_id, 'operator fakultas');
    } else {
        $untuk_user_id  = Helpers::idOperator($fakultas_id, 'operator untan');
        $dari_user_id   = Helpers::idOperator($fakultas_id, 'operator fakultas');
        $email          = Helpers::emailOperator($fakultas_id, 'operator untan');
        $no_hp          = Helpers::noHpOperator($fakultas_id, 'operator untan');
    }


    $untuk_user_id = Helpers::idOperator($fakultas_id, 'operator fakultas');

    $data = NotifikasiWaEmail::create([
        'pesan' => $pesan,
        'waktu_kirim' => \Carbon\Carbon::now(),
        'email' => $email,
        'no_hp' => $no_hp,
        'untuk_user_id' => $untuk_user_id,
        'dari_user_id' => $dari_user_id,
        'usulan_pensiun_id' => $usulan_pensiun_id,
        'nip' => $nip,
    ]);

    //kirim::wa(tujuan , pesan)
    $kirimWa = Kirim::wa($data->no_hp, $pesan);

    //kirim email

});


Route::get('/kirim-wa', function () {
    $tujuan = '089501942413';
    $pesan = 'tes kirim wa 08';
    $wa = Kirim::wa($tujuan, $pesan);
    return $wa;
});

Route::get('cek-role', function () {

    $user = User::where('fakultas_id', 4)->with('roles')->get();
    foreach ($user as $keys => $value) {
        foreach ($value->roles as $key => $val) {
            if ($val->name == 'operator untan') {
                if ($val) {
                    // $data['role'] = $val;
                    $noHP = $value->no_hp;
                } else {
                    $noHp = 000;
                }

            }
        }
    }

    return $noHP;
});

Route::get('jenis-pensiun', function () {
    $data = DB::table('jenis_pensiun')->get();
    return response()->json($data, 200);
});

//notifikasi
Route::get('notifikasi-baca/{id}', 'NotifikasiController@baca')->name('notifikasi-baca');
Route::get('notifikasi-baca-semua/{fakultas_id}','NotifikasiController@bacasemua')->name('notifikasi-baca-semua');
Route::get('notifikasi-baca-semua/operatoruntan/{fakultas_id}','NotifikasiController@bacasemua_untan')->name('notifikasi-baca-semua-operatoruntan');

//berkas
Route::get('dpcp/{nip}', 'BerkasController@dpcp')->name('dpcp');
Route::get('cek/dpcp/{usulan_pensiun_id}', 'BerkasController@cek_dpcp')->name('cek_dpcp');
Route::get('sp4/{nip}', 'BerkasController@sp4')->name('sp4');
Route::get('cek/sp4/{usulan_pensiun_id}', 'BerkasController@cek_sp4')->name('cek_sp4');
Route::get('surat_pengantar_bup/{nip}', 'BerkasController@surat_pengantar_bup')->name('surat_pengantar_bup');
Route::get('cek/surat_pengantar_bup/{usulan_pensiun_id}', 'BerkasController@cek_surat_pengantar')->name('cek_surat_pengantar');
Route::get('surat_bebas_pidana/{nip}', 'BerkasController@surat_bebas_pidana')->name('surat_bebas_pidana');
Route::get('cek/surat_bebas_pidana/{usulan_pensiun_id}', 'BerkasController@cek_surat_bebas_pidana')->name('cek_surat_bebas_pidana');
Route::get('surat_bebas_hukum/{nip}', 'BerkasController@surat_bebas_hukum')->name('surat_bebas_hukum');
Route::get('cek/surat_bebas_hukum/{usulan_pensiun_id}', 'BerkasController@cek_surat_bebas_hukum')->name('cek_surat_bebas_hukum');

@include('superadmin.php');
@include('kepegawaian_fakultas.php');
@include('kepegawaian_untan.php');
@include('pegawai.php');
@include('kapala_kepegawaian.php');
@include('frontend.php');

// route tes kirim wa
// Route::get('/notifikas', '');
