<?php
Route::group(['prefix' => 'kepegawaian-fakultas', 'namespace' => 'kepegawaian_fakultas', 'middleware' => ['auth', 'role:operator fakultas']], function () {
    Route::get('beranda', 'BerandaController@index')->name('kepegawaian-fakultas.beranda');
    Route::get('usulan', 'UsulanController@index')->name('kepegawaian-fakultas.usulan.index');
    Route::get('usulan/cari-data-pegawai/{nip}', 'UsulanController@cari_data_pegawai')->name('kepegawaian-fakultas.usulan.cari-data-pegawai');
    Route::post('usulan/tambah', 'UsulanController@tambah_usulan')->name('kepegawaian-fakultas.usulan.tambah');
    Route::post('usulan/hapus/{id}', 'UsulanController@hapus_usulan')->name('kepegawaian-fakultas.usulan.hapus');
    Route::get('usulan/kelola/{id}', 'UsulanController@kelola_usulan')->name('kepegawaian-fakultas.usulan.kelola');
    Route::get('usulan/kelola/{id}/berkas', 'UsulanController@kelola_usulan_berkas')->name('kepegawaian-fakultas.usulan.kelola.berkas');
    Route::post('usulan/kelola/{id}/berkas/tambah', 'UsulanController@kelola_usulan_berkas_tambah')->name('kepegawaian-fakultas.usulan.kelola.berkas.tambah');
    Route::post('usulan/kelola/berkas/verifikasi/{id}', 'UsulanController@verifikasi_berkas')->name('kepegawaian-fakultas.usulan.kelola.berkas.verifikasi_berkas');
    Route::post('usulan/verifikasi/{id}', 'UsulanController@verifikasi_usulan')->name('kepegawaian-fakultas.usulan.verifikasi');
    Route::post('usulan/kirim/{id}', 'UsulanController@kirim_usulan')->name('kepegawaian-fakultas.usulan.kirim');
    Route::get('usulan/{id}/lihat-catatan', 'UsulanController@lihat_catatan')->name('kepegawaian-fakultas.usulan.lihat-catatan');
    Route::post('usulan/{id}/catatan-selesai', 'UsulanController@catatan_selesai')->name('kepegawaian-fakultas.usulan.catatan-selesai');

    Route::group(['prefix' => 'usulan/terbit'], function () {
        Route::get('/', 'UsulanTerbitController@index')->name('kepegawaian-fakultas.usulan.terbit.index');
        Route::get('kelola/{usulan_pensiun_id}', 'UsulanTerbitController@kelola')->name('kepegawaian-fakultas.usulan.terbit.kelola');
        Route::get('kelola/{id}/berkas', 'UsulanTerbitController@kelola_usulan_berkas')->name('kepegawaian-fakultas.usulan.terbit.kelola_berkas');
    });

    //profil akun
    Route::get('profil','UserController@index')->name('kepegawaian-fakultas.profil');
    Route::get('profil/editProfil/{id}', 'UserController@editProfil')->name('kepegawaian-fakultas.profil.editprofil');
    Route::post('profil/update/{id}', 'UserController@update');
    Route::get('profile','UserController@profile');

    //revisi
    Route::get('revisi', 'RevisiController@index')->name('kepegawaian-fakultas.usulan.revisi');
    Route::get('revisi/{upi}', 'RevisiController@detail_revisi')->name('kepegawaian-fakultas.usulan.detail');
    Route::post('revisiSelesai/{id}', 'RevisiController@revisi_selesai')->name('kepegawaian-fakultas.usulan.selesai');

    //dpcp
    Route::get('usulan/dpcp/{nip}', 'DpcpController@index')->name('kepegawaian-fakultas.dpcp.index');
    Route::get('usulan/dpcp/cetak-pdf/{nip}', 'DpcpController@cetak')->name('kepegawaian-fakultas.dpcp.cetak');

    //berkas otomatis
    Route::group(['prefix' => 'Kelengkapan_berkas'], function () {
        Route::get('{usulan_id}/', 'KelengkapanBerkasController@index')->name('kepegawaian-fakultas.Kelengkapan_berkas.index');
        Route::get('{usulan_id}/srt_pernyataan_tidak_pernah_pidana', 'KelengkapanBerkasController@srt_pernyataan_tidak_pernah_pidana')->name('kepegawaian-fakultas.Kelengkapan_berkas.srt_pernyataan_tidak_pernah_pidana');
        Route::get('{usulan_id}/daftar_riwayat_pekerjaan', 'KelengkapanBerkasController@daftar_riwayat_pekerjaan')->name('kepegawaian-fakultas.Kelengkapan_berkas.daftar_riwayat_pekerjaan');
        Route::get('{usulan_id}/srt_pernyataan_bebas_hukuman', 'KelengkapanBerkasController@srt_pernyataan_bebas_hukuman')->name('kepegawaian-fakultas.Kelengkapan_berkas.srt_pernyataan_bebas_hukuman');
        Route::get('{usulan_id}/susunan_keluarga', 'KelengkapanBerkasController@susunan_keluarga')->name('kepegawaian-fakultas.Kelengkapan_berkas.susunan_keluarga');
        Route::get('{usulan_id}/srt_pernyataan_pembayaran_pensiun_pertama', 'KelengkapanBerkasController@srt_pernyataan_pembayaran_pensiun')->name('kepegawaian-fakultas.Kelengkapan_berkas.srt_pernyataan_pembayaran_pensiun_pertama');
        Route::get('{usulan_id}/usul_pemberhentian', 'KelengkapanBerkasController@usul_pemberhentian')->name('kepegawaian-fakultas.Kelengkapan_berkas.usul_pemberhentian');
    });

    //caripegawai
    Route::get('search/{input}', 'CariController@search')->name('kepegawaian-fakultas.search');
    Route::get('data-dosen/bpkad/{nip}', 'CariController@bpkad')->name('kepegawaian-fakultas.search.bpkad');

    //update Dosen
    // Route::get('datapegawai', 'DatapegawaiController@index')->name('kepegawaian-fakultas.datapegawai.index');
    Route::get('data/editData/{nip}', 'UpdatedataController@edit')->name('kepegawaian-fakultas.data.editdata');
    Route::post('data/update/{nip}', 'UpdatedataController@update')->name('kepegawaian-fakultas.data.update');
    Route::get('data/show/{nip}', 'UpdatedataController@show')->name('kepegawaian-fakultas.data.show');
    //update data anak pegawai
    Route::post('data/tambah/anak', 'UpdatedataController@tambah_anak')->name('kepegawaian-fakultas.data.tambah_anak');
    Route::post('data/update/{id}/anak', 'UpdatedataController@update_anak')->name('kepegawaian-fakultas.data.update_anak');
    Route::delete('data/delete/anak/{id}', 'UpdatedataController@destroy_anak')->name('kepegawaian-fakultas.data.destroy_anak');
    //update data anak keluarga
    Route::post('data/tambah/keluarga', 'UpdatedataController@tambah_keluarga')->name('kepegawaian-fakultas.data.tambah_keluarga');
    Route::post('data/update/{id}/keluarga', 'UpdatedataController@update_keluarga')->name('kepegawaian-fakultas.data.update_keluarga');
    Route::delete('data/delete/keluarga/{id}', 'UpdatedataController@destroy_keluarga')->name('kepegawaian-fakultas.data.destroy_keluarga');
    // Route::post('datapegawai/store/{id}', 'DatapegawaiController@store')->name('kepegawaian-fakultas.datapegawai.store');
});
