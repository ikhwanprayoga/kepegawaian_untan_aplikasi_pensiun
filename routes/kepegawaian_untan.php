
<?php

Route::group(['prefix' => 'kepegawaian-untan', 'namespace' => 'kepegawaian_untan', 'middleware' => ['auth', 'role:operator untan']], function () {
    Route::get('beranda', 'BerandaController@index')->name('kepegawaian-untan.beranda');

    Route::group(['prefix' => 'usulan'], function () {
        Route::get('/', 'UsulanController@index')->name('kepegawaian-untan.usulan.index');
        Route::post('kirim-notif/{id}', 'UsulanController@kirim_notif')->name('kepegawaian-fakultas.usulan.kirim-notif');
        Route::get('kelola/{id}', 'UsulanController@kelola_usulan')->name('kepegawaian-untan.usulan.kelola_usulan');
        Route::get('kelola/{id}/data/{nip}', 'UsulanController@lihat_data_pegawai')->name('kepegawaian-untan.usulan.lihat_data_pegawai');
        Route::get('kelola/{id}/berkas', 'UsulanController@kelola_usulan_berkas')->name('kepegawaian-untan.usulan.kelola_usulan.berkas');
        Route::post('kelola/berkas/verifikasi/{id}', 'UsulanController@verifikasi_berkas')->name('kepegawaian-untan.usulan.kelola_usulan.berkas.verifikasi_berkas');
        Route::post('verifikasi/{id}', 'UsulanController@verifikasi_usulan')->name('kepegawaian-untan.usulan.verifikasi');
        Route::post('kirim/{id}', 'UsulanController@kirim_usulan')->name('kepegawaian-untan.usulan.kirim');
        Route::post('sk-terbit/{id}', 'UsulanController@sk_terbit_usulan')->name('kepegawaian-untan.usulan.sk-terbit');
        Route::get('revisi/{nip}', 'UsulanController@revisiUsulan')->name('kepegawaian-untan.usulan.revisi');
        Route::post('revisi_send', 'UsulanController@revisiUsulan_send')->name('kepegawaian-untan.usulan.revisi-send');
        Route::post('{usulan_id}/kirim-resi', 'UsulanController@kirim_resi')->name('kepegawaian-untan.usulan.kirim-resi');
        Route::post('{id}/kembalikan', 'UsulanController@kembalikan_usulan')->name('kepegawaian-untan.usulan.kembalikan');
        Route::post('{id}/tambah-catatan-baru', 'UsulanController@tambah_catatan_baru')->name('kepegawaian-untan.usulan.tambah_catatan_baru');
        Route::post('{id}/batalverifikasi', 'UsulanController@batal_verifikasi')->name('kepegawaian-untan.usulan.batal_verifikasi');
        Route::get('{id}/lihat-catatan', 'UsulanController@lihat_catatan')->name('kepegawaian-untan.usulan.lihat_catatan');
        
        Route::post('berkasSend', 'RevisiController@sendRevision')->name('kepegawaian-untan.usulan.send');
        //update Dosen
        Route::get('data/editData/{nip}', 'UpdatedataController@edit')->name('kepegawaian-untan.data.editdata');
        Route::post('data/update/{nip}', 'UpdatedataController@update')->name('kepegawaian-untan.data.update');
        Route::get('data/show/{nip}', 'UpdatedataController@show')->name('kepegawaian-untan.data.show');

        //update data anak pegawai
        Route::post('data/tambah/anak', 'UpdatedataController@tambah_anak')->name('kepegawaian-untan.data.tambah_anak');
        Route::post('data/update/{id}/anak', 'UpdatedataController@update_anak')->name('kepegawaian-untan.data.update_anak');
        Route::delete('data/delete/anak/{id}', 'UpdatedataController@destroy_anak')->name('kepegawaian-untan.data.destroy_anak');

        //update data anak keluarga
        Route::post('data/tambah/keluarga', 'UpdatedataController@tambah_keluarga')->name('kepegawaian-untan.data.tambah_keluarga');
        Route::post('data/update/{id}/keluarga', 'UpdatedataController@update_keluarga')->name('kepegawaian-untan.data.update_keluarga');
        Route::delete('data/delete/keluarga/{id}', 'UpdatedataController@destroy_keluarga')->name('kepegawaian-untan.data.destroy_keluarga');

        //dpcp
        Route::get('dpcp/{nip}', 'DpcpController@index')->name('kepegawaian-untan.dpcp.index');
        Route::get('dpcp/cetak-pdf/{nip}', 'DpcpController@cetak')->name('kepegawaian-untan.dpcp.cetak');
        Route::post('dpcp/upload/{usulan_pensiun_id}', 'DpcpController@upload')->name('kepegawaian-untan.dpcp.upload');
        Route::post('sp4/upload/{usulan_pensiun_id}', 'DpcpController@sp4_upload')->name('kepegawaian-untan.sp4.upload');
        Route::post('sk/upload/{usulan_pensiun_id}', 'DpcpController@sk_upload')->name('kepegawaian-untan.sk.upload');
        Route::post('surat-pengantar/upload/{usulan_pensiun_id}', 'DpcpController@surat_pengantar_upload')->name('kepegawaian-untan.surat-pengantar.upload');

        Route::group(['prefix' => 'terbit'], function () {
            Route::get('/', 'UsulanTerbitController@index')->name('kepegawaian-untan.usulan.terbit.index');
            Route::get('kelola/{usulan_pensiun_id}', 'UsulanTerbitController@kelola')->name('kepegawaian-untan.usulan.terbit.kelola');
            Route::get('kelola/{id}/berkas', 'UsulanTerbitController@kelola_usulan_berkas')->name('kepegawaian-untan.usulan.terbit.kelola_berkas');
        });

    });

    Route::get('kelolaprofil', 'ProfilController@index')->name('kepegawaian-untan.lihatprofil.kelola');
    Route::put('kelolaprofil/{id}', 'ProfilController@updateProfil')->name('kepegawaian-untan.lihatprofil.update');

    Route::get('traceusulan.index','TraceUsulanController@index')->name('traceusulan.index');
    Route::get('page-tracking-usulan/{id}','TraceUsulanController@trackingusulan')->name('page-tracking-usulan');
    Route::get('page-trace-usulan/{id}','TraceUsulanController@traceusulan')->name('page-trace-usulan');
    Route::get('page-trace-usulan.index', 'TraceUsulanController@traceindex')->name('page-trace-usulan.index');

    
});
