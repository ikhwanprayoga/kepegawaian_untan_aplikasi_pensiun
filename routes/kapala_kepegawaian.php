<?php

Route::group(['prefix' => 'kepala-kepegawaian', 'namespace' => 'kepala_kepegawaian', 'middleware' => ['auth', 'role:kepala kepegawaian untan']], function () {
    Route::group(['prefix' => 'usulan'], function () {
        Route::get('/', 'UsulanController@index')->name('kepala-kepegawaian.usulan.index');
        Route::get('trace/{id}','TraceController@traceusulan')->name('kepala-kepegawaian.usulan.trace');
    });
});