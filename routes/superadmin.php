<?php

Route::group(['prefix' => 'superadmin', 'namespace' => 'superadmin', 'middleware' => ['auth', 'role:superadmin']], function () {
    Route::get('beranda', 'BerandaController@index')->name('superadmin.beranda');
    Route::resource('user', 'UserController')->names([
        'index' => 'superadmin.user.index',
        'create' => 'superadmin.user.create',
        'store' => 'superadmin.user.store',
        'show' => 'superadmin.user.show',
        'edit' => 'superadmin.user.edit',
        'update' => 'superadmin.user.update',
        'destroy' => 'superadmin.user.destroy',
    ]);
    Route::resource('role', 'RoleController')->names([
        'index' => 'superadmin.role.index',
        'create' => 'superadmin.role.create',
        'store' => 'superadmin.role.store',
        'show' => 'superadmin.role.show',
        'edit' => 'superadmin.role.edit',
        'update' => 'superadmin.role.update',
        'destroy' => 'superadmin.role.destroy',
        ]);
   
    Route::resource('permission', 'PermissionController')->names([
        'index' => 'superadmin.permission.index',
        'create' => 'superadmin.permission.create',
        'store' => 'superadmin.permission.store',
        'show' => 'superadmin.permission.show',
        'edit' => 'superadmin.permission.edit',
        'update' => 'superadmin.permission.update',
        'destroy' => 'superadmin.permission.destroy',
    ]);
    Route::resource('jenis-pensiun', 'JenisPensiunController')->names([
        'index' => 'superadmin.jenis-pensiun.index',
        'create' => 'superadmin.jenis-pensiun.create',
        'store' => 'superadmin.jenis-pensiun.store',
        'show' => 'superadmin.jenis-pensiun.show',
        'edit' => 'superadmin.jenis-pensiun.edit',
        'update' => 'superadmin.jenis-pensiun.update',
        'destroy' => 'superadmin.jenis-pensiun.destroy',
    ]);
    Route::resource('berkas-pensiun', 'BerkasController')->names([
        'index' => 'superadmin.berkas-pensiun.index',
        'create' => 'superadmin.berkas-pensiun.create',
        'store' => 'superadmin.berkas-pensiun.store',
        'show' => 'superadmin.berkas-pensiun.show',
        'edit' => 'superadmin.berkas-pensiun.edit',
        'update' => 'superadmin.berkas-pensiun.update',
        'destroy' => 'superadmin.berkas-pensiun.destroy',
    ]);
});